#!/bin/bash

echo -n "Checking for SCons                :  "
if [ -z $SCONS ]; then
  SCONS=`which scons 2> /dev/null`
fi

if [ ! -x "$SCONS" ]; then
      echo -e "not found, please install scons."
else
      echo -e "running scons..."
      $SCONS
fi
