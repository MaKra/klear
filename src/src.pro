# Diese Datei wurde mit dem qmake-Manager von KDevelop erstellt. 
# ------------------------------------------- 
# Unterordner relativ zum Projektordner: ./src
# Das Target ist eine Anwendung:  klear

INSTALLS += target 
target.path = /usr/bin 
FORMS += GUI/View/CKlearUIAbout.ui \
         GUI/View/CKlearUIConfig.ui \
         GUI/View/CKlearUIMain.ui \
         GUI/View/CKlearUISchedulerOverview.ui \
         GUI/View/CKlearUIScheduler.ui \
         GUI/View/CKlearUIEPGMain.ui \
         GUI/View/CKlearUIScanView.ui \
         GUI/View/CKlearUITXTMain.ui 
DISTFILES += ../distfiles/AUTHORS \
             ../distfiles/COPYING \
             ../distfiles/INSTALL \
             ../distfiles/README \
             ../distfiles/BUGS \
             ../distfiles/changelog \
             ../distfiles/copyright 
IMAGES += ../media/hi16-app-klear.png \
          ../media/hi32-app-klear.png \
          ../media/record.png \
          ../media/stop.png \
          ../media/about.png \
          ../media/configure.png \
          ../media/pause.png \
          ../media/eject.png \
          ../media/EPG.png \
          ../media/play.png \
          ../media/hi48-app-klear.png 
IDLS += GUI/View/CKlearUIAbout.ui \
        GUI/View/CKlearUIConfig.ui \
        GUI/View/CKlearUIMain.ui \
        GUI/View/CKlearUISchedulerOverview.ui \
        GUI/View/CKlearUIScheduler.ui \
        GUI/View/CKlearUIEPGMain.ui 
HEADERS += App/CKlearAppConfig.h \
           App/CKlearAppRecorder.h \
           App/CKlearAppRecordSet.h \
           App/CKlearAppScheduler.h \
           GUI/Controller/CKlearControllerConfig.h \
           GUI/Controller/CKlearControllerMain.h \
           GUI/Controller/CKlearControllerScheduler.h \
           GUI/Controller/CKlearControllerSchedulerOverview.h \
           Utilities/ts2pes.h \
           App/CKlearAppTray.h \
           config.h \
           App/Engines/KlearMPlayer/CKlearEngineMPlayer.h \
           App/Engines/KlearXine/CKlearEngineXine.h \
           App/Engines/CKlearEngineAdapter.h \
           App/Exceptions/CKlearAppException.h \
           App/Exceptions/CKlearAppErrorException.h \
           App/Exceptions/CKlearAppFatalException.h \
           App/Exceptions/CKlearExceptionReporter.h \
           App/Exceptions/CKlearAppInputException.h \
           App/Exceptions/CKlearAppMemoryException.h \
           App/Exceptions/CKlearAppFileException.h \
           App/Tuner/CKlearAppTunerC.h \
           App/Tuner/CKlearAppTuner.h \
           App/Tuner/CKlearAppTunerS.h \
           App/Tuner/CKlearAppTunerT.h \
           App/EPG/CKlearAppDataDecoder.h \
           App/EPG/CKlearAppDataLayer.h \
           App/EPG/CKlearAppDecodingTables.h \
           App/EPG/CKlearAppDescriptorData.h \
           App/EPG/CKlearAppDescriptorParser.h \
           App/EPG/CKlearAppEITData.h \
           App/EPG/CKlearAppSectionData.h \
           App/EPG/CKlearAppStreamReader.h \
           GUI/Controller/CKlearControllerEPGMain.h \
           GUI/Controller/CKlearControllerScanView.h \
           GUI/Controller/CKlearControllerTXTMain.h \
           App/TXT/CKlearAppTXTStreamer.h \
           App/TXT/CKlearAppTXTDecoder.h \
           App/CKlearAppDecodingHelper.h \
           App/Engines/KlearXineTNG/CKlearEngineXineTNG.h 
SOURCES += main.cpp \
           App/CKlearAppConfig.cpp \
           App/CKlearAppRecorder.cpp \
           App/CKlearAppRecordSet.cpp \
           App/CKlearAppScheduler.cpp \
           GUI/Controller/CKlearControllerConfig.cpp \
           GUI/Controller/CKlearControllerMain.cpp \
           GUI/Controller/CKlearControllerScheduler.cpp \
           GUI/Controller/CKlearControllerSchedulerOverview.cpp \
           Utilities/ts2pes.cpp \
           App/CKlearAppTray.cpp \
           App/Engines/KlearMPlayer/CKlearEngineMPlayer.cpp \
           App/Engines/KlearXine/CKlearEngineXine.cpp \
           App/Engines/CKlearEngineAdapter.cpp \
           App/Exceptions/CKlearAppException.cpp \
           App/Exceptions/CKlearAppErrorException.cpp \
           App/Exceptions/CKlearAppFatalException.cpp \
           Utilities/icons.cpp \
           App/Exceptions/CKlearExceptionReporter.cpp \
           App/Exceptions/CKlearAppInputException.cpp \
           App/Exceptions/CKlearAppMemoryException.cpp \
           App/Exceptions/CKlearAppFileException.cpp \
           App/Tuner/CKlearAppTunerC.cpp \
           App/Tuner/CKlearAppTuner.cpp \
           App/Tuner/CKlearAppTunerS.cpp \
           App/Tuner/CKlearAppTunerT.cpp \
           App/EPG/CKlearAppDataDecoder.cpp \
           App/EPG/CKlearAppDataLayer.cpp \
           App/EPG/CKlearAppDescriptorData.cpp \
           App/EPG/CKlearAppDescriptorParser.cpp \
           App/EPG/CKlearAppEITData.cpp \
           App/EPG/CKlearAppSectionData.cpp \
           App/EPG/CKlearAppStreamReader.cpp \
           GUI/Controller/CKlearControllerEPGMain.cpp \
           GUI/Controller/CKlearControllerScanView.cpp \
           GUI/Controller/CKlearControllerTXTMain.cpp \
           App/TXT/CKlearAppTXTStreamer.cpp \
           App/TXT/CKlearAppTXTDecoder.cpp \
           App/Engines/KlearXineTNG/CKlearEngineXineTNG.cpp 
CKlearUISchedulerOverview.ui.target = GUI/UI/CKlearUISchedulerOverview.ui
CKlearUISchedulerOverview.ui.commands = $$IDL_COMPILER
CKlearUIScheduler.ui.target = UI/CKlearUIScheduler.ui
CKlearUIScheduler.ui.commands = $$IDL_COMPILER
CKlearUIMain.ui.target = UI/CKlearUIMain.ui
CKlearUIMain.ui.commands = $$IDL_COMPILER
CKlearUIEPGMain.ui.target = GUI/View/CKlearUIEPGMain.ui
CKlearUIEPGMain.ui.commands = $$IDL_COMPILER
CKlearUIConfig.ui.target = UI/CKlearUIConfig.ui
CKlearUIConfig.ui.commands = $$IDL_COMPILER
CKlearUIAbout.ui.target = UI/CKlearUIAbout.ui
CKlearUIAbout.ui.commands = $$IDL_COMPILER
CKlearControllerSchedulerOverview.ui.target = UI/CKlearControllerSchedulerOverview.ui
LIBS += -lXtst \
-lkio \
-lxine \
-lkdeui
INCLUDEPATH += ../src \
/usr/kde/3.4/include \
/usr/include/qt \
/usr/qt/3/include \
/usr/share/qt3/include \
/usr/share/qt3 \
/usr/qt/3/include \
/usr/kde/3.3/include \
/opt/kde3/include \
/opt/kde/include \
/usr/include/qt3/ \
/usr/include/kde/
MAKEFILE = Makefile
MOC_DIR = ./moc
UI_DIR = ./GUI/View
OBJECTS_DIR = ../bin
QMAKE_LIBDIR = /usr/kde/3.4/lib \
/usr/lib/qt3 \
/usr/lib/qt \
/usr/share/qt/3/lib \
/usr/share/qt3/lib \
/usr/share/qt/lib \
/usr/share/kde3/lib \
/usr/share/kde/lib \
/opt/qt3/lib \
/opt/qt/lib \
/opt/kde3/lib \
/opt/kde/lib \
/usr/X11R6/lib/ \
/usr/local/lib/ \
/usr/lib
QMAKE_CXXFLAGS_RELEASE += -O2
TARGET = klear
DESTDIR = ../bin
CONFIG += release \
warn_on \
qt \
thread \
x11 \
exceptions \
stl \
precompile_header
TEMPLATE = app
