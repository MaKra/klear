/*  Code from :

 *  mpegtools for the Siemens Fujitsu DVB PCI card
 *
 * Copyright (C) 2000, 2001 Marcus Metzler
 *            for convergence integrated media GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * Or, point your browser to http://www.gnu.org/copyleft/gpl.html
 */

#ifndef TS2PES_H
#define TS2PES_H

#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>

#define PROG_STREAM_MAP  0xBC
#ifndef PRIVATE_STREAM1
#define PRIVATE_STREAM1  0xBD
#endif
#define PADDING_STREAM   0xBE
#ifndef PRIVATE_STREAM2
#define PRIVATE_STREAM2  0xBF
#endif
#define AUDIO_STREAM_S   0xC0
#define AUDIO_STREAM_E   0xDF
#define VIDEO_STREAM_S   0xE0
#define VIDEO_STREAM_E   0xEF
#define ECM_STREAM       0xF0
#define EMM_STREAM       0xF1
#define DSM_CC_STREAM    0xF2
#define ISO13522_STREAM  0xF3
#define PROG_STREAM_DIR  0xFF
#define PID_MASK_HI    0x1F

#define IPACKS 2048
#define TS_SIZE 188
#define IN_SIZE TS_SIZE*10

#define MAX_PLENGTH 0xFFFF
#define MMAX_PLENGTH (8*MAX_PLENGTH)

#define PTS_ONLY 0x80
#define PTS_DTS_FLAGS 0xC0
#define PTS_DTS 0xC0

#define PS_HEADER_L1    14
#define PS_HEADER_L2    (PS_HEADER_L1+18)
#define VIDEO_MODE_PAL		0
#define VIDEO_MODE_NTSC		1



typedef struct video_i{
	uint32_t horizontal_size;
	uint32_t vertical_size 	;
	uint32_t aspect_ratio	;
	double framerate	;
	uint32_t video_format;
	uint32_t bit_rate 	;
	uint32_t comp_bit_rate	;
	uint32_t vbv_buffer_size;
	uint32_t CSPF 		;
	uint32_t off;
} VideoInfo;



typedef struct audio_i{
	int layer;
	uint32_t bit_rate;
	uint32_t frequency;
	uint32_t mode;
	uint32_t mode_extension;
	uint32_t emphasis;
	uint32_t framesize;
	uint32_t off;
} AudioInfo;



typedef struct ipack_s {
	int size;
	int size_orig;
	int found;
	int ps;
	int has_ai;
	int has_vi;
	AudioInfo ai;
	VideoInfo vi;
	uint8_t *buf;
	uint8_t cid;
	uint32_t plength;
	uint8_t plen[2];
	uint8_t flag1;
	uint8_t flag2;
	uint8_t hlength;
	uint8_t pts[5];
	uint8_t last_pts[5];
	int mpeg;
	uint8_t check;
	int which;
	int done;
	void *data;
	void (*func)(uint8_t *buf,  int size, void *priv);
	int count;
	int start;
	int fd;
} ipack;



typedef struct ps_packet_{
	uint8_t scr[6];
	uint8_t mux_rate[3];
	uint8_t stuff_length;
	uint8_t *data;
	uint8_t sheader_llength[2];
	int sheader_length;
	uint8_t rate_bound[3];
	uint8_t audio_bound;
	uint8_t video_bound;
	uint8_t reserved;
	int npes;
	int mpeg;
} ps_packet;



#include <qstring.h>
#include <qfile.h>



class Ts2Pes
{

public :

	Ts2Pes( QString outfile, uint16_t pida, uint16_t pidv, int ps, bool *ok );
	~Ts2Pes();
	void run( uint8_t *buf, int count );
	void go();
	void stop();
	static void write_out( uint8_t *buf, int count,void  *p);

private :

	QFile fileOutPes;

	void write_ipack(ipack *p, uint8_t *data, int count);
	uint16_t get_pid(uint8_t *pid);
	uint64_t trans_pts_dts(uint8_t *pts);
	void reset_ipack(ipack *p);
	void init_ipack(ipack *p, int size, void (*func)(uint8_t *buf,  int size, void *priv), int ps);
	void send_ipack(ipack *p);
	void instant_repack (uint8_t *buf, int count, ipack *p);
	int getAinfo(uint8_t *mbuf, int count, AudioInfo *ai, int pr);
	int getVinfo(uint8_t *mbuf, int count, VideoInfo *vi, int pr);
	void psPes(ipack *p);
	int write_ps_header(uint8_t *buf, uint32_t   SCR, long  muxr, uint8_t    audio_bound, uint8_t    fixed, uint8_t    CSPS,
		uint8_t    audio_lock, uint8_t    video_lock, uint8_t    video_bound, uint8_t    stream1, uint8_t    buffer1_scale,
		uint32_t   buffer1_size, uint8_t    stream2, uint8_t    buffer2_scale, uint32_t   buffer2_size);
	int cwrite_ps(uint8_t *buf, ps_packet *p, long length);
	void init_ps(ps_packet *p);
	void kill_ps(ps_packet *p);
	void setl_ps(ps_packet *p);
	
	uint16_t pidAudio;
	uint16_t pidVideo;
	int pType;
	ipack pa, pv;
	ipack *p;
	
};

#endif
