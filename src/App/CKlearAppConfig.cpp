/**
\author Klear.org
\file CKlearAppConfig.cpp
\brief Klear configuration backend
*/

#include "./CKlearAppConfig.h"
#include "../GUI/Controller/CKlearControllerConfig.h"  // prevents recursion!

CKlearAppConfig::CKlearAppConfig()
{
  const QString __FUNC__ = "CKlearAppConfig::CKlearAppConfig()";

  SetDefaults(); // set up default values

  if( CheckOldConfig() == true )
  {
      // if old settings exist
      try{
               ReadConfig();  // read old settings from configfile
      }catch(CKlearAppFileException &e){
               e.addErrorMessage(__LOC__,i18n("Could not read old config file"));
               throw;
      }catch( std::exception &x ){  // all system exceptions
               CKlearAppException e( __LOC__, x.what() );
               throw e;
      }

      // check if read version matches our needes for this klear version
      if(! this->CheckConfigfileRevision() )
      {
         kdDebug() << "Configfile-Revision check failed. We need to create a new one\n";

         QMessageBox* alert = new QMessageBox();
         alert->setText( i18n("<h2>Your configuration file for Klear belongs to an old version. We'll try to update for you.</h2>") );
         alert->setGeometry( QRect(200, 200, 400, 120) );
         alert->exec();
         delete alert;

        std::cerr << "Running Config dialog" << std::endl;
        this->ConfigVersion = __CONFIGVERSION__;  // update config version
        CKlearControllerConfig::CKlearControllerConfig(this).exec();  // force the run of the setting dialog (modal)

      }else{
         kdDebug() << "Configfile-Version seems to be fine. Revision check passed\n";
      }

  }else{ // else create new settings
     QMessageBox* alert = new QMessageBox();
     alert->setText(i18n("<h3>This seems to be the first time you run Klear.<br>Klear now creates a default configfile for you in "+QDir::homeDirPath()+"/.klear<br><p>You also need to put a t/c/s-zap compatible channels.conf in your .klear directory.</h3>"));
     alert->setGeometry( QRect(200, 200, 400, 200) );
     alert->exec();
     delete alert;

     this->CreateNewConfigfile();
  }
}

CKlearAppConfig::~CKlearAppConfig()
{
}

void CKlearAppConfig::CreateNewConfigfile()
{
     const QString __FUNC__ = "CKlearAppConfig::CreateNewConfigfile()";

     // set values to default. Just to be sure. Sometimes we already read in some values befor creating a new file.
     SetDefaults(); // set up default values

     kdDebug() << "Creating settings directory\n";
     QDir::QDir().mkdir( KlearConfigPath );  // create config directory

     try{
           WriteConfig();  // write current (here, the defaults were loaded) to file
     }catch(CKlearAppException &e){
               e.addErrorMessage(__LOC__,i18n("Could not read old config file"));
               throw;
     }catch( std::exception &x ){  // all system exceptions
               CKlearAppException e( __LOC__, x.what() );
               throw e;
     }

     std::cerr << "Running Config dialog" << std::endl;
     CKlearControllerConfig::CKlearControllerConfig(this).exec();  // force the run of the setting dialog (modal)
}

void CKlearAppConfig::ReadConfig()
{
   const QString __FUNC__ = "CKlearAppConfig::ReadConfig()";

   kdDebug() << "Reading klear.conf\n";
   QFile ConfigFile( KlearConfigPath+"/"+KlearConfigFile );

   if ( ConfigFile.open( IO_ReadOnly ) ) {
        QTextStream stream( &ConfigFile );
        for (int i=1; !stream.atEnd(); i++ )
            ParseConfigLine( stream.readLine().latin1() );
        ConfigFile.close();
    }else{
       CKlearAppFileException e( __LOC__, i18n("Could not open existing configfile" ));
       throw e;
   }

  //PrintSettings();  // DEBUG !!
}


void CKlearAppConfig::StoreServiceID(QString ChannelName)
{
   const QString __FUNC__ = "CKlearAppConfig::StoreServiceID(QString ChannelName)";

  QFile ChannelsConf( this->getKlearConfigPath()+"/"+this->getKlearChannelsConf() );

  if ( ChannelsConf.open( IO_ReadOnly ) )
  {
        QTextStream stream( &ChannelsConf );
        QString line;
        for( int i = 1; !stream.atEnd(); i++ )
        {
            line = stream.readLine().latin1();
            if(ChannelName == line.section( ':', 0, 0 ) )
             {
               this->CurrentServiceID = line.section( ':', -1, -1).toUInt() ;
               this->WriteConfig();
               ChannelsConf.close();
               break;
             }
        }
  }else{
     ChannelsConf.close();
     CKlearAppErrorException e( __LOC__, i18n("No ServiceID for channel found!"));
     throw e;
  }
}

void CKlearAppConfig::ParseConfigLine(QString ConfigLine)
{
   const QString __FUNC__ = "CKlearAppConfig::ParseConfigLine(QString ConfigLine)";

   if( ! ConfigLine.contains("=") )   // if line contains no config-data...
       return;   // ...abort this line

   QString ConfCriteria  = ConfigLine.section( '=', 0, 0 );
   QString ConfData      = ConfigLine.section( '=', 1, 1 );

   if( ConfCriteria == "DVBAdapter" )
   {
        this->DVBAdapter = ConfData;
        return;
   }
   else if( ConfCriteria == "DVBDvr" )
   {
        this->DVBDvr = ConfData;
        return;
   }
   else if( ConfCriteria == "DVBFrontend" )
   {
        this->DVBFrontend = ConfData;
        return;
   }
   else if( ConfCriteria == "SystrayMute" )
   {
        if(ConfData == "1")
            this->muteSystray = true;
        else
            this->muteSystray = false;
        return;
   }
   else if( ConfCriteria == "IsMinimized" )
   {
        if(ConfData == "1")
            this->IsMinimized = true;
        else
            this->IsMinimized = false;
        return;
   }
   else if( ConfCriteria == "IsDeinterlaced" )
   {
        if(ConfData == "1")
            this->IsDeinterlaced = true;
        else
            this->IsDeinterlaced = false;
        return;
   }
   else if( ConfCriteria == "ConfigfileRevision" )
   {
        this->ConfigVersion = ConfData;
        return;
   }
   else if( ConfCriteria == "DVBDemux" )
   {
        this->DVBDemux = ConfData;
        return;
   }
   else if( ConfCriteria == "DVBMode" )
   {
        this->DVBMode = ConfData.toInt();
        return;
   }
   else if( ConfCriteria == "PlaybackEngine" )
   {
        this->PlaybackEngine = ConfData;
        return;
   }
   else if( ConfCriteria == "DisableScreensaver" )
   {
        this->disableScreensaver = ConfData.toInt();
        return;
   }
   else if( ConfCriteria == "CurrentChannel" )
   {
        this->CurrentChannel = ConfData;
        return;
   }
   else if( ConfCriteria == "CurrentServiceID" )
   {
        this->CurrentServiceID = ConfData.toUInt();
        return;
   }
   else if( ConfCriteria == "CurrentVolume" )
   {
        this->CurrentVolume = ConfData.toInt() ;
        return;
   }
   else if( ConfCriteria == "ScreenshotDir" )
   {
        this->ScreenshotDir = ConfData;
        return;
   }
   else if( ConfCriteria == "ScreenshotName" )
   {
        this->ScreenshotName = ConfData;
        return;
   }
   else if( ConfCriteria == "ScreenshotFormat" )
   {
        this->ScreenshotFormat = ConfData;
        return;
   }
   else if( ConfCriteria == "RecordingDir" )
   {
        this->RecordingDir = ConfData;
        return;
   }
   else if( ConfCriteria == "RecordingFormat" )
   {
        this->RecordingFormat = ConfData;
        return;
   }
   else if( ConfCriteria == "RecordingName" )
   {
        this->RecordingName = ConfData;
        return;
   }
   else if( ConfCriteria == "TimeShiftName" )
   {
        this->TimeShiftName = ConfData;
        return;
   }
   else if( ConfCriteria == "WindowHeight" )
   {
        this->WindowHeight = ConfData.toInt();
        return;
   }
   else if( ConfCriteria == "WindowWidth" )
   {
        this->WindowWidth = ConfData.toInt();
        return;
   }
   else if( ConfCriteria == "AccAbout" )
   {
        this->AccAbout = ConfData;
        return;
   }
   else if( ConfCriteria == "AccChannellist" )
   {
        this->AccChannellist = ConfData;
        return;
   }
   else if( ConfCriteria == "AccConfig" )
   {
        this->AccConfig = ConfData;
        return;
   }
   else if( ConfCriteria == "AccDecChannel" )
   {
        this->AccDecChannel = ConfData;
        return;
   }
   else if( ConfCriteria == "AccDeinterlace" )
   {
        this->AccDeinterlace = ConfData;
        return;
   }
   else if( ConfCriteria == "AccEpg" )
   {
        this->AccEpg = ConfData;
        return;
   }
   else if( ConfCriteria == "AccTxt" )
   {
        this->AccTxt = ConfData;
        return;
   }
   else if( ConfCriteria == "AccFullscreen" )
   {
        this->AccFullscreen = ConfData;
        return;
   }
   else if( ConfCriteria == "AccIncChannel" )
   {
        this->AccIncChannel = ConfData;
        return;
   }
   else if( ConfCriteria == "AccMute" )
   {
        this->AccMute = ConfData;
        return;
   }
   else if( ConfCriteria == "AccOsd" )
   {
        this->AccOsd = ConfData;
        return;
   }
   else if( ConfCriteria == "AccQuit" )
   {
        this->AccQuit = ConfData;
        return;
   }
   else if( ConfCriteria == "AccRecording" )
   {
        this->AccRecording = ConfData;
        return;
   }
   else if( ConfCriteria == "AccShoot" )
   {
        this->AccShoot = ConfData;
        return;
   }
   else if( ConfCriteria == "AccTimeshift" )
   {
        this->AccTimeshift = ConfData;
        return;
   }
    else if( ConfCriteria == "MenuAlignment" )
   {
        this->menuAlignment = ConfData.toInt();  // 0 = right , 1 = left
        return;
   }
    else if( ConfCriteria == "AutoShowOsd" )
   {
        if(ConfData == "1")
            this->autoShowOsd = true;
        else
           this->autoShowOsd = false;
        return;
   }
   else if( ConfCriteria == "PreEPGmargin" )
   {
        this->preEPGmargin = ConfData.toInt();
        return;
   }
   else if( ConfCriteria == "PostEPGmargin" )
   {
        this->postEPGmargin = ConfData.toInt();
        return;
   }

}

bool CKlearAppConfig::WriteConfig()
{
  const QString __FUNC__ = "CKlearAppConfig::WriteConfig()";

  kdDebug() << "Saving configfile\n";
  //PrintSettings();  // DEBUG, MKr

  if( QFile::QFile( KlearConfigPath+"/"+KlearConfigFile ).exists() == true ) // if configfile exists...
  {
      try{
            QFile::QFile( KlearConfigPath+"/"+KlearConfigFile ).remove(); // ...delete the old one
      }catch(...){
            CKlearAppFileException e( __LOC__, i18n("Could not overwrite/remove old config settings" ));
            throw e;
      }
  }

  QFile newConfig( KlearConfigPath+"/"+KlearConfigFile ); // create new Configfile

  if ( newConfig.open( IO_WriteOnly ) ) {
        QTextStream stream( &newConfig );
            stream << "#\n"
                   << "# Klear  -  http://www.klear.org\n"
                   << "#\n"
                   << "# (c) 2004-2005 by Klear.org\n"
                   << "# Published under GPL v2 or higher. See COPYING for more information.\n"
                   << "#\n"
                   << "# This configfile is created automatically.\n"
                   << "# Please only change within Klear->SettingsDialog\n"
                   << "#\n"
                   << "\n"
                   << "DVBAdapter=" << this->DVBAdapter << "\n"
                   << "DVBDvr=" << this->DVBDvr << "\n"
                   << "DVBFrontend=" << this->DVBFrontend << "\n"
                   << "DVBDemux=" << this->DVBDemux << "\n"
                   << "DVBMode=" << this->DVBMode << "\n"
                   << "SystrayMute=" << this->muteSystray << "\n"
                   << "PlaybackEngine=" << this->PlaybackEngine << "\n"
                   << "ConfigfileRevision=" << this->ConfigVersion << "\n"
                   << "#\n"
                   << "ScreenshotDir=" << this->ScreenshotDir << "\n"
                   << "ScreenshotName=" << this->ScreenshotName << "\n"
                   << "ScreenshotFormat=" << this->ScreenshotFormat << "\n"
                   << "#\n"
                   << "RecordingDir=" << this->RecordingDir << "\n"
                   << "RecordingName=" << this->RecordingName << "\n"
                   << "RecordingFormat=" << this->RecordingFormat << "\n"
                   << "TimeShiftName=" << this->TimeShiftName << "\n"
                   << "#\n"
                   << "CurrentChannel=" << this->CurrentChannel.latin1() << "\n"
                   << "CurrentServiceID=" << this->CurrentServiceID << "\n"
                   << "CurrentVolume=" << this->CurrentVolume << "\n"
                   << "#\n"
                   << "WindowWidth=" << this->WindowWidth << "\n"
                   << "WindowHeight=" << this->WindowHeight << "\n"
                   << "#\n"
                   << "AccAbout=" << this->AccAbout << "\n"
                   << "AccChannellist=" << this->AccChannellist << "\n"
                   << "AccConfig=" << this->AccConfig << "\n"
                   << "AccDecChannel=" << this->AccDecChannel << "\n"
                   << "AccDeinterlace=" << this->AccDeinterlace << "\n"
                   << "AccEpg=" << this->AccEpg << "\n"
                   << "AccTxt=" << this->AccTxt << "\n"
                   << "AccFullscreen=" << this->AccFullscreen << "\n"
                   << "AccIncChannel=" << this->AccIncChannel << "\n"
                   << "AccMute=" << this->AccMute << "\n"
                   << "AccOsd=" << this->AccOsd << "\n"
                   << "AccQuit=" << this->AccQuit << "\n"
                   << "AccRecording=" << this->AccRecording << "\n"
                   << "AccShoot=" << this->AccShoot << "\n"
                   << "AccTimeshift=" << this->AccTimeshift << "\n"
                   << "#\n"
                   << "MenuAlignment=" << this->menuAlignment << "\n"
                   << "AutoShowOsd=" << this->autoShowOsd << "\n"
                   << "IsMinimized=" << this->IsMinimized << "\n"
                   << "IsDeinterlaced=" << this->IsDeinterlaced << "\n"
                   << "DisableScreensaver=" << this->disableScreensaver << "\n"
                   << "PreEPGmargin=" << this->preEPGmargin << "\n"
                   << "PostEPGmargin=" << this->postEPGmargin << "\n"
                   << "#\n"
                   << "\n";

        newConfig.close();
    }else{ // could not write to new created configfile
         CKlearAppFileException e( __LOC__, i18n("Could not write to configfile" ));
         throw e;
    }

  return true;
}

bool CKlearAppConfig::CheckConfigfileRevision()
{
  // if stored version is smaller than the one that klear wants
  QString KlearConfigversion = __CONFIGVERSION__;

  // debug
  std::cout << "Current configuration-version: " << this->ConfigVersion.toShort() << std::endl;
  std::cout << "Needed configuration-version: " << KlearConfigversion.toShort() << std::endl;

  if( this->ConfigVersion.toShort() <  KlearConfigversion.toShort() )
      return false;
   else
      return true;
}

bool CKlearAppConfig::CheckOldConfig()
{
  if ( QDir::QDir( KlearConfigPath ).isReadable() == false )  // directory is not readable
    return false;

  if( QFileInfo( KlearConfigPath+"/"+KlearConfigFile ).isReadable() == false )  // file is not readable
    return false;

  return true;
}

void CKlearAppConfig::SetDefaults()
{
    // unchangable data ! Not saved in Configfile.
    this->KlearConfigFile = "klear.conf";
    this->KlearChannelsConf = "channels.conf";
    this->KlearConfigPath = QDir::homeDirPath()+"/.klear/";
    this->KlearBuffer = "KlearBuffer.ts";
    this->KlearSchedulerDatafile = "KlearSchedulerData.dat";
    this->ConfigVersion = __CONFIGVERSION__; // new entry set to current value
    this->TimeShiftName = "tmp_timeShifted";

    // changable data. Can be configered via configfile later
    this->DVBAdapter = "adapter0";
    this->DVBDvr = "dvr0";
    this->DVBDemux = "demux0";
    this->DVBFrontend = "frontend0";
    this->DVBMode = 1;   // terrestric = 1, sat = 2, cable = 3

    this->muteSystray = false;
    this->IsDeinterlaced = true;
    this->IsMinimized = false;
    this->disableScreensaver = 60;  // send fake event every 60 seconds

    this->ScreenshotDir = QDir::homeDirPath()+"/Desktop/";
    this->ScreenshotName = "klear_screenshot";
    this->ScreenshotFormat = "png";

    this->RecordingDir = QDir::homeDirPath()+"/Desktop/";
    this->RecordingName = "klear_record";
    this->RecordingFormat = "MPEG TS";

    this->PlaybackEngine = "KlearXine";

    this->CurrentChannel = "none";
    this->CurrentServiceID = 0;
    this->CurrentVolume = 100;
    this->WindowHeight = 338;
    this->WindowWidth = 594;

    this->AccAbout = "A";
    this->AccChannellist = "M";
    this->AccConfig = "C";
    this->AccDecChannel = "PgUp";
    this->AccDeinterlace ="D";
    this->AccEpg = "E";
    this->AccTxt = "X";
    this->AccFullscreen = "F";
    this->AccIncChannel = "PgDown";
    this->AccMute = "V";
    this->AccOsd = "I";
    this->AccQuit = "Q";
    this->AccRecording = "R";
    this->AccShoot = "S";
    this->AccTimeshift = "T";

    this->menuAlignment = 0;
    this->autoShowOsd = 0;

    this->preEPGmargin = 0;
    this->postEPGmargin = 0;
}

void CKlearAppConfig::PrintSettings()
{
    kdDebug() << this->KlearConfigFile
              << "\n"+this->KlearChannelsConf
              << "\n"+this->KlearConfigPath
              << "\n"+this->DVBAdapter
              << "\n"+this->DVBDvr
              << "\n"+this->ConfigVersion
              << "\n"+this->ScreenshotDir
              << "\n"+this->DVBDemux
              << "\n"+this->DVBFrontend
              << "\n"+this->DVBMode
              << "\n"+this->muteSystray
              << "\n"+this->PlaybackEngine
              << "\n"+this->CurrentChannel
              << "\n"+this->CurrentVolume
              << "\n"+this->RecordingName
              << "\n"+this->RecordingDir
              << "\n"+this->CurrentServiceID
              << "\n"+this->RecordingFormat
              << "\n"+this->ScreenshotFormat
              << "\n"+this->ScreenshotDir
              << "\n"+this->ScreenshotName
              << "\n"+this->TimeShiftName
              << "\n"+this->AccAbout
              << "\n"+this->AccChannellist
              << "\n"+this->AccConfig
              << "\n"+this->AccDecChannel
              << "\n"+this->AccDeinterlace
              << "\n"+this->AccEpg
              << "\n"+this->AccTxt
              << "\n"+this->AccFullscreen
              << "\n"+this->AccIncChannel
              << "\n"+this->AccMute
              << "\n"+this->AccOsd
              << "\n"+this->AccQuit
              << "\n"+this->AccRecording
              << "\n"+this->AccShoot
              << "\n"+this->AccTimeshift
              << "\n"+this->menuAlignment
              << "\n"+this->autoShowOsd
              << "\n"+this->disableScreensaver
              << "\n"+this->IsMinimized
              << "\n"+this->IsDeinterlaced
              << "\n"+this->preEPGmargin
              << "\n"+this->postEPGmargin
              <<+"\n";
}

int CKlearAppConfig::getPreEPGmargin() const
{
     return this->preEPGmargin;
}

int CKlearAppConfig::getPostEPGmargin() const
{
     return this->postEPGmargin;
}

void CKlearAppConfig::setPreEPGmargin(int pre)
{
     this->preEPGmargin = pre;
}

void CKlearAppConfig::setPostEPGmargin(int post)
{
     this->postEPGmargin = post;
}

QString CKlearAppConfig::getKlearSchedulerDatafile() const
{
     return this->KlearSchedulerDatafile;
}

QString CKlearAppConfig::getKlearConfigPath() const
{
     return this->KlearConfigPath;
}

QString CKlearAppConfig::getKlearChannelsConf() const
{
     return this->KlearChannelsConf;
}

QString CKlearAppConfig::getDVBAdapter() const
{
     return this->DVBAdapter;
}

QString CKlearAppConfig::getDVBDvr() const
{
     return this->DVBDvr;
}

QString CKlearAppConfig::getDVBDemux() const
{
     return this->DVBDemux;
}

QString CKlearAppConfig::getDVBFrontend() const
{
     return this->DVBFrontend;
}

int CKlearAppConfig::getDVBMode() const
{
     return this->DVBMode;
}

QString CKlearAppConfig::getPlaybackEngine() const
{
     return this->PlaybackEngine;
}

void CKlearAppConfig::setDVBAdapter(QString setDVBAdapter)
{
     this->DVBAdapter = setDVBAdapter;
}

void CKlearAppConfig::setDVBDvr(QString setDVBDvr)
{
     this->DVBDvr = setDVBDvr;
}

void CKlearAppConfig::setDVBDemux(QString setDVBDemux)
{
     this->DVBDemux = setDVBDemux;
}

void CKlearAppConfig::setDVBFrontend(QString setDVBFrontend)
{
     this->DVBFrontend = setDVBFrontend;
}

void CKlearAppConfig::setDVBMode(int setDVBMode)
{
     this->DVBMode = setDVBMode;
}

void CKlearAppConfig::setPlaybackEngine(QString setPlaybackEngine)
{
     this->PlaybackEngine = setPlaybackEngine;
}

u_int CKlearAppConfig::getServiceID() const
{
     return this->CurrentServiceID;
}

QString CKlearAppConfig::getScreenshotDir() const
{
     return this->ScreenshotDir;
}

QString CKlearAppConfig::getScreenshotName() const
{
     return this->ScreenshotName;
}

QString CKlearAppConfig::getScreenshotFormat() const
{
     return this->ScreenshotFormat;
}

void CKlearAppConfig::setScreenshotDir(QString setScreenshotDir)
{
     this->ScreenshotDir = setScreenshotDir;
}

void CKlearAppConfig::setScreenshotName(QString setScreenshotName)
{
     this->ScreenshotName = setScreenshotName;
}

void CKlearAppConfig::setScreenshotFormat(QString setScreenshotFormat)
{
     this->ScreenshotFormat = setScreenshotFormat;
}

QString CKlearAppConfig::getRecordingDir() const
{
     return this->RecordingDir;
}

QString CKlearAppConfig::getRecordingName() const
{
     return this->RecordingName;
}

QString CKlearAppConfig::getRecordingFormat() const
{
     return this->RecordingFormat;
}

void CKlearAppConfig::setRecordingDir( QString setRecordingDir )
{
     this->RecordingDir = setRecordingDir;
}

void CKlearAppConfig::setRecordingName( QString setRecordingName )
{
     this->RecordingName = setRecordingName;
}

void CKlearAppConfig::setRecordingFormat( QString setRecordingFormat )
{
     this->RecordingFormat = setRecordingFormat;
}

int CKlearAppConfig::getApid() const
{
     return this->apid;
}

int  CKlearAppConfig::getDisableScreensaver() const
{
   return this->disableScreensaver;
}

void  CKlearAppConfig::setDisableScreensaver( int disableScreen )
{
   this->disableScreensaver = disableScreen;
}

int CKlearAppConfig::getVpid() const
{
     return this->vpid;
}

void CKlearAppConfig::setApid( int setApid )
{
     this->apid = setApid;
}

void CKlearAppConfig::setVpid( int setVpid )
{
     this->vpid = setVpid;
}

QString CKlearAppConfig::getCurrentChannel() const
{
     return this->CurrentChannel;
}

int CKlearAppConfig::getCurrentVolume() const
{
     return this->CurrentVolume;
}

void CKlearAppConfig::setCurrentVolume( int setCurrentVolume )
{
     this->CurrentVolume = setCurrentVolume;
}

void CKlearAppConfig::setCurrentChannel( QString setCurrentChannel )
{
     this->CurrentChannel = setCurrentChannel;
}

bool CKlearAppConfig::getMuteSystray() const
{
   return this->muteSystray;
}

void CKlearAppConfig::setMuteSystray(bool isMuteSystray)
{
   this->muteSystray = isMuteSystray;
}

bool CKlearAppConfig::getIsDeinterlaced() const
{
   return this->IsDeinterlaced;
}

void CKlearAppConfig::setIsDeinterlaced(bool isDeint)
{
   this->IsDeinterlaced = isDeint;
}

bool CKlearAppConfig::getIsMinimized() const
{
   return this->IsMinimized;
}

void CKlearAppConfig::setIsMinimized(bool isMini)
{
   this->IsMinimized = isMini;
}

QString CKlearAppConfig::getTimeShiftName() const
{
   return this->TimeShiftName;
}

void  CKlearAppConfig::setTimeShiftName( QString TimeShiftName )
{
   this->TimeShiftName = TimeShiftName;
}

void CKlearAppConfig::setWindowSize(QSize size)
{
  WindowWidth = size.width();
  WindowHeight = size.height();

}

QSize CKlearAppConfig::getWindowSize()
{
    return QSize( WindowWidth, WindowHeight );
}


QString CKlearAppConfig::getAccDecChannel()
{
    return this->AccDecChannel;
}

QString CKlearAppConfig::getAccIncChannel()
{
    return this->AccIncChannel;
}

QString CKlearAppConfig::getAccChannellist()
{
    return this->AccChannellist;
}

QString CKlearAppConfig::getAccFullscreen()
{
    return this->AccFullscreen;
}

QString CKlearAppConfig::getAccDeinterlace()
{
    return this->AccDeinterlace;
}

QString CKlearAppConfig::getAccOsd()
{
    return this->AccOsd;
}

QString CKlearAppConfig::getAccEpg()
{
    return this->AccEpg;
}

QString CKlearAppConfig::getAccTxt()
{
    return this->AccTxt;
}

QString CKlearAppConfig::getAccQuit()
{
    return this->AccQuit;
}

QString CKlearAppConfig::getAccConfig()
{
    return this->AccConfig;
}

QString CKlearAppConfig::getAccAbout()
{
    return this->AccAbout;
}

QString CKlearAppConfig::getAccMute()
{
    return this->AccMute;
}

QString CKlearAppConfig::getAccRecording()
{
    return this->AccRecording;
}

QString CKlearAppConfig::getAccShoot()
{
    return this->AccShoot;
}

QString CKlearAppConfig::getAccTimeshift()
{
    return this->AccTimeshift;
}

void CKlearAppConfig::setAccDecChannel( QString key )
{
     this->AccDecChannel = key;
}

void CKlearAppConfig::setAccIncChannel( QString key )
{
     this->AccIncChannel = key;
}

void CKlearAppConfig::setAccChannellist( QString key )
{
     this->AccChannellist = key;
}

void CKlearAppConfig::setAccFullscreen( QString key )
{
     this->AccFullscreen = key;
}

void CKlearAppConfig::setAccDeinterlace( QString key )
{
     this->AccDeinterlace = key;
}

void CKlearAppConfig::setAccOsd( QString key )
{
     this->AccOsd =  key;
}

void CKlearAppConfig::setAccEpg( QString key )
{
     this->AccEpg =  key;
}

void CKlearAppConfig::setAccTxt( QString key )
{
     this->AccTxt =  key;
}

void CKlearAppConfig::setAccQuit( QString key )
{
     this->AccQuit = key;
}

void CKlearAppConfig::setAccConfig( QString key )
{
     this->AccConfig = key;
}

void CKlearAppConfig::setAccAbout( QString key )
{
     this->AccAbout = key;
}

void CKlearAppConfig::setAccMute( QString key )
{
     this->AccMute = key;
}

void CKlearAppConfig::setAccRecording( QString key )
{
     this->AccRecording = key;
}

void CKlearAppConfig::setAccShoot( QString key )
{
     this->AccShoot = key;
}

void CKlearAppConfig::setAccTimeshift( QString key )
{
    this->AccTimeshift = key;
}

int CKlearAppConfig::getMenuAlignment() const
{
    return this->menuAlignment;
}

void CKlearAppConfig::setMenuAlignment( int alignment )
{
    this->menuAlignment = alignment;
}

void CKlearAppConfig::setAutoShowOsd( bool isOsdAutoShown )
{
    this->autoShowOsd = isOsdAutoShown;
}

bool CKlearAppConfig::getAutoShowOsd() const
{
    return this->autoShowOsd;
}
