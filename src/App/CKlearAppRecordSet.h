/**
\author Klear.org
\file CKlearAppRecordSet.h
\brief Storage class for storing start -, endtime, channel and path for each recording.
*/

#include <iostream>
#include <qdatetime.h>
#include <qthread.h>
#include <qstring.h>
#include <kdebug.h>

#include "./Exceptions/CKlearAppException.h"       // klear exception class

#ifndef CKLEARAPPRECORDSET_H
#define CKLEARAPPRECORDSET_H

/**
\class CKlearAppRecordSet
\author Klear.org
\brief Storage class for storing start -, endtime, channel and path for each recording.
*/
class CKlearAppRecordSet
{

public:

     /**
     \fn void CKlearAppRecordSet();
     \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
     \retval void no return value
     \brief Constructor which constructs an empty recordset
     */
     CKlearAppRecordSet();

     /**
     \fn void CKlearAppRecordSet( const QString RecordFile, const QDateTime startDateTime, const QDateTime endDateTime, const QString channel);
     \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
     \param RecordFile Path and name with which the record file will be created
     \param startDateTime Specifies the start time of the record
     \param endDateTime Specifies the end time of the record
     \param channel the channel to be recorded
     \retval void no return value
     \brief Constructor which constructs a recordset using the specified parameters
     */
     CKlearAppRecordSet( const QString RecordFile,
                         const QDateTime startDateTime,
                         const QDateTime endDateTime,
                         const QString channel );

     /**
     \fn void ~CKlearAppRecordSet();
     \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
     \retval void no return value
     \brief Destructor
     */
    ~CKlearAppRecordSet();

     /**
     \fn QString getRecordFile() const;
     \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
     \retval QString the Path and name with which the record file will be created
     \brief returns the currently set Path and name with which the record file will be created
     */
    QString getRecordFile() const;

      /**
     \fn QDateTime getStartDateTime() const;
     \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
     \retval QDateTime the start time of the recordset
     \brief returns the currently set start time of the this recordset
     */
     QDateTime getStartDateTime() const;

      /**
     \fn QDateTime getEndDateTime() const;
     \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
     \retval QDateTime the end time of the recordset
     \brief returns the currently set end time of the this recordset
     */
    QDateTime getEndDateTime() const;

      /**
     \fn QString getChannel() const;
     \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
     \retval QString the channel of the recordset
     \brief returns the currently set channel of the this recordset
     */
    QString getChannel() const;


    /**
    \fn void setRecordFile( const QString RecordFile );
    \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
    \param RecordFile Path and name with which the record file will be created
    \retval void no return value
    \brief either sets or changes the RecordFile of this recordset to the RecordFile specified by the parameter
    */
    void setRecordFile( const QString RecordFile);

    /**
    \fn void setStartDateTime( const QDateTime startDateTime );
    \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
    \param startDateTime Specifies the start time of the record
    \retval void no return value
    \brief either sets or changes the startDateTime of this recordset to the startDateTime specified by the parameter
    */
    void setStartDateTime( const QDateTime startDateTime );

    /**
    \fn void setEndDateTime( const QDateTime endDateTime );
    \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
    \param endDateTime Specifies the end time of the record
    \retval void no return value
    \brief either sets or changes the endDateTime of this recordset to the endDateTime specified by the parameter
    */
    void setEndDateTime( const QDateTime endDateTime );

    /**
    \fn void setChannel( const QString channel );
    \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
    \param channel the channel to be recorded
    \retval void no return value
    \brief either sets or changes the record channel of this recordset to the channel specified by the parameter
    */
    void setChannel( const QString channel );


private:
    QString channel ;
    QString RecordFile ;
    QDateTime startDateTime ;
    QDateTime endDateTime ;

};

#endif
