/**
\author Klear.org
\file CKlearEngineAdapter.cpp
\brief Abstract Player class for playback engines
*/
#include "CKlearEngineAdapter.h"

CKlearEngineAdapter::CKlearEngineAdapter()
{
   kdDebug() << "Starting CKlearEngineAdapter\n";

   kdDebug() << "Setting up Sound-Mixer\n";

   this->mMixerDevice = "/dev/mixer0"; // get this from configuration in future, MKr, 13.03.2005

   mMixer = open(this->mMixerDevice, O_RDWR);
       if(mMixer < 0)
          kdDebug() << "Sound-Mixer successful initialized.\n";
       else
          kdDebug() << "Sound-Mixer init failed.\n";
}

CKlearEngineAdapter::CKlearEngineAdapter(QWidget* parent, const char* name)  : QWidget(parent,name)
{
   kdDebug() << "Starting CKlearEngineAdapter\n";

   kdDebug() << "Setting up Sound-Mixer\n";

   this->mMixerDevice = "/dev/mixer0"; // get this from configuration in future, MKr, 13.03.2005

   mMixer = open(this->mMixerDevice, O_RDWR);

   if(mMixer < 0)
      kdDebug() << "Sound-Mixer successful initialized.\n";
   else
      kdDebug() << "Sound-Mixer init failed.\n";

}

CKlearEngineAdapter::~CKlearEngineAdapter()
{
  if (this->mMixer > 0)
   close(this->mMixer);
}

void CKlearEngineAdapter::setVolume(int vol)
{
  unsigned char temp[4];
  int channel=1;
  int left = vol;
  int right = vol;
  mix_channel[channel].left = left;
  mix_channel[channel].right = right;
  mix_channel[channel].dev_num = 1;
  temp[0]=(unsigned char)left;
  temp[1]=(unsigned char)right;
  temp[2]=temp[3]=0;
  ioctl(this->mMixer, MIXER_WRITE(mix_channel[channel].dev_num), temp);
}

int CKlearEngineAdapter::getVolume()
{
  unsigned char temp[4];
  unsigned char left;
  unsigned char right;
  int channel=1;
  mix_channel[channel].dev_num = 1;
  ioctl(this->mMixer, MIXER_READ(mix_channel[channel].dev_num), &temp);
  left = temp[0];
  right = temp[1];
 if ( temp[0] >= temp[1] )
  {
      if (left <= 100)
        return( left);
        else return 0;
  }
  else
  {
     if ( right <=100)
  return( right);
     else return 0;
 }

}

void CKlearEngineAdapter::toggleMute()
{
   kdDebug() << "Muting sound system (adapter)...\n";

   if(this->getVolume() <= 0) // muted, so switch on sound again
   {
      this->setVolume(95); // Hm, not trying to restore previous volume here ??
   }
   else // not muted, so do it...
   {
         this->setVolume(0);
   }
}
