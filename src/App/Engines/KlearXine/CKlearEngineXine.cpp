/**
\author Klear.org
\file CKlearEngineXine.cpp
\brief Xine Engine
Playbacksystem, using xine
*/

#include "CKlearEngineXine.h"

CKlearEngineXine::CKlearEngineXine(QWidget* parent, const char* name, std::vector<CKlearAppEITData *> *storage,
          const QString& audioDriver, const QString& videoDriver, bool verbose) : CKlearEngineAdapter(parent,name), QThread()
{
  this->isPlaying = false;
  this->EITDataVector = storage;
  kdDebug() << "Starting KlearXineWidget\n";

  setMinimumSize(QSize(20,20)); // set a size hint
  setPaletteBackgroundColor(QColor(0,0,0)); //black
  setUpdatesEnabled(false);
  setMouseTracking(true);
  setEnabled(true);

  vo_driver    = "auto";
  ao_driver    = "auto";

  // define mouse hinting slot
  connect( &mouseHideTimer, SIGNAL(timeout()), this, SLOT(slotHideMouse()));
  connect( &klearOSDHideTimer, SIGNAL(timeout()), this, SLOT(slotHideKlearOSD()));    // hide OSD informaion after timeout
  connect( &osdTimer, SIGNAL(timeout()), this, SLOT(slotOSDHide()));

  connect( this, SIGNAL(signalFullScreen()), parent->parentWidget()->parentWidget(), SLOT(slotFullscreenWindow()));
  connect( this, SIGNAL(signalShowMenu()), parent->parentWidget()->parentWidget(), SLOT(slotMinimizeWindow()));
  connect( this, SIGNAL(signalToggleMute()), parent->parentWidget()->parentWidget(), SLOT(slotToggleMuteButton()));

  shiftPos = 0;
  resumeTime = 0;
  klearOSD = 0; // explicite set to zero
  this->mvol = 0;  // set volume to zero. will be restored by configuration

  autoresizeEnabled = true;
  this->isReady = false;
  this->verbose = verbose;
}


CKlearEngineXine::~CKlearEngineXine ()
{
    const QString __FUNC__ = "CKlearEngineXine::~CKlearEngineXine";

    if (osd)
    {
        xine_osd_free(osd);
    }

    xine_close(stream);
    xine_event_dispose_queue(event_queue);
    xine_dispose(stream);
    if(ao_port)
        xine_close_audio_driver(xine, ao_port);
    xine_close_video_driver(xine, vo_port);
    xine_exit(xine);

    if (osdInfo)
    {
        free(osdInfo);
    }

    XLockDisplay(display);
    XUnmapWindow(display,  window);
    XDestroyWindow(display, window);
    XUnlockDisplay(display);

    XCloseDisplay (display);

    mouseHideTimer.stop();
}



void CKlearEngineXine::init()
{
    const QString __FUNC__ = "CKlearEngineXine::init";
    globalPosChanged();

    if(!XInitThreads())
    {
        CKlearAppFatalException e( __LOC__, i18n("XInitThreads() failed\n"));
        throw e;
    }

    if((display = XOpenDisplay(getenv("DISPLAY"))) == NULL)
    {
        CKlearAppFatalException e( __LOC__, i18n("XOpenDisplay() failed.\n"));
        throw e;
    }

    screen = XDefaultScreen(display);
    window = winId();

    XLockDisplay(display);
    XSelectInput (display, window, INPUT_MOTION);

    res_h = (DisplayWidth(display, screen) * 1000 / DisplayWidthMM(display, screen));
    res_v = (DisplayHeight(display, screen) * 1000 / DisplayHeightMM(display, screen));
    pixel_aspect          = res_v / res_h;

    XUnlockDisplay(display);

    /**** INIT XINE ENGINE ****/

    xine = xine_new();

    if ( !xine )
    {
        CKlearAppFatalException e( __LOC__, i18n("XineInit() failed.\n"));
        throw e;
    }

    if ( verbose )
    {
        xine_engine_set_param( xine, XINE_ENGINE_PARAM_VERBOSITY, 99 );
    }

    xine_config_load(xine, configfile);
    xine_init(xine);

    osdInfo = strdup(i18n("Show OSD Messages").utf8());
    osdShow = (bool)xine_config_register_bool( xine, "misc.osd_messages", 1, osdInfo,
                                         NULL, 10, &CKlearEngineXine::showOSDMessagesChangedCallback, this);

    vis.display           = display;
    vis.screen            = screen;
    vis.d                 = window;
    vis.dest_size_cb      = &CKlearEngineXine::destSizeCallback;
    vis.frame_output_cb   = &CKlearEngineXine::frameOutputCallback;
    vis.user_data         = (void*) this;

    if((vo_port = xine_open_video_driver( xine, vo_driver, XINE_VISUAL_TYPE_X11, (void *) &(vis) )) == NULL )
    {
        CKlearAppFatalException e( __LOC__, QString(i18n("I'm unable to initialize video driver %1 Giving up.\n")).arg((char*)&vo_driver)  );
        throw e;
    }


    if( !(ao_port     = xine_open_audio_driver(xine , ao_driver, NULL)) )
    {
        CKlearAppFatalException e( __LOC__, QString(i18n("I'm unable to initialize audio driver %1 Giving up.\n")).arg((char*)&ao_driver) );
        throw e;
    }

    if ( !(stream      = xine_stream_new(xine, ao_port, vo_port)) )
    {
        CKlearAppFatalException e( __LOC__, i18n("I'm unable to initialize Xine stream. \n"));
        throw e;
    }

    if ( !(event_queue = xine_event_new_queue( stream )))
    {
        CKlearAppFatalException e( __LOC__, i18n("I'm unable to initialize Xine Event Queue. \n"));
        throw e;
    }

    /*** OSD ***/
    try{
    initOSD();
    }catch ( CKlearAppErrorException &e ){
       e.addErrorMessage( __LOC__, i18n("Could not init OSD. \n" ));
       throw;
    }

    xine_event_create_listener_thread( event_queue, &CKlearEngineXine::xineEventListener, (void*)this);

    this->isReady = true;
    running = 1;

    try{
       start();
    }catch ( CKlearAppException &e ){   // Wir fangen eine Referenz
       e.addErrorMessage( __LOC__, i18n("Could not start CKlearEngineXine. \n" ));
       throw;  // werfen automatisch das gefangene Element
    }

}

void CKlearEngineXine::initOSD()
{
  const QString __FUNC__ = "CKlearEngineXine::initOSD";

  osd = xine_osd_new( stream, 10, 10, 1000, DEFAULT_OSD_FONT_SIZE * 6 + 10);
  if (osd)
  {
    xine_cfg_entry_t config;
    char* font = NULL;

    if (xine_config_lookup_entry( xine, "misc.spu_font", &config))
    {
       font = new char[strlen(config.str_value)+1];
       strcpy(font, config.str_value);
    }
    else
    {
       font = new char[5];
       strcpy(font, "sans");
    }
    if (!xine_osd_set_font(osd, font, DEFAULT_OSD_FONT_SIZE))
    {

      delete [] font;
      font = new char[5];
      strcpy(font, "sans");
      xine_osd_set_font(osd, font, DEFAULT_OSD_FONT_SIZE);
    }

    xine_osd_set_text_palette(osd, XINE_TEXTPALETTE_WHITE_BLACK_TRANSPARENT, XINE_OSD_TEXT1);
    osdUnscaled = (xine_osd_get_capabilities( osd ) & XINE_OSD_CAP_UNSCALED);
    if (osdUnscaled)

    delete [] font;
  }else
  {
   CKlearAppErrorException e( __LOC__, i18n("Initialisation of xine OSD failed. \n"));
   throw e;
  }
}


void CKlearEngineXine::run()
{
    const QString __FUNC__ = "CKlearEngineXine::run";
   try{
      startUp();
   }catch( CKlearAppException &e )
   {
      e.addShowException(__LOC__, i18n("Xine Engine broken" ));

   }
}


void CKlearEngineXine::startUp()
{
   const QString __FUNC__ = "CKlearEngineXine::startUp";
   kdDebug() << "KlearXineWidget running\n";

   XEvent event;
   while(running) {

      XNextEvent (display, &event);

      XLockDisplay(display);

     if (event.type == Expose)
     {

        if (event.xexpose.count == 0)
        {


            if ( xine_port_send_gui_data(vo_port, XINE_GUI_SEND_EXPOSE_EVENT, &event) != 0 )
            {
                CKlearAppFatalException e( __LOC__, i18n("Unable to open send gui data \n"));
                throw e;
            }
        }
     }

     XUnlockDisplay(display);
  }


}


void CKlearEngineXine::EnginePlayStream(const QString& MRL)
{
    const QString __FUNC__ = "CKlearEngineXine::EnginePlayStream";
    try{
        if(this->EngineIsPlaying())
        {
            kdDebug() << "restarting engine\n";
            this->stop();
            this->play( MRL.latin1() );
            return;
        }
        else
        {
            if (this->isReady)
                {
                    this->play( MRL.latin1 ());
                }
            else
                {
                    this->init();
                    while(!this->isReady)
                    {
                        this->usleep(1000);
                    }
                    this->play( MRL.latin1 ());
                }
        }
        this->isPlaying = true;
    }catch ( CKlearAppException &e ){   // Wir fangen eine Referenz
     e.addErrorMessage( __LOC__, i18n("Error during start of playback \n" ));
     throw;  // werfen automatisch das gefangene Element
    }

}


void CKlearEngineXine::EngineStopStream()
{
    if(this->EngineIsPlaying() == true)
    {
        this->stop();
        isPlaying = false;
    }
}


bool CKlearEngineXine::EngineIsPlaying()
{
    return this->isPlaying;
}

void CKlearEngineXine::hideMouse()
{
   this->startMouseHideTimer();
}

void CKlearEngineXine::setVolume( const int vol )
{
    kdDebug() << vol << "  setting Volume  (KlearXine)...\n";
  this->mvol = vol; // store local var.

  if (! this->isReady)
      return;

    xine_set_param(stream, XINE_PARAM_AUDIO_AMP_LEVEL, vol);
    // hard// xine_set_param(stream, XINE_PARAM_AUDIO_VOLUME, vol);
}

int CKlearEngineXine::getVolume()
{
   kdDebug() << mvol << "  getting volume (KlearXine)...\n";
   return this->mvol;
}

void CKlearEngineXine::toggleMute()
{
   this->toggleMuting();
}

void CKlearEngineXine::showOSDMessage( const QString& message, const uint duration )
{
    kdDebug() << "showOSDMessage engine\n";
    showXineOSDMessage( message, duration );
}

void CKlearEngineXine::showInfoOSD(const QString CurrChannel)
{
  //
  // Read http://www.xinehq.de/index.php/hackersguide#OSD for detailed OSD coding instructions. MKr, 19.11.04
  //
  if(! klearOSD ){
     int w = xine_get_stream_info( stream, XINE_STREAM_INFO_VIDEO_WIDTH);
     int h = xine_get_stream_info( stream, XINE_STREAM_INFO_VIDEO_HEIGHT);
     QString CurrTime = QTime::currentTime().toString( "hh:mm" );

     this->klearOSD = xine_osd_new( stream, 0, 0, w, h ); // declare new OSD like screensize
     xine_osd_set_font( klearOSD, "sans", 28 );  // see fonts in /usr/share/xine/libxine1/fonts like serif, sans, mono, cc, cci, cetus
     xine_osd_set_encoding( klearOSD, NULL );

     // see xine/osd.h
     // color definitions
     /*   WHITE_BLACK_TRANSPARENT    0
          WHITE_NONE_TRANSPARENT     1
          WHITE_NONE_TRANSLUCID      2
          YELLOW_BLACK_TRANSPARENT   3
          GREEN_TRANSLUCENT          4
          GREEN_NONE_TRANSLUCENT     5
          LIGHT GREEN FILLES         6  */
     xine_osd_set_text_palette( klearOSD, 0, XINE_OSD_TEXT1 ); // white
     xine_osd_set_text_palette( klearOSD, 1, XINE_OSD_TEXT2 ); // white
     xine_osd_set_text_palette( klearOSD, 2, XINE_OSD_TEXT3 ); // white
     xine_osd_set_text_palette( klearOSD, 3, XINE_OSD_TEXT4 ); // yellow transparent
     xine_osd_set_text_palette( klearOSD, 4, XINE_OSD_TEXT5 ); // green translucent
     xine_osd_set_text_palette( klearOSD, 5, XINE_OSD_TEXT6 ); // green filled
     xine_osd_set_text_palette( klearOSD, 6, XINE_OSD_TEXT7 ); // light green filled

//     xine_osd_draw_rect( klearOSD, 6, h, w, h-(h/4), 2, 1 ); // (x,y,w,h, color , filled) // draw translucent (color Nr. 3) background window  // full bottom box
     xine_osd_draw_rect( klearOSD, 20, h-10, w-20, h-(h/4), 5, 1 ); // (x,y,w,h, color , filled) // draw translucent (color Nr. 3) background window

//    xine_osd_draw_line( klearOSD, 40, h-(h/4), w-40, h-(h/4), 6 ); // (x1,y1,x2,y2,color)
//     xine_osd_draw_line( klearOSD, 20, h-(h/4), w-20, h-(h/4), 6 ); // (x1,y1,x2,y2,color)

     // xine_osd_get_text_size( klearOSD, "Kanal Blorg", &textwidth, &textheight );

     // calculate sizes with osd_get_text_size(), MKr, 19.11.04
     xine_osd_draw_text( klearOSD, (w/18),  int(h-(h/4.3)), CurrChannel, XINE_OSD_TEXT3 );
     xine_osd_draw_text( klearOSD, w-(w/6), int(h-(h/4.3)), CurrTime, XINE_OSD_TEXT3 );

     std::cout << "Getting EPG data for OSD" << std::endl;
     CKlearAppDataLayer *DatabaseLayer = new CKlearAppDataLayer( this->EITDataVector );
     std::cout << "DataBaselayer for xine created" << std::endl;

     QDateTime TempDateStartCurrent;  // temp. Date and Time object to identify selected object
     QDateTime TempDateStartNext;  // temp. Date and Time object to identify selected object

     int TIMESHIFT = QDateTime::currentDateTime ( Qt::LocalTime ).toTime_t() - QDateTime::currentDateTime ( Qt::UTC ).toTime_t();

      QString currentRunning;
      CKlearAppEITData *currentData = DatabaseLayer->getCurrentRunning();
      if( currentData == NULL)
               currentRunning = "00:00   scanning";
      else
      {
              TempDateStartCurrent.setTime_t( currentData->StartTime+TIMESHIFT );
              currentRunning =  TempDateStartCurrent.toString("hh:mm")+"   "+currentData->ShortDesc;
      }

     std::cout << "Current running read" << std::endl;

      QString nextRunning;
      CKlearAppEITData *nextData = DatabaseLayer->getNextRunning();
      if( nextData == NULL)
               nextRunning = "00:00   scanning";
      else
      {
              TempDateStartNext.setTime_t( nextData->StartTime+TIMESHIFT );
              nextRunning =  TempDateStartNext.toString("hh:mm")+"   "+nextData->ShortDesc;
      }

      std::cout << "printing text: " <<  currentRunning << std::endl;
      std::cout << "printing text: " <<  nextRunning << std::endl;

//      xine_osd_draw_line( klearOSD, (w/18), h-(h/5.5), w-45, h-(h/5.5), 0 ); // (x1,y1,x2,y2,color)
     xine_osd_draw_rect( klearOSD, 30, h-20, w-30, h-(h/6.0), 6, 1 ); // (x,y,w,h, color , filled) // draw translucent (color Nr. 3) background window

      xine_osd_draw_text( klearOSD, (w/18),  int(h-(h/6.5)), currentRunning, XINE_OSD_TEXT4 );
      xine_osd_draw_text( klearOSD, (w/18),  int(h-(h/10.0)), nextRunning, XINE_OSD_TEXT4 );
  delete DatabaseLayer;

     klearOSDHideTimer.start( 4000, true ); // set timeout to 4000ms

     xine_osd_show( klearOSD, 0 );  // show until time emits timeout, the last option means NOW ;-)
  }
}

void CKlearEngineXine::toggleDeinterlacing()
{
    if ( xine_get_param( stream, XINE_PARAM_VO_DEINTERLACE ) )
    {
         xine_set_param( stream, XINE_PARAM_VO_DEINTERLACE, false );
    }
    else
    {
        xine_set_param( stream, XINE_PARAM_VO_DEINTERLACE, true );
    }
}

void CKlearEngineXine::timeShift( QString iMrl)
{
    if ( this->EngineIsPlaying() )
    {

        shiftPos = shiftPos + shiftTimer.elapsed();
        xine_stop(stream);
        this->isPlaying = false;
        this->showOSDMessage(i18n("Pausing timeshifted Playback"),3000);
        kdDebug() << "timeshift playback stop at "  << shiftPos <<"\n";
    }
    else
    {
       if ( resumeTime == 0 )
        {
            kdDebug() << "timeshift playback first start\n";
            this->play( iMrl.latin1() );
            shiftTimer.start();
            this->isPlaying = true;
            this->showOSDMessage(i18n("Starting timeshifted Playback"),3000);
            resumeTime = 1;

        }
        else
        {
            kdDebug() << "timeshift playback start\n";
           if ( shiftPos >= 10000 )
            {
               shiftPos = shiftPos - 10000;
           }
            else
            {
               shiftPos  = 0 ;
            }
            xine_play(stream, 0, shiftPos );    // ignored atm
            this->isPlaying = true;
            this->showOSDMessage(i18n("Resuming timeshifted Playback"),3000);
            kdDebug() << "timeshift playback resuming at "  << shiftPos <<"\n";
        }

    }
}

void CKlearEngineXine::play(const char *const iMrl)
{
    const QString __FUNC__ = "CKlearEngineXine::play";

    posTimer.stop();
    setCursor(QCursor(Qt::WaitCursor));
    kdDebug() << "KlearXineWidget: play\n";
    if((!xine_open(stream, iMrl)) || (!xine_play(stream, 0, 0)))
    {
        kdDebug() << "thrown exception   \n";
        CKlearAppFileException e( __LOC__, QString(i18n("Unable to open mrl: %1\n")).arg((char*)&iMrl));
        throw e;
    }

    videoFrameWidth = xine_get_stream_info(stream, XINE_STREAM_INFO_VIDEO_WIDTH);
    videoFrameHeight = xine_get_stream_info(stream, XINE_STREAM_INFO_VIDEO_HEIGHT);
    posTimer.start(500);
    setCursor(QCursor(Qt::ArrowCursor));

    std::cout << "Stream now established" << std::endl;
    emit signalStreamEstablished();
}


void CKlearEngineXine::stop()
{
    kdDebug() << "KlearXineWidget should STOP\n";
    posTimer.stop();
    xine_stop(stream);
    xine_close(stream);
}


/******** mouse hinting ****/
void CKlearEngineXine::startMouseHideTimer()
{
  mouseHideTimer.start(3000);
}

void CKlearEngineXine::stopMouseHideTimer()
{
  mouseHideTimer.stop();
}

void CKlearEngineXine::slotHideMouse()
{
  if (cursor().shape() == Qt::ArrowCursor)
  {
    setCursor(QCursor(Qt::BlankCursor));
  }
}

 bool CKlearEngineXine::isMute()
 {
    int muteParam = XINE_PARAM_AUDIO_MUTE;
    bool b = xine_get_param(stream, muteParam);
    std::cout << "isMute: " << b <<"\n";
    return b;
 }

void CKlearEngineXine::toggleMuting()
{
  int muteParam;

  muteParam = XINE_PARAM_AUDIO_AMP_MUTE;
  //hars//muteParam = XINE_PARAM_AUDIO_MUTE;

  std::cout << xine_get_param(stream, muteParam) << " muteParam: " << muteParam << "\n"; 
  if (xine_get_param(stream, muteParam))
  {
    xine_set_param(stream, muteParam, 0); /* mute off */
    //emit signalXineStatus(i18n("Mute") + ": " + i18n("Off"));
    kdDebug() << "Unmuting sound system  (KlearXine)...\n";
  }
  else
  {
    xine_set_param(stream, muteParam, 1); /* mute on */
    //emit signalXineStatus(i18n("Mute") + ": " + i18n("On"));
    std::cout << xine_get_param(stream, muteParam) << " muteParam: " << muteParam << "\n"; 
    kdDebug() << "Muting sound system  (KlearXine)...\n";
  }
}

void CKlearEngineXine::destSizeCallback(void* p,int /*video_width*/, int /*video_height*/, double /*video_aspect*/,
                       int* dest_width, int* dest_height, double* dest_aspect)

{
    if (p == NULL) return;
    CKlearEngineXine* vw = (CKlearEngineXine*) p;

    *dest_width =  vw->width() ;
    *dest_height = vw->height();
    *dest_aspect = vw->pixel_aspect;
}


void CKlearEngineXine::frameOutputCallback(void* p, int video_width, int video_height, double video_aspect,
                          int* dest_x, int* dest_y, int* dest_width, int* dest_height,
                          double* dest_aspect, int* win_x, int* win_y)

{
    if (p == NULL) return;
    CKlearEngineXine* vw = (CKlearEngineXine*) p;

    *dest_x = 0;
    *dest_y = 0 ;
    *dest_width = vw->width();
    *dest_height = vw->height();
    *win_x = vw->globalX;
    *win_y = vw->globalY;
    *dest_aspect = vw->pixel_aspect;

    /* correct size with video aspect */
    if (video_aspect >= vw->pixel_aspect)
        video_width  = (int) ( (double) (video_width * video_aspect / vw->pixel_aspect + 0.5) );
    else
        video_height = (int) ( (double) (video_height * vw->pixel_aspect / video_aspect) + 0.5);

    /* frame size changed */
    if ( (video_width != vw->width()) || (video_height != vw->height()) )
    {
        vw->videoFrameWidth = video_width;
        vw->videoFrameHeight = video_height;
        vw->videoAspect = video_aspect;
        QApplication::postEvent(vw, new QTimerEvent(TIMER_EVENT_FRAME_FORMAT_CHANGE));

        /* auto-resize parent widget */
        if ((vw->autoresizeEnabled) && (vw->parentWidget()) && (vw->posTimer.isActive()) && (!vw->parentWidget()->isFullScreen())
            && (video_width > 0) && (video_height > 0))
        {
        vw->newParentSize = vw->parentWidget()->size() - QSize((vw->width() - video_width), vw->height() - video_height);

        /* we should not do a resize() inside a xine thread,
            but post an event to the main thread */
        QApplication::postEvent(vw, new QTimerEvent(TIMER_EVENT_RESIZE_PARENT));
        }
    }
}


void CKlearEngineXine::xineEventListener(void *user_data, const xine_event_t *event)
{

    if (user_data == NULL) return;
    CKlearEngineXine* vw = (CKlearEngineXine*) user_data;

    switch(event->type)
    {
        case XINE_EVENT_UI_PLAYBACK_FINISHED:
            {
            vw->running = 0;
            break;
            }
        case XINE_EVENT_FRAME_FORMAT_CHANGE:
        {
            QApplication::postEvent(vw, new QTimerEvent(TIMER_EVENT_FRAME_FORMAT_CHANGE));
            break;
        }
        default:
        {
            break;
        }
    }

}


void CKlearEngineXine::globalPosChanged()
{
  QPoint g = mapToGlobal(QPoint(0,0));
  globalX = g.x();
  globalY = g.y();
}


void CKlearEngineXine::timerEvent( QTimerEvent* tevent )
{

  switch ( tevent->timerId() )
  {
    case TIMER_EVENT_FRAME_FORMAT_CHANGE:
    {
         //emit signalVideoSizeChanged();
         break;
    }
    case TIMER_EVENT_RESIZE_PARENT:
    {
        //parentWidget()->resize(newParentSize);
        break;
    }
        default: break;
  }

}


void CKlearEngineXine::showOSDMessagesChangedCallback(void* p, xine_cfg_entry_t* entry)
{
  if (p == NULL) return;
  CKlearEngineXine* vw = (CKlearEngineXine*) p;

  if (vw->osd)
    vw->osdShow = (bool)entry->num_value;
}


void CKlearEngineXine::showXineOSDMessage(const QString& message, const uint duration, const int priority)
{

  if ((!osd) || (!osdShow) || (isHidden()))
    return;

  static int prevOsdPriority = 0;
  if (osdTimer.isActive() && prevOsdPriority > priority)
    return;
  prevOsdPriority = priority;


  xine_osd_clear(osd);
  xine_osd_draw_text(osd, 0, 0, message.local8Bit(), XINE_OSD_TEXT1);

  if (osdUnscaled)
    xine_osd_show_unscaled(osd, 0);
   else
    xine_osd_show(osd, 0);


   osdTimer.start(duration);
}


void CKlearEngineXine::slotOSDHide()
{
  xine_osd_hide(osd, 0);
  osdTimer.stop();
}


void CKlearEngineXine::slotHideKlearOSD()
{
    if ( klearOSD ) {
       xine_osd_hide( klearOSD, 0 );
       xine_osd_free( klearOSD );
       klearOSD = 0;
    }
}


void CKlearEngineXine::mouseMoveEvent(QMouseEvent* mev)
{
   const QString __FUNC__ = "CKlearEngineXine::mouseMoveEvent(QMouseEvent* mev)";

   if ( !isReady ) return;

   if (cursor().shape() == Qt::BlankCursor)
   {
     setCursor(QCursor(Qt::ArrowCursor));
   }

    x11_rectangle_t   rect;
    xine_event_t      event;
    xine_input_data_t input;

    rect.x = mev->x();
    rect.y = mev->y();
    rect.w = 0;
    rect.h = 0;


    if ( xine_port_send_gui_data ( vo_port, XINE_GUI_SEND_TRANSLATE_GUI_TO_VIDEO, (void*)&rect) != 0 )
            {
                CKlearAppFatalException e( __LOC__, i18n("Unable to open send gui data \n"));
                e.showExceptions();
            }


    event.type        = XINE_EVENT_INPUT_MOUSE_MOVE;
    event.data        = &input;
    event.data_length = sizeof(input);
    input.button      = 0;
    input.x           = rect.x;
    input.y           = rect.y;
    xine_event_send( stream, &event);
}

void CKlearEngineXine::mouseDoubleClickEvent( QMouseEvent * e )
{
    if ( e->button() == QMouseEvent::LeftButton )
    {
        emit signalFullScreen();
    }

    if ( e->button() == QMouseEvent::RightButton )
    {
        emit signalShowMenu();
    }

     if ( e->button() == QMouseEvent::MidButton )
    {
        emit signalToggleMute();
    }
   //TODO i am a terrible hack so fix me
   //       how to access slotFullscreenWindow in main emit signal ???
  kdDebug() << "doubleclicked...\n";
}


QImage CKlearEngineXine::getStreamSnapshot()
{
   const QString __FUNC__ = "CKlearEngineXine::getStreamSnapshot";
   uchar *rgbPile = NULL;
   int width, height;
   double scaleFactor;


   try{
      getScreenshot(rgbPile, width, height, scaleFactor);
   }catch ( CKlearAppException &e ){   // Wir fangen eine Referenz
      e.addErrorMessage( __LOC__, i18n("Could not create StreamSnapshot \n" ));
      throw;  // werfen automatisch das gefangene Element
   }

   if (!rgbPile) return QImage();

   QImage screenShot(rgbPile, width, height, 32, 0, 0, QImage::IgnoreEndian);
   if (scaleFactor >= 1.0)
       width = (int)((double) width * scaleFactor + 0.5);
     else
       height = (int)((double) height / scaleFactor + 0.5);

   screenShot = screenShot.smoothScale(width, height);

   delete []rgbPile;
   return screenShot;
}


void CKlearEngineXine::getScreenshot(uchar*& rgb32BitData, int& videoWidth, int& videoHeight, double& scaleFactor)
{
  const QString __FUNC__ = "CKlearEngineXine::getScreenshot";
  QString errmsg = i18n("Not enough memory to make screenshot! \n");
  QString errmsg2 = i18n("Error retrieving current frame \n");

  uint8_t   *yuv = NULL, *y = NULL, *u = NULL, *v =NULL;

  int        width, height, ratio, format;

  if (!xine_get_current_frame(stream, &width, &height, &ratio, &format, NULL))
  {
     CKlearAppException e( __LOC__, errmsg2 );
     throw e;
  }

  yuv = new uint8_t[((width+8) * (height+1) * 2)];

  if (yuv == NULL)
  {
     CKlearAppException e( __LOC__, errmsg );
     throw e;
  }

  if(!xine_get_current_frame(stream, &width, &height, &ratio, &format, yuv))
  {
     CKlearAppException e( __LOC__, errmsg2 );
     throw e;
  }

  videoWidth = width;
  videoHeight = height;

/*
 * convert to yv12 if necessary
 */

  switch (format) {
  case XINE_IMGFMT_YUY2:
    {
      uint8_t *yuy2 = yuv;

      yuv = new uint8_t[(width * height * 2)];

      if (yuv == NULL)
      {
         CKlearAppException e( __LOC__, errmsg );
         throw e;
      }

      y = yuv;
      u = yuv + width * height;
      v = yuv + width * height * 5 / 4;

      yuy2Toyv12 (y, u, v, yuy2, width, height);

      delete [] yuy2;
    }
    break;
  case XINE_IMGFMT_YV12:
    y = yuv;
    u = yuv + width * height;
    v = yuv + width * height * 5 / 4;

    break;
  default:
    {
      delete [] yuv;
      CKlearAppException e( __LOC__, QString(i18n("Screenshot: Format %1 not supportet!")).arg((char*)&format) );
      throw e;

    }
  }

  rgb32BitData = yv12ToRgb (y, u, v, width, height); //  convert to rgb

  scaleFactor = videoAspect;

  delete [] yuv;
}


uchar* CKlearEngineXine::yv12ToRgb ( uint8_t *src_y, uint8_t *src_u, uint8_t *src_v, int width, int height )
{
/*
 *   Create rgb data from yv12
 */

#define clip_8_bit(val)              \
{                                    \
   if (val < 0)                      \
      val = 0;                       \
   else                              \
      if (val > 255) val = 255;      \
}

  int     i, j;

  int     y, u, v;
  int     r, g, b;

  int     sub_i_uv;
  int     sub_j_uv;

  int     uv_width, uv_height;

  uchar *rgb;

  uv_width  = width / 2;
  uv_height = height / 2;

  rgb = new uchar[(width * height * 4)]; //qt needs a 32bit align
  if (!rgb)
  {
//    kdError(555) << "Not enough memory!" << endl;
    return NULL;
  }

  for (i = 0; i < height; ++i) {
    /*
     * calculate u & v rows
     */
    sub_i_uv = ((i * uv_height) / height);

    for (j = 0; j < width; ++j) {
      /*
       * calculate u & v columns
       */
      sub_j_uv = ((j * uv_width) / width);

      /***************************************************
       *
       *  Colour conversion from
       *    http://www.inforamp.net/~poynton/notes/colour_and_gamma/ColorFAQ.html#RTFToC30
       *
       *  Thanks to Billy Biggs <vektor@dumbterm.net>
       *  for the pointer and the following conversion.
       *
       *   R' = [ 1.1644         0    1.5960 ]   ([ Y' ]   [  16 ])
       *   G' = [ 1.1644   -0.3918   -0.8130 ] * ([ Cb ] - [ 128 ])
       *   B' = [ 1.1644    2.0172         0 ]   ([ Cr ]   [ 128 ])
       *
       *  Where in xine the above values are represented as
       *
       *   Y' == image->y
       *   Cb == image->u
       *   Cr == image->v
       *
       ***************************************************/

      y = src_y[(i * width) + j] - 16;
      u = src_u[(sub_i_uv * uv_width) + sub_j_uv] - 128;
      v = src_v[(sub_i_uv * uv_width) + sub_j_uv] - 128;

      r = (int)((1.1644 * (double)y) + (1.5960 * (double)v));
      g = (int)((1.1644 * (double)y) - (0.3918 * (double)u) - (0.8130 * (double)v));
      b = (int)((1.1644 * (double)y) + (2.0172 * (double)u));

      clip_8_bit (r);
      clip_8_bit (g);
      clip_8_bit (b);

      rgb[(i * width + j) * 4 + 0] = b;
      rgb[(i * width + j) * 4 + 1] = g;
      rgb[(i * width + j) * 4 + 2] = r;
      rgb[(i * width + j) * 4 + 3] = 0;

    }
  }

  return rgb;
}


void CKlearEngineXine::yuy2Toyv12 (uint8_t *y, uint8_t *u, uint8_t *v, uint8_t *input, int width, int height)
{

  int    i, j, w2;

  w2 = width / 2;

  for (i = 0; i < height; i += 2) {
    for (j = 0; j < w2; j++) {
      /*
       * packed YUV 422 is: Y[i] U[i] Y[i+1] V[i]
       */
      *(y++) = *(input++);
      *(u++) = *(input++);
      *(y++) = *(input++);
      *(v++) = *(input++);
    }

    /*
     * down sampling
     */

    for (j = 0; j < w2; j++) {
      /*
       * skip every second line for U and V
       */
      *(y++) = *(input++);
      input++;
      *(y++) = *(input++);
      input++;
    }
  }
}
