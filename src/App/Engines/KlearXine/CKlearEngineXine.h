/**
\author Klear.org
\file CKlearEngineXine.h
\brief Implementation of CKlearEngineAdapter for xine playback
*/
#ifndef CKlearEngineXine_H
#define CKlearEngineXine_H

#include <iostream>           // standard output
#include <cstdio>

#include <qdir.h>             // get home-directory and dir rights
#include <qimage.h>           // screenshot
#include <qdatetime.h>        // screenshot
#include <qobject.h>
#include <qwidget.h>
#include <qthread.h>
#include <qapplication.h>
#include <qtimer.h>
#include <qcursor.h>
#include <qstring.h>
#include <qevent.h>

#include <klocale.h>          // i18n()
#include <kdebug.h>           // kdDebug(), kdFatal(), kdError()

#include "../../CKlearAppConfig.h"
#include "../CKlearEngineAdapter.h"
#include "../../Exceptions/CKlearAppException.h"     // klear exception class
#include "../../Exceptions/CKlearAppFatalException.h"
#include "../../Exceptions/CKlearAppFileException.h"
#include "../../Exceptions/CKlearAppErrorException.h"

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>

#include <xine.h>
#include <xine/xineutils.h>


#define TIMER_EVENT_FRAME_FORMAT_CHANGE 108
#define TIMER_EVENT_RESIZE_PARENT       300

#define INPUT_MOTION (ExposureMask)

#define OSD_MESSAGE_NORMAL_PRIORITY 2
#define DEFAULT_OSD_FONT_SIZE 18
#define DEFAULT_OSD_DURATION  5000
#define OSD_MESSAGE_LOW_PRIORITY    1

/**
\class CKlearEngineXine
\author Klear.org
\brief Implementation of CKlearEngineAdapter for xine playback
*/
class CKlearEngineXine :  public CKlearEngineAdapter, private QThread
{

Q_OBJECT

public:

     /**
     \fn void CKlearEngineXine(QWidget *parent=0, const char *name=0, std::vector<CKlearAppEITData *> *storage = NULL, const QString& audioDriver = QString::null, const QString& videoDriver = QString::null, bool verbose = false)
     \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
     \param parent pointer to parent widget
     \param name name of the widget
     \param audioDriver specifies audiodriver, for xine engine (auto if null)
     \param videoDriver specifies videodriver, for xine engine (auto if null)
     \param storage The EPg data vector
     \param verbose enables xine verbose msg output
     \brief Constructor
     */
     CKlearEngineXine(QWidget *parent=0, const char *name=0, std::vector<CKlearAppEITData *> *storage = NULL,
          const QString& audioDriver = QString::null, const QString& videoDriver = QString::null,
          bool verbose = false);

     /**
     \fn void ~CKlearEngineXine();
     \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
     \brief Destructor
     */
     ~CKlearEngineXine();

     /**
     \fn void EnginePlayStream(const QString& PlayFile);
     \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
     \retval void no retval
     \param PlayFile file to be displayed
     \brief tell backend system to play a new file
     */
     void EnginePlayStream(const QString& PlayFile);

     /**
     \fn void EngineStopStream()
     \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
     \retval void no retval
     \brief tell backend system to stop playing
     */
     void EngineStopStream();

     /**
     \fn bool EngineIsPlaying()
     \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
     \retval bool if engine is playing a stream at the moment
     \brief if engine is playing a stream at the moment return value is true, else false
     */
     bool EngineIsPlaying();

     /**
     \fn uchar* yv12ToRgb (uint8_t *src_y, uint8_t *src_u, uint8_t *src_v, int width, int height)
     \author Kaffeine.sf.net
     \retval uchar* RGB map
     \brief converts yuv raw surface to RGB value map
     */
     uchar* yv12ToRgb (uint8_t *src_y, uint8_t *src_u, uint8_t *src_v, int width, int height);

     /**
     \fn void yuy2Toyv12 (uint8_t *y, uint8_t *u, uint8_t *v, uint8_t *input, int width, int height)
     \author Kaffeine.sf.net
     \retval void no retval
     \brief converts yuy2 to yv12 surface
     */
     void yuy2Toyv12 (uint8_t *y, uint8_t *u, uint8_t *v, uint8_t *input, int width, int height);

     /**
     \fn void hideMouse();
     \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
     \retval void no retval
     \brief hides the mouspointer (with a static delay)
     */
     void hideMouse();

     /**
     \fn void setVolume(const int vol);
     \author J�rgen Kofler
     \retval void no retval
     \param vol mixer-volume
     \brief sets volume of xine softmixer
     */
     void setVolume(const int vol);


     /**
     \fn int getVolume();
     \author Marco Kraus <marco@klear.org>
     \retval int volume
     \brief getss volume of xine softmixer
     */
     int getVolume();

     /**
     \fn void toggleMute()
     \author J�rgen Kofler
     \retval void no retval
     \brief mutes soudnsystem
     */
     void toggleMute();

     /**
     \fn QImage getStreamSnapshot();
     \author Kaffeine.sf.net
     \retval QImage The screen snapshot in a QImage fileformat
     \brief grabs a stream-frame and returns it with a QImage
     */
     QImage getStreamSnapshot();

     /**
     \fn void showOSDMessage(const QString& message, const uint duration);
     \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
     \retval void no retval
     \param message The shown Messagestring
     \param duration The time in mSec to display the message
     \brief shows a given Message via OSD System
     */
     void showOSDMessage(const QString& message,const uint duration);

     /**
     \fn void showInfoOSD(const QString CurrChannel);
     \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
     \retval void no retval
     \brief Shows the normal information OSD Message for the current channel
     */
     void showInfoOSD(const QString CurrChannel);

     /**
     \fn void toggleDeinterlacing()
     \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
     \retval void no retval
     \brief set / unset default deinterlace filter
     */
     void toggleDeinterlacing();
      /**
    \fn play(const char *const iMrl);
    \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
    \retval void no retval
    \param iMrl the file to be played by the backend
    \brief tell engine to play new source specified by iMrl
    */
     void play( const char *const iMrl );


    /**
    \fn stop();
    \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
    \retval void no retval
    \brief tell engine to stop playing
    */
    void stop();


    /**
    \fn init();
    \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
    \retval void no retval
    \brief initializes the engine components
    */
    void init();


    /**
    \fn void toggleMuting()
    \author J�rgen Kofler
    \retval void no retval
    \brief mutes the soundsystem
    */
    void toggleMuting();


    /**
    \fn isMute();
    \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
    \retval bool mute status
    \brief returns true if sound is muted
    */
    bool isMute();

    void timeShift( QString iMrl );
    int resumeTime;

public slots:
  void slotHideKlearOSD();
  void slotHideMouse();
  void slotOSDHide();

signals:

void signalNewPosition(int, const QTime&);
void signalStreamEstablished();
void signalShowMenu();
void signalFullScreen();
void signalToggleMute();


protected:

    static void destSizeCallback(void* p, int video_width, int video_height, double video_aspect,
                        int* dest_width, int* dest_height, double* dest_aspect);


    static void frameOutputCallback(void* p, int video_width, int video_height, double video_aspect,
                            int* dest_x, int* dest_y, int* dest_width, int* dest_height,
                            double* dest_aspect, int* win_x, int* win_y);

    static void xineEventListener(void *user_data, const xine_event_t *event);

    void globalPosChanged();
    void run();
    void timerEvent( QTimerEvent* tevent );
    void showXineOSDMessage(const QString& message, const uint duration = DEFAULT_OSD_DURATION /* ms */, const int priority = OSD_MESSAGE_NORMAL_PRIORITY);
    // mouse hiding
    void startMouseHideTimer();
    void stopMouseHideTimer();

    double videoAspect;
    bool isReady;
    xine_stream_t       *stream;

    void mouseMoveEvent( QMouseEvent* me );
    void mouseDoubleClickEvent( QMouseEvent * e );
    void initOSD();

    QTimer mouseHideTimer;
    xine_osd_t* osd;
    bool osdUnscaled;
    bool osdShow;
    char* osdInfo;
    QTimer osdTimer;


private:

    std::vector<CKlearAppEITData *> *EITDataVector;

    void getScreenshot(uchar*& rgb32BitData, int& videoWidth, int& videoHeight, double& scaleFactor);
    bool isPlaying;
    bool                verbose;
    bool                running;
    bool                autoresizeEnabled;
    xine_t              *xine;
    xine_video_port_t   *vo_port;
    xine_audio_port_t   *ao_port;
    xine_event_queue_t  *event_queue;
    char              configfile[2048];
    x11_visual_t      vis;
    double            res_h;
    double            res_v;
    const char             *vo_driver;
    const char             *ao_driver;
    Display             *display;
    int                  screen;
    Window               window;
    int     mvol;
    int    videoFrameWidth;
    int    videoFrameHeight;
    int    globalX;
    int    globalY;
    double pixel_aspect;
    QSize  newParentSize;

    int shiftPos;
    QTime shiftTimer;
    QTimer posTimer;
    xine_osd_t *klearOSD;
    QTimer klearOSDHideTimer;
    static void showOSDMessagesChangedCallback(void* p, xine_cfg_entry_t* entry);
    void startUp();
};

#endif
