/**
\author Klear.org
\file CKlearEngineMPlayer.cpp
\brief MPlayer Engine
*/

#include "CKlearEngineMPlayer.h"

CKlearEngineMPlayer::CKlearEngineMPlayer(QWidget *pParent, const char *pName) :  CKlearEngineAdapter(), QThread()
{
    mPlayerProcess = new QProcess();
    this->mMRL = "";

// debug mplayer output
//    connect ( mPlayerProcess, SIGNAL( readyReadStdout() ), this, SLOT ( slotReadStdout() ) );
//    connect ( mPlayerProcess, SIGNAL( readyReadStderr() ), this, SLOT ( slotReadStderr() ) );
}

CKlearEngineMPlayer::~CKlearEngineMPlayer ()
{
}

void CKlearEngineMPlayer::EnginePlayStream(const QString& MRL)
{
   if(this->EngineIsPlaying())
   {
      return;
   }
   else
   {
      this->mMRL = MRL;
      this->start(); // start thhread
   }
}

void CKlearEngineMPlayer::run()
{

    mPlayerProcess->clearArguments();

    QStringList listCommand;

    listCommand.append(QString("mplayer"));
    listCommand.append(QString("-zoom"));   // Automatically adjust to window size
    listCommand.append(QString("-noautosub"));
    listCommand.append(QString("-cache"));
    listCommand.append(QString("8192"));
    listCommand.append(QString("-double"));
    listCommand.append(QString("-really-quiet"));
    listCommand.append(QString("-ontop"));
    listCommand.append(QString("-wid"));
    listCommand.append(QString("%1").arg(this->winId()));
    listCommand.append(QString("-slave"));
    listCommand.append(QString("-ao"));
    listCommand.append(QString("oss"));
    listCommand.append(QString("-vo"));
    listCommand.append(QString("xv"));
    listCommand.append(QString("-xy")); // Scales it by keeping aspect ratio
    listCommand.append(QString("%1").arg(this->height()));
//    listCommand.append(QString("-y"));
//    listCommand.append(QString("%1").arg(this->height()));
    listCommand.append(QString("-framedrop"));
    listCommand.append(QString("--"));
    listCommand.append(mMRL);

    mPlayerProcess->setArguments(listCommand);

    kdDebug() << mPlayerProcess->arguments() << "\n";

    mPlayerProcess->start();
}


void CKlearEngineMPlayer::EngineStopStream()
{
    if(this->EngineIsPlaying() == true)
    {
        this->mPlayerProcess->tryTerminate();
        QTimer::singleShot( 5000, this->mPlayerProcess, SLOT( kill() ) );
    }
}

QImage CKlearEngineMPlayer::getStreamSnapshot()
{
   return QImage();
}

bool CKlearEngineMPlayer::EngineIsPlaying()
{
  return this->mPlayerProcess->isRunning();
}

void CKlearEngineMPlayer::hideMouse()
{
}

void CKlearEngineMPlayer::showOSDMessage(const QString& message, uint duration)
{
}

void CKlearEngineMPlayer::showInfoOSD(const QString CurrChannel)
{
}

void CKlearEngineMPlayer::toggleDeinterlacing()
{
}

void CKlearEngineMPlayer::slotReadStdout()
{
    QString qsStdout(this->mPlayerProcess->readStdout());

    if (qsStdout.length() == 0)
        return;

     std::cout << "qsStdout: " << qsStdout << "\n" << std::endl;
}

void CKlearEngineMPlayer::slotReadStderr()
{
    QString qsStderr(this->mPlayerProcess->readStdout());
    if (qsStderr.length() == 0)
        return;

     std::cout << "qsStderr: " << qsStderr << "\n" << std::endl;
}

bool CKlearEngineMPlayer::isMute()
{
   return false ; // should be fixed to return correct mute status
}
