/**
\author Klear.org
\file CKlearEngineMPlayer.h
\brief MPlayer Engine
*/

#ifndef CKLEARENGINEMPLAYER_H
#define CKLEARENGINEMPLAYER_H

#include <qimage.h>
#include <qprocess.h>
#include <qthread.h>
#include <qtimer.h>

#include <kdebug.h>
#include <iostream>

#include "../CKlearEngineAdapter.h"

class CKlearEngineMPlayer : public CKlearEngineAdapter, private QThread
{

Q_OBJECT

public:

    CKlearEngineMPlayer(QWidget *pParent=0, const char *pName=0);
    ~CKlearEngineMPlayer();

    void EnginePlayStream(const QString& PlayFile);
    void EngineStopStream();

    QImage getStreamSnapshot();
    void hideMouse();
    void showOSDMessage(const QString& message, uint duration);
    void showInfoOSD(const QString CurrChannel);
    void toggleDeinterlacing();
    bool EngineIsPlaying();
    bool isMute();

public slots:
    void slotReadStdout();
    void slotReadStderr();

private:
   QProcess    *mPlayerProcess;
   void run();
   QString mMRL;
};

#endif
