/**
\author Klear.org
\file <filname>.h
\brief <Short Description Here>
*/

#ifndef CKLEARENGINEXINETNG_H
#define CKLEARENGINEXINETNG_H

#include "../CKlearEngineAdapter.h"
#include <qthread.h>

/**
Next Generation Xine Backend for Klear

	@author Klear.org <developers@klear.org>
*/
class CKlearEngineXineTNG : public CKlearEngineAdapter, public QThread
{
public:
    CKlearEngineXineTNG(QWidget *parent=0, const char *name=0, std::vector<CKlearAppEITData *> *storage = NULL,
          const QString& audioDriver = QString::null, const QString& videoDriver = QString::null,
          bool verbose = false);

    ~CKlearEngineXineTNG();

    void run();
    void EnginePlayStream(const QString&);
    void EngineStopStream();
    bool EngineIsPlaying();
    void hideMouse();
    QImage getStreamSnapshot();
    void showOSDMessage(const QString&, uint);
    void showInfoOSD(QString);
    void toggleDeinterlacing();
    bool isMute();

};

#endif
