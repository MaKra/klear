/**
\author Klear.org
\file <filname>.cpp
\brief <Short Description Here>
*/
#include "CKlearEngineXineTNG.h"

CKlearEngineXineTNG::CKlearEngineXineTNG(QWidget* parent, const char* name, std::vector<CKlearAppEITData *> *storage,
          const QString& audioDriver, const QString& videoDriver, bool verbose) : CKlearEngineAdapter(parent,name), QThread()
{
}


CKlearEngineXineTNG::~CKlearEngineXineTNG()
{
}

void CKlearEngineXineTNG::run()
{
}

void CKlearEngineXineTNG::EnginePlayStream(const QString&)
{
}

void CKlearEngineXineTNG::EngineStopStream()
{
}

bool CKlearEngineXineTNG::EngineIsPlaying()
{
}

void CKlearEngineXineTNG::hideMouse()
{
}

QImage CKlearEngineXineTNG::getStreamSnapshot()
{
}

void CKlearEngineXineTNG::showOSDMessage(const QString&, uint)
{
}

void CKlearEngineXineTNG::showInfoOSD(QString)
{
}

void CKlearEngineXineTNG::toggleDeinterlacing()
{
}

bool CKlearEngineXineTNG::isMute()
{
}
