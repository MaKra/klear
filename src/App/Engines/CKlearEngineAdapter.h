/**
\author Klear.org
\file CKlearEngineAdapter.h
\brief Abstract Player class for playback engines
*/
#ifndef CKlearEngineAdapter_H
#define CKlearEngineAdapter_H

#include <iostream>           // standard output
#include <stdio.h>
#include <vector>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <asm/types.h>
#include <sys/soundcard.h>

#include <qdir.h>             // get home-directory and dir rights
#include <qaccel.h>           // accelerator keys
#include <qpushbutton.h>      // all buttons
#include <qmessagebox.h>      // startup Infomessage
#include <qimage.h>           // screenshot
#include <qdatetime.h>        // screenshot
#include <qlistview.h>        // channellist
#include <qlabel.h>           // text labels
#include <qslider.h>          // volume slider
#include <qgroupbox.h>

#include <klocale.h>          // i18n()
#include <kdebug.h>           // kdDebug(), kdFatal(), kdError()

#include "../CKlearAppConfig.h"
#include "../EPG/CKlearAppDataLayer.h"
#include "../../config.h"

/**
\class CKlearEngineAdapter
\author Klear.org
\brief Abstract engine layer
*/
class CKlearEngineAdapter : public QWidget
{

public:
     /**
     \fn void CKlearEngineAdapter()
     \author Marco Kraus <marco@klear.org>
     \brief Constructor
     */
     CKlearEngineAdapter();

     /**
     \fn void CKlearEngineAdapter(QWidget* parent, const char* name)
     \author Patric Sherif <patric@klear.org> and Manuel Habermann <manuel@klear.org>
     \brief overloaded Constructor
     */
     CKlearEngineAdapter(QWidget* parent, const char* name);


     /**
     \fn void ~CKlearEngineAdapter()
     \author Marco Kraus <marco@klear.org>
     \brief Destructor
     */
     virtual ~CKlearEngineAdapter();

     /**
     \fn void EnginePlayStream(const QString& PlayFile)
     \author Marco Kraus <marco@klear.org>
     \retval void no retval
     \param PlayFile file to be displayed
     \brief tell backend system to play a new file or stream
     */
     virtual void EnginePlayStream(const QString& PlayFile) = 0;

     /**
     \fn void EngineStopStream()
     \author Marco Kraus <marco@klear.org>
     \retval void no retval
     \brief stop currently played stream or file
     */
     virtual void EngineStopStream() = 0;

     /**
     \fn void EngineIsPlaying()
     \author Marco Kraus <marco@klear.org>
     \retval bool if current stream is playing
     \brief shows if a stream is currently playing
     */
     virtual bool EngineIsPlaying() = 0;

     /**
     \fn void hideMouse()
     \author Marco Kraus <marco@klear.org>
     \retval void no retval
     \brief hides the mouspointer (with a static delay)
     */
     virtual void hideMouse() = 0;

     /**
     \fn QImage getStreamSnapshot()
     \author Marco Kraus <marco@klear.org>
     \retval QImage The screenshots in a QImage fileformat
     \brief grabs a screen image and returns it with a QImage
     */
     virtual QImage getStreamSnapshot() = 0;

     /**
     \fn void showOSDMessage(const QString& message, uint duration);
     \author Marco Kraus <marco@klear.org>
     \retval void no retval
     \param message The shown messagestring
     \param duration The time in mSec to display the message
     \brief shows a given Message via OSD System
     */
     virtual void showOSDMessage(const QString& message, uint duration) = 0;

     /**
     \fn void showInfoOSD(const QString CurrChannel);
     \author Marco Kraus <marco@klear.org>
     \retval void no retval
     \brief Shows the normal information OSD Message for the current channel
     */
     virtual void showInfoOSD(const QString CurrChannel) = 0;

     /**
     \fn void setVolume(int vol);
     \author Marco Kraus <marco@klear.org>
     \retval void no retval
     \param vol volume value between 0 and 100 (0 = mute)
     \brief Sets a new volume. This method is part of the player-independant sound-controller. You normally don't have to overwrite this function.
     */
     virtual void setVolume(int vol);

     /**
     \fn int getVolume();
     \author Marco Kraus <marco@klear.org>
     \retval int volume
     \brief Gets the current volume. This method is part of the player-independant sound-controller. You normally don't have to overwrite this function.
     */
     virtual int getVolume();

     /**
     \fn void toggleMute()
     \author Marco Kraus <marco@klear.org>
     \retval void no retval
     \brief set / unset audio muting. This method is part of the player-independant sound-controller. You normally don't have to overwrite this function.
     */
     virtual void toggleMute();

     /**
     \fn void toggleDeinterlacing()
     \author Marco Kraus <marco@klear.org>
     \retval void no retval
     \brief set / unset default deinterlace filter
     */
     virtual void toggleDeinterlacing() = 0;

    /**
    \fn isMute()
    \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
    \retval bool mute status
    \brief returns true if sound is muted
    */
     virtual bool isMute() = 0;

private:
    int mMixer; // Mixer FD
    QString mMixerDevice;

   struct mix_channel
   {
      char *label;
      int left;
      int right;
      int dev_num;
   } mix_channel[20];

};

#endif
