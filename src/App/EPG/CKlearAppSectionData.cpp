#include "CKlearAppSectionData.h"

CKlearAppSectionData::CKlearAppSectionData()
{
  this->table_id = 0;
  this->section_syntax_indicator = 0;
  this->reserved_1 = 0;
  this->reserved_2 = 0;
  this->section_length = 0;
  this->service_id = 0;
  this->reserved_3 = 0;
  this->version_number = 0;
  this->current_next_indicator = 0;
  this->section_number = 0;
  this->last_section_number = 0;
  this->transport_stream_id = 0;
  this->original_network_id = 0;
  this->segment_last_section_number = 0;
  this->last_table_id = 0;
}


CKlearAppSectionData::~CKlearAppSectionData()
{
   // just free memory, vector is erased automatically
   //for( int k = 1; this->DescDataVector.size() > k; k++ )
   //   delete this->DescDataVector[k];
}


