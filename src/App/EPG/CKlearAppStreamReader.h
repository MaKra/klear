#ifndef CKLEARAPPSTREAMREADER_H
#define CKLEARAPPSTREAMREADER_H

#include <qthread.h>

#include "../CKlearAppConfig.h"

#include "../Exceptions/CKlearAppException.h"
#include "../Exceptions/CKlearAppFatalException.h"
#include "../Exceptions/CKlearAppErrorException.h"
#include "../Exceptions/CKlearAppFileException.h"

#include "CKlearAppDataDecoder.h"
#include "CKlearAppEITData.h"

/**
Filters for PID 0x12 (EIT) on demux device, grabs data
and passes the buffer to the decoder

@author Marco Kraus <marco@klear.org> with many code from dvbsnoop, vlc and libdvbpsi
*/
class CKlearAppStreamReader : public QThread
{

public:
    CKlearAppStreamReader(std::vector<CKlearAppEITData *> *storage, CKlearAppConfig *config);
    ~CKlearAppStreamReader();

    void setRunning(bool v);
    void readContent();
    void run();

private:
   void setUpDemux();
   void tearDownDemux();

   void readSectorLoop();
   long readBuffer();

   u_char  buf[READ_BUF_SIZE];       /* data buffer */
   int DemuxFD;
   bool isRunning;
   CKlearAppConfig *KlearConfig;
   CKlearAppDataDecoder *decoder;
};

#endif
