#include "CKlearAppDataDecoder.h"

CKlearAppDataDecoder::CKlearAppDataDecoder(std::vector<CKlearAppEITData *> *storage, CKlearAppConfig *Config)
{
  this->EITDataVector = storage;
  this->KlearConfig = Config;
}

CKlearAppDataDecoder::~CKlearAppDataDecoder()
{
}

void CKlearAppDataDecoder::decodeBuffer( u_char *buf, int len )
{
  const QString __FUNC__ = "CKlearAppDataDecoder::decodeBuffer( u_char *buf, int len )";

  // -- do a soft crc check
  // -- if soft crc fails, ignore packet
  int softcrc_fail = 0;

  u_long crc = crc32( (char *)buf, len );
  if(crc)
      softcrc_fail = 1;

  if (! softcrc_fail)
  {
      try{
             this->decodeSection(buf,len);        /* data correct, so start EIT decoding */
      }catch( CKlearAppErrorException &e ){  // all system exceptions
            e.addErrorMessage( __LOC__, i18n("Could not decode buffer\n") );
            throw;
      }catch( std::exception &x ){  // all system exceptions
            CKlearAppErrorException e( __LOC__, x.what() );
            throw e;
      }
  }
  else
     std::cerr << "Packet soft CRC failed, skipping packet..." << std::endl;
}


void CKlearAppDataDecoder::decodeSection (u_char *b, int len)
{
  const QString __FUNC__ = "CKlearAppDataDecoder::decodeSection (u_char *b, int len)";

 CKlearAppSectionData *SectionData = new CKlearAppSectionData();

 SectionData->table_id                                = b[0];
 SectionData->section_syntax_indicator        = getBits (b, 0,  8,  1);
 SectionData->reserved_1                           = getBits (b, 0,  9,  1);
 SectionData->reserved_2                           = getBits (b, 0, 10,  2);
 SectionData->section_length                       = getBits (b, 0, 12, 12);
 SectionData->service_id                             = getBits (b, 0, 24, 16);
 SectionData->reserved_3                           = getBits (b, 0, 40,  2);
 SectionData->version_number                    = getBits (b, 0, 42,  5);
 SectionData->current_next_indicator           = getBits (b, 0, 47,  1);
 SectionData->section_number                    = getBits (b, 0, 48,  8);
 SectionData->last_section_number             = getBits (b, 0, 56,  8);
 SectionData->transport_stream_id              = getBits (b, 0, 64, 16);
 SectionData->original_network_id               = getBits (b, 0, 80, 16);
 SectionData->segment_last_section_number  = getBits (b, 0, 96,  8);
 SectionData->last_table_id                            = getBits (b, 0,104,  8);

 if( SectionData->service_id != this->KlearConfig->getServiceID() ) // not the channel we need from this Bouquet
 {
     try{
            delete SectionData;
      }catch( std::exception &x ){  // all system exceptions
            CKlearAppErrorException e( __LOC__, x.what() );
            throw e;
      }

     return;
 }

 // - header data after length value
 int len1 = SectionData->section_length - 11;
 b   += 14;

 while (len1 > 4)
 {
   CKlearAppDescriptorData *DescData = new CKlearAppDescriptorData();

   DescData->event_id                   = getBits (b, 0,  0, 16);
   DescData->start_time_MJD             = getBits (b, 0, 16, 16);
   DescData->start_time_UTC             = getBits (b, 0, 32, 24);
   DescData->duration                   = getBits (b, 0, 56, 24);
   DescData->running_status             = getBits (b, 0, 80, 3);
   DescData->free_CA_mode               = getBits (b, 0, 83, 1);
   DescData->descriptors_loop_length    = getBits (b, 0, 84, 12);

   DescData->dur_hour = ((DescData->duration >> 20)&0xf)*10+((DescData->duration >> 16)&0xf);
   DescData->dur_min =  ((DescData->duration >> 12)&0xf)*10+((DescData->duration >>  8)&0xf);
   DescData->dur_sec =  ((DescData->duration >>  4)&0xf)*10+((DescData->duration      )&0xf);

   if( DescData->start_time_MJD > 0 )  // see ETSI EN 300 468 - ANNEX C
   {
      DescData->start_year =  (long) ((DescData->start_time_MJD  - 15078.2) / 365.25);
      DescData->start_month =  (long) ((DescData->start_time_MJD - 14956.1 - (long)(DescData->start_year * 365.25) ) / 30.6001);
      DescData->start_day =  (long) (DescData->start_time_MJD - 14956 - (long)(DescData->start_year * 365.25) - (long)(DescData->start_month * 30.6001));
      long k = (DescData->start_month == 14 || DescData->start_month == 15) ? 1 : 0;
      DescData->start_year = DescData->start_year + k + 1900;
      DescData->start_month = DescData->start_month - 1 - k*12;
   }

   DescData->start_hour = (DescData->start_time_UTC>>16)&0xFF;
   DescData->start_min  = (DescData->start_time_UTC>>8)&0xFF;
   DescData->start_sec  = (DescData->start_time_UTC)&0xFF;

   b    += 12;
   len1 -= (12 + DescData->descriptors_loop_length);
   int len2 = DescData->descriptors_loop_length;

   while (len2 > 0)
   {
      int x;

      try{
            x = this->decodeDescriptor(b, DVB_SI, DescData);
      }catch( CKlearAppErrorException &e ){  // all system exceptions
            e.addErrorMessage( __LOC__, i18n("Could not decode descriptor\n") );
            throw;
      }catch( std::exception &x ){  // all system exceptions
            CKlearAppErrorException e( __LOC__, x.what() );
            throw e;
      }

      b    += x;
      len2 -= x;
   }

   if( DescData->ShortEvent == "" )
      DescData->ShortEvent = i18n("No name available");

   if( DescData->ExtendedEvent == "" )
      DescData->ExtendedEvent = i18n("No description available");

   /* CHECK ME ! Why Data > 0xFF ?? Error in DataReader ? MKr, 18.08 */
   if( DescData->Genre > 0xFF )
      DescData->Genre = 0x00;

   /* CHECK ME ! Why Data > 0xFF ?? Error in DataReader ? MKr, 18.08 */
   if( DescData->ParentalRating > 0xFF )
      DescData->ParentalRating = 0x00;

   // add DescData to SectionData Object-vector
     try{
            SectionData->DescDataVector.push_back( DescData ); // Delete DescData in vector destructor
      }catch( std::exception &x ){  // all system exceptions
            CKlearAppErrorException e( __LOC__, x.what() );
            throw e;
      }

 } // while len1

  SectionData->crc = getBits (b, 0, 0, 32); // section crc

  if( SectionData->DescDataVector.size() <= 0)
  {
    // no events, so drop section
     try{
            delete SectionData;
      }catch( std::exception &x ){  // all system exceptions
            CKlearAppErrorException e( __LOC__, x.what() );
            throw e;
      }

    return;
  }

   //this->debugPrintSectionDataItem(SectionData);  // only print for debugging purposes
   //std::cout << "Creating new data storage layer" << std::endl;
   CKlearAppDataLayer *DatabaseLayer = new CKlearAppDataLayer( this->EITDataVector );

   // ------------ now save data to database -------------- //
   for( int k = 0; SectionData->DescDataVector.size() > k; k++ )
   {

     CKlearAppEITData *DataItem = new CKlearAppEITData(); // deleted when deleted from storage vector

     DataItem->EventID = SectionData->DescDataVector[k]->event_id;
     DataItem->ServiceID = SectionData->service_id;
     DataItem->NetworkID = SectionData->original_network_id;
     DataItem->StartTime = QDateTime( QDate( SectionData->DescDataVector[k]->start_year, SectionData->DescDataVector[k]->start_month, SectionData->DescDataVector[k]->start_day ),
                                      QTime( QString("%1").arg( SectionData->DescDataVector[k]->start_hour, 0 , 16 ).toInt(), QString("%1").arg( SectionData->DescDataVector[k]->start_min, 0 , 16 ).toInt(), QString("%1").arg( SectionData->DescDataVector[k]->start_sec, 0 , 16 ).toInt() )
                                    ).toTime_t();
     DataItem->Duration = (SectionData->DescDataVector[k]->dur_hour*60*60)+(SectionData->DescDataVector[k]->dur_min*60)+SectionData->DescDataVector[k]->dur_sec;
     DataItem->CA = SectionData->DescDataVector[k]->free_CA_mode;
     DataItem->Rating = SectionData->DescDataVector[k]->ParentalRating;
     DataItem->Genre = SectionData->DescDataVector[k]->Genre;
     DataItem->ShortDesc = SectionData->DescDataVector[k]->ShortEvent;
     DataItem->LongDesc = SectionData->DescDataVector[k]->ExtendedEvent;

     try{
               if( DatabaseLayer->searchItem( DataItem->EventID ) == true )
               {
                     // std::cout << "Item already in database. Section dropped" << std::endl;
                     delete DataItem;
                     break; // end section, because all events in section are always the same
               }else{
                     DatabaseLayer->addItem( DataItem );  // Data Item will deleted, when storage deletes it
                  // std::cout << "Add Event: " << DataItem->ShortDesc << " mit EventID: " << DataItem->EventID << " um " << DataItem->StartTime << std::endl;
               }
     }catch( CKlearAppErrorException &e ){  // all system exceptions
            e.addErrorMessage( __LOC__, i18n("Vector communication failed\n") );
            throw;
     }catch( std::exception &x ){  // all system exceptions
            CKlearAppErrorException e( __LOC__, x.what() );
            throw e;
     }


  } // end for() for descriptors to database

  try{
         delete DatabaseLayer;    // delete database layer

         //  std::cout << "Delete Descriptor Data..." << std::endl;
         for( int k = 0; SectionData->DescDataVector.size() > k; k++ )  // now delete the DescriptorData already parsed
               delete SectionData->DescDataVector[k];

         delete SectionData;  // finished package, now we can delete it
   }catch( std::exception &x ){  // all system exceptions
            CKlearAppErrorException e( __LOC__, x.what() );
            throw e;
   }

      //  std::cout << "done" << std::endl;
}

/*
  determine descriptor type and print it...
  return byte length
*/
int  CKlearAppDataDecoder::decodeDescriptor(u_char *b, DTAG_SCOPE scope, CKlearAppDescriptorData *DD)
{
  const QString __FUNC__ = "CKlearAppStreamReader::decodeDescriptor()";

  int id  =  (int)b[0];
  int len = ((int)b[1]) + 2;

   try{
         CKlearAppDescriptorParser *d = new CKlearAppDescriptorParser(b, DD);
         delete d;
   }catch( CKlearAppErrorException &e ){  // all system exceptions
         e.addErrorMessage( __LOC__, i18n("Could not use DescriptorParser\n") );
         throw;
   }catch( std::exception &x ){  // all system exceptions
         CKlearAppErrorException e( __LOC__, x.what() );
         throw e;
   }

  return len;   // (descriptor total length)
}

void  CKlearAppDataDecoder::debugPrintSectionDataItem(CKlearAppSectionData *SectionData)
{
 std::cout << "\n\n============== SECTION START ==================" << std::endl;
 std::cout << "TableID: " << findTableID (TableIDs, SectionData->table_id) << std::endl;
 if (   SectionData->table_id != 0x4E && SectionData->table_id != 0x4F && !(SectionData->table_id >= 0x50 && SectionData->table_id <= 0x6F) )
 {
   std::cerr << "Wrong TableID" << std::endl;
   return;
 }

 std::cout << "section_syntax_indicator: " << SectionData->section_syntax_indicator << std::endl;
 std::cout << "reserved_1: " << SectionData->reserved_1 << std::endl;
 std::cout << "reserved_2: " << SectionData->reserved_2 << std::endl;
 std::cout << "Section_length: " << SectionData->section_length << std::endl;
 std::cout << "Service_ID: " << SectionData->service_id << " (refers to PMT program_number)" << std::endl;
 if (SectionData->service_id == 0xFFFF) std::cout << "EIT is scrambled" << std::endl;
 std::cout << "reserved_3: " << SectionData->reserved_3 <<std::endl;
 std::cout << "Version_number: " << SectionData->version_number << std::endl;
 std::cout << "current_next_indicator: " << SectionData->current_next_indicator << " (" << findTableID(TableIndicator, SectionData->current_next_indicator) << ")"<< std::endl;
 std::cout << "Section_number: " << SectionData->section_number << std::endl;
 std::cout << "Last_Section_number: " << SectionData->last_section_number << std::endl;
 std::cout << "Transport_stream_ID: " << SectionData->transport_stream_id << std::endl;
 std::cout << "Original_network_ID: " << SectionData->original_network_id << " (" << findTableID( OriginalNetwork_ID_Table,SectionData->original_network_id) << ")" << std::endl;
 std::cout << "Segment_last_Section_number: " << SectionData->segment_last_section_number << std::endl;
 std::cout << "Last_table_id: " << SectionData->last_table_id << " (" << findTableID(TableIDs ,SectionData->last_table_id) << ")" << std::endl;
 std::cout << "CRC: " << SectionData->crc << std::endl;
 std::cout << "Descriptor-Menge der Section: " << SectionData->DescDataVector.size() << std::endl;

 if( SectionData->DescDataVector.size() > 0)
 {
   for( int k = 0; SectionData->DescDataVector.size() > k; k++ )
   {
      std::cout << "\n-------------- new Event ---------------- " << std::endl;
      std::cout << "\nEvent ID: " << SectionData->DescDataVector[k]->event_id << std::endl;
      std::cout << "Program Name: " << SectionData->DescDataVector[k]->ShortEvent << std::endl;
      printf("Duration: %02d:%02d:%02d\n", SectionData->DescDataVector[k]->dur_hour, SectionData->DescDataVector[k]->dur_min, SectionData->DescDataVector[k]->dur_sec);
      printf("Date: %02d.%02d.%02d\n", SectionData->DescDataVector[k]->start_day, SectionData->DescDataVector[k]->start_month, SectionData->DescDataVector[k]->start_year );
      printf("Time: %02lx:%02lx:%02lx (UTC)\n", SectionData->DescDataVector[k]->start_hour, SectionData->DescDataVector[k]->start_min, SectionData->DescDataVector[k]->start_sec);
      std::cout << "Running_status: " << SectionData->DescDataVector[k]->running_status << " (" << findTableID(RunningStatus_Table, SectionData->DescDataVector[k]->running_status) << ")" << std::endl;
      std::cout << "Free_CA_mode: " << SectionData->DescDataVector[k]->free_CA_mode << std::endl; // (e2.free_CA_mode) ? "streams [partially] CA controlled" : "unscrambled")
      std::cout << "Descriptors_loop_length: " << SectionData->DescDataVector[k]->descriptors_loop_length << std::endl;
      std::cout << "Rating: " << SectionData->DescDataVector[k]->ParentalRating << " (" << findTableID( ParentalRatingTable, SectionData->DescDataVector[k]->ParentalRating ) << ")" << std::endl;
      std::cout << "Genre: " << SectionData->DescDataVector[k]->Genre << " (" << findTableID( ContentTable, SectionData->DescDataVector[k]->Genre ) << ")" << std::endl;
      std::cout << "Program Description: " << SectionData->DescDataVector[k]->ExtendedEvent << std::endl;
   }
 }

 std::cout << "\n===========================================" << std::endl;
}
