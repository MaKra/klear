#include "CKlearAppDataLayer.h"

CKlearAppDataLayer::CKlearAppDataLayer(std::vector<CKlearAppEITData *> *storage)
{
   this->EITDataVector = storage;
}

CKlearAppDataLayer::~CKlearAppDataLayer()
{
}

void CKlearAppDataLayer::addItem(CKlearAppEITData *item)
{
    const QString __FUNC__ = "CKlearAppDataLayer::addItem(CKlearAppEITData *item)";

     try{
             EITDataVector->push_back(item);
      }catch( std::exception &x ){  // all system exceptions
            CKlearAppErrorException e( __LOC__, x.what() );
            throw e;
      }catch( ... ){  // all system exceptions
            CKlearAppErrorException e( __LOC__, i18n("Could not add item to vector") );
            throw e;
      }

};

CKlearAppEITData* CKlearAppDataLayer::getCurrentRunning()
{
   for( std::vector<CKlearAppEITData *>::iterator it = this->EITDataVector->begin(); it != this->EITDataVector->end(); ++it )
   {
        if( ( (*it)->StartTime < QDateTime::currentDateTime(Qt::UTC).toTime_t() ) &&
            ( (*it)->StartTime+(*it)->Duration > QDateTime::currentDateTime(Qt::UTC).toTime_t() ) )
        return *it;
   }

   return NULL; // nothing found
};

CKlearAppEITData* CKlearAppDataLayer::getNextRunning()
{
   CKlearAppEITData *data;
   for( std::vector<CKlearAppEITData *>::iterator it = this->EITDataVector->begin(); it != this->EITDataVector->end(); ++it )
   {
         data = getCurrentRunning();
         if( data->StartTime+data->Duration == (*it)->StartTime )
             return *it;
   }

   return NULL; // nothing found
};

void CKlearAppDataLayer::cleanUpDatabase()
{
   const QString __FUNC__ = "CKlearAppDataLayer::cleanUpDatabase()";

   try{
               if( EITDataVector->empty() )
                  return;

               // free memory
               for( std::vector<CKlearAppEITData *>::iterator it = this->EITDataVector->begin(); it != this->EITDataVector->end(); ++it )
                  delete *it;

               // flush vector from freed pointers
               EITDataVector->clear();
    }catch( std::exception &x ){  // all system exceptions
            CKlearAppErrorException e( __LOC__, x.what() );
            throw e;
    }catch( ... ){  // all system exceptions
            CKlearAppErrorException e( __LOC__, i18n("Could not clean up EPG database") );
            throw e;
    }
}

bool CKlearAppDataLayer::searchItem(long EID)
{
   for( std::vector<CKlearAppEITData *>::iterator it = this->EITDataVector->begin(); it != this->EITDataVector->end(); ++it )
   {
        if( (*it)->EventID == EID ) // Event found in vector
            return true;
   }

   return false;
}
