#include "CKlearAppStreamReader.h"

CKlearAppStreamReader::CKlearAppStreamReader(std::vector<CKlearAppEITData *> *storage, CKlearAppConfig *config)
{
 const QString __FUNC__ = "CKlearAppStreamReader::CKlearAppStreamReader()";

 this->KlearConfig = config;

  try{
      this->decoder = new CKlearAppDataDecoder(storage, config);
      this->isRunning = true;
      this->setUpDemux();
  }catch( CKlearAppErrorException &e ){  // all system exceptions
        e.addShowException( __LOC__, i18n("Could not setup StreamReader\n") );
  }catch( std::exception &x ){  // all system exceptions
        CKlearAppErrorException e;
        e.addShowException( __LOC__, x.what() );
   }

}

CKlearAppStreamReader::~CKlearAppStreamReader()
{
   this->tearDownDemux();
   delete decoder;
}

void CKlearAppStreamReader::run()
{
   const QString __FUNC__ = "CKlearAppStreamReader::run()";

  std::cout << "Reading in Content" << std::endl;

  try{
      this->readContent();
  }catch( CKlearAppErrorException &e ){  // all system exceptions
        e.addShowException( __LOC__, i18n("Could not read EPG content\n") );
  }catch( std::exception &x ){  // all system exceptions
        CKlearAppErrorException e;
        e.addShowException( __LOC__, x.what() );
  }

}

void CKlearAppStreamReader::readContent()
{
   const QString __FUNC__ = "CKlearAppStreamReader::readContent()";

  try{
      this->readSectorLoop();
  }catch( CKlearAppErrorException &e ){  // all system exceptions
        e.addErrorMessage( __LOC__, i18n("Could not read sector loop\n") );
       throw;
  }catch( std::exception &x ){  // all system exceptions
        CKlearAppErrorException e( __LOC__, x.what() );
        throw e;
  }

}

void CKlearAppStreamReader::setRunning(bool v)
{
   this->isRunning = v;
}

void CKlearAppStreamReader::setUpDemux()
{
   const QString __FUNC__ = "CKlearAppStreamReader::setUpDemux()";

  std::cout << "Demux: " << this->KlearConfig->getDVBDemux() << std::endl;
  if((this->DemuxFD = open("/dev/dvb/"+this->KlearConfig->getDVBAdapter()+"/"+this->KlearConfig->getDVBDemux(), O_RDWR)) < 0){
      std::cerr << "EIT EPG: Could not open demux device for reading! Continue without EPG" << std::endl;
      CKlearAppErrorException e(__LOC__,  i18n("Could not open demux device for reading!\nWill continue without EPG."));
      throw e;
  }

  if( ioctl(this->DemuxFD, DMX_SET_BUFFER_SIZE, SECT_BUF_SIZE) < 0 ) {
      std::cerr << "DMX_SET_BUFFER_SIZE failed" << std::endl;
      this->tearDownDemux();
      CKlearAppErrorException e(__LOC__,  i18n("DMX_SET_BUFFER_SIZE failed"));
      throw e;
  }

   struct dmx_sct_filter_params flt;
   memset( &flt, 0, sizeof(struct dmx_sct_filter_params) );
   flt.pid = EIT_PID;
   flt.filter.filter[0] = 0;
   flt.filter.mask[0] = 0;
   flt.timeout = 0;
   flt.flags = DMX_IMMEDIATE_START; // |= DMX_CHECK_CRC;

   if (ioctl (this->DemuxFD, DMX_SET_FILTER, &flt) < 0) {
      std::cerr << "DMX_SET_FILTER failed" << std::endl;
      this->tearDownDemux();
      CKlearAppErrorException e(__LOC__,  i18n("DMX_SET_FILTER failed"));
      throw e;
   }

}

void CKlearAppStreamReader::tearDownDemux()
{
   const QString __FUNC__ = "CKlearAppStreamReader::tearDownDemux()";

  ioctl (this->DemuxFD, DMX_STOP, 0);
  close(this->DemuxFD);
}


long CKlearAppStreamReader::readBuffer()
{
   const QString __FUNC__ = "CKlearAppStreamReader::readBuffer()";

    int    n;
    int    sect_len;

    // give parsed buffer data to decoder
      try{
            n = read(this->DemuxFD, this->buf,3);             // read section header
      }catch( std::exception &x ){  // all system exceptions
            CKlearAppErrorException e( __LOC__, x.what() );
            throw e;
      }

    if (n <= 0) return n;           // error or strange, abort

    sect_len = ((buf[1] & 0x0F) << 8) + buf[2]; // get section size
    if (sect_len > (sizeof(this->buf)-3)) return -1;   // something odd?

      try{
            n = read(this->DemuxFD,this->buf+3,sect_len);        // read 1 section
      }catch( std::exception &x ){  // all system exceptions
            CKlearAppErrorException e( __LOC__, x.what() );
            throw e;
      }

    if (n >=0)
        n += 3;              // we already read header bytes

    return n;
}

void CKlearAppStreamReader::readSectorLoop()
{
  const QString __FUNC__ = "CKlearAppStreamReader::readSectorLoop()";

  std::cerr << "Reading sector loop" << std::endl;
  while( isRunning == true )  // to stop reading, set running to false
  {
     long n;

      try{
            n = readBuffer(); // modifies this->buf with demux data
      }catch( std::exception &x ){  // all system exceptions
            CKlearAppErrorException e( __LOC__, x.what());
           throw e;
      }

    if (n < 0) { // -- error or eof?
         if (errno == ETIMEDOUT)
               break;        // Timout, abort
         else
               continue;
    }

    if (n == 0)
            continue;  // dmxmode = no eof!

    // give parsed buffer data to decoder
      try{
             this->decoder->decodeBuffer( this->buf, n );
      }catch( CKlearAppErrorException &e ){  // all system exceptions
            e.addErrorMessage( __LOC__, i18n("Could not decode buffer\n") );
            throw;
      }catch( std::exception &x ){  // all system exceptions
            CKlearAppErrorException e( __LOC__, x.what() );
            throw e;
      }

  }

}
