#ifndef CKLEARAPPEITDATA_H
#define CKLEARAPPEITDATA_H

#include <qstring.h>

/**
Class to store all EIT Data, splitted after each Event and added
with all necessary section data

@author Marco Kraus <marco@klear.org>
*/

class CKlearAppEITData
{

public:
    CKlearAppEITData();
    ~CKlearAppEITData();

    long EventID;
    long ServiceID;
    long NetworkID;
    int StartTime;
    long Duration;
    int CA;
    int Rating;
    int Genre;
    QString ShortDesc;
    QString LongDesc;
};

#endif
