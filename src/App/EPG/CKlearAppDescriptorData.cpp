#include "CKlearAppDescriptorData.h"

CKlearAppDescriptorData::CKlearAppDescriptorData()
{
    // set all items explicite to zero
    this->event_id = 0;
    this->start_time_MJD = 0;
    this->start_time_UTC = 0;
    this->duration = 0;
    this->running_status = 0;
    this->free_CA_mode = 0;
    this->descriptors_loop_length = 0;

    this->dur_hour = 0;
    this->dur_min = 0;
    this->dur_sec = 0;

    this->start_year = 0;
    this->start_month = 0;
    this->start_day = 0;

    this->start_hour = 0;
    this->start_min = 0;
    this->start_sec = 0;

    QString ShortEvent = "";
    QString ExtendedEvent = "";
    u_int   ParentalRating = 0x00;
    u_int   Genre = 0x00;
}

CKlearAppDescriptorData::~CKlearAppDescriptorData()
{
}
