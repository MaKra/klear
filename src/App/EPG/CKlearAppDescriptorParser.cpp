#include "CKlearAppDescriptorParser.h"

CKlearAppDescriptorParser::CKlearAppDescriptorParser()
{
}

CKlearAppDescriptorParser::CKlearAppDescriptorParser(u_char *b, CKlearAppDescriptorData *DD)
{
  const QString __FUNC__ = "CKlearAppDescriptorParser::CKlearAppDescriptorParser()";

  try{
      this->descriptorDVB(b, DD);
  }catch( CKlearAppErrorException &e ){  // all system exceptions
        e.addErrorMessage( __LOC__, i18n("Could not read sector loop\n") );
       throw;
  }catch( std::exception &x ){  // all system exceptions
        CKlearAppErrorException e( __LOC__, x.what() );
        throw e;
  }
}

CKlearAppDescriptorParser::~CKlearAppDescriptorParser()
{
}


/*
  determine descriptor type and print it...

  EN 300 468:
  descriptor_tag: The descriptor tag is an 8-bit field which identifies
  each descriptor. Those values with MPEG-2 normative meaning are described
  in ISO/IEC 13818-1 [1].

  return byte length
*/
int  CKlearAppDescriptorParser::descriptorDVB (u_char *b, CKlearAppDescriptorData *DD)
{
  const QString __FUNC__ = "CKlearAppDescriptorParser::descriptorDVB()";

  int id  =  (int) b[0];
  int len = ((int) b[1]) + 2;

  //std::cout << "DVB-DescriptorTag: " << id << " (" << findTableID(DescriptorTags,id) << ")" << std::endl;
  //std::cout << "Descriptor_length: " << len << std::endl;

  // empty ??
  len = ((int)b[1]) + 2;
  if (b[1] == 0)
     return len;

  // print hex buf of descriptor
  //printhex_buf (9, b,len);

  try{
         switch(b[0]) {
            case 0x4D:
                  //std::cout << "Event_name: " << this->descriptorDVB_ShortEvent(b) << std::endl;
                  DD->ShortEvent += this->descriptorDVB_ShortEvent(b);  // append
                  if (len > strlen(DD->ShortEvent) + 5) // if extra long short description
                        DD->ExtendedEvent += this->descriptorDVB_ShortEvent2(b);  // append
                  break;
            case 0x4E:
                  //std::cout << "Extended Event: " << this->descriptorDVB_ExtendedEvent(b) << std::endl;
                  DD->ExtendedEvent += this->descriptorDVB_ExtendedEvent(b);  // append
                  break;
            case 0x55:
                  //std::cout << "Rating: " << findTableID( ParentalRatingTable, this->descriptorDVB_ParentalRating(b) ) << std::endl;
                  DD->ParentalRating = this->descriptorDVB_ParentalRating(b);  // replace, should normally only appear once
                  break;
            case 0x54:
                  //  std::cout << "Genre: " << findTableID( ContentTable, this->descriptorDVB_Content(b) ) << std::endl;
                  DD->Genre = this->descriptorDVB_Content(b);  // replace, should normally only appear once
                  break;
            default:
                  //if (b[0] < 0x80)
                  //   std::cerr << "  ----> ERROR: unimplemented descriptor (dvb context), Report!" << std::endl;
                  // descriptor_any (b);
         //      descriptor_PRIVATE (b, DVB_SI);
               break;
         }
  }catch( CKlearAppErrorException &e ){  // all system exceptions
        e.addErrorMessage( __LOC__, i18n("Could not decode DescriptorDVB\n") );
       throw;
  }catch( std::exception &x ){  // all system exceptions
        CKlearAppErrorException e( __LOC__, x.what() );
        throw e;
  }

  return len;   // (descriptor total length)
}


/*
  0x4D  Short Event  descriptor
  ETSI EN 300 468     6.2.xx
  -- checked v1.6.1
*/
QString CKlearAppDescriptorParser::descriptorDVB_ShortEvent(u_char *b)
{
  u_char   ISO639_2_language_code[4];
  getISO639_3(ISO639_2_language_code, b+2);
  //std::cout << "ISO639_2_language_code: " << ISO639_2_language_code << std::endl;
  b+= 5;

  int len2 = getBits(b, 0, 0, 8);
  b += 1;

  return print_text2_468A(b,len2);
}


QString CKlearAppDescriptorParser::descriptorDVB_ShortEvent2(u_char *b)
{

  u_char   ISO639_2_language_code[4];
  getISO639_3(ISO639_2_language_code, b+2);
  //std::cout << "ISO639_2_language_code: " << ISO639_2_language_code << std::endl;
  b+= 5;

  int len2 = getBits(b, 0, 0, 8);

  b += len2+1;

  len2 =  getBits(b,  0, 0, 8);
  b += 1;

  return print_text2_468A (b,len2);
}



/*
  0x4E  Extended Event  descriptor
  ETSI EN 300 468     6.2.xx
  -- checked v1.6.1
*/
QString CKlearAppDescriptorParser::descriptorDVB_ExtendedEvent  (u_char *b)
{

 typedef struct  _descExtendedEvent {
    u_int      descriptor_tag;
    u_int      descriptor_length;
    u_int      descriptor_number;
    u_int      last_descriptor_number;
    u_char     ISO639_2_language_code[4];
    u_int      length_of_items;
    u_int      text_length;
 } descExtendedEvent;


 typedef struct  _descExtendedEvent2 {
    u_int      item_description_length;
    u_int      item_length;
 } descExtendedEvent2;


 descExtendedEvent    d;
 descExtendedEvent2   d2;
 int                  len1, lenB;

 d.descriptor_tag        = b[0];
 d.descriptor_length     = b[1];

 d.descriptor_number         = getBits (b, 0, 16, 4);
 d.last_descriptor_number    = getBits (b, 0, 20, 4);
 getISO639_3 (d.ISO639_2_language_code, b+3);
 d.length_of_items       = getBits (b, 0, 48, 8);

/*
 out_SB_NL (4,"descriptor_number: ",d.descriptor_number);
 out_SB_NL (4,"last_descriptor_number: ",d.last_descriptor_number);
 out_nl    (4,"iSO639_2_language_code:  %3.3s", d.ISO639_2_language_code);
 out_SB_NL (5,"length_of_items: ",d.length_of_items);
*/

 b   += 7;
 lenB = d.descriptor_length - 5;
 len1 = d.length_of_items;

 while (len1 > 0)
 {
   d2.item_description_length    = getBits (b, 0, 0, 8);
   b += 1 + d2.item_description_length;
   d2.item_length        = getBits (b, 0, 0, 8);
   b += 1 + d2.item_length;
   len1 -= (2 + d2.item_description_length + d2.item_length);
   lenB -= (2 + d2.item_description_length + d2.item_length);
 }

   d.text_length         = getBits (b, 0, 0, 8);
   b += 1;
   lenB -= 1;

   return print_text2_468A( b, d.text_length );
}


/*
  0x55  Parental Rating  descriptor
  ETSI EN 300 468     6.2.xx
*/
u_int CKlearAppDescriptorParser::descriptorDVB_ParentalRating (u_char *b)
{
 const QString __FUNC__ = "CKlearAppDescriptorParser::descriptorDVB_ParentalRating()";

 typedef struct  _descParentalRating {
    u_int      descriptor_tag;
    u_int      descriptor_length;
 } descParentalRating;

 typedef struct  _descParentalRating2 {
    u_char     country_code[4];
    u_int      rating;
 } descParentalRating2;

 descParentalRating   d;
 descParentalRating2  d2;
 int                  len;

 d.descriptor_tag        = b[0];
 d.descriptor_length         = b[1];

 len = d.descriptor_length;
 b  += 2;

 while (len > 0) {

      try{
            strncpy((char *)d2.country_code, (char *)b, 3);
      }catch( std::exception &x ){  // all system exceptions
            CKlearAppErrorException e( __LOC__, x.what() );
            throw e;
      }catch( ... ){  // all system exceptions
            CKlearAppErrorException e( __LOC__, i18n("Could not stringcopy parental rating\n") );
            throw e;
      }

    d2.rating            = getBits (b,0,24,8);

    b += 4;
    len -= 4;

    //printf("Country_code:  %3.3s", d2.country_code);
    return d2.rating;
 }

}


/*
  0x54  Content  descriptor
  ETSI EN 300 468     6.2.xx
*/
u_int CKlearAppDescriptorParser::descriptorDVB_Content(u_char *b)
{

 typedef struct  _descContent {
    u_int      descriptor_tag;
    u_int      descriptor_length;
 } descContent;

 typedef struct  _descContent2 {
    u_int      content_nibble_level_1;
    u_int      content_nibble_level_2;
    u_int      user_nibble_1;
    u_int      user_nibble_2;
 } descContent2;

 descContent   d;
 descContent2  d2;
 int           len;

 d.descriptor_tag            = b[0];
 d.descriptor_length         = b[1];

 len = d.descriptor_length;
 b  += 2;

 while ( len > 0) {
    d2.content_nibble_level_1    = getBits (b,0, 0,4);
    d2.content_nibble_level_2    = getBits (b,0, 4,4);
    d2.user_nibble_1         = getBits (b,0, 8,4);
    d2.user_nibble_2         = getBits (b,0,12,4);

    b   += 2;
    len -= 2;

    return((d2.content_nibble_level_1 << 8) | d2.content_nibble_level_2 );
 }

}
