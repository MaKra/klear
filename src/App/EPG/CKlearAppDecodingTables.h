#ifndef CKLEARAPPDECODINGTABLES_H
#define CKLEARAPPDECODINGTABLES_H

/* ****************************************************** */
//  Decoding tables
//  Mainly ripped from dvbsnoop
//
//  Adapted for Klear my Marco Kraus <marco@klear.org>
//
/* ***************************************************** */

typedef struct _STR_TABLE {
   u_int    from;          /* e.g. from id 1  */
   u_int    to;            /*      to   id 3  */
   u_char   *str;          /*      is   string xxx */
} STR_TABLE;

typedef enum {
    MPEG, DVB_SI, DSMCC_STREAM, DSMCC_CAROUSEL, DSMCC_INT_UNT, MHP_AIT, TVA_RNT
} DTAG_SCOPE;

static const char* findTableID (STR_TABLE *t, u_int id)
{
  while (t->str) {
    if (t->from <= id && t->to >= id)
       return (char *)t->str;
    t++;
  }
  std::cerr << "Error Table: 0x" << std::hex << id << std::endl;;
  return "ERROR: TableID not defined . Please report!";
}

static STR_TABLE  OriginalNetwork_ID_Table[] = {
    // -- updated 2003-10-16
    // -- { Original Network ID, Original Network ID,   "Description | Operator" },
    { 0x0000, 0x0000,   (u_char *)"Reserved | Reserved" },
    { 0x0001, 0x0001,   (u_char *)"Astra Satellite Network 19,2�E | Soci�t� Europ�enne des Satellites" },
    { 0x0002, 0x0002,   (u_char *)"Astra Satellite Network 28,2�E | Soci�t� Europ�enne des Satellites" },
    { 0x0003, 0x0019,   (u_char *)"Astra | Soci�t� Europ�enne des Satellites" },
    { 0x001A, 0x001A,   (u_char *)"Quiero Televisi�n | Quiero Televisi�n" },
    { 0x001B, 0x001B,   (u_char *)"RAI | RAI" },
    { 0x001F, 0x001F,   (u_char *)"Europe Online Networks (EON) | Europe Online Networks S.A" },
    { 0x0020, 0x0020,   (u_char *)"ASTRA | Soci�t� Europ�enne des Satellites" },
    { 0x0021, 0x0026,   (u_char *)"Hispasat Network | Hispasat S.A." },
    { 0x0027, 0x0027,   (u_char *)"Hispasat 30�W | Hispasat FSS" },
    { 0x0028, 0x0028,   (u_char *)"Hispasat 30�W | Hispasat DBS" },
    { 0x0029, 0x0029,   (u_char *)"Hispasat 30�W | Hispasat America" },
    { 0x002E, 0x002E,   (u_char *)"Xantic | Xantic BU Broadband" },
    { 0x002F, 0x002F,   (u_char *)"TVNZ Digital | TVNZ" },
    { 0x0030, 0x0030,   (u_char *)"Canal+ Satellite Network | Canal+ SA (for Intelsat 601-325�E)" },
    { 0x0031, 0x0031,   (u_char *)"Hispasat - VIA DIGITAL | Hispasat S.A." },
    { 0x0032, 0x0034,   (u_char *)"Hispasat Network | Hispasat S.A." },
    { 0x0035, 0x0035,   (u_char *)"Nethold Main Mux System | NetHold IMS" },
    { 0x0036, 0x0036,   (u_char *)"TV Cabo | TV Cabo Portugal" },
    { 0x0037, 0x0037,   (u_char *)"STENTOR | France Telecom, CNES and DGA" },
    { 0x0038, 0x0038,   (u_char *)"OTE | Hellenic Telecommunications Organization S.A." },
    { 0x0040, 0x0040,   (u_char *)"Croatian Post and Telecommunications | HPT Croatian Post and Telecommunications" },
    { 0x0041, 0x0041,   (u_char *)"Mindport network | Mindport" },
    { 0x0046, 0x0047,   (u_char *)"1 degree W | Telenor" },
    { 0x0048, 0x0048,   (u_char *)"STAR DIGITAL | STAR DIGITAL A.S." },
    { 0x0049, 0x0049,   (u_char *)"Sentech Digital Satellite | Sentech" },
    { 0x0050, 0x0050,   (u_char *)"Croatian Radio and Television | HRT Croatian Radio and Television" },
    { 0x0051, 0x0051,   (u_char *)"Havas | Havas" },
    { 0x0052, 0x0052,   (u_char *)"Osaka Yusen Satellite | StarGuide Digital Networks" },
    { 0x0054, 0x0054,   (u_char *)"Teracom Satellite | Teracom AB Satellite Services" },
    { 0x0055, 0x0055,   (u_char *)"Sirius Satellite System European Coverage | NSAB (Teracom)" },
    { 0x0058, 0x0058,   (u_char *)"(Thiacom 1 & 2 co-located 78.5�E) | UBC Thailand" },
    { 0x005E, 0x005E,   (u_char *)"Sirius Satellite System Nordic Coverage | NSAB" },
    { 0x005F, 0x005F,   (u_char *)"Sirius Satellite System FSS | NSAB" },
    { 0x0060, 0x0060,   (u_char *)"MSG MediaServices GmbH | MSG MediaServices GmbH" },
    { 0x0069, 0x0069,   (u_char *)"(Optus B3 156�E) | (Optus Communications)" },
    { 0x0070, 0x0070,   (u_char *)"BONUM1; 36 Degrees East | NTV+" },
    { 0x0073, 0x0073,   (u_char *)"(PanAmSat 4 68.5�E) | (Pan American Satellite System)" },
    { 0x007D, 0x007D,   (u_char *)"Skylogic | Skylogic Italia" },
    { 0x007E, 0x007E,   (u_char *)"Eutelsat Satellite System at 7�E | European Telecommunications Satellite Organization" },
    { 0x007F, 0x007F,   (u_char *)"Eutelsat Satellite System at 7�E | EUTELSAT - European Telecommunications Satellite Organization" },
    { 0x0085, 0x0085,   (u_char *)"BetaTechnik | BetaTechnik" },
    { 0x0090, 0x0090,   (u_char *)"National network | TDF" },
    { 0x00A0, 0x00A0,   (u_char *)"National Cable Network | News Datacom" },
    { 0x00A1, 0x00A5,   (u_char *)"News Satellite Network | News Datacom" },
    { 0x00A6, 0x00A6,   (u_char *)"ART | ART" },
    { 0x00A7, 0x00A7,   (u_char *)"Globecast | France Telecom" },
    { 0x00A8, 0x00A8,   (u_char *)"Foxtel | Foxtel" },
    { 0x00A9, 0x00A9,   (u_char *)"Sky New Zealand | Sky New Zealand" },
    { 0x00B0, 0x00B3,   (u_char *)"TPS | La T�l�vision Par Satellite" },
    { 0x00B4, 0x00B4,   (u_char *)"Telesat 107.3�W | Telesat Canada" },
    { 0x00B5, 0x00B5,   (u_char *)"Telesat 111.1�W | Telesat Canada" },
    { 0x00B6, 0x00B6,   (u_char *)"Telstra Saturn | TelstraSaturn Limited" },
    { 0x00BA, 0x00BA,   (u_char *)"Satellite Express 6 (80�E) | Satellite Express" },
    { 0x00C0, 0x00CD,   (u_char *)"Canal + | Canal+" },
    { 0x00EB, 0x00EB,   (u_char *)"Eurovision Network | European Broadcasting Union" },
    { 0x0100, 0x0100,   (u_char *)"ExpressVu | ExpressVu Inc." },
    { 0x010D, 0x010D,   (u_char *)"Skylogic | Skylogic Italia" },
    { 0x010E, 0x010E,   (u_char *)"Eutelsat Satellite System at 10�E | European Telecommunications Satellite Organization" },
    { 0x010F, 0x010F,   (u_char *)"Eutelsat Satellite System at 10�E | EUTELSAT - European Telecommunications Satellite Organization" },
    { 0x0110, 0x0110,   (u_char *)"Mediaset | Mediaset" },
    { 0x011F, 0x011F,   (u_char *)"visAvision Network | European Telecommunications Satellite Organization" },
    { 0x013D, 0x013D,   (u_char *)"Skylogic | Skylogic Italia" },
    { 0x013E, 0x013E,   (u_char *)"Eutelsat Satellite System 13�E | European Telecommunications Satellite Organization" },
    { 0x013F, 0x013F,   (u_char *)"Eutelsat Satellite System at 13�E | EUTELSAT - European Telecommunications Satellite Organization" },
    { 0x016D, 0x016D,   (u_char *)"Skylogic | Skylogic Italia" },
    { 0x016E, 0x016E,   (u_char *)"Eutelsat Satellite System at 16�E | European Telecommunications Satellite Organization" },
    { 0x016F, 0x016F,   (u_char *)"Eutelsat Satellite System at 16�E | EUTELSAT - European Telecommunications Satellite Organization" },
    { 0x01F4, 0x01F4,   (u_char *)"MediaKabel B.V" },
    { 0x022D, 0x022D,   (u_char *)"Skylogic | Skylogic Italia" },
    { 0x022E, 0x022F,   (u_char *)"Eutelsat Satellite System at 21.5�E | EUTELSAT - European Telecommunications Satellite Organization" },
    { 0x026D, 0x026D,   (u_char *)"Skylogic | Skylogic Italia" },
    { 0x026E, 0x026F,   (u_char *)"Eutelsat Satellite System at 25.5�E | EUTELSAT - European Telecommunications Satellite Organization" },
    { 0x029D, 0x029D,   (u_char *)"Skylogic | Skylogic Italia" },
    { 0x029E, 0x029E,   (u_char *)"Eutelsat Satellite System at 29�E | European Telecommunications Satellite Organization" },
    { 0x029F, 0x029F,   (u_char *)"Eutelsat Satellite System at 28.5�E | EUTELSAT - European Telecommunications Satellite Organization" },
    { 0x02BE, 0x02BE,   (u_char *)"Arabsat | Arabsat (Scientific Atlanta, Eutelsat)" },
    { 0x033D, 0x033D,   (u_char *)"Skylogic at 33�E | Skylogic Italia" },
    { 0x033E, 0x033f,   (u_char *)"Eutelsat Satellite System at 33�E | Eutelsat" },
    { 0x036D, 0x036D,   (u_char *)"Skylogic | Skylogic Italia" },
    { 0x036E, 0x036E,   (u_char *)"Eutelsat Satellite System at 36�E | European Telecommunications Satellite Organization" },
    { 0x036F, 0x036F,   (u_char *)"Eutelsat Satellite System at 36�E | EUTELSAT - European Telecommunications Satellite Organization" },
    { 0x03E8, 0x03E8,   (u_char *)"Telia | Telia, Sweden" },
    { 0x047D, 0x047D,   (u_char *)"Skylogic | Skylogic Italia" },
    { 0x047E, 0x047f,   (u_char *)"Eutelsat Satellite System at 12.5�W | EUTELSAT - European Telecommunications Satellite Organization" },
    { 0x048D, 0x048D,   (u_char *)"Skylogic | Skylogic Italia" },
    { 0x048E, 0x048E,   (u_char *)"Eutelsat Satellite System at 48�E | European Telecommunications Satellite Organization" },
    { 0x048F, 0x048F,   (u_char *)"Eutelsat Satellite System at 48�E | EUTELSAT - European Telecommunications Satellite Organization" },
    { 0x052D, 0x052D,   (u_char *)"Skylogic | Skylogic Italia" },
    { 0x052E, 0x052f,   (u_char *)"Eutelsat Satellite System at 8�W | EUTELSAT - European Telecommunications Satellite Organization" },
    { 0x055D, 0x055D,   (u_char *)"Skylogic at 5�W | Skylogic Italia" },
    { 0x055E, 0x055f,   (u_char *)"Eutelsat Satellite System at 5�W | Eutelsat" },
    { 0x0600, 0x0600,   (u_char *)"UPC Satellite | UPC" },
    { 0x0601, 0x0601,   (u_char *)"UPC Cable | UPC" },
    { 0x0602, 0x0602,   (u_char *)"Tevel | Tevel Cable (Israel)" },
    { 0x071D, 0x071D,   (u_char *)"Skylogic at 70.5�E | Skylogic Italia" },
    { 0x071E, 0x071f,   (u_char *)"Eutelsat Satellite System at 70.5�E | Eutelsat" },
    { 0x0800, 0x0801,   (u_char *)"Nilesat 101 | Nilesat" },
    { 0x0880, 0x0880,   (u_char *)"MEASAT 1, 91.5�E | MEASAT Broadcast Network Systems SDN. BHD. (Kuala Lumpur, Malaysia)" },
    { 0x0882, 0x0882,   (u_char *)"MEASAT 2, 91.5�E | MEASAT Broadcast Network Systems SDN. BHD. (Kuala Lumpur, Malaysia)" },
    { 0x0883, 0x0883,   (u_char *)"MEASAT 2, 148.0�E | Hsin Chi Broadcast Company Ltd." },
    { 0x088F, 0x088F,   (u_char *)"MEASAT 3 | MEASAT Broadcast Network Systems SDN. BHD. (Kuala Lumpur, Malaysia)" },
    { 0x1000, 0x1000,   (u_char *)"Optus B3 156�E | Optus Communications" },
    { 0x1001, 0x1001,   (u_char *)"DISH Network | Echostar Communications" },
    { 0x1002, 0x1002,   (u_char *)"Dish Network 61.5 W | Echostar Communications" },
    { 0x1003, 0x1003,   (u_char *)"Dish Network 83 W | Echostar Communications" },
    { 0x1004, 0x1004,   (u_char *)"Dish Network 119 W | Echostar Communications" },
    { 0x1005, 0x1005,   (u_char *)"Dish Network 121 W | Echostar Communications" },
    { 0x1006, 0x1006,   (u_char *)"Dish Network 148 W | Echostar Communications" },
    { 0x1007, 0x1007,   (u_char *)"Dish Network 175 W | Echostar Communications" },
    { 0x1008, 0x1008,   (u_char *)"Dish Network W | Echostar Communications" },
    { 0x1009, 0x1009,   (u_char *)"Dish Network X | Echostar Communications" },
    { 0x100A, 0x100A,   (u_char *)"Dish Network Y | Echostar Communications" },
    { 0x100B, 0x100B,   (u_char *)"Dish Network Z | Echostar Communications" },
    { 0x1010, 0x1010,   (u_char *)"ABC TV | Australian Broadcasting Corporation" },
    { 0x1011, 0x1011,   (u_char *)"SBS | SBS Australia" },
    { 0x1012, 0x1012,   (u_char *)"Nine Network Australia | Nine Network Australia" },
    { 0x1013, 0x1013,   (u_char *)"Seven Network Australia | Seven Network Limited" },
    { 0x1014, 0x1014,   (u_char *)"Network TEN Australia | Network TEN Limited" },
    { 0x1015, 0x1015,   (u_char *)"WIN Television Australia | WIN Television Pty Ltd" },
    { 0x1016, 0x1016,   (u_char *)"Prime Television Australia | Prime Television Limited" },
    { 0x1017, 0x1017,   (u_char *)"Southern Cross Broadcasting Australia | Southern Cross Broadcasting (Australia) Limited" },
    { 0x1018, 0x1018,   (u_char *)"Telecasters Australia | Telecasters Australia Limited" },
    { 0x1019, 0x1019,   (u_char *)"NBN Australia | NBN Limited" },
    { 0x101A, 0x101A,   (u_char *)"Imparja Television Australia | Imparja Television Australia" },
    { 0x101B, 0x101f,   (u_char *)"Reserved for Australian broadcasters | Reserved for Australian broadcasters" },
    { 0x1100, 0x1100,   (u_char *)"GE Americom | GE American Communications" },
    { 0x2000, 0x2000,   (u_char *)"Thiacom 1 & 2 co-located 78.5�E | Shinawatra Satellite" },
    { 0x2024, 0x2024,   (u_char *)"Australian Digital Terrestrial Television | Australian Broadcasting Authority" },
    { 0x2038, 0x2038,   (u_char *)"Belgian Digital Terrestrial Television | BIPT" },
    { 0x20CB, 0x20CB,   (u_char *)"Czech Republic Digital Terrestrial Television | Czech Digital Group" },
    { 0x20D0, 0x20D0,   (u_char *)"Danish Digital Terrestrial Television | National Telecom Agency Denmark" },
    { 0x20E9, 0x20E9,   (u_char *)"Estonian Digital Terrestrial Television | Estonian National Communications Board" },
    { 0x20F6, 0x20F6,   (u_char *)"Finnish Digital Terrestrial Television | Telecommunicatoins Administratoin Centre, Finland" },
    { 0x2114, 0x2114,   (u_char *)"German Digital Terrestrial Television | IRT on behalf of the German DVB-T broadcasts" },
    { 0x2174, 0x2174,   (u_char *)"Irish Digital Terrestrial Television | Irish Telecommunications Regulator" },
    { 0x2178, 0x2178,   (u_char *)"Israeli Digital Terrestrial Television | BEZEQ (The Israel Telecommunication Corp Ltd.)" },
    { 0x2210, 0x2210,   (u_char *)"Netherlands Digital Terrestrial Television | Nozema" },
    { 0x22BE, 0x22BE,   (u_char *)"Singapore Digital Terrestrial Television | Singapore Broadcasting Authority" },
    { 0x22D4, 0x22D4,   (u_char *)"Spanish Digital Terrestrial Television | Spanish Broadcasting Regulator" },
    { 0x22F1, 0x22F1,   (u_char *)"Swedish Digital Terrestrial Television | Swedish Broadcasting Regulator" },
    { 0x22F4, 0x22F4,   (u_char *)"Swiss Digital Terrestrial Television | OFCOM" },
    { 0x233A, 0x233A,   (u_char *)"UK Digital Terrestrial Television | Independent Television Commission" },
    { 0x3000, 0x3000,   (u_char *)"PanAmSat 4 68.5�E | Pan American Satellite System" },
    { 0x5000, 0x5000,   (u_char *)"Irdeto Mux System | Irdeto Test Laboratories" },
    { 0x616D, 0x616D,   (u_char *)"BellSouth Entertainment | BellSouth Entertainment, Atlanta, GA, USA" },
    { 0x6600, 0x6600,   (u_char *)"UPC Satellite | UPC" },
    { 0x6601, 0x6601,   (u_char *)"UPC Cable | UPC" },
    { 0xF000, 0xF000,   (u_char *)"Small Cable networks | Small cable network network operators" },
    { 0xF001, 0xF001,   (u_char *)"Deutsche Telekom | Deutsche Telekom AG" },
    { 0xF010, 0xF010,   (u_char *)"Telef�nica Cable | Telef�nica Cable SA" },
    { 0xF020, 0xF020,   (u_char *)"Cable and Wireless Communication | Cable and Wireless Communications" },
    { 0xF100, 0xF100,   (u_char *)"Casema | Casema N.V." },
    { 0xF750, 0xF750,   (u_char *)"Telewest Communications Cable Network | Telewest Communications Plc" },
    { 0xF751, 0xF751,   (u_char *)"OMNE Communications | OMNE Communications Ltd." },
    { 0xFBFC, 0xFBFC,   (u_char *)"MATAV | MATAV (Israel)" },
    { 0xFBFD, 0xFBFD,   (u_char *)"Telia Kabel-TV | Telia, Sweden" },
    { 0xFBFE, 0xFBFE,   (u_char *)"TPS | La T�l�vision Par Satellite" },
    { 0xFBFF, 0xFBFF,   (u_char *)"Sky Italia | Sky Italia Spa." },
    { 0xFC10, 0xFC10,   (u_char *)"Rh�ne Vision Cable | Rh�ne Vision Cable" },
    { 0xFC41, 0xFC41,   (u_char *)"France Telecom Cable | France Telecom" },
    { 0xFD00, 0xFD00,   (u_char *)"National Cable Network | Lyonnaise Communications" },
    { 0xFE00, 0xFE00,   (u_char *)"TeleDenmark Cable TV | TeleDenmark" },
    { 0xFEC0, 0xFEff,   (u_char *)"Network Interface Modules | Common Interface" },
    { 0xFF00, 0xFFfe,   (u_char *)"Private_temporary_use | ETSI" },
    {  0,0, NULL }
  };

static STR_TABLE  RunningStatus_Table[] = {
     {  0x00, 0x00,  (u_char *)"undefined" },
     {  0x01, 0x01,  (u_char *)"not running" },
     {  0x02, 0x02,  (u_char *)"starts in a few seconds (e.g. for VCR)" },
     {  0x03, 0x03,  (u_char *)"pausing" },
     {  0x04, 0x04,  (u_char *)"running" },
     {  0x05, 0x07,  (u_char *)"reserved" },
     {  0,0, NULL }
  };

/*
  --  Table IDs (sections)
 ETSI EN 468   5.2
 EN 301 192
 TR 102 006
 RE 101 202
 ISO 13818-1
 TS 102 323
*/
static STR_TABLE TableIDs[] = {
     {  0x00, 0x00,  (u_char *)"Program Association Table (PAT)" },
     {  0x01, 0x01,  (u_char *)"Conditional Access Table (CAT)" },
     {  0x02, 0x02,  (u_char *)"Program Map Table (PMT)" },
     {  0x03, 0x03,  (u_char *)"Transport Stream Description Table (TSDT)" },
     {  0x04, 0x04,  (u_char *)"ISO_IEC_14496_scene_description_section" },   /* $$$ TODO */
     {  0x05, 0x05,  (u_char *)"ISO_IEC_14496_object_description_section" },  /* $$$ TODO */
     {  0x06, 0x06,  (u_char *)"Metadata Table" },                // $$$ TODO H.222.0 AMD1
     {  0x07, 0x07,  (u_char *)"IPMP_Control_Information_section (ISO 13818-11)" }, // $$$ TODO H.222.0 AMD1
     {  0x08, 0x37,  (u_char *)"ITU-T Rec. H.222.0|ISO/IEC13818 reserved" },
     {  0x38, 0x39,  (u_char *)"ISO/IEC 13818-6 reserved" },
     {  0x3a, 0x3a,  (u_char *)"DSM-CC - multiprotocol encapsulated data" },
     {  0x3b, 0x3b,  (u_char *)"DSM-CC - U-N messages (DSI or DII)" },
     {  0x3c, 0x3c,  (u_char *)"DSM-CC - Download Data Messages (DDB)" },    /* TR 101 202 */
     {  0x3d, 0x3d,  (u_char *)"DSM-CC - stream descriptorlist" },
     {  0x3e, 0x3e,  (u_char *)"DSM-CC - private data section  // DVB datagram" }, /* EN 301 192 // ISO 13818-6 */
     {  0x3f, 0x3f,  (u_char *)"ISO/IEC 13818-6 reserved" },
     {  0x40, 0x40,  (u_char *)"Network Information Table (NIT) - actual network" },
     {  0x41, 0x41,  (u_char *)"Network Information Table (NIT) - other network" },
     {  0x42, 0x42,  (u_char *)"Service Description Table (SDT) - actual transport stream" },
     {  0x43, 0x45,  (u_char *)"reserved" },
     {  0x46, 0x46,  (u_char *)"Service Description Table (SDT) - other transport stream" },
     {  0x47, 0x49,  (u_char *)"reserved" },
     {  0x4A, 0x4A,  (u_char *)"Bouquet Association Table (BAT)" },
     {  0x4B, 0x4B,  (u_char *)"Update Notification Table (UNT)" },   /* TR 102 006 */
     {  0x4C, 0x4C,  (u_char *)"IP/MAC Notification Table (INT) [EN 301 192]" },  /* EN 192 */
     {  0x4D, 0x4D,  (u_char *)"reserved" },
     {  0x4E, 0x4E,  (u_char *)"Event Information Table (EIT) - actual transport stream, present/following" },
     {  0x4F, 0x4F,  (u_char *)"Event Information Table (EIT) - other transport stream, present/following" },
     {  0x50, 0x5F,  (u_char *)"Event Information Table (EIT) - actual transport stream, schedule" },
     {  0x60, 0x6F,  (u_char *)"Event Information Table (EIT) - other transport stream, schedule" },
     {  0x70, 0x70,  (u_char *)"Time Date Table (TDT)" },
     {  0x71, 0x71,  (u_char *)"Running Status Table (RST)" },
     {  0x72, 0x72,  (u_char *)"Stuffing Table (ST)" },
     {  0x73, 0x73,  (u_char *)"Time Offset Table (TOT)" },
     {  0x74, 0x74,  (u_char *)"MHP- Application Information Table (AIT)" },  /* MHP */
     {  0x75, 0x75,  (u_char *)"TVA- Container Table (CT)" },             /* TS 102 323 */
     {  0x76, 0x76,  (u_char *)"TVA- Related Content Table (RCT)" },      /* TS 102 323 */
     {  0x77, 0x77,  (u_char *)"TVA- Content Identifier Table (CIT)" },       /* TS 102 323 */
     {  0x78, 0x78,  (u_char *)"MPE-FEC Table (MFT)" },               /* EN 301 192 v1.4.1*/
     {  0x79, 0x79,  (u_char *)"TVA- Resolution Notification Table (RNT)" },  /* TS 102 323 */
     {  0x80, 0x7D,  (u_char *)"reserved" },
     {  0x7E, 0x7E,  (u_char *)"Discontinuity Information Table (DIT)" },
     {  0x7F, 0x7F,  (u_char *)"Selection Information Table (SIT)" },
     {  0x80, 0x8F,  (u_char *)"DVB CA message section (EMM/ECM)" },   /* ITU-R BT.1300 ref. */
     {  0x90, 0xBF,  (u_char *)"User private" },
     {  0xC0, 0xFE,  (u_char *)"ATSC reserved" },     /* ETR 211e02 */
     {  0xFF, 0xFF,  (u_char *)"forbidden" },
     {  0,0, NULL }
};


static STR_TABLE Tags[] = {
     {  0x00, 0x3F,  (u_char *)"Forbidden descriptor in DVB context" },   // MPEG Context

    // ETSI 300 468
    // updated EN 302 192 v 1.4.1
     {  0x40, 0x40,  (u_char *)"network_name_descriptor" },
     {  0x41, 0x41,  (u_char *)"service_list_descriptor" },
     {  0x42, 0x42,  (u_char *)"stuffing_descriptor" },
     {  0x43, 0x43,  (u_char *)"satellite_delivery_system_descriptor" },
     {  0x44, 0x44,  (u_char *)"cable_delivery_system_descriptor" },
     {  0x45, 0x45,  (u_char *)"VBI_data_descriptor" },
     {  0x46, 0x46,  (u_char *)"VBI_teletext_descriptor" },
     {  0x47, 0x47,  (u_char *)"bouquet_name_descriptor" },
     {  0x48, 0x48,  (u_char *)"service_descriptor" },
     {  0x49, 0x49,  (u_char *)"country_availibility_descriptor" },
     {  0x4A, 0x4A,  (u_char *)"linkage_descriptor" },
     {  0x4B, 0x4B,  (u_char *)"NVOD_reference_descriptor" },
     {  0x4C, 0x4C,  (u_char *)"time_shifted_service_descriptor" },
     {  0x4D, 0x4D,  (u_char *)"short_event_descriptor" },
     {  0x4E, 0x4E,  (u_char *)"extended_event_descriptor" },
     {  0x4F, 0x4F,  (u_char *)"time_shifted_event_descriptor" },
     {  0x50, 0x50,  (u_char *)"component_descriptor" },
     {  0x51, 0x51,  (u_char *)"mosaic_descriptor" },
     {  0x52, 0x52,  (u_char *)"stream_identifier_descriptor" },
     {  0x53, 0x53,  (u_char *)"CA_identifier_descriptor" },
     {  0x54, 0x54,  (u_char *)"content_descriptor" },
     {  0x55, 0x55,  (u_char *)"parental_rating_descriptor" },
     {  0x56, 0x56,  (u_char *)"teletext_descriptor" },
     {  0x57, 0x57,  (u_char *)"telephone_descriptor" },
     {  0x58, 0x58,  (u_char *)"local_time_offset_descriptor" },
     {  0x59, 0x59,  (u_char *)"subtitling_descriptor" },
     {  0x5A, 0x5A,  (u_char *)"terrestrial_delivery_system_descriptor" },
     {  0x5B, 0x5B,  (u_char *)"multilingual_network_name_descriptor" },
     {  0x5C, 0x5C,  (u_char *)"multilingual_bouquet_name_descriptor" },
     {  0x5D, 0x5D,  (u_char *)"multilingual_service_name_descriptor" },
     {  0x5E, 0x5E,  (u_char *)"multilingual_component_descriptor" },
     {  0x5F, 0x5F,  (u_char *)"private_data_specifier_descriptor" },
     {  0x60, 0x60,  (u_char *)"service_move_descriptor" },
     {  0x61, 0x61,  (u_char *)"short_smoothing_buffer_descriptor" },
     {  0x62, 0x62,  (u_char *)"frequency_list_descriptor" },
     {  0x63, 0x63,  (u_char *)"partial_transport_stream_descriptor" },
     {  0x64, 0x64,  (u_char *)"data_broadcast_descriptor" },
     {  0x65, 0x65,  (u_char *)"CA_system_descriptor" },
     {  0x66, 0x66,  (u_char *)"data_broadcast_id_descriptor" },
     {  0x67, 0x67,  (u_char *)"transport_stream_descriptor" },
     {  0x68, 0x68,  (u_char *)"DSNG_descriptor" },
     {  0x69, 0x69,  (u_char *)"PDC_descriptor" },
     {  0x6A, 0x6A,  (u_char *)"AC3_descriptor" },
     {  0x6B, 0x6B,  (u_char *)"ancillary_data_descriptor" },
     {  0x6C, 0x6C,  (u_char *)"cell_list_descriptor" },
     {  0x6D, 0x6D,  (u_char *)"cell_frequency_list_descriptor" },
     {  0x6E, 0x6E,  (u_char *)"announcement_support_descriptor" },
     {  0x6F, 0x6F,  (u_char *)"application_signalling_descriptor" },
     {  0x70, 0x70,  (u_char *)"adaptation_field_data_descriptor" },
     {  0x71, 0x71,  (u_char *)"service_identifier_descriptor" },
     {  0x72, 0x72,  (u_char *)"service_availability_descriptor" },
     {  0x73, 0x73,  (u_char *)"default_authority_descriptor" },      // TS 102 323
     {  0x74, 0x74,  (u_char *)"related_content_descriptor" },        // TS 102 323
     {  0x75, 0x75,  (u_char *)"TVA_id_descriptor" },             // TS 102 323
     {  0x76, 0x76,  (u_char *)"content_identifier_descriptor" },     // TS 102 323
     {  0x77, 0x77,  (u_char *)"time_slice_fec_identifier_descriptor" },  // EN 300 468 v1.6.1
     {  0x78, 0x78,  (u_char *)"ECM_repetition_rate_descriptor" },    // EN 300 468 v1.6.1
     {  0x79, 0x7F,  (u_char *)"reserved_descriptor" },
     {  0x80, 0xAF,  (u_char *)"User defined/ATSC reserved" },        /* ETR 211e02 */
     {  0xB0, 0xFE,  (u_char *)"User defined" },
     {  0xFF, 0xFF,  (u_char *)"Forbidden" },
     {  0,0, NULL }
  };


/*
 -- current_next_indicator
 -- ISO/IEC13818-1|ITU H.222.0
*/
static STR_TABLE  TableIndicator[] = {
     {  0x00, 0x00,  (u_char *)"valid next" },
     {  0x01, 0x01,  (u_char *)"valid now" },
     {  0,0, NULL }
  };


/*
  -- Descriptor table tags
*/
static  STR_TABLE  DescriptorTags[] = {
     {  0x00, 0x3F,  (u_char *)"Forbidden descriptor in DVB context" },   // MPEG Context

    // ETSI 300 468
    // updated EN 302 192 v 1.4.1
     {  0x40, 0x40,  (u_char *)"network_name_descriptor" },
     {  0x41, 0x41,  (u_char *)"service_list_descriptor" },
     {  0x42, 0x42,  (u_char *)"stuffing_descriptor" },
     {  0x43, 0x43,  (u_char *)"satellite_delivery_system_descriptor" },
     {  0x44, 0x44,  (u_char *)"cable_delivery_system_descriptor" },
     {  0x45, 0x45,  (u_char *)"VBI_data_descriptor" },
     {  0x46, 0x46,  (u_char *)"VBI_teletext_descriptor" },
     {  0x47, 0x47,  (u_char *)"bouquet_name_descriptor" },
     {  0x48, 0x48,  (u_char *)"service_descriptor" },
     {  0x49, 0x49,  (u_char *)"country_availibility_descriptor" },
     {  0x4A, 0x4A,  (u_char *)"linkage_descriptor" },
     {  0x4B, 0x4B,  (u_char *)"NVOD_reference_descriptor" },
     {  0x4C, 0x4C,  (u_char *)"time_shifted_service_descriptor" },
     {  0x4D, 0x4D,  (u_char *)"short_event_descriptor" },
     {  0x4E, 0x4E,  (u_char *)"extended_event_descriptor" },
     {  0x4F, 0x4F,  (u_char *)"time_shifted_event_descriptor" },
     {  0x50, 0x50,  (u_char *)"component_descriptor" },
     {  0x51, 0x51,  (u_char *)"mosaic_descriptor" },
     {  0x52, 0x52,  (u_char *)"stream_identifier_descriptor" },
     {  0x53, 0x53,  (u_char *)"CA_identifier_descriptor" },
     {  0x54, 0x54,  (u_char *)"content_descriptor" },
     {  0x55, 0x55,  (u_char *)"parental_rating_descriptor" },
     {  0x56, 0x56,  (u_char *)"teletext_descriptor" },
     {  0x57, 0x57,  (u_char *)"telephone_descriptor" },
     {  0x58, 0x58,  (u_char *)"local_time_offset_descriptor" },
     {  0x59, 0x59,  (u_char *)"subtitling_descriptor" },
     {  0x5A, 0x5A,  (u_char *)"terrestrial_delivery_system_descriptor" },
     {  0x5B, 0x5B,  (u_char *)"multilingual_network_name_descriptor" },
     {  0x5C, 0x5C,  (u_char *)"multilingual_bouquet_name_descriptor" },
     {  0x5D, 0x5D,  (u_char *)"multilingual_service_name_descriptor" },
     {  0x5E, 0x5E,  (u_char *)"multilingual_component_descriptor" },
     {  0x5F, 0x5F,  (u_char *)"private_data_specifier_descriptor" },
     {  0x60, 0x60,  (u_char *)"service_move_descriptor" },
     {  0x61, 0x61,  (u_char *)"short_smoothing_buffer_descriptor" },
     {  0x62, 0x62,  (u_char *)"frequency_list_descriptor" },
     {  0x63, 0x63,  (u_char *)"partial_transport_stream_descriptor" },
     {  0x64, 0x64,  (u_char *)"data_broadcast_descriptor" },
     {  0x65, 0x65,  (u_char *)"CA_system_descriptor" },
     {  0x66, 0x66,  (u_char *)"data_broadcast_id_descriptor" },
     {  0x67, 0x67,  (u_char *)"transport_stream_descriptor" },
     {  0x68, 0x68,  (u_char *)"DSNG_descriptor" },
     {  0x69, 0x69,  (u_char *)"PDC_descriptor" },
     {  0x6A, 0x6A,  (u_char *)"AC3_descriptor" },
     {  0x6B, 0x6B,  (u_char *)"ancillary_data_descriptor" },
     {  0x6C, 0x6C,  (u_char *)"cell_list_descriptor" },
     {  0x6D, 0x6D,  (u_char *)"cell_frequency_list_descriptor" },
     {  0x6E, 0x6E,  (u_char *)"announcement_support_descriptor" },
     {  0x6F, 0x6F,  (u_char *)"application_signalling_descriptor" },
     {  0x70, 0x70,  (u_char *)"adaptation_field_data_descriptor" },
     {  0x71, 0x71,  (u_char *)"service_identifier_descriptor" },
     {  0x72, 0x72,  (u_char *)"service_availability_descriptor" },
     {  0x73, 0x73,  (u_char *)"default_authority_descriptor" },      // TS 102 323
     {  0x74, 0x74,  (u_char *)"related_content_descriptor" },        // TS 102 323
     {  0x75, 0x75,  (u_char *)"TVA_id_descriptor" },             // TS 102 323
     {  0x76, 0x76,  (u_char *)"content_identifier_descriptor" },     // TS 102 323
     {  0x77, 0x77,  (u_char *)"time_slice_fec_identifier_descriptor" },  // EN 300 468 v1.6.1
     {  0x78, 0x78,  (u_char *)"ECM_repetition_rate_descriptor" },    // EN 300 468 v1.6.1
     {  0x79, 0x7F,  (u_char *)"reserved_descriptor" },
     {  0x80, 0xAF,  (u_char *)"User defined/ATSC reserved" },        /* ETR 211e02 */
     {  0xB0, 0xFE,  (u_char *)"User defined" },
     {  0xFF, 0xFF,  (u_char *)"Forbidden" },
     {  0,0, NULL }
};


/*
  -- Parental Rating Info
  -- ETSI EN 300 468   6.2.25
*/
static STR_TABLE  ParentalRatingTable[] = {
     {  0x00, 0x00,  (u_char *)"undefined" },
     {  0x01, 0x01,  (u_char *)"minimum age: 4 years" },
     {  0x02, 0x02,  (u_char *)"minimum age: 5 years" },
     {  0x03, 0x03,  (u_char *)"minimum age: 6 years" },
     {  0x04, 0x04,  (u_char *)"minimum age: 7 years" },
     {  0x05, 0x05,  (u_char *)"minimum age: 8 years" },
     {  0x06, 0x06,  (u_char *)"minimum age: 9 years" },
     {  0x07, 0x07,  (u_char *)"minimum age: 10 years" },
     {  0x08, 0x08,  (u_char *)"minimum age: 11 years" },
     {  0x09, 0x09,  (u_char *)"minimum age: 12 years" },
     {  0x0A, 0x0A,  (u_char *)"minimum age: 13 years" },
     {  0x0B, 0x0B,  (u_char *)"minimum age: 14 years" },
     {  0x0C, 0x0C,  (u_char *)"minimum age: 15 years" },
     {  0x0D, 0x0D,  (u_char *)"minimum age: 16 years" },
     {  0x0E, 0x0E,  (u_char *)"minimum age: 17 years" },
     {  0x0F, 0x0F,  (u_char *)"minimum age: 18 years" },
     {  0x10, 0xFF,  (u_char *)"defined by broadcaster" },
     {  0,0, NULL }
};


/*
  -- Content Nibble Types (Content descriptor)
  -- ETSI EN 300 468   6.2.8
*/
  static STR_TABLE  ContentTable[] = {
     // ContenNibble_1 << 8    |  ContentNibble_2
     //  4 bit                 |       4 bit
     {  0x0000, 0x000F,  (u_char *)"undefined" },

     // Movie/Drama
     {  0x0100, 0x0100,  (u_char *)"movie/drama (general)" },
     {  0x0101, 0x0101,  (u_char *)"detective/thriller" },
     {  0x0102, 0x0102,  (u_char *)"adventure/western/war" },
     {  0x0103, 0x0103,  (u_char *)"science fiction/fantasy/horror" },
     {  0x0104, 0x0104,  (u_char *)"comedy" },
     {  0x0105, 0x0105,  (u_char *)"soap/melodram/folkloric" },
     {  0x0106, 0x0106,  (u_char *)"romance" },
     {  0x0107, 0x0107,  (u_char *)"serious/classical/religious/historical movie/drama" },
     {  0x0108, 0x0108,  (u_char *)"adult movie/drama" },

     {  0x0109, 0x010E,  (u_char *)"reserved" },
     {  0x010F, 0x010F,  (u_char *)"user defined" },

     // News Current Affairs
     {  0x0200, 0x0200,  (u_char *)"news/current affairs (general)" },
     {  0x0201, 0x0201,  (u_char *)"news/weather report" },
     {  0x0202, 0x0202,  (u_char *)"news magazine" },
     {  0x0203, 0x0203,  (u_char *)"documentary" },
     {  0x0204, 0x0204,  (u_char *)"discussion/interview/debate" },
     {  0x0205, 0x020E,  (u_char *)"reserved" },
     {  0x020F, 0x020F,  (u_char *)"user defined" },

     // Show Games show
     {  0x0300, 0x0300,  (u_char *)"show/game show (general)" },
     {  0x0301, 0x0301,  (u_char *)"game show/quiz/contest" },
     {  0x0302, 0x0302,  (u_char *)"variety show" },
     {  0x0303, 0x0303,  (u_char *)"talk show" },
     {  0x0304, 0x030E,  (u_char *)"reserved" },
     {  0x030F, 0x030F,  (u_char *)"user defined" },

     // Sports
     {  0x0400, 0x0400,  (u_char *)"sports (general)" },
     {  0x0401, 0x0401,  (u_char *)"special events" },
     {  0x0402, 0x0402,  (u_char *)"sports magazine" },
     {  0x0403, 0x0403,  (u_char *)"football/soccer" },
     {  0x0404, 0x0404,  (u_char *)"tennis/squash" },
     {  0x0405, 0x0405,  (u_char *)"team sports" },
     {  0x0406, 0x0406,  (u_char *)"athletics" },
     {  0x0407, 0x0407,  (u_char *)"motor sport" },
     {  0x0408, 0x0408,  (u_char *)"water sport" },
     {  0x0409, 0x0409,  (u_char *)"winter sport" },
     {  0x040A, 0x040A,  (u_char *)"equestrian" },
     {  0x040B, 0x040B,  (u_char *)"martial sports" },
     {  0x040C, 0x040E,  (u_char *)"reserved" },
     {  0x040F, 0x040F,  (u_char *)"user defined" },

     // Children/Youth
     {  0x0500, 0x0500,  (u_char *)"childrens's/youth program (general)" },
     {  0x0501, 0x0501,  (u_char *)"pre-school children's program" },
     {  0x0502, 0x0502,  (u_char *)"entertainment (6-14 year old)" },
     {  0x0503, 0x0503,  (u_char *)"entertainment (10-16 year old)" },
     {  0x0504, 0x0504,  (u_char *)"information/education/school program" },
     {  0x0505, 0x0505,  (u_char *)"cartoon/puppets" },
     {  0x0506, 0x050E,  (u_char *)"reserved" },
     {  0x050F, 0x050F,  (u_char *)"user defined" },

     // Music/Ballet/Dance
     {  0x0600, 0x0600,  (u_char *)"music/ballet/dance (general)" },
     {  0x0601, 0x0601,  (u_char *)"rock/pop" },
     {  0x0602, 0x0602,  (u_char *)"serious music/classic music" },
     {  0x0603, 0x0603,  (u_char *)"folk/traditional music" },
     {  0x0604, 0x0604,  (u_char *)"jazz" },
     {  0x0605, 0x0605,  (u_char *)"musical/opera" },
     {  0x0606, 0x0606,  (u_char *)"ballet" },
     {  0x0607, 0x060E,  (u_char *)"reserved" },
     {  0x060F, 0x060F,  (u_char *)"user defined" },

     // Arts/Culture
     {  0x0700, 0x0700,  (u_char *)"arts/culture (without music, general)" },
     {  0x0701, 0x0701,  (u_char *)"performing arts" },
     {  0x0702, 0x0702,  (u_char *)"fine arts" },
     {  0x0703, 0x0703,  (u_char *)"religion" },
     {  0x0704, 0x0704,  (u_char *)"popular culture/traditional arts" },
     {  0x0705, 0x0705,  (u_char *)"literature" },
     {  0x0706, 0x0706,  (u_char *)"film/cinema" },
     {  0x0707, 0x0707,  (u_char *)"experimental film/video" },
     {  0x0708, 0x0708,  (u_char *)"broadcasting/press" },
     {  0x0709, 0x0709,  (u_char *)"new media" },
     {  0x070A, 0x070A,  (u_char *)"arts/culture magazine" },
     {  0x070B, 0x070B,  (u_char *)"fashion" },
     {  0x070C, 0x070E,  (u_char *)"reserved" },
     {  0x070F, 0x070F,  (u_char *)"user defined" },

     // Social/Political/Economics
     {  0x0800, 0x0800,  (u_char *)"social/political issues/economics (general)" },
     {  0x0801, 0x0801,  (u_char *)"magazines/reports/documentary" },
     {  0x0802, 0x0802,  (u_char *)"economics/social advisory" },
     {  0x0803, 0x0803,  (u_char *)"remarkable people" },
     {  0x0804, 0x080E,  (u_char *)"reserved" },
     {  0x080F, 0x080F,  (u_char *)"user defined" },

     // Education/Science/...
     {  0x0900, 0x0900,  (u_char *)"education/science/factual topics (general)" },
     {  0x0901, 0x0901,  (u_char *)"nature/animals/environment" },
     {  0x0902, 0x0902,  (u_char *)"technology/natural science" },
     {  0x0903, 0x0903,  (u_char *)"medicine/physiology/psychology" },
     {  0x0904, 0x0904,  (u_char *)"foreign countries/expeditions" },
     {  0x0905, 0x0905,  (u_char *)"social/spiritual science" },
     {  0x0906, 0x0906,  (u_char *)"further education" },
     {  0x0907, 0x0907,  (u_char *)"languages" },
     {  0x0908, 0x090E,  (u_char *)"reserved" },
     {  0x090F, 0x090F,  (u_char *)"user defined" },

     // Leisure hobies
     {  0x0A00, 0x0A00,  (u_char *)"leisure hobbies (general)" },
     {  0x0A01, 0x0A01,  (u_char *)"tourism/travel" },
     {  0x0A02, 0x0A02,  (u_char *)"handicraft" },
     {  0x0A03, 0x0A03,  (u_char *)"motoring" },
     {  0x0A04, 0x0A04,  (u_char *)"fitness & health" },
     {  0x0A05, 0x0A05,  (u_char *)"cooking" },
     {  0x0A06, 0x0A06,  (u_char *)"advertisement/shopping" },
     {  0x0A07, 0x0A07,  (u_char *)"gardening" },
     {  0x0A08, 0x0A0E,  (u_char *)"reserved" },
     {  0x0A0F, 0x0A0F,  (u_char *)"user defined" },

     {  0x0B00, 0x0B00,  (u_char *)"original language" },
     {  0x0B01, 0x0B01,  (u_char *)"black & white" },
     {  0x0B02, 0x0B02,  (u_char *)"unpublished" },
     {  0x0B03, 0x0B03,  (u_char *)"live broadcast" },
     {  0x0B04, 0x0B0E,  (u_char *)"reserved" },
     {  0x0B0F, 0x0B0F,  (u_char *)"user defined" },

     {  0x0C00, 0x0E0F,  (u_char *)"reserved" },
     {  0x0F00, 0x0F0F,  (u_char *)"user defined" },

     {  0,0, NULL }
  };

#endif
