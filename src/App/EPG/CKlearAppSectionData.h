#ifndef CKLEARAPPSECTIONDATA_H
#define CKLEARAPPSECTIONDATA_H

#include "CKlearAppDescriptorData.h"

/**
The data container for section data, including Descriptor data vector

@author Marco Kraus
*/
class CKlearAppSectionData
{

public:
    CKlearAppSectionData();
    ~CKlearAppSectionData();
    // section data
    u_int      table_id;
    u_int      section_syntax_indicator;
    u_int      reserved_1;
    u_int      reserved_2;
    u_int      section_length;
    u_int      service_id;
    u_int      reserved_3;
    u_int      version_number;
    u_int      current_next_indicator;
    u_int      section_number;
    u_int      last_section_number;
    u_int      transport_stream_id;
    u_int      original_network_id;
    u_int      segment_last_section_number;
    u_int      last_table_id;
    unsigned long crc;

    std::vector<CKlearAppDescriptorData *>  DescDataVector;  // descriptor data storage
};

#endif
