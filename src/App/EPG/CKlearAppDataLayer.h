#ifndef CKLEARAPPDATALAYER_H
#define CKLEARAPPDATALAYER_H

#include <iostream>
#include <vector>

#include <qdatetime.h>
#include <klocale.h>

#include "CKlearAppEITData.h"

#include "../Exceptions/CKlearAppException.h"       // klear exception class
#include "../Exceptions/CKlearAppFatalException.h"
#include "../Exceptions/CKlearAppErrorException.h"
#include "../Exceptions/CKlearAppFileException.h"

/**
Storage vector for all EIT Sector with all its descriptordata.
Storageclass has a methods for the common data requestes (current program,
program of current channel for today or tomorrow,...)

@author Marco Kraus <marco@klear.org>
*/
class CKlearAppDataLayer
{

public:
    CKlearAppDataLayer( std::vector<CKlearAppEITData *> *storage );
    ~CKlearAppDataLayer();

    /**
    \fn void readInContent()
    \author Marco Kraus <marco@klear.org>
    \brief fill container vector with all EIT Data from current channel
    \retval void no retval
    */
    void readInContent();

    /**
    \fn void cleanUpDatabase()
    \author Marco Kraus <marco@klear.org>
    \brief clean up dabatbase vector
    \retval void no retval
    */
    void cleanUpDatabase();

    /**
   \fn CKlearAppEITData* getCurrentRunning()
    \author Marco Kraus <marco@klear.org>
    \brief returns the EITData Item of the current programm on current channel
    \retval CKlearAppEITData A EITData ITEM from EITDataVector with the current program
    */
    CKlearAppEITData* getCurrentRunning();

    /**
   \fn CKlearAppEITData* getNextRunning()
    \author Marco Kraus <marco@klear.org>
    \brief returns the EITData Item of the next programm on current channel
    \retval CKlearAppEITData A EITData ITEM from EITDataVector with the next program
    */
    CKlearAppEITData* getNextRunning();

    /**
   \fn void addItem(CKlearAppEITData *item)
    \author Marco Kraus <marco@klear.org>
    \brief adds a new event to the event storage vector
    \retval void no retval
    */
    void addItem(CKlearAppEITData *item);

    /**
   \fn bool searchItem(long EID)
    \author Marco Kraus <marco@klear.org>
    \brief returns if Item with EID already exists
    \retval bool if exists or not
    */
    bool searchItem(long EID);

private:
    std::vector<CKlearAppEITData *> *EITDataVector;
};

#endif
