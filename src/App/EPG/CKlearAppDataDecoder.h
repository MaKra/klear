#ifndef CKLEARAPPDATADECODER_H
#define CKLEARAPPDATADECODER_H

#include <iostream>
#include <cstdio>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <linux/dvb/dmx.h>
#include <errno.h>

#include <qdatetime.h>

#include "../CKlearAppConfig.h"
#include "../../config.h"

#include "../CKlearAppDecodingHelper.h"

#include "CKlearAppDecodingTables.h"
#include "CKlearAppDescriptorParser.h"

#include "CKlearAppEITData.h" // general storage class
#include "CKlearAppSectionData.h"  // Section storage class
#include "CKlearAppDescriptorData.h"  // Descriptor storage class

#include "CKlearAppDataLayer.h" // VectorDatabase Layer

#include "../Exceptions/CKlearAppException.h"
#include "../Exceptions/CKlearAppFatalException.h"
#include "../Exceptions/CKlearAppErrorException.h"
#include "../Exceptions/CKlearAppFileException.h"


/**
Gets the read buffer and decodes the event table,
see ETSI EN-300 468

@author Marco Kraus <marco@klear.org> with many code from dvbsnoop, vlc and libdvbpsi
*/
class CKlearAppDataDecoder
{

public:
    CKlearAppDataDecoder(std::vector<CKlearAppEITData *> *storage, CKlearAppConfig *Config);
    ~CKlearAppDataDecoder();

    void decodeBuffer( u_char *buf, int len );

private:
    void debugPrintSectionDataItem(CKlearAppSectionData *SectionData);
    void setupEPGDatabase();
    CKlearAppConfig *KlearConfig;
    void decodeSection(u_char *b, int len);
    int  decodeDescriptor(u_char *b, DTAG_SCOPE scope, CKlearAppDescriptorData *DD);
    std::vector<CKlearAppEITData *> *EITDataVector;
};

#endif
