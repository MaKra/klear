#ifndef CKLEARAPPDESCRIPTORDATA_H
#define CKLEARAPPDESCRIPTORDATA_H

#include <vector>
#include <qstring.h>
#include <sys/types.h>

/**
Descriptor data of a section....includes the current programm data

@author Marco Kraus <marco@klear.org>
*/
class CKlearAppDescriptorData
{
public:
    CKlearAppDescriptorData();
    ~CKlearAppDescriptorData();

    u_int      event_id;
    u_long     start_time_MJD;  // 16
    u_long     start_time_UTC;  // 24
    u_long     duration;
    u_int      running_status;
    u_int      free_CA_mode;
    u_int      descriptors_loop_length;

    long dur_hour;
    long dur_min;
    long dur_sec;

    long start_year;
    long start_month;
    long start_day;

    long start_hour;
    long start_min;
    long start_sec;

    QString ShortEvent;
    QString ExtendedEvent;
    u_int   ParentalRating;
    u_int   Genre;
};

#endif
