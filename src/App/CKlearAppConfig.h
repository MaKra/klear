/**
\author Klear.org
\file CKlearAppConfig.h
\brief The configuration backend class
Stores local configuration variables, but also
saves and restores the written config files.
This is the backend for the KlearConfigDialog
*/

#ifndef CKLEARAPPCONFIG_H
#define CKLEARAPPCONFIG_H

#include <sys/types.h>
#include <iostream>

#include <qdir.h>
#include <qsize.h>
#include <qmessagebox.h>
#include <qfileinfo.h>
#include <qcheckbox.h>

#include "./Exceptions/CKlearAppException.h"
#include "./Exceptions/CKlearAppFatalException.h"
#include "./Exceptions/CKlearAppErrorException.h"
#include "./Exceptions/CKlearAppFileException.h"

#include "../config.h"

#include <klocale.h>        // i18n()
#include <kdebug.h>         // kdDebug(), kdFatal(), kdError()

/**
\class CKlearAppConfig
\author Klear.org
\brief The configuration backend class
Stores local configuration variables, but also
saves and restores the written config files.
This is the backend for the KlearConfigDialog
*/
class CKlearAppConfig
{

public:

     /**
     \fn CKlearAppConfig()
     \author Marco Kraus <marco@klear.org>
     \exception CKlearAppException
     \brief Constructor
     */
     CKlearAppConfig();

     /**
     \fn ~CKlearAppConfig()
     \author Marco Kraus <marco@klear.org>
     \brief Destructor
     */
     ~CKlearAppConfig();

     /**
     \fn void ReadConfig()
     \author Marco Kraus <marco@klear.org>
     \retval void no retval
     \exception CKlearAppException
     \brief Read the current configuration from file
     */
     void ReadConfig();

     /**
     \fn void WriteConfig()
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \exception CKlearAppException
     \brief Write configurationdata to disk
     */
     bool WriteConfig();

     /**
     \fn void StoreServiceID(QString ChannelName)
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \exception CKlearAppErrorException
     \brief Stores ServiceID for given ChannelName to configuration file
     */
     void StoreServiceID(QString ChannelName);

     /**
     \fn void PrintSettings()
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \brief Shows all configuration data in object. Debugging function.
     */
     void PrintSettings();

     /**
     \fn bool CheckConfigfileRevision();
     \author Marco Kraus <marco@klear.org>
     \retval bool true if current configfile version matches for current klear version
     \brief checks if the current saved configfile is usable for the currently used klear version
     */
     bool CheckConfigfileRevision();

     /**
     \fn void CreateNewConfigfile()
     \author Marco Kraus <marco@klear.org>
     \retval void no retval
     \exception CKlearAppException
     \brief creates a new plain Klear configfile
     */
      void CreateNewConfigfile();

     /**
     \fn void getKlearConfigPath() const
     \author Marco Kraus <marco@klear.org>
     \retval QString path to configfile
     \brief Get the path to the Klear configfile
     */
     QString getKlearConfigPath() const;

     /**
     \fn void getKlearSchedulerDatafile() const
     \author Marco Kraus <marco@klear.org>
     \retval QString the name of the schedulerdata file
     \brief gets the name of the schedulerdata file
     */
     QString getKlearSchedulerDatafile() const;

     /**
     \fn void getKlearChannelsConf() const
     \author Marco Kraus <marco@klear.org>
     \retval QString the name of the channels.conf file
     \brief Gets the name of the channels.conf file
     */
     QString getKlearChannelsConf() const;

     /**
     \fn void getDVBAdapter() const
     \author Marco Kraus <marco@klear.org>
     \retval QString Name of the DVBAdapter device
     \brief Gets the name of the DVB adapter device
     */
     QString getDVBAdapter() const;

     /**
     \fn void getDVBDvr() const
     \author Marco Kraus <marco@klear.org>
     \retval Qstring the name of the DVB dvr device
     \brief Gets the name of the DVB dvr device
     */
     QString getDVBDvr() const;

     /**
     \fn void getDVBDemux() const
     \author Marco Kraus <marco@klear.org>
     \retval QString the name of the DVB demux device
     \brief Gets the name of the DVB demux device
     */
     QString getDVBDemux() const;

     /**
     \fn void getDVBFrontend() const
     \author Marco Kraus <marco@klear.org>
     \retval QString the name of the DVB frontend device
     \brief Gets the name of the DVB frontend device
     */
     QString getDVBFrontend() const;

     /**
     \fn void getDVBMode() const
     \author Marco Kraus <marco@klear.org>
     \retval int The DVB Mode (satellite = 2, terrestric = 1 or cable = 3)
     \brief Gets the DVB Mode (satellite, terrestric or cable)
     */
     int getDVBMode() const;

     /**
     \fn void getPlaybackEngine() const
     \author Marco Kraus <marco@klear.org>
     \retval QString Playback engine name (xine, klear, mplayer, ...)
     \brief gets current Playback engine for stream
     */
     QString getPlaybackEngine() const;

     /**
     \fn void setDVBAdapter(QString setDVBAdapter)
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \param setDVBAdapter name of the new adapter device
     \brief sets the DVB Adapter to a new value
     */
     void setDVBAdapter( QString setDVBAdapter );

     /**
     \fn void setDVBDvr(QString setDVBDvr)
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \param setDVBDvr name of the new DVR device
     \brief sets the DVB dvr to a new value
     */
     void setDVBDvr( QString setDVBDvr );

     /**
     \fn void setDVBDemux(QString setDVBDemux)
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \param setDVBDemux name of the new demux device
     \brief sets the DVB demux to a new value
     */
     void setDVBDemux( QString setDVBDemux );

     /**
     \fn void setDVBFrontend(QString setDVBFrontend)
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \param setDVBFrontend name of the new frontend device
     \brief sets the DVB frontend to a new value
     */
     void setDVBFrontend( QString setDVBFrontend );

     /**
     \fn void setDVBMode(QString setDVBMode)
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \param setDVBMode name of the DVB mode
     \brief sets the DVB mode (satellite, cable, terrestric) to a new value
     */
     void setDVBMode( int setDVBMode );

     /**
     \fn void setPlaybackEngine(QString setPlaybackEngine)
     \author Marco Kraus <marco@klear.org>
     \retval void no retval
     \brief sets current Playback engine for stream
     */
     void setPlaybackEngine(QString setPlaybackEngine);

     /**
     \fn void getScreenshotDir() const
     \author Marco Kraus <marco@klear.org>
     \retval QString directory where screenshot are stored
     \brief get directory where screenshot are stored
     */
     QString getScreenshotDir() const;

     /**
     \fn void getScreenshotName() const
     \author Marco Kraus <marco@klear.org>
     \retval QString the name-prefix of the screenshots
     \brief get the name-prefix of the screenshots
     */
     QString getScreenshotName() const;

     /**
     \fn void getScreenshotFormat() const
     \author Marco Kraus <marco@klear.org>
     \retval QString the screenshot format (png or bmp)
     \brief gets the screenshot format (png or bmp)
     */
     QString getScreenshotFormat() const;

     /**
     \fn void setScreenshotDir(QString setScreenshotDir)
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \param setScreenshotDir set new path to screenshot directory
     \brief set new path to screenshot directory
     */
     void setScreenshotDir( QString setScreenshotDir );

     /**
     \fn void setScreenshotName(QString setScreenshotName)
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \param setScreenshotName new Value for directoryname
     \brief set a new value for the screenshot directory
     */
     void setScreenshotName( QString setScreenshotName );

     /**
     \fn void setScreenshotFormat(QString setScreenshotFormat)
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \param setScreenshotFormat sets format of screenshot to new value (png or bmp)
     \brief sets format of screenshot to new value (png or bmp)
     */
     void setScreenshotFormat( QString setScreenshotFormat );

     /**
     \fn void getRecordingDir() const
     \author Marco Kraus <marco@klear.org>
     \retval QString returns the current directory where all recordings are stored
     \brief returns the current directory where all recordings are stored
     */
     QString getRecordingDir() const;

     /**
     \fn void getRecordingName() const
     \author Marco Kraus <marco@klear.org>
     \retval QString current name-prefix of recordings
     \brief gets the current name-prefix of recordings
     */
     QString getRecordingName() const;

     /**
     \fn void getRecordingFormat() const
     \author Marco Kraus <marco@klear.org>
     \retval QString the current recording format (ts or PES)
     \brief gets the current recording format (TS or PES)
     */
     QString getRecordingFormat() const;

     /**
     \fn void setRecordingDir(QString setRecordingDir)
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \param setRecordingDir ne wvalue for recording directory
     \brief set new recording directory
     */
     void setRecordingDir( QString setRecordingDir );

     /**
     \fn void setRecordingName(QString setRecordingName)
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \param setRecordingName set new value for recording name-prefix
     \brief sets new recording name-prefix
     */
     void setRecordingName( QString setRecordingName );

     /**
     \fn void setRecordingFormat(QString setRecordingFormat)
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \param setRecordingFormat sets new value for recording format (TS or PES)
     \brief set new value for recording format (TS or PES)
     */
     void setRecordingFormat( QString setRecordingFormat );

     /**
     \fn int getApid() const
     \author Marco Kraus <marco@klear.org>
     \retval int current AudioPID
     \brief gets current AudioPID of tuned channel
     */
     int getApid() const;

     /**
     \fn int getVpid() const
     \author Marco Kraus <marco@klear.org>
     \retval int current VideoPID of tuned channel
     \brief gets current VideoPID of tuned channel
     */
     int getVpid() const;

     /**
     \fn void setApid(int setApid)
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \param setApid new AudioPID
     \brief set the AudioPID to a new value
     */
     void setApid( int setApid );

     /**
     \fn void setVpid(int setVpid)
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \param setVpid new VideoPID
     \brief set the VideoPID to a new value
     */
     void setVpid( int setVpid );

     /**
     \fn QString getCurrentChannel() const
     \author Marco Kraus <marco@klear.org>
     \retval QString current channelname
     \brief gets the current channelname
     */
     QString getCurrentChannel() const;

     /**
     \fn void setCurrentChannel(QString setCurrentChannel)
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \param setCurrentChannel set the channelname to new value
     \brief sets a new channelname
     */
     void setCurrentChannel( QString setCurrentChannel );

     /**
     \fn u_int getServiceID() const
     \author Marco Kraus <marco@klear.org>
     \retval QString current ServiceID
     \brief gets the current ServiceID for current channel
     */
     u_int getServiceID() const;

     /**
     \fn int getCurrentVolume() const
     \author Marco Kraus <marco@klear.org>
     \retval int current volume
     \brief return the current volume
     */
     int getCurrentVolume() const;

     /**
     \fn void setCurrentVolume(int setCurrentVolume)
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \param setCurrentVolume new volume
     \brief set a new volume
     */
     void setCurrentVolume( int setCurrentVolume );

     /**
     \fn bool getMuteSystray() const;
     \author Marco Kraus <marco@klear.org>
     \retval bool if muting in systray is wanted
     \brief tells if user wants klear to mute when docked in systray
     */
     bool getMuteSystray() const;

     /**
     \fn void setMuteSystray( bool isMuteSystray )
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \param isMuteSystray if systray-mute is enabled
     \brief sets systray-muting
     */
     void setMuteSystray( bool isMuteSystray );

        /**
     \fn QString getTimeShiftName() const
     \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
     \retval QString current TimeShiftName
     \brief gets the current TimeShiftrecord filename
     */
     QString getTimeShiftName() const;

     /**
     \fn void setTimeShiftName(QString TimeShiftName)
     \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
     \retval void no return value
     \param TimeShiftName set the TimeShiftName to new value
     \brief sets the current TimeShiftrecord filename
     */
     void setTimeShiftName( QString TimeShiftName );

        /**
     \fn void setWindowSize(QSize size)
     \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
     \retval void no return value
     \param size the new window size value
     \brief sets the current window size
     */
     void setWindowSize(QSize size);

     /**
     \fn QSize getWindowSize()
     \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
     \retval QSize current window size
     \brief returns the current window size
     */
     QSize getWindowSize();

     QString getAccDecChannel();
     QString getAccIncChannel();
     QString getAccChannellist();
     QString getAccFullscreen();
     QString getAccDeinterlace();
     QString getAccOsd();
     QString getAccEpg();
     QString getAccTxt();
     QString getAccQuit();
     QString getAccConfig();
     QString getAccAbout();
     QString getAccMute();
     QString getAccRecording();
     QString getAccShoot();
     QString getAccTimeshift();

     void setAccDecChannel( QString key );
     void setAccIncChannel( QString key );
     void setAccDeinterlace( QString key );
     void setAccFullscreen( QString key );
     void setAccChannellist( QString key );
     void setAccOsd( QString key );
     void setAccEpg( QString key );
     void setAccTxt( QString key );
     void setAccQuit( QString key );
     void setAccConfig( QString key );
     void setAccAbout( QString key );
     void setAccMute( QString key );
     void setAccRecording( QString key );
     void setAccShoot( QString key );
     void setAccTimeshift( QString key );

       /**
     \fn void setMenuAlignment( QString alignment )
     \author Manuel Habermann <manuel@klear.org>
     \retval void no return value
     \param alignment the new alignment
     \brief sets the menu alignment to either left or right
     */
     void setMenuAlignment( int alignment );

      /**
     \fn QString getMenuAlignment() const
     \author Manuel Habermann <manuel@klear.org>
     \retval int the current menu alignment, 0 = RIGHT, 1 = LEFT
     \brief returns the current menu alignment
     */
     int getMenuAlignment() const;

      /**
     \fn bool getAutoShowOsd() const
     \author Manuel Habermann <manuel@klear.org>
     \retval bool true or false depending on weater showing  osd on channelswitching is on or off
     \brief returns true or false depending on weater showing  osd on channelswitching is on or off
     */
     bool getAutoShowOsd() const;

      /**
     \fn void setAutoShowOsd( bool isOsdAutoShown)
     \author Manuel Habermann <manuel@klear.org>
     \retval void no return value
     \param isOsdAutoShown true or false enables or disables showing  osd on channelswitching
     \brief  sets showing of osd on channelswitching to ether on or off
     */
     void setAutoShowOsd( bool isOsdAutoShown );

      /**
     \fn bool getIsDeinterlaced() const;
     \author Marco Kraus <marco@klear.org>
     \retval bool true or false depending on weater deinterlacing is enabled
     \brief returns true or false depending on weater deinterlacing is enabled or not
     */
     bool getIsDeinterlaced() const;

      /**
     \fn void setIsDeinterlaced( bool isDeinterlaced )
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \param isDeintercaed true or false enables or disables interlacing
     \brief  sets flag if deinterlaced or not in configfile
     */
     void setIsDeinterlaced( bool isDeint );

      /**
     \fn bool getIsMinimized() const
     \author Marco Kraus <marco@klear.org>
     \retval bool true or false depending on weater menu is minimized
     \brief returns true or false depending on weater menu is minimized or not
     */
     bool getIsMinimized() const;

      /**
     \fn void setIsMinimized( bool isMinimized )
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \param isMinimized true or false enables or disables interlacing
     \brief  sets flag if menu is minimized or not in configfile
     */
     void setIsMinimized( bool isMini );

      /**
     \fn bool getDisableScreensaver() const
     \author Marco Kraus <marco@klear.org>
     \retval int true or false depending on weater screensaver should be disabled
     \brief returns true or false depending on weater screensaver should be disabled
     */
     int getDisableScreensaver() const;

      /**
     \fn void setDisableScreensaver( bool disableScreen )
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \param disableScreen timeout in seconds, that is sent to prevent screensaver startup
     \brief  timeout in seconds to prevent screensaver from starting up
     */
     void setDisableScreensaver( int disableScreen );

      /**
     \fn int getPreEPGmargin()
     \author Marco Kraus <marco@klear.org>
     \retval int return pre EPG margin
     \brief return the pre margin value to set for EPG recording
     */
     int getPreEPGmargin() const;

      /**
     \fn void setPreEPGmargin(int pre)
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \param pre pre EPG margin in minutes
     \brief  sets the pre recording marge for EPG recording in minutes
     */
     void setPreEPGmargin(int pre);

      /**
     \fn int getPostEPGmargin() const
     \author Marco Kraus <marco@klear.org>
     \retval int return post EPG margin
     \brief return the post margin value to set for EPG recording
     */
     int getPostEPGmargin() const;
     void setPostEPGmargin(int post);

private:
    // const data. not changable by user !
    QString KlearConfigFile;  // holds the klear configfilename
    QString KlearChannelsConf;  // path to the channels configuration file

    QString KlearConfigPath;  // holds the full path to klearconfigfile
    QString KlearBuffer;   // fifo buffer

    QString KlearSchedulerDatafile;  // file to save scheduler data

    QString ConfigVersion;    // config revision marker

    // settings, changable by user
    QString DVBAdapter;
    QString DVBDvr;
    QString DVBDemux;
    QString DVBFrontend;
    int DVBMode;

    QString PlaybackEngine;

    QString ScreenshotDir;
    QString ScreenshotName;
    QString ScreenshotFormat;

    QString RecordingDir;
    QString RecordingName;
    QString RecordingFormat;

    QString TimeShiftName;
    int WindowWidth;
    int WindowHeight;

    QString AccDecChannel;
    QString AccIncChannel;
    QString AccDeinterlace;
    QString AccFullscreen;
    QString AccChannellist;
    QString AccOsd;
    QString AccEpg;
    QString AccTxt;
    QString AccQuit;
    QString AccConfig;
    QString AccAbout;
    QString AccMute;
    QString AccRecording;
    QString AccShoot;
    QString AccTimeshift;

    int menuAlignment;
    bool autoShowOsd;
    int preEPGmargin;
    int postEPGmargin;

    // settings automatically saved on channel change
    QString CurrentChannel;
    int CurrentServiceID;

    // settings automatically saved on exit
    int CurrentVolume;

    bool IsDeinterlaced;
    bool IsMinimized;

    bool IsMute;
    bool muteSystray;

    u_int disableScreensaver;  // send fake events every XY seconds

    int apid;
    int vpid;

    bool CheckOldConfig(); // exist old settings for read ?
    void SetDefaults();  // set local member vars with default values
    void ParseConfigLine(QString ConfigLine); // parse a line for the configure data
};

#endif
