/**
\author Klear.org
\file CKlearAppTray.h
\brief Class for Customizing Klear Trayicon funtions
*/

#ifndef CKLEARAPPTRAY_H
#define CKLEARAPPTRAY_H

#include <qwidget.h>
#include <ksystemtray.h>

/**
\class CKlearAppTray
\author Klear.org
\brief Class for Customizing Klear Trayicon funtions
*/
class CKlearAppTray : public KSystemTray
{

public:
    /**
   \fn void CKlearAppTray( QWidget* parent = 0, const char* name = 0 );
   \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
   \param   parent pointer to parent QWidget Object.
   \param   name Name of the Tray Icon
   \retval void no return value
   \brief  Constructor for CKlearAppTray Object
   */
    CKlearAppTray( QWidget* parent = 0, const char* name = 0 );


     /**
    \fn void ~CKlearAppTray();
    \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
    \retval void no return value
    \brief  Destructor
     */
    ~CKlearAppTray();


public slots:

    /**
     \fn slotQuitTray();
     \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
     \retval void no return value
     \brief Quits and closes Tray object
     */
    void slotQuitTray();

};

#endif
