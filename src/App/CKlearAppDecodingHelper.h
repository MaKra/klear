/*
 DVBSNOOP - Helper methods
 http://dvbsnoop.sourceforge.net/
 (c) 2001-2004   Rainer.Scherg@gmx.de (rasc)

 Modified for Klear by Marco Kraus <marco@klear.org> and Omar El-Dakhloul <omar@klear.org>
*/

#ifndef __HELPER_H
#define __HELPER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <linux/dvb/dmx.h>
#include <errno.h>
#include <qstring.h>
#include <stdarg.h>

#define MAX_INDENT_LEVEL  ( ( sizeof( table_indent ) / sizeof ( int ) ) - 1 )
#define MAX_PID      0x1FFF
#define out_SB_NL(v,str,hex)   out_nl((v),"%s%u (0x%02x)",(str),(hex),(hex))

// CRC32 lookup table for polynomial 0x04c11db7
static UINT32 crc32(char *data, int len);
static unsigned long getBits (u_char *buf, int byte_offset, int startbit, int bitlen);
static long long getBits48 (u_char *buf, int byte_offset, int startbit, int bitlen);
static unsigned long long getBits64 (u_char *buf, int byte_offset, int startbit, int bitlen);
static u_char* getISO639_3 (u_char *str, u_char *buf);
static QString print_text2_468A(u_char *b, u_int len);
static void print_std_ascii (const char *s, u_char *b, u_int len);
static void print_databytes (const char *str, u_char *b, u_int len);
static void print_private_data (int v, u_char *b, u_int len);
static void print_databytes_line (int v, const char *str, u_char *b, u_int len);
static long str2i  (char *s);
static int str2barray  (char *s, u_char *barray, int max_len);
static char *str_cell_latitude (long latitude);
static char *str_cell_longitude (long longitude);
static char *_str_cell_latitude_longitude (long ll, int angle);
static char *str_bit32 (u_long value, int bits);
static int lenCheckErrOut (int v, int len);
//----------------------------------------
static int table_indent[] = { 0, 4, 8, 12, 15, 18, 21, 24, 27, 30 };
static int verbose_level = 0;
static int col0 = 0;
static int indent_level = 0;
static void out_nl ( int verbose, const char *msgfmt, ... );
static void print_indent ( void );
static void out_nl2 ( int verbose );
static void out(const char *msgfmt,...);

static int descriptor ( u_char *b );
static int getVerboseLevel ();

static int  store_PidToMem (int pid);
enum PIDMEM_STATUS { PIDMEM_UNUSED, PIDMEM_STORED, PIDMEM_ISUSED };
static int  pidMEM[MAX_PID+1];

static u_char unhamB84 (u_char c);
static u_char unhamW84  (u_char lsb, u_char msb);
static u_long unhamT24_18 (u_char lsb, u_char msb1, u_char msb2);

static unsigned long getBits (u_char *buf, int byte_offset, int startbit, int bitlen)
{
 u_char *b;
 unsigned long  v;
 unsigned long mask;
 unsigned long tmp_long;
 int           bitHigh;

 b = &buf[byte_offset + (startbit >> 3)];
 startbit %= 8;

 switch ((bitlen-1) >> 3) {
     case -1:   // -- <=0 bits: always 0
        return 0L;
        break;

    case 0:     // -- 1..8 bit
        tmp_long = (unsigned long)(
            (*(b  )<< 8) +  *(b+1) );
        bitHigh = 16;
        break;

    case 1:     // -- 9..16 bit
        tmp_long = (unsigned long)(
            (*(b  )<<16) + (*(b+1)<< 8) +  *(b+2) );
        bitHigh = 24;
        break;

    case 2:     // -- 17..24 bit
        tmp_long = (unsigned long)(
            (*(b  )<<24) + (*(b+1)<<16) +
            (*(b+2)<< 8) +  *(b+3) );
        bitHigh = 32;
        break;

    case 3:     // -- 25..32 bit
            // -- to be safe, we need 32+8 bit as shift range
        return (unsigned long) getBits48 (b, 0, startbit, bitlen);
        break;

    default:    // -- 33.. bits: fail, deliver constant fail value
        printf(" Error: getBits() request out of bound!!!! (report!!) \n");
        return (unsigned long) 0xFEFEFEFE;
        break;
 }

 startbit = bitHigh - startbit - bitlen;
 tmp_long = tmp_long >> startbit;
 mask     = (1ULL << bitlen) - 1;  // 1ULL !!!
 v        = tmp_long & mask;

 return v;
}


/*
  -- get bits out of buffer  (max 48 bit)
  -- extended bitrange, so it's slower
  -- return: value
 */
static long long getBits48 (u_char *buf, int byte_offset, int startbit, int bitlen)
{
 u_char *b;
 unsigned long long v;
 unsigned long long mask;
 unsigned long long tmp;

 if (bitlen > 48) {
    printf(" Error: getBits48() request out of bound!!!! (report!!) \n");
    return 0xFEFEFEFEFEFEFEFELL;
 }


 b = &buf[byte_offset + (startbit / 8)];
 startbit %= 8;


 // -- safe is 48 bitlen
 tmp = (unsigned long long)(
     ((unsigned long long)*(b  )<<48) + ((unsigned long long)*(b+1)<<40) +
     ((unsigned long long)*(b+2)<<32) + ((unsigned long long)*(b+3)<<24) +
     (*(b+4)<<16) + (*(b+5)<< 8) + *(b+6) );

 startbit = 56 - startbit - bitlen;
 tmp      = tmp >> startbit;
 mask     = (1ULL << bitlen) - 1;   // 1ULL !!!
 v        = tmp & mask;

 return v;
}



/*
  -- get bits out of buffer   (max 64 bit)
  -- extended bitrange, so it's slower
  -- return: value
 */

static unsigned long long getBits64 (u_char *buf, int byte_offset, int startbit, int bitlen)
{
  unsigned long long x1,x2,x3;

  if (bitlen <= 32) {
     x3 = getBits (buf,byte_offset,startbit,bitlen);
  } else {
     x1 = getBits (buf,byte_offset,startbit,32);
     x2 = getBits (buf,byte_offset,startbit+32,bitlen-32);
     x3 = (x1<<(bitlen-32)) + x2;
  }
  return x3;
}


/*
  -- get ISO 639  (3char) language code into string[4]
  -- terminate string with \0
  -- return ptr to buf;
 */
static u_char* getISO639_3 (u_char *str, u_char *buf)
{
  int i;

  strncpy ((char *)str, (char *)buf, 3);
  *(str+3) = '\0';

  // secure print of string
  for (i=0; i<3; i++) {
     if (!isprint(*(str+i))) {
         *(str+i) = '.';
     }
  }

  return str;
}



/*
  -- print_text_468A
  -- ETSI EN 300 468  Annex A
  -- evaluate string and look on DVB control codes
  -- returns the string
*/
static QString print_text2_468A (u_char *b, u_int len)
{
  int    in_emphasis = 0;
  u_char c;
  u_char em_ON  = 0x86;
  u_char em_OFF = 0x87;
  QString ret ="";

  for (uint i=0; i<len; i++) {
    c = b[i];

    if (i == 0 && c < 0x20) continue;   // opt. charset descript.

    if (c == em_ON) {
       in_emphasis = 1;
       ret += "<EM>";
       continue;
    }
    if (c == em_OFF) {
       in_emphasis = 0;
       ret += "</EM>";
       continue;
    }

       if (c == 0x8A)     ret += "<BR>";
       else if (c < 0x20) ret += ".";
       else               ret += c;

  } // for

return ret;
}


/*
  -- print standard ascii text
*/
static void print_std_ascii (const char *s, u_char *b, u_int len)
{
  u_char c;

  printf("%s\"",s);
  for (uint i=0; i<len; i++) {
    c = b[i];
    if (!isprint (c)) c = '.';
    printf("%c", c);
  }
  printf("\"");

}


/*
  -- print data bytes (str + hexdump)
  -- print  "Private Data" and Hex-Dump

*/
static void print_databytes (const char *str, u_char *b, u_int len)
{
  if (len > 0) {
    printf("%s",str);
    //printhex_buf (b,len);
  }
}

static void print_databytes_line  (const char *str, u_char *b, u_int len)
{
  if (len > 0) {
     printf("%s ",str);
//     printhexline_buf (b,len);
  }
}


/*
   -- str2i
   -- string to integer
   --   x, 0x ist Hex
   --   ansonsten Dezimal
   return:  long int

*/
static long str2i  (const char *s)
{
 long v;

 if (!s) s = "";

 v = strtol (s, NULL, 0);
 return v;

}


/*
   -- str2barray
   -- string to integer (byte array)
   --   x, 0x ist Hex, ansonsten Dezimal oder octal
   --- Input:  1 byte:    0xF0
   ---         multibyte: 0xFE.12.43.4F.6F (etc) (hex)
                          123.255.24 (dec)
   -- return:  <0 = error, 0 = no filter, >0 = count filter bytes
*/
static int str2barray  (const char *s, u_char *barray, int max_len)
{
 int  i = 0;
 long v;
 int  base = 10;
 char *endptr = NULL;


 if (!s) s = "";

 // -- get base
 if (*s == '0') {
    base = 8;  // octal
    if (*s && *(s+1) == 'x') base = 16; // hex
 }

 while (1)  {
     if ( i >= max_len) break;

     v = strtol (s, &endptr, base);
     if ( v < 0 || v > 0xFF) return -1;
     if (s == endptr) return -1; // illegal char...
     barray[i++] = v;
     if (! *endptr) break;   // end of string
     s = endptr + 1;
 }

 return i;
}


/*
 -- latitude coordinates   (Cell Descriptors)
 -- longitude coordinates   (Cell Descriptors)
 -- ETSI EN 300 468
*/

static char *str_cell_latitude (long latitude)
{
 // cell_latitude: This 16-bit field, coded as a two's complement number,
 // shall specify the latitude of the corner of a spherical rectangle that
 // approximately describes the coverage area of the cell indicated. It shall
 // be calculated by multiplying the value of the latitude field by
 // (90 � /2^15 ). Southern latitudes shall be considered negative and
 // northern latitudes positive.

 return _str_cell_latitude_longitude (latitude, 90);
}


static char *str_cell_longitude (long longitude)
{
 // cell_longitude: This 16-bit field, coded as a two's complement number, shall
 // specify the longitude of the corner of a spherical rectangle that approximately
 // describes the coverage area of the cell indicated. It shall be calculated by
 // multiplying the value of the longitude field by (180 � /2^15 ). Western
 // longitudes shall be considered negative and eastern longitudes positive.

 return _str_cell_latitude_longitude (longitude, 180);
}


static char *_str_cell_latitude_longitude (long ll, int angle)
{
 long long  x;
 long       g1,g2;
 static     char s[40]; // $$$ not thread safe!

 x = (long long) ll * angle * 1000;
 x = x / (1<<15);

 g1 = x / 1000;
 g2 = x % 1000;
 if (g2 <0) g2 = 0 - g2;

 sprintf(s,"%ld.%03ld degree",g1,g2);
 return s;
}



/*
 * --  return a bit string for value, len bits
 * --  NOT thread-safe !!!  $$$
 */

static char *str_bit32 (u_long value, int bits)
{
   static char bitstr[65];
   char *s = bitstr;

   if (bits > 64) bits = 64;
   s += bits;

   // reverse bit shift to get real order

   *s = '\0';
   while (bits-- > 0) {
       *(--s) = (value & 0x01) ? '1' :'0';
       value = value >> 1;
   }

   return bitstr;
}


//
//
// -- Length Check Helper
// -- return 1: ok, 0 = len<0
//
static int lenCheckErrOut (int len)
{
  const char *err_str="==> Something is seriously wrong (length overrun check)!!!";

  if (len < 0) {
      printf("%s",err_str);
      return 0;
  }
  return 1;

}


// CRC32 lookup table for polynomial 0x04c11db7

static UINT32 crc_table[256] = {
    0x00000000, 0x04c11db7, 0x09823b6e, 0x0d4326d9, 0x130476dc, 0x17c56b6b,
    0x1a864db2, 0x1e475005, 0x2608edb8, 0x22c9f00f, 0x2f8ad6d6, 0x2b4bcb61,
    0x350c9b64, 0x31cd86d3, 0x3c8ea00a, 0x384fbdbd, 0x4c11db70, 0x48d0c6c7,
    0x4593e01e, 0x4152fda9, 0x5f15adac, 0x5bd4b01b, 0x569796c2, 0x52568b75,
    0x6a1936c8, 0x6ed82b7f, 0x639b0da6, 0x675a1011, 0x791d4014, 0x7ddc5da3,
    0x709f7b7a, 0x745e66cd, 0x9823b6e0, 0x9ce2ab57, 0x91a18d8e, 0x95609039,
    0x8b27c03c, 0x8fe6dd8b, 0x82a5fb52, 0x8664e6e5, 0xbe2b5b58, 0xbaea46ef,
    0xb7a96036, 0xb3687d81, 0xad2f2d84, 0xa9ee3033, 0xa4ad16ea, 0xa06c0b5d,
    0xd4326d90, 0xd0f37027, 0xddb056fe, 0xd9714b49, 0xc7361b4c, 0xc3f706fb,
    0xceb42022, 0xca753d95, 0xf23a8028, 0xf6fb9d9f, 0xfbb8bb46, 0xff79a6f1,
    0xe13ef6f4, 0xe5ffeb43, 0xe8bccd9a, 0xec7dd02d, 0x34867077, 0x30476dc0,
    0x3d044b19, 0x39c556ae, 0x278206ab, 0x23431b1c, 0x2e003dc5, 0x2ac12072,
    0x128e9dcf, 0x164f8078, 0x1b0ca6a1, 0x1fcdbb16, 0x018aeb13, 0x054bf6a4,
    0x0808d07d, 0x0cc9cdca, 0x7897ab07, 0x7c56b6b0, 0x71159069, 0x75d48dde,
    0x6b93dddb, 0x6f52c06c, 0x6211e6b5, 0x66d0fb02, 0x5e9f46bf, 0x5a5e5b08,
    0x571d7dd1, 0x53dc6066, 0x4d9b3063, 0x495a2dd4, 0x44190b0d, 0x40d816ba,
    0xaca5c697, 0xa864db20, 0xa527fdf9, 0xa1e6e04e, 0xbfa1b04b, 0xbb60adfc,
    0xb6238b25, 0xb2e29692, 0x8aad2b2f, 0x8e6c3698, 0x832f1041, 0x87ee0df6,
    0x99a95df3, 0x9d684044, 0x902b669d, 0x94ea7b2a, 0xe0b41de7, 0xe4750050,
    0xe9362689, 0xedf73b3e, 0xf3b06b3b, 0xf771768c, 0xfa325055, 0xfef34de2,
    0xc6bcf05f, 0xc27dede8, 0xcf3ecb31, 0xcbffd686, 0xd5b88683, 0xd1799b34,
    0xdc3abded, 0xd8fba05a, 0x690ce0ee, 0x6dcdfd59, 0x608edb80, 0x644fc637,
    0x7a089632, 0x7ec98b85, 0x738aad5c, 0x774bb0eb, 0x4f040d56, 0x4bc510e1,
    0x46863638, 0x42472b8f, 0x5c007b8a, 0x58c1663d, 0x558240e4, 0x51435d53,
    0x251d3b9e, 0x21dc2629, 0x2c9f00f0, 0x285e1d47, 0x36194d42, 0x32d850f5,
    0x3f9b762c, 0x3b5a6b9b, 0x0315d626, 0x07d4cb91, 0x0a97ed48, 0x0e56f0ff,
    0x1011a0fa, 0x14d0bd4d, 0x19939b94, 0x1d528623, 0xf12f560e, 0xf5ee4bb9,
    0xf8ad6d60, 0xfc6c70d7, 0xe22b20d2, 0xe6ea3d65, 0xeba91bbc, 0xef68060b,
    0xd727bbb6, 0xd3e6a601, 0xdea580d8, 0xda649d6f, 0xc423cd6a, 0xc0e2d0dd,
    0xcda1f604, 0xc960ebb3, 0xbd3e8d7e, 0xb9ff90c9, 0xb4bcb610, 0xb07daba7,
    0xae3afba2, 0xaafbe615, 0xa7b8c0cc, 0xa379dd7b, 0x9b3660c6, 0x9ff77d71,
    0x92b45ba8, 0x9675461f, 0x8832161a, 0x8cf30bad, 0x81b02d74, 0x857130c3,
    0x5d8a9099, 0x594b8d2e, 0x5408abf7, 0x50c9b640, 0x4e8ee645, 0x4a4ffbf2,
    0x470cdd2b, 0x43cdc09c, 0x7b827d21, 0x7f436096, 0x7200464f, 0x76c15bf8,
    0x68860bfd, 0x6c47164a, 0x61043093, 0x65c52d24, 0x119b4be9, 0x155a565e,
    0x18197087, 0x1cd86d30, 0x029f3d35, 0x065e2082, 0x0b1d065b, 0x0fdc1bec,
    0x3793a651, 0x3352bbe6, 0x3e119d3f, 0x3ad08088, 0x2497d08d, 0x2056cd3a,
    0x2d15ebe3, 0x29d4f654, 0xc5a92679, 0xc1683bce, 0xcc2b1d17, 0xc8ea00a0,
    0xd6ad50a5, 0xd26c4d12, 0xdf2f6bcb, 0xdbee767c, 0xe3a1cbc1, 0xe760d676,
    0xea23f0af, 0xeee2ed18, 0xf0a5bd1d, 0xf464a0aa, 0xf9278673, 0xfde69bc4,
    0x89b8fd09, 0x8d79e0be, 0x803ac667, 0x84fbdbd0, 0x9abc8bd5, 0x9e7d9662,
    0x933eb0bb, 0x97ffad0c, 0xafb010b1, 0xab710d06, 0xa6322bdf, 0xa2f33668,
    0xbcb4666d, 0xb8757bda, 0xb5365d03, 0xb1f740b4
};


static UINT32 crc32(char *data, int len)
{
    register int i;
    u_long crc = 0xffffffff;

    for (i=0; i<len; i++)
        crc = (crc << 8) ^ crc_table[((crc >> 24) ^ *data++) & 0xff];

    return crc;
}

//-------------------------------------------

static void out_nl ( int verbose, const char *msgfmt, ... )
{
  va_list args;

  if ( verbose <= verbose_level )
  {
     print_indent();  //2
     va_start( args, msgfmt );
     vfprintf( stdout, msgfmt, args );
     va_end( args );
     out_nl2( verbose );
  }
}

static void print_indent ( void )
{
  int i;

  if ( ! col0 )
    return; //2

  for ( i = 0; i < table_indent[ indent_level ]; i++ )
    fputc ( ' ', stdout );

  col0 = 0; //2
}

static void  out_nl2 ( int verbose )
{
  if ( verbose <= verbose_level )
  {
    fputc ( '\n',stdout );
    col0 = 1; //2
  }
}

static int descriptor ( u_char *b )
{
  int id;
  int len;

  id  =  ( int ) b[ 0 ];
  len = ( ( int ) b[ 1 ] ) + 2;

  if ( getVerboseLevel() < 4 )
    return len;

  return len;
}

static int getVerboseLevel ()
{
  return verbose_level;
}
static u_char unham84tab[256] = {
  0x01, 0xff, 0x01, 0x01, 0xff, 0x00, 0x01, 0xff,
  0xff, 0x02, 0x01, 0xff, 0x0a, 0xff, 0xff, 0x07,
  0xff, 0x00, 0x01, 0xff, 0x00, 0x80, 0xff, 0x00,
  0x06, 0xff, 0xff, 0x0b, 0xff, 0x00, 0x03, 0xff,
  0xff, 0x0c, 0x01, 0xff, 0x04, 0xff, 0xff, 0x07,
  0x06, 0xff, 0xff, 0x07, 0xff, 0x07, 0x07, 0x87,
  0x06, 0xff, 0xff, 0x05, 0xff, 0x00, 0x0d, 0xff,
  0x86, 0x06, 0x06, 0xff, 0x06, 0xff, 0xff, 0x07,
  0xff, 0x02, 0x01, 0xff, 0x04, 0xff, 0xff, 0x09,
  0x02, 0x82, 0xff, 0x02, 0xff, 0x02, 0x03, 0xff,
  0x08, 0xff, 0xff, 0x05, 0xff, 0x00, 0x03, 0xff,
  0xff, 0x02, 0x03, 0xff, 0x03, 0xff, 0x83, 0x03,
  0x04, 0xff, 0xff, 0x05, 0x84, 0x04, 0x04, 0xff,
  0xff, 0x02, 0x0f, 0xff, 0x04, 0xff, 0xff, 0x07,
  0xff, 0x05, 0x05, 0x85, 0x04, 0xff, 0xff, 0x05,
  0x06, 0xff, 0xff, 0x05, 0xff, 0x0e, 0x03, 0xff,
  0xff, 0x0c, 0x01, 0xff, 0x0a, 0xff, 0xff, 0x09,
  0x0a, 0xff, 0xff, 0x0b, 0x8a, 0x0a, 0x0a, 0xff,
  0x08, 0xff, 0xff, 0x0b, 0xff, 0x00, 0x0d, 0xff,
  0xff, 0x0b, 0x0b, 0x8b, 0x0a, 0xff, 0xff, 0x0b,
  0x0c, 0x8c, 0xff, 0x0c, 0xff, 0x0c, 0x0d, 0xff,
  0xff, 0x0c, 0x0f, 0xff, 0x0a, 0xff, 0xff, 0x07,
  0xff, 0x0c, 0x0d, 0xff, 0x0d, 0xff, 0x8d, 0x0d,
  0x06, 0xff, 0xff, 0x0b, 0xff, 0x0e, 0x0d, 0xff,
  0x08, 0xff, 0xff, 0x09, 0xff, 0x09, 0x09, 0x89,
  0xff, 0x02, 0x0f, 0xff, 0x0a, 0xff, 0xff, 0x09,
  0x88, 0x08, 0x08, 0xff, 0x08, 0xff, 0xff, 0x09,
  0x08, 0xff, 0xff, 0x0b, 0xff, 0x0e, 0x03, 0xff,
  0xff, 0x0c, 0x0f, 0xff, 0x04, 0xff, 0xff, 0x09,
  0x0f, 0xff, 0x8f, 0x0f, 0xff, 0x0e, 0x0f, 0xff,
  0x08, 0xff, 0xff, 0x05, 0xff, 0x0e, 0x0d, 0xff,
  0xff, 0x0e, 0x0f, 0xff, 0x0e, 0x8e, 0xff, 0x0e,
};

static u_long unhamT24_18 (u_char lsb, u_char msb1, u_char msb2)
{
   u_long  v;
   v = 0;
   v |= (lsb & 0x04) >> 2;  	// D1
   v |= (lsb & 0x70) >> 3;  	// D2..D4
   v |= ((msb1 & 0x7F ) << 8);	// D5..D11
   v |= ((msb2 & 0x7F ) << 16);	// D12..D18
   return v;
}

static u_char unhamW84 (u_char lsb, u_char msb)
{
  u_char c1,c2;

  c1=unham84tab[lsb];
  c2=unham84tab[msb];
  //  if ((c1 | c2) & 0x40)  bad ham!
  return (c2 << 4) | (c1 & 0x0f);
}

static u_char unhamB84 (u_char c)
{
  return unham84tab[c];
}

static u_char invtab[256] = {
  0x00, 0x80, 0x40, 0xc0, 0x20, 0xa0, 0x60, 0xe0,
  0x10, 0x90, 0x50, 0xd0, 0x30, 0xb0, 0x70, 0xf0,
  0x08, 0x88, 0x48, 0xc8, 0x28, 0xa8, 0x68, 0xe8,
  0x18, 0x98, 0x58, 0xd8, 0x38, 0xb8, 0x78, 0xf8,
  0x04, 0x84, 0x44, 0xc4, 0x24, 0xa4, 0x64, 0xe4,
  0x14, 0x94, 0x54, 0xd4, 0x34, 0xb4, 0x74, 0xf4,
  0x0c, 0x8c, 0x4c, 0xcc, 0x2c, 0xac, 0x6c, 0xec,
  0x1c, 0x9c, 0x5c, 0xdc, 0x3c, 0xbc, 0x7c, 0xfc,
  0x02, 0x82, 0x42, 0xc2, 0x22, 0xa2, 0x62, 0xe2,
  0x12, 0x92, 0x52, 0xd2, 0x32, 0xb2, 0x72, 0xf2,
  0x0a, 0x8a, 0x4a, 0xca, 0x2a, 0xaa, 0x6a, 0xea,
  0x1a, 0x9a, 0x5a, 0xda, 0x3a, 0xba, 0x7a, 0xfa,
  0x06, 0x86, 0x46, 0xc6, 0x26, 0xa6, 0x66, 0xe6,
  0x16, 0x96, 0x56, 0xd6, 0x36, 0xb6, 0x76, 0xf6,
  0x0e, 0x8e, 0x4e, 0xce, 0x2e, 0xae, 0x6e, 0xee,
  0x1e, 0x9e, 0x5e, 0xde, 0x3e, 0xbe, 0x7e, 0xfe,
  0x01, 0x81, 0x41, 0xc1, 0x21, 0xa1, 0x61, 0xe1,
  0x11, 0x91, 0x51, 0xd1, 0x31, 0xb1, 0x71, 0xf1,
  0x09, 0x89, 0x49, 0xc9, 0x29, 0xa9, 0x69, 0xe9,
  0x19, 0x99, 0x59, 0xd9, 0x39, 0xb9, 0x79, 0xf9,
  0x05, 0x85, 0x45, 0xc5, 0x25, 0xa5, 0x65, 0xe5,
  0x15, 0x95, 0x55, 0xd5, 0x35, 0xb5, 0x75, 0xf5,
  0x0d, 0x8d, 0x4d, 0xcd, 0x2d, 0xad, 0x6d, 0xed,
  0x1d, 0x9d, 0x5d, 0xdd, 0x3d, 0xbd, 0x7d, 0xfd,
  0x03, 0x83, 0x43, 0xc3, 0x23, 0xa3, 0x63, 0xe3,
  0x13, 0x93, 0x53, 0xd3, 0x33, 0xb3, 0x73, 0xf3,
  0x0b, 0x8b, 0x4b, 0xcb, 0x2b, 0xab, 0x6b, 0xeb,
  0x1b, 0x9b, 0x5b, 0xdb, 0x3b, 0xbb, 0x7b, 0xfb,
  0x07, 0x87, 0x47, 0xc7, 0x27, 0xa7, 0x67, 0xe7,
  0x17, 0x97, 0x57, 0xd7, 0x37, 0xb7, 0x77, 0xf7,
  0x0f, 0x8f, 0x4f, 0xcf, 0x2f, 0xaf, 0x6f, 0xef,
  0x1f, 0x9f, 0x5f, 0xdf, 0x3f, 0xbf, 0x7f, 0xff,
};

static void out(const char *msgfmt,...)
{
  va_list args;

  print_indent(); //2
  va_start (args,msgfmt);
  vfprintf (stdout, msgfmt, args);
  va_end   (args);
}

#endif
