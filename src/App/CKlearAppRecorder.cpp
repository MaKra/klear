/**
\author Klear.org
\file CKlearAppRecorder.cpp
\brief basic recording class to save mpeg ts and mpeg ps data streams from DVB device
*/

#include "CKlearAppRecorder.h"

CKlearAppRecorder::CKlearAppRecorder( const QString RecordFile , const CKlearAppConfig *const KlearConfig, const bool isTimeShifted ) : QThread()
{

     this->IsRunning = false; // we are not running, just startiung up....
     this->KlearConfig = KlearConfig ;
     this->isTS=isTimeShifted;

     if ( isTS )
     {
        this->RecordFile = KlearConfig->getRecordingDir() +"/"+ KlearConfig->getTimeShiftName() + ".mpeg" ;
        kdDebug() << "timeshift path set \n" ;
        kdDebug() << this->RecordFile << "\n" ;
     }
     else
     {
        this->RecordFile = RecordFile+"_"+KlearConfig->getCurrentChannel().remove('/')+"_"+QDateTime::currentDateTime().toString( "dd.MM.yy-hh.mm.ss" )+".mpeg"  ;
     }
     dvbDevice = "/dev/dvb/"+this->KlearConfig->getDVBAdapter()+"/"+this->KlearConfig->getDVBDvr() ;
     this->infile = NULL ;
     this->recFormat = KlearConfig->getRecordingFormat();
     this->BUFFER = new char[ BUFFER_LEN ];
     //memset( BUFFER, 0, BUFFER_LEN );  // clear BUFFER

}


CKlearAppRecorder::~CKlearAppRecorder()
{
    const QString __FUNC__ = "CKlearAppRecorder::~CKlearAppRecorder()";

    delete instream;
    delete infile;
    delete BUFFER;
    close(fifowritestream);
    unlink(recordfifo.latin1());

//TODO: ask config wheater questioning the user or not
    if( isTS && !(QMessageBox::question( &QWidget(),
            (i18n("Delete Timeshifting File ?")),
            (i18n("Do you want to delete the Timeshifting recordfile ?")),
            (i18n("&Yes")), (i18n("&No")),
            QString::null, 0, 1 ) ))
    {
      try{
            kdDebug() << " killing temporary timeshifting file \n" ;
            QFile::QFile( this->RecordFile ).remove(); // ...delete the old one
      }catch(...){
            CKlearAppFileException e( __LOC__, i18n("Could not remove temporary timeshifting file" ));
            throw e;
      }
    }   // user wants to store file thus we rename it before anybody overwrites it
        else
    {
          kdDebug() << " renamed \n" ;
          QFile file( this->RecordFile );
          //TODO: rename it, i do not know why he ignores setName

        //  file.setName( KlearConfig->getRecordingDir() +"/" + KlearConfig->getCurrentChannel()+"_"+QDateTime::currentDateTime().toString( "dd.MM.yy-hh.mm.ss" )+".mpeg" );
      rename ( (this->RecordFile).latin1(), (( KlearConfig->getRecordingDir() +"/" + KlearConfig->getCurrentChannel()+"_"+QDateTime::currentDateTime().toString( "dd.MM.yy-hh.mm.ss" )+".mpeg" )).latin1() );
       // file.open( IO_Raw | IO_WriteOnly | IO_Append );
      //  file.close();
    }
}


void CKlearAppRecorder::StopRecording()
{
     this->IsRunning = false;
}


void CKlearAppRecorder::run()
{
   const QString __FUNC__ = "CKlearAppRecorder::run()";
   try{
      this->startUp();
   }catch ( CKlearAppException &e ){
      e.addShowException(__LOC__, i18n("Recording failure \n"));   // show catched expection
      close(fifowritestream);
      unlink(recordfifo.latin1());
      std::exit(-1);  // exit hard
   }
}


void CKlearAppRecorder::startUp()
{
const QString __FUNC__ = "CKlearAppRecorder::startUp()";

     if( recFormat == "MPEG PES" )
     {
          try{
                this->pes_Recording();
          }catch ( CKlearAppException &e ){
                e.addErrorMessage( __LOC__, i18n("Error while doing MPEG PES recording " ));
                throw;
          }

     }
     else if( recFormat == "MPEG TS" )
     {
          try{
              this->ts_Recording();
          }catch ( CKlearAppException &e ){
              e.addErrorMessage( __LOC__, i18n("Error while doing MPEG TS recording " ));
              throw;
          }
     }
     else
     {
        CKlearAppFatalException e( __LOC__, i18n("Failed to determine record format: ")+KlearConfig->getRecordingFormat()+i18n("\nPlease check your klearconfig.") );
        throw e;
     }

}


void CKlearAppRecorder::initRecording()
{
     const QString __FUNC__ = "CKlearRecorder::initRecording()";

     this->infile = new QFile( dvbDevice );
     kdDebug()  << "infile successfully created for init\n";

     if (!infile->open( IO_ReadOnly ))
     {
          CKlearAppFileException e(__LOC__,i18n("Error opening DVB-Device: ") + dvbDevice );
          throw e;
     }

     kdDebug()  << "infile successfully opened for init\n";

     this->instream = new QDataStream( infile );
     kdDebug()  << "instream successfully created for init\n";

     recordfifo = "/tmp/Klear_temp.fifo";
     kdDebug()  << "Setting up FIFO device\n";

    if (!this->isTS)
    {
        while(true)
        {
            int retval = mkfifo( recordfifo.latin1(), S_IRWXU );
            kdDebug() << "FIFO check loop\n";

           if( retval == -1 ) // if FIFO already exists
            {
                perror("Error while Creating FIFO: ");
                kdDebug() << "Old FIFO Buffer found. Klear is deleting the old file.\n";
                unlink(recordfifo.latin1());  // just delete file
                continue; // do another try
            }else if( retval != 0 ){  // another error occures
                CKlearAppFileException e(__LOC__,i18n("Error creating record-fifo \n" ));
                throw e;
            }
            break; // everything went fine
        }

        kdDebug()  << " TS not active FIFO created\n";
        fifowritestream = open(recordfifo.latin1(), O_WRONLY);
    }

}


void CKlearAppRecorder::ts_Recording()
{
     const QString __FUNC__ = "CKlearAppRecorder::ts_Recording()";

     IsRunning = true;

     try{
         initRecording();
     }catch ( CKlearAppException &e ){
         e.addErrorMessage( __LOC__, i18n("error initializing recording") );
         throw;
     }

     QFile outfile(RecordFile);
     if ( !outfile.open( IO_Raw | IO_WriteOnly | IO_Truncate ) )
     {
           CKlearAppFileException e(__LOC__,i18n("Error creating recordfile:") + RecordFile );
           throw e;
     }
     kdDebug() << "recordfile: "  << RecordFile <<"\n";
     
     QDataStream outstream( &outfile );
     kdDebug()  << "tsrecording: outstream successfully created\n";

     while( this->IsRunning == true )
     {
          try{
               instream->readRawBytes( BUFFER, BUFFER_LEN );
          }catch( ... ){
               CKlearAppFileException e(__LOC__,i18n("Error while reading from inputstream"));
               throw e;
          }

         try{
               outstream.writeRawBytes( BUFFER, BUFFER_LEN );
         }catch( ... ){
               CKlearAppFileException e(__LOC__,i18n("Error while writing to outputstream"));
               throw e;
         }

         if (!this->isTS)
         {
            try{
                write(fifowritestream, BUFFER, BUFFER_LEN);
            }catch( ... ){
                CKlearAppFileException e(__LOC__,i18n("Error while writing to record-fifo"));
                throw e;
            }
         }
     } //end of while

   kdDebug() << "Recorder: recording finished\n";

   outfile.close();
}


void CKlearAppRecorder::pes_Recording()
{
     const QString __FUNC__ = "CKlearAppRecorder::pes_Recording()";

     char readbuf[ TS_SIZE ];

     int ps = 0 ;
     bool okay =true;
     IsRunning = true;

     int thWrite = 0;

     uint16_t pida = KlearConfig->getApid() ;
     uint16_t pidv = KlearConfig->getVpid() ;

     try{
       initRecording();
     }catch ( CKlearAppException &e ){
       e.addErrorMessage( __LOC__, "error initializing recording" );
       throw;
     }

     pesrecorder = new Ts2Pes( RecordFile , pida , pidv , ps , &okay);

     if ( !okay )
     {
          delete pesrecorder;
          pesrecorder = 0;
          CKlearAppException e(__LOC__," Error creating pesrecorder-object." );
          throw;
     }

     pesrecorder->go();

     while( this->IsRunning == true )
     {

                try{
                    instream->readRawBytes( readbuf, TS_SIZE );
                }catch( ... )
                {
                    CKlearAppException e(__LOC__,i18n("Error while reading from inputstream"));
                    throw e;
                }

                try{
                    memcpy( BUFFER + thWrite, readbuf, TS_SIZE );
                    thWrite += TS_SIZE;

                    if( thWrite == TS_SIZE * 8 )
                    {
                        pesrecorder->run( ( uint8_t* ) BUFFER, TS_SIZE * 8 );
                        thWrite = 0;
                        if (!this->isTS)
                        {
                            write(fifowritestream, BUFFER, TS_SIZE * 8 );
                        }
                    }
                }catch( ... )
                {
                    CKlearAppException e(__LOC__,i18n("Error while copying streamdata"));
                    throw e;
                }

     }

    pesrecorder->stop();
    delete pesrecorder;
    pesrecorder = 0;
}
