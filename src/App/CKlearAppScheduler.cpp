/**
\author klear.org
\file CKlearAppScheduler.cpp
\brief Class which handles and organizes recording.
*/

#include "CKlearAppScheduler.h"


CKlearAppScheduler::CKlearAppScheduler( CKlearAppConfig *const Config, CKlearAppTuner *const KlearTuner, QObject *const parent ) :  QObject() , QThread()
{
     const QString __FUNC__ = "CKlearAppScheduler::CKlearAppScheduler()";

     this->IsRunning = false;
     this->RecordInProgress = false;
     this->KlearConfig = Config;
     this->KlearTuner = KlearTuner;
     this->parent = parent;
     this->isQuick = false;
     this->Recorder = NULL;
     this->timeShifting = false;

     try{
         initRecordSet();
     }catch(CKlearAppException &e){
         e.addShowException(__LOC__,i18n("Could not initialize scheduler"));
     }

}


CKlearAppScheduler::~CKlearAppScheduler()
{
}

// main loop
void CKlearAppScheduler::run()
{
     const QString __FUNC__ = "CKlearAppScheduler::run()";

     try{
          this->startUp();
     }catch( CKlearAppErrorException &e ){   // spezielle Ausnahmen
          e.addShowException(__LOC__, i18n("General error in scheduler" ));
     }catch( CKlearAppFileException &e ){   // spezielle Ausnahmen
          e.addShowException(__LOC__, i18n("Fileerror in scheduler" ));
     }catch( CKlearAppInputException &e ){   // spezielle Ausnahmen
          e.addShowException(__LOC__, i18n("Inputerror in scheduler") );
     }catch( CKlearAppException &e ){    // alle anderen Ausnahmen
          e.addShowException(__LOC__, i18n("Scheduler thread broken") );
          emit signalCloseMain();
          QApplication::postEvent( parent, new QEvent( QEvent::None ) );
    }catch( std::exception &x ){  // all system exceptions
       CKlearAppException e( __LOC__, x.what() );
       e.showExceptions();   // show catched expection
       emit signalCloseMain();
       QApplication::postEvent( parent, new QEvent( QEvent::None ) );
       // exit(-1);
    }
}

void CKlearAppScheduler::stop()
{
     if ( Recorder != NULL )
     {
          abortRecording();
     }

     this->IsRunning = false;

     /*
     while( RecordStorage.size() > 0 )
     {
          removeRecordSet();
     }
 */
}

void CKlearAppScheduler::startUp()
{
 const QString __FUNC__ = "CKlearAppScheduler::startUp()";

 this->IsRunning = true;

 while( this->IsRunning == true )
 {
     usleep(1000000);  // sleep one second to check for new events
     //kdDebug() << "SchedulerLoop\n";

     if ( !this->isQuick )
     {
        if( RecordInProgress == true )
        {
        // check whether end of record is reached
            if(  (current->getEndDateTime()) <= QDateTime::currentDateTime() )
            {
            stopRecording();
            }
        }
        else
        {
            // check for scheduled record to start
            if( ( RecordStorage.size() > 0 ) &&
                ( ( RecordStorage.front()->getStartDateTime().addSecs(-5) < QDateTime::currentDateTime() ) &&
                    ( RecordStorage.front()->getStartDateTime().addSecs(5) > QDateTime::currentDateTime() ) ) )
            {
               startRecording();
            }
        } // if end
     } // if end (isQuick)
 } // while end
}

void CKlearAppScheduler::startRecording()
{
 this->current = new CKlearAppRecordSet();
 this->current->setRecordFile(RecordStorage.front()->getRecordFile());
 this->current->setStartDateTime(RecordStorage.front()->getStartDateTime());
 this->current->setEndDateTime(RecordStorage.front()->getEndDateTime());
 this->current->setChannel(RecordStorage.front()->getChannel());

       //checks if channel needs to be switched
       if( KlearConfig->getCurrentChannel() != RecordStorage.front()->getChannel() )
       {
               // code for Tuning to recordchannel
               kdDebug() << "trying to tune in scheduler "<< "\n" ;
               if( KlearTuner->running() )
               {
                    KlearTuner->isFinished = true; // finish thread regularly
                    KlearTuner->wait(); //  wait for thread to return
               }

               KlearTuner->setChannel( RecordStorage.front()->getChannel() );
               KlearTuner->start();  // start threaded Tuner

               while( KlearTuner->isReady == false) // wait here while Tuner isn't established correctly
               {
                    usleep(1000);  // wait another 1000s for the tuner
               }

               KlearConfig->setCurrentChannel(RecordStorage.front()->getChannel()); // set current channel in config-class
               KlearConfig->WriteConfig(); // write config to store the last settings

               kdDebug() << "tuned in scheduler to channel: "<< RecordStorage.front()->getChannel() <<"\n" ;
        }

        kdDebug() << "scheduler starting recording "<< "\n" ;
        kdDebug() << "Start: "<< RecordStorage.front()->getStartDateTime().toString() << "\n" ;
        kdDebug() << "current: " << QDateTime::currentDateTime().toString() << "\n" ;
        kdDebug() << "End:  "<< RecordStorage.front()->getEndDateTime().toString() << "\n" ;

        this->RecordInProgress = true;

        emit signalStartingScheduled();
        QApplication::postEvent( parent, new QEvent( QEvent::None ) );

        Recorder = new CKlearAppRecorder( RecordStorage.front()->getRecordFile(), KlearConfig );

        removeRecordSet(); //testing

        while(!KlearTuner->isReady)  // wait here while Tuner isn't established correctly
        {
               usleep(1000);
        }
        Recorder->start();

        usleep(500000);
        emit signalPushQueue();
}

void CKlearAppScheduler::stopRecording()
{
          kdDebug() << "scheduler stopping recording" << "\n" ;
          abortRecording();

          emit signalStoppingScheduled();
          QApplication::postEvent( parent, new QEvent( QEvent::None ) );

}


void CKlearAppScheduler::sortRecordSets()
{
   const QString __FUNC__ = "CKlearAppScheduler::sortRecordSets()";

   CKlearAppRecordSet *temp;
   bool sorted = false;

   //sorting recordsetvector
   while(!sorted){
   sorted = true ;
     for( unsigned int i = 0; i < RecordStorage.size()-1; i++ )
     {
          if( RecordStorage[i]->getStartDateTime() > RecordStorage[i+1]->getStartDateTime() )
          {
            temp = RecordStorage[i];
            RecordStorage[i] = RecordStorage[i+1];
            RecordStorage[i+1] = temp;
            sorted = false ;
          }
     }
   }

   // checking for obsolete recordsets
   while( (RecordStorage.size() > 0) && ((RecordStorage.front()->getStartDateTime()).addSecs(5) < QDateTime::currentDateTime()) )
   {
     kdDebug() << "debug : sort : delete obsolete rs\n";
     RecordStorage.erase( RecordStorage.begin() );
   }

}


/**
 Read recordsets from file
*/
void CKlearAppScheduler::initRecordSet()
{
     const QString __FUNC__ = "CKlearAppScheduler::initRecordSet()";

     kdDebug() << "Reading scheduler data\n";
     QFile RecordStorageFile( KlearConfig->getKlearConfigPath()+"/"+KlearConfig->getKlearSchedulerDatafile() );

     if ( RecordStorageFile.open( IO_ReadOnly ) )
     {
          QTextStream stream( &RecordStorageFile );

          for (int i=1; !stream.atEnd(); i++ )
          {
               try{
                  ParseScheduledRecords( stream.readLine().latin1() );
               }catch(CKlearAppErrorException &e){
                  e.addShowException( __LOC__,i18n("Failed to initiate a recordset" ));
               }catch(CKlearAppException &e){
                  e.addShowException( __LOC__,i18n("Failed to init recordsets" ));
                  throw;
               }

          }

          RecordStorageFile.close();
     }else{
          kdDebug() << "No scheduler data file found\n";
     }


     if(RecordStorage.size() > 0)
     {
          kdDebug() << RecordStorage.size() << " scheduled recordset loaded \n";
     }
}


void CKlearAppScheduler::startQuickRecording()
{
     const QString __FUNC__ = "CKlearAppScheduler::startQuickRecording()";

     this->isQuick = true ;
     kdDebug() << "starting quick recording "<< "\n" ;

     QString recordfile = this->KlearConfig->getRecordingDir()+"/"+this->KlearConfig->getRecordingName();
     current = new CKlearAppRecordSet( recordfile, QDateTime::currentDateTime(), QDateTime::currentDateTime(), this->KlearConfig->getCurrentChannel() ) ;

     if ( this->isTimeShifted() )
     {
        Recorder = new CKlearAppRecorder( recordfile, KlearConfig, true );
        kdDebug() << "klearscheduler do timeshift recording"<< "\n" ;
        current->setRecordFile(KlearConfig->getRecordingDir() +"/"+ KlearConfig->getTimeShiftName() + ".mpeg");
     }
     else
     {
        Recorder = new CKlearAppRecorder( recordfile, KlearConfig );
     }

     Recorder->start();

     this->RecordInProgress = true;
     usleep(500000);

     if ( !(this->isTimeShifted()) )
     {
        kdDebug() << "klearscheduler signal pushqueue"<< "\n" ;

        emit signalPushQueue();
     }
}

void CKlearAppScheduler::abortRecording()
{
     Recorder->StopRecording();
     Recorder->wait();

     delete Recorder;
     delete current;

     this->RecordInProgress = false;
     this->isQuick = false;

     Recorder = NULL;
     current = NULL;
}


void CKlearAppScheduler::isValidRecordSet( const CKlearAppRecordSet *const rs ) const
{
    const QString __FUNC__ = "CKlearAppScheduler::isValidRecordSet( CKlearAppRecordSet *rs )";

    //check dates for validity
    if (!rs->getStartDateTime().isValid() || !rs->getEndDateTime().isValid())
    {
      CKlearAppInputException e( __LOC__, i18n("Specified Times are not valid \n" ));
      throw e;
    }


    QFile ChannelsConf( KlearConfig->getKlearConfigPath()+"/"+KlearConfig->getKlearChannelsConf() );

    //check if channel is contained in channelsconf
    if ( ChannelsConf.open( IO_ReadOnly ) )
    {
       bool found = false;
       QTextStream stream( &ChannelsConf );
       QString line;
       for (int i=1; !stream.atEnd(); i++ )
       {
          line = stream.readLine().latin1();
          QString channel = line.section( ':', 0, 0 );
          if ( channel == rs->getChannel() )
          {
             found = true;
          }
       }
       ChannelsConf.close();
       if( !found )
       {
          CKlearAppInputException e( __LOC__, i18n("Channel unknown \n" ));
          throw e;
       }

    }else
    {
      CKlearAppErrorException e( __LOC__, i18n("Could not load Channellist" ));
      throw e;
    }

    QFileInfo* fi = new QFileInfo(rs->getRecordFile());
    QDir* qd = new QDir(fi->dirPath());


    //check for recordfile
    if( QFile::exists( rs->getRecordFile()) )
    {
      CKlearAppFileException e( __LOC__, i18n("File of given name already exists \n") );
      throw e;
    }

    //check for recorddir
    if( !qd->exists() )
    {
      CKlearAppFileException e( __LOC__, i18n("Directory doesn't exist \n" ));
      throw e;
    }

    // check for write permissions
    if( access(fi->dirPath().latin1(), W_OK) == -1 )
    {
      CKlearAppFileException e( __LOC__, i18n("Directory isn't writable\n" ));
      throw e;
    }

     //check for obselete or invalid recordsets
    if( !isQuick && (( rs->getStartDateTime() >= rs->getEndDateTime() ) || ( rs->getStartDateTime() < QDateTime::currentDateTime() )) )
    {
      CKlearAppErrorException e( __LOC__, i18n("RecordSet invalid:\n\n<br><p>Currenttime: ") + QDateTime::currentDateTime().toString() + i18n("\n<br>Starttime: ") +  rs->getStartDateTime().toString() + i18n("\n<br>Endtime: ")+rs->getEndDateTime().toString() + i18n("\n\n<br><p>Start time must be after current AND before end time.") );
      throw e;
    }

    //check for overlapping recordsets
    for( unsigned int i = 0; i < RecordStorage.size(); i++ )
     {
          // TODO adding of timestamp should be optional
          //  check in case of no timestamp
          if( rs->getRecordFile() == RecordStorage[i]->getRecordFile() )
          {
              CKlearAppFileException e( __LOC__, i18n("Scheduled Record with FileName: \n\n")+rs->getRecordFile()+i18n("\n\nalready exists. \nNew RecordSet rejected.") );
              throw e;
          }

          if( (rs->getStartDateTime() > RecordStorage[i]->getStartDateTime() &&  rs->getStartDateTime() < RecordStorage[i]->getEndDateTime() ) || ( RecordStorage[i]->getStartDateTime() > rs->getStartDateTime() &&  RecordStorage[i]->getStartDateTime() < rs->getEndDateTime() ) )
          {
              CKlearAppInputException e( __LOC__, i18n("New RecordSet:\n\n<br><p> Starttime: ") +  rs->getStartDateTime().toString() + i18n("\n<br>Endtime: ")+rs->getEndDateTime().toString() + i18n("\n\n<br><p>overlaps with\n\n<br><p>old RecordSet:\n\n<br><p>Starttime: ") +  RecordStorage[i]->getStartDateTime().toString() + i18n("\n<br><p>Endtime: ")+RecordStorage[i]->getEndDateTime().toString() + i18n("\n\n<br><p>new RecordSet rejected.") );
              throw e;
          }

     }
}

void CKlearAppScheduler::addRecordSet( const QString RecordFile, const QDateTime startDateTime, const QDateTime endDateTime, const QString channel )
{
    const QString __FUNC__ = "CKlearAppScheduler::addRecordSet(QString RecordFile, QDateTime startDateTime, QDateTime endDateTime, QString channel)";

    CKlearAppRecordSet *candidate = new CKlearAppRecordSet( RecordFile, startDateTime, endDateTime, channel ) ;

    try{
       isValidRecordSet(candidate);
    }catch(CKlearAppErrorException &e){
       e.addErrorMessage( __LOC__,i18n("Failed adding one recordset" ));
       throw;
    }catch(CKlearAppException &e){
       e.addErrorMessage( __LOC__,i18n("Failed adding recordset" ));
       throw;
    }

    RecordStorage.push_back( candidate );

    try{
      sortRecordSets();
    }catch(CKlearAppErrorException &e){
       e.addErrorMessage( __LOC__,i18n("Failed adding one recordset" ));
       throw;
    }catch(CKlearAppException &e){
       e.addErrorMessage( __LOC__,i18n("Failed adding recordset" ));
       throw;
    }

    kdDebug() << "RecordSet added"<< "\n";
}


void CKlearAppScheduler::removeRecordSet()
{
    delete RecordStorage.front();
    RecordStorage.erase( RecordStorage.begin() ); // delete first Element
}


void CKlearAppScheduler::removeRecordSet( const int i)
{
    delete RecordStorage[i];
    RecordStorage.erase( RecordStorage.begin() + i ); // delete element with index i
}


QString CKlearAppScheduler::getRecordFile()
{
    return this->current->getRecordFile();
    //this->RecordStorage.front()->getRecordFile();
}


bool CKlearAppScheduler::isRecordInProgress()
{
    return this->RecordInProgress;
}


int CKlearAppScheduler::getRecordStorageSize()
{
    return this->RecordStorage.size();
}


CKlearAppRecordSet* CKlearAppScheduler::getRecordSet(const int i)
{
    return RecordStorage[i];
}


void CKlearAppScheduler::ParseScheduledRecords( const QString recordDataLine )
{
   const QString __FUNC__ = "CKlearAppScheduler::ParseScheduledRecords(QString ConfigLine)";

   if( ! recordDataLine.contains(";") )   // if line contains no RecSet at all...
      return;

   QString recordfile = recordDataLine.section( ';', 0, 0 );
   QString starttime  = recordDataLine.section( ';', 1, 1 );
   QString endtime    = recordDataLine.section( ';', 2, 2 );
   QString channel    = recordDataLine.section( ';', 3, 3 );

   kdDebug() << "Rec file: " << recordfile << "\n";
   kdDebug() << "Rec starttime: " << starttime << "\n";
   kdDebug() << "Rec endtime: " << endtime << "\n";
   kdDebug() << "Rec channel: " << channel << "\n";

   QDateTime startDateTime = QDateTime::fromString(starttime,Qt::ISODate);
   QDateTime endDateTime = QDateTime::fromString(endtime,Qt::ISODate);

   try{
      addRecordSet(recordfile, startDateTime, endDateTime, channel);
   }catch(CKlearAppErrorException &e){
      e.addErrorMessage( __LOC__,i18n("Failed parsing a ScheduledRecords" )); // just one record set failed
      throw;
   }catch(CKlearAppException &e){
      e.addErrorMessage( __LOC__,i18n("Failed parsing ScheduledRecords" ));
      throw;
   }

}

void CKlearAppScheduler::WriteScheduledRecords()
{
  const QString __FUNC__ = "CKlearAppScheduler::WriteScheduledRecords() ";

  kdDebug() << "Saving Scheduled Records\n";
  kdDebug() << "Rec size: " << RecordStorage.size()<< "\n";

  if( QFile::QFile(  KlearConfig->getKlearConfigPath()+"/"+KlearConfig->getKlearSchedulerDatafile() ).exists() == true ) // if configfile exists...
      if(!QFile::QFile(  KlearConfig->getKlearConfigPath()+"/"+KlearConfig->getKlearSchedulerDatafile() ).remove()) // ...delete the old one
      {
         CKlearAppFileException e( __LOC__, i18n("Could not remove old Scheduledrecords File \n"));
         throw e;
      }

  QFile newRecordStorageFile( KlearConfig->getKlearConfigPath()+"/"+KlearConfig->getKlearSchedulerDatafile()  ); // create new scheduledrecords file

  if ( newRecordStorageFile.open( IO_WriteOnly ) ) {
        QTextStream stream( &newRecordStorageFile );

            stream << "#\n"
                   << "# Klear  -  http://www.klear.org\n"
                   << "#\n"
                   << "# (c) 2004-2005 by Klear.org\n"
                   << "# Published under GPL. See COPYING for more information.\n"
                   << "#\n"
                   << "# This file stores scheduled records and is created automatically.\n"
                   << "# Please only change within Klear->SettingsDialog\n"
                   << "#\n"
                   << "\n";

      //QString RecordFile, QDateTime startDateTime, QDateTime endDateTime, QString channel
       for( unsigned int i = 0; i < RecordStorage.size(); i++)
       {
          stream  << RecordStorage[i]->getRecordFile() << ";"
                  << RecordStorage[i]->getStartDateTime().toString(Qt::ISODate) << ";"
                  << RecordStorage[i]->getEndDateTime().toString(Qt::ISODate) << ";"
                  << RecordStorage[i]->getChannel() << "\n";
       }
       newRecordStorageFile.close();

  }else{
     CKlearAppFileException e( __LOC__, i18n("Could not write Scheduledrecords-File \n"));
     throw e;
  }

}

bool CKlearAppScheduler::isTimeShifted()
{
    return this->timeShifting;
}


void CKlearAppScheduler::setTimeShifted( bool timeShifting )
{
    this->timeShifting = timeShifting;
}

