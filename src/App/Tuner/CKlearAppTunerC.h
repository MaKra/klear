/**
\author klear.org
\file CKlearAppTunerC.h
\brief Tuner for DVB-C
Tuner for DVB-C derived from the KlearTuner-Baseclass
*/
#ifndef CKLEARAPPTUNERC_H
#define CKLEARAPPTUNERC_H

#include "CKlearAppTuner.h"
#include <cstdlib>

/**
\class CKlearAppTunerC
\author Klear.org
\brief Tuner for DVB-C derived from the CKlearTuner-Baseclass
*/
class CKlearAppTunerC : public CKlearAppTuner
{

public:
     /**
     \fn CKlearAppTunerC();
     \author DVBUtils, adapted for Klear by Marco Kraus <marco@klear.org>
     \brief Constructor
     */
     CKlearAppTunerC();

     /**
     \fn CKlearAppTunerC(CKlearAppConfig* KlearConfig);
     \author DVBUtils, adapted for Klear by Marco Kraus <marco@klear.org>
     \param KlearConfig A configurationobject to setup tuner
     \brief overloaded Constructor
     */
     CKlearAppTunerC(CKlearAppConfig* KlearConfig);

     /**
     \fn void ~CKlearAppTunerC();
     \author DVBUtils, adapted for Klear by Marco Kraus <marco@klear.org>
     \brief Destructor
     */
     virtual ~CKlearAppTunerC();

     /**
     \fn void run();
     \author DVBUtils, adapted for Klear by Marco Kraus <marco@klear.org>
     \retval void no retval
     \brief start threaded main method
     */
    virtual void run();


private:
    char line_buf[256];
    void startUp();
    struct dvb_frontend_parameters frontend_param;

    int parse_param(const char *val, const Param * plist, int list_size);
    char* find_channel(FILE *f, int list_channels, int *chan_no);
    int parse(const char *fname, int list_channels, int chan_no, struct dvb_frontend_parameters *frontend, int *vpid, int *apid);
    int set_pesfilter (int fd, int pid, dmx_pes_type_t type, int dvr);
    int setup_frontend(int fe_fd, struct dvb_frontend_parameters *frontend);
    int check_frontend (int fe_fd);
    int list_channels;
    int chan_no;
    int frontend_fd;
    int vpid, apid;
    int video_fd, audio_fd;
};

#endif
