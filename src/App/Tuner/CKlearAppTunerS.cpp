/**
\author klear.org
\file CKlearAppTunerS.cpp
\brief Satellite DVB tuner
*/

#include "CKlearAppTunerS.h"

CKlearAppTunerS::CKlearAppTunerS() : CKlearAppTuner()
{
}

CKlearAppTunerS::CKlearAppTunerS(CKlearAppConfig* KlearConfig) : CKlearAppTuner(KlearConfig)
{
  // Konfiguration is given to the super class and passed to member configuration there.
}

CKlearAppTunerS::~CKlearAppTunerS()
{
}

void CKlearAppTunerS::run()
{
   unsigned int chan_no = 0;
   kdDebug() << "using " << this->FRONTEND_DEV << " and " << this->DEMUX_DEV.utf8() << "\n";

   if ( ! read_channels(this->ConfigFileAbsolute, chan_no, this->ChannelName) )
      return;
}

int CKlearAppTunerS::read_channels(const char *filename, uint32_t chan_no, const char *chan_name)
{
   FILE *cfp;
   char buf[4096];
   char *field, *tmp;
   unsigned int line = 0;
   unsigned int freq, pol, sat_no, sr, vpid, apid;

   if( ! ( cfp = fopen(filename, "r") ) ){
      fprintf(stderr, "error opening channel list '%s': %d %m\n",
       filename, errno);
      return FALSE;
   }

   while (!feof(cfp)) {
      if (fgets(buf, sizeof(buf), cfp)) {
  line++;

  if (chan_no && chan_no != line)
     continue;

  tmp = buf;
  field = strsep(&tmp, ":");

  if (!field)
     goto syntax_err;

  if (chan_name && strcasecmp(chan_name, field) != 0)
     continue;

  printf("zapping to '%s':\n", field);

  if (!(field = strsep(&tmp, ":")))
     goto syntax_err;

  freq = strtoul(field, NULL, 0);

  if (!(field = strsep(&tmp, ":")))
     goto syntax_err;

  pol = (field[0] == 'h' ? 0 : 1);

  if (!(field = strsep(&tmp, ":")))
     goto syntax_err;

  sat_no = strtoul(field, NULL, 0);

  if (!(field = strsep(&tmp, ":")))
     goto syntax_err;

  sr = strtoul(field, NULL, 0) * 1000;

  if (!(field = strsep(&tmp, ":")))
     goto syntax_err;

  vpid = strtoul(field, NULL, 0);

  if (!(field = strsep(&tmp, ":")))
     goto syntax_err;

  apid = strtoul(field, NULL, 0);

  printf("sat %u, frequency = %u MHz %c, symbolrate %u, vpid = 0x%04x, apid = 0x%04x\n", sat_no, freq, pol ? 'V' : 'H', sr, vpid, apid);

  fclose(cfp);

  // setting apid and vpid , MKr
  std::cout << "AudioPid: " << apid << std::endl;
  std::cout << "VideoPid: " << vpid << std::endl;
  this->KlearConfig->setApid( apid );
  this->KlearConfig->setVpid( vpid );

  if (zap_to(sat_no, freq * 1000, pol, sr, vpid, apid))
     return TRUE;
  else
     return FALSE;

  syntax_err:
  fprintf(stderr, "syntax error in line %u: '%s'\n", line, buf);
      } else if (ferror(cfp)) {
  fprintf(stderr, "error reading channel list '%s': %d %m\n",
   filename, errno);
  fclose(cfp);
  return FALSE;
      } else
  break;
   }

   fclose(cfp);

   return TRUE;
}

int CKlearAppTunerS::zap_to(unsigned int sat_no, unsigned int freq, unsigned int pol, unsigned int sr, unsigned int vpid, unsigned int apid)
{
   kdDebug() << "Zapping in....DVB-S\n";

   int fefd, videofd, audiofd;
   uint32_t ifreq;
   int hiband, result;
   struct dvb_frontend_info fe_info;

   printf("using '%s' and '%s'\n", this->FRONTEND_DEV.latin1(), this->DEMUX_DEV.latin1());

   if( (fefd = open(this->FRONTEND_DEV.latin1(), O_RDWR | O_NONBLOCK)) < 0 )
   {
      perror("opening frontend failed");
      return FALSE;
   }

   result = ioctl(fefd, FE_GET_INFO, &fe_info);

   if (result < 0) {
      perror("ioctl FE_GET_INFO failed");
      close(fefd);
      return FALSE;
   }

   if (fe_info.type != FE_QPSK) {
      fprintf(stderr, "frontend device is not a QPSK (DVB-S) device!\n");
      close(fefd);
      return FALSE;
   }

   if ((videofd = open(this->DEMUX_DEV.latin1(), O_RDWR)) < 0) {
      perror("opening video demux failed");
      close(fefd);
      return FALSE;
   }

   if ((audiofd = open(this->DEMUX_DEV.latin1(), O_RDWR)) < 0) {
      perror("opening audio demux failed");
      close(videofd);
      close(fefd);
      return FALSE;
   }

   hiband = (freq >= SWITCHFREQ);
   if (hiband)
      ifreq = freq - LOF_HI;
   else
      ifreq = freq - LOF_LO;

   result = FALSE;

   if (diseqc(fefd, sat_no, pol, hiband))
      if (do_tune(fefd, ifreq, sr))
   if (set_demux(videofd, vpid, 0))
      if (set_demux(audiofd, apid, 1))
         result = TRUE;

   check_frontend (fefd);

   close(audiofd);
   close(videofd);
   close(fefd);

   return result;
}

int CKlearAppTunerS::check_frontend (int fe_fd)
{
   kdDebug() << "checking Frontend for DVB-S\n";

   fe_status_t status;
   uint16_t snr, signal;
   uint32_t ber, uncorrected_blocks;
   int timeout = 0;

   this->isFinished = false;  // we are starting a new turn...

   do{
      if (ioctl(fe_fd, FE_READ_STATUS, &status) == -1)
         perror("FE_READ_STATUS failed");

      /* some frontends might not support all these ioctls, thus we
       * avoid printing errors */
      if (ioctl(fe_fd, FE_READ_SIGNAL_STRENGTH, &signal) == -1)
         signal = -2;
      if (ioctl(fe_fd, FE_READ_SNR, &snr) == -1)
         snr = -2;
      if (ioctl(fe_fd, FE_READ_BER, &ber) == -1)
         ber = -2;
      if (ioctl(fe_fd, FE_READ_UNCORRECTED_BLOCKS, &uncorrected_blocks) == -1)
         uncorrected_blocks = -2;

      printf ("status %02x | signal %04x | snr %04x | ber %08x | unc %08x | ", status, signal, snr, ber, uncorrected_blocks);

      if (status & FE_HAS_LOCK)
      kdDebug() << "FE_HAS_LOCK\n";

     // disabled for testing purposes, MKr 30.12
    //  if( (status & FE_HAS_LOCK) || (++timeout >= 10) )
    //     break;

      this->isReady = true; // tuner established...
      usleep(1000000);

   }while(this->isFinished == false);

   return 0;
}

int CKlearAppTunerS::do_tune(int fefd, unsigned int ifreq, unsigned int sr)
{
   struct dvb_frontend_parameters tuneto;
   struct dvb_frontend_event ev;

   /* discard stale QPSK events */
   while (1) {
      if (ioctl(fefd, FE_GET_EVENT, &ev) == -1)
          break;
   }

   tuneto.frequency = ifreq;
   tuneto.inversion = INVERSION_AUTO;
   tuneto.u.qpsk.symbol_rate = sr;
   tuneto.u.qpsk.fec_inner = FEC_AUTO;

   if (ioctl(fefd, FE_SET_FRONTEND, &tuneto) == -1) {
      perror("FE_SET_FRONTEND failed");
      return FALSE;
   }

   return TRUE;
}

/* digital satellite equipment control,
 * specification is available from http://www.eutelsat.com/
 */
int CKlearAppTunerS::diseqc(int secfd, int sat_no, int pol_vert, int hi_band)
{
   struct diseqc_cmd cmd = { {{0xe0, 0x10, 0x38, 0xf0, 0x00, 0x00}, 4}, 0 };

   /* param: high nibble: reset bits, low nibble set bits,
    * bits are: option, position, polarizaion, band
    */
   cmd.cmd.msg[3] = 0xf0 | (((sat_no * 4) & 0x0f) | (hi_band ? 1 : 0) | (pol_vert ? 0 : 2));

   diseqc_send_msg(secfd, pol_vert ? SEC_VOLTAGE_13 : SEC_VOLTAGE_18,  &cmd, hi_band ? SEC_TONE_ON : SEC_TONE_OFF, (sat_no / 4) % 2 ? SEC_MINI_B : SEC_MINI_A );

   return TRUE;
}

void CKlearAppTunerS::diseqc_send_msg(int fd, fe_sec_voltage_t v, struct diseqc_cmd *cmd, fe_sec_tone_mode_t t, fe_sec_mini_cmd_t b)
{
   if (ioctl(fd, FE_SET_TONE, SEC_TONE_OFF) == -1)
      perror("FE_SET_TONE failed");
   if (ioctl(fd, FE_SET_VOLTAGE, v) == -1)
      perror("FE_SET_VOLTAGE failed");
   usleep(15 * 1000);
   if (ioctl(fd, FE_DISEQC_SEND_MASTER_CMD, &cmd->cmd) == -1)
      perror("FE_DISEQC_SEND_MASTER_CMD failed");
   usleep(cmd->wait * 1000);
   usleep(15 * 1000);
   if (ioctl(fd, FE_DISEQC_SEND_BURST, b) == -1)
      perror("FE_DISEQC_SEND_BURST failed");
   usleep(15 * 1000);
   if (ioctl(fd, FE_SET_TONE, t) == -1)
      perror("FE_SET_TONE failed");
}


int CKlearAppTunerS::set_demux(int dmxfd, int pid, int audio)
{
   struct dmx_pes_filter_params pesfilter;
   int dvr = 1;

   if (pid <= 0 || pid >= 0x1fff) /* ignore this pid to allow radio services */
    return TRUE;

   if (dvr) {
      int buffersize = 64 * 1024;
      if (ioctl(dmxfd, DMX_SET_BUFFER_SIZE, buffersize) == -1)
        perror("DMX_SET_BUFFER_SIZE failed");
   }

   pesfilter.pid = pid;
   pesfilter.input = DMX_IN_FRONTEND;
   pesfilter.output = dvr ? DMX_OUT_TS_TAP : DMX_OUT_DECODER;
   pesfilter.pes_type = audio ? DMX_PES_AUDIO : DMX_PES_VIDEO;
   pesfilter.flags = DMX_IMMEDIATE_START;

   if( ioctl(dmxfd, DMX_SET_PES_FILTER, &pesfilter) == -1 )
   {
      fprintf(stderr, "DMX_SET_PES_FILTER failed (PID = 0x%04x): %d %m\n", pid, errno);
      return FALSE;
   }

   return TRUE;
}
