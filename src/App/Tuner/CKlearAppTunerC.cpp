/**
\file CKlearAppTunerC.cpp
\author Klear.org
\brief Tuner for DVB-C
Cable Tuner for DVB
*/

#include "CKlearAppTunerC.h"

CKlearAppTunerC::CKlearAppTunerC() : CKlearAppTuner()
{
}

CKlearAppTunerC::CKlearAppTunerC(CKlearAppConfig* KlearConfig) : CKlearAppTuner(KlearConfig)
{
  // Konfiguration is given to the super class and passed to member configuration there.
}

CKlearAppTunerC::~CKlearAppTunerC()
{
}

void CKlearAppTunerC::run()
{
  const QString __FUNC__ = "CKlearAppTunerC::run()";

  kdDebug() << "using " << this->FRONTEND_DEV << " and " << this->DEMUX_DEV.utf8() << "\n";

  this->list_channels = 0;  // init some vars.
  this->chan_no = 0;

  try{
     this->startUp();
  }catch( CKlearAppException &e ){  // behandle alle anderen KlearExceptions als Fatal
     e.showExceptions();
     //emit this->signalCloseMain();
     std::exit(-1); // hard exit
  }catch( std::exception &x ){  // all system exceptions
     CKlearAppException e( __LOC__, x.what() );
     e.showExceptions();   // show catched expection
     //emit this->signalCloseMain();
     std::exit(-1);
  }

 return;
}

void CKlearAppTunerC::startUp()
{
   const QString __FUNC__ = "CKlearAppTunerC::startUp()";

if(! memset(&this->frontend_param, 0, sizeof(struct dvb_frontend_parameters)) )
 {
    CKlearAppFatalException e( __LOC__, "Failed setting frontend_param to zero!" );
    throw e;
 }


 try{
    parse( this->ConfigFileAbsolute.latin1(), this->list_channels, this->chan_no, &frontend_param, &vpid, &apid);
 }catch ( CKlearAppException &e ){
    e.addErrorMessage( __LOC__,"Failed setting up DVB device" );
    throw;
 }

 if (list_channels)
 {
    CKlearAppFatalException e( __LOC__, "List channels failed" );
    throw e;
 }

 if( (this->frontend_fd = open(FRONTEND_DEV, O_RDWR)) < 0 )
 {
    CKlearAppFatalException e( __LOC__, "Failed opening DVB device" );
    throw e;
 }

 try{
    setup_frontend( this->frontend_fd, &frontend_param );
 }catch ( CKlearAppException &e ){
    e.addErrorMessage( __LOC__, "Failed setting up DVB device" );
    throw;
 }

 if( (this->video_fd = open(DEMUX_DEV, O_RDWR)) < 0 )
 {
    CKlearAppFatalException e( __LOC__, "Failed opening DEMUX device for video" );
    throw e;
 }

 try{
   set_pesfilter( this->video_fd, this->vpid, DMX_PES_VIDEO, this->dvr );
 }catch(  CKlearAppException &e ){
    e.addErrorMessage( __LOC__, "Could not set PES filter for video" );
    throw;
 }


 if( (this->audio_fd = open(DEMUX_DEV, O_RDWR)) < 0 )
 {
    CKlearAppFatalException e( __LOC__, "Failed opening DEMUX device for audio" );
    throw e;
 }

 try{
    set_pesfilter( this->audio_fd, this->apid, DMX_PES_AUDIO, this->dvr );
 }catch( CKlearAppException &e )
 {
    e.addErrorMessage( __LOC__, "Could not set PES filter for audio" );
    throw;
 }

 // setting apid and vpid , MKr
 std::cout << "AudioPid: " << apid << std::endl;
 std::cout << "VideoPid: " << vpid << std::endl;
 this->KlearConfig->setApid( apid );
 this->KlearConfig->setVpid( vpid );

 try{
    check_frontend(frontend_fd);
 }catch( CKlearAppException &e )
 {
    e.addErrorMessage( __LOC__, "Could not check frontend device" );
    throw e;
 }

 close (this->audio_fd);
 close (this->video_fd);
 close (this->frontend_fd);
}


int CKlearAppTunerC::parse_param(const char *val, const Param * plist, int list_size)
{
    const QString __FUNC__ = "parse_param(const char *val, const Param * plist, int list_size)";

    int i;

    for (i = 0; i < list_size; i++) {
        if (strcasecmp(plist[i].name, val) == 0)
            return plist[i].value;
    }

    CKlearAppFatalException e( __LOC__, "Parse param failed" );
    throw e;
}


char* CKlearAppTunerC::find_channel(FILE *f, int list_channels, int *chan_no)
{
    const QString __FUNC__ = "find_channel(FILE *f, int list_channels, int *chan_no)";

    size_t l;
    int lno = 0;

    l = KlearConfig->getCurrentChannel() ? strlen( KlearConfig->getCurrentChannel() ) : 0;
    while (!feof(f)) {
        if (!fgets(line_buf, sizeof(line_buf), f))
            return NULL;
        lno++;
        if (list_channels) {
            printf("%3d %s", lno, line_buf);
        }
        else if (*chan_no) {
            if (*chan_no == lno)
                return line_buf;
        }
        else if ((strncasecmp(KlearConfig->getCurrentChannel(), line_buf, l) == 0)
                && (line_buf[l] == ':')) {
            *chan_no = lno;
            return line_buf;
        }
    };

    return NULL;
}


int CKlearAppTunerC::parse(const char *fname, int list_channels, int chan_no, struct dvb_frontend_parameters *frontend, int *vpid, int *apid)
{
    const QString __FUNC__ = "parse(const char *fname, int list_channels, int chan_no, struct dvb_frontend_parameters *frontend, int *vpid, int *apid)";

    FILE *f;
    char *chan;
    char *name, *inv, *fec, *mod;

    if ((f = fopen(fname, "r")) == NULL) {
        CKlearAppFileException e( __LOC__, "Could not open file" );
        throw e;
    }

    chan = find_channel(f, list_channels, &chan_no );
    fclose(f);

    if (list_channels)
        return 0;

    if (!chan) {
        CKlearAppFileException e( __LOC__, "Could not find channel in channel list" );
        throw e;
    }

    printf("%3d %s", chan_no, chan);

    if ((sscanf(chan, "%a[^:]:%d:%a[^:]:%d:%a[^:]:%a[^:]:%d:%d\n",
                &name, &frontend->frequency, &inv, &frontend->u.qam.symbol_rate,
                &fec, &mod, vpid, apid) != 8) || !name || !inv || !fec | !mod)
    {
        CKlearAppErrorException e( __LOC__, "cannot parse service data" );
        throw e;
    }

    frontend->inversion = (fe_spectral_inversion_t)parse_param(inv, inversion_list, LIST_SIZE(inversion_list));

    if (frontend->inversion < 0) {
        CKlearAppFatalException e( __LOC__, "inversion field syntax" );
        throw e;
    }

    frontend->u.qam.fec_inner = (fe_code_rate_t)parse_param(fec, fec_list, LIST_SIZE(fec_list));

    if (frontend->u.qam.fec_inner < 0) {
        CKlearAppFatalException e( __LOC__, "FEC field syntax" );
        throw e;
    }

    frontend->u.qam.modulation = (fe_modulation_t)parse_param(mod, modulation_list, LIST_SIZE(modulation_list));

    if (frontend->u.qam.modulation < 0) {
        CKlearAppFatalException e( __LOC__, "modulation field syntax" );
        throw e;
    }

    printf("%3d %s: f %d, s %d, i %d, fec %d, qam %d, v %#x, a %#x\n",
            chan_no, name, frontend->frequency, frontend->u.qam.symbol_rate,
            frontend->inversion, frontend->u.qam.fec_inner,
            frontend->u.qam.modulation, *vpid, *apid);

    free(name);
    free(inv);
    free(fec);
    free(mod);

    return 0;
}



int CKlearAppTunerC::set_pesfilter (int fd, int pid, dmx_pes_type_t type, int dvr)
{
    const QString __FUNC__ = "set_pesfilter (int fd, int pid, dmx_pes_type_t type, int dvr)";

    struct dmx_pes_filter_params pesfilter;

    if (pid <= 0 || pid >= 0x1fff)
        return 0;

    pesfilter.pid = pid;
    pesfilter.input = DMX_IN_FRONTEND;
    pesfilter.output = dvr ? DMX_OUT_TS_TAP : DMX_OUT_DECODER;
    pesfilter.pes_type = type;
    pesfilter.flags = DMX_IMMEDIATE_START;

    if (ioctl(fd, DMX_SET_PES_FILTER, &pesfilter) < 0) {
        PERROR ("ioctl(DMX_SET_PES_FILTER) for %s PID failed", type == DMX_PES_AUDIO ? "Audio" :  type == DMX_PES_VIDEO ? "Video" : "??");
        CKlearAppFatalException e( __LOC__, "ioctl(DMX_SET_PES_FILTER) failed" );
        throw e;
    }

    return 0;
}


int CKlearAppTunerC::setup_frontend(int fe_fd, struct dvb_frontend_parameters *frontend)
{
    const QString __FUNC__ = "setup_frontend(int fe_fd, struct dvb_frontend_parameters *frontend)";

    struct dvb_frontend_info fe_info;

    if (ioctl(fe_fd, FE_GET_INFO, &fe_info) < 0) {
        CKlearAppFatalException e( __LOC__, "ioctl FE_GET_INFO failed" );
        throw e;
        PERROR ("ioctl FE_GET_INFO failed");
    }

    if (fe_info.type != FE_QAM) {
        ERROR ("frontend device is not a QAM (DVB-C) device");
        CKlearAppFatalException e( __LOC__, "ioctl FE_GET_INFO failed" );
        throw e;
    }

    if (ioctl(fe_fd, FE_SET_FRONTEND, frontend) < 0) {
        PERROR ("ioctl FE_SET_FRONTEND failed");
        CKlearAppFatalException e( __LOC__, "ioctl FE_GET_INFO failed" );
        throw e;
    }

    return 0;
}


int CKlearAppTunerC::check_frontend (int fe_fd)
{
    const QString __FUNC__ = "check_frontend (int fe_fd)";

    fe_status_t status;
    uint16_t snr, signal;
    uint32_t ber, uncorrected_blocks;

   this->isFinished = false;  // we are starting a new turn...

    do {
        ioctl(fe_fd, FE_READ_STATUS, &status);
        ioctl(fe_fd, FE_READ_SIGNAL_STRENGTH, &signal);
        ioctl(fe_fd, FE_READ_SNR, &snr);
        ioctl(fe_fd, FE_READ_BER, &ber);
        ioctl(fe_fd, FE_READ_UNCORRECTED_BLOCKS, &uncorrected_blocks);

        //printf ("status %02x | signal %04x | snr %04x | ber %08x | unc %08x | ", status, signal, snr, ber, uncorrected_blocks);
        /*
        if (status & FE_HAS_LOCK)
            printf("FE_HAS_LOCK");
         */

        this->isReady = true; // tuner established...
        usleep(1000000);

    } while( this->isFinished == false );

    return 0;
}
