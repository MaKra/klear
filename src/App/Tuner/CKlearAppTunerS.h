/**
\author Klear.org
\file CKlearAppTunerS.h
\brief Satellite tuner main class
*/

#ifndef CKLEARAPPTUNERS_H
#define CKLEARAPPTUNERS_H

#include "CKlearAppTuner.h"


#define SWITCHFREQ 11700000
#define LOF_HI 10600000
#define LOF_LO 9750000

struct diseqc_cmd {
      struct dvb_diseqc_master_cmd cmd;
     uint32_t wait;
};


/**
\class CKlearAppTunerS
\author Klear.org
\brief Satellite tuner class
*/
class CKlearAppTunerS : public CKlearAppTuner
{

public:
     /**
     \fn CKlearAppTunerS()
     \author DVBUtils adapted for Klear by Marco Kraus <marco@klear.org>
     \brief Constructor
     */
     CKlearAppTunerS();

     /**
     \fn CKlearAppTunerS(CKlearAppConfig* KlearConfig)
     \author DVBUtils adapted for Klear by Marco Kraus <marco@klear.org>
     \param KlearConfig object of the ConfigBase to retrieve settings for the tuner
     \brief overloaded Constructor
     */
     CKlearAppTunerS(CKlearAppConfig* KlearConfig);

     /**
     \fn ~CKlearAppTunerS()
     \author DVBUtils adapted for Klear by Marco Kraus <marco@klear.org>
     \brief Destructor
     */
     ~CKlearAppTunerS();

     /**
     \fn void run()
     \author DVBUtils adapted for Klear by Marco Kraus <marco@klear.org>
     \retval void no retval
     \brief starts the threaded main method
     */
     void run();

private:
    int read_channels(const char *filename, uint32_t chan_no, const char *chan_name);
    int zap_to(unsigned int sat_no, unsigned int freq, unsigned int pol, unsigned int sr, unsigned int vpid, unsigned int apid);
    int check_frontend (int fe_fd);
    int do_tune(int fefd, unsigned int ifreq, unsigned int sr);
    int diseqc(int secfd, int sat_no, int pol_vert, int hi_band);
    int set_demux(int dmxfd, int pid, int audio);
    void diseqc_send_msg(int fd, fe_sec_voltage_t v, struct diseqc_cmd *cmd, fe_sec_tone_mode_t t, fe_sec_mini_cmd_t b);

};

#endif
