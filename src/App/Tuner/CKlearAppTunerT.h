/**
\author klear.org
\file CKlearAppTunerT.h
\brief Tuner for DVB-T
Tuner for DVB-T derived from the KlearTuner-Baseclass
*/
#ifndef CKLEARAPPTUNERT_H
#define CKLEARAPPTUNERT_H

#include "CKlearAppTuner.h"

/**
\class CKlearAppTunerT
\author Klear.org
\brief Tuner for DVB-T derived from the CKlearTuner-Baseclass
*/
class CKlearAppTunerT : public CKlearAppTuner
{

public:
     /**
     \fn CKlearAppTunerT();
     \author DVBUtils, adapted for Klear by Marco Kraus <marco@klear.org>
     \brief Constructor
     */
     CKlearAppTunerT();

     /**
     \fn CKlearAppTunerT(CKlearAppConfig* KlearConfig);
     \author DVBUtils, adapted for Klear by Marco Kraus <marco@klear.org>
     \param KlearConfig A configurationobject to setup tuner
     \brief overloaded Constructor
     */
     CKlearAppTunerT(CKlearAppConfig* KlearConfig);

     /**
     \fn void ~CKlearAppTunerT();
     \author DVBUtils, adapted for Klear by Marco Kraus <marco@klear.org>
     \brief Destructor
     */
     virtual ~CKlearAppTunerT();

     /**
     \fn void run();
     \author DVBUtils, adapted for Klear by Marco Kraus <marco@klear.org>
     \retval void no retval
     \brief start threaded main method
     */
    virtual void run();

private:
    struct dvb_frontend_parameters frontend_param;

    int parse_param (int fd, const Param * plist, int list_size, int *param);
    int parse_int(int fd, int *val);
    int find_channel(int fd);
    int try_parse_int(int fd, int *val);
    int try_parse_param(int fd, const Param * plist, int list_size, int *param);
    int parse(const char *fname, struct dvb_frontend_parameters *frontend, int *vpid, int *apid);
    int set_pesfilter (int fd, int pid, dmx_pes_type_t type, int dvr);
    int setup_frontend (int fe_fd, struct dvb_frontend_parameters *frontend);
    int check_frontend (int fe_fd);
    void startUp();

    int vpid;
    int apid;
    int frontend_fd;
    int audio_fd;
    int video_fd;
    int opt;
};

#endif
