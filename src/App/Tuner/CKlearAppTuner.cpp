/**
\file CKlearAppTuner.cpp
\author Klear.org
\brief Tuner baseclass for T-,-S and C-Tuners
The base class of all derived tuner classes.
*/

#include "CKlearAppTuner.h"

CKlearAppTuner::CKlearAppTuner()
{
}

CKlearAppTuner::CKlearAppTuner(CKlearAppConfig* KlearConfig)
{
 this->KlearConfig = KlearConfig;  // point config to local member
 ConfigFileAbsolute = KlearConfig->getKlearConfigPath()+"/"+KlearConfig->getKlearChannelsConf();

 FRONTEND_DEV = "/dev/dvb/"+KlearConfig->getDVBAdapter()+"/"+KlearConfig->getDVBFrontend();
 DEMUX_DEV = "/dev/dvb/"+KlearConfig->getDVBAdapter()+"/"+KlearConfig->getDVBDemux();

 // init some base vars. // read this from config later on!! MKr, 30.12.2004
 adapter = 0;
 frontend = 0;
 demux = 0;
 dvr = 1;

 isFinished = false; // not not finished...just starting up ;-)
 isReady = false;  // we are starting up, and tuner is not established right now (set to true in checkFrontend()
}

CKlearAppTuner::~CKlearAppTuner()
{
}

void CKlearAppTuner::setChannel(const QString& ChannelName)
{
   this->ChannelName = ChannelName;
}
