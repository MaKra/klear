/**
\file CKlearAppTunerT.cpp
\author Klear.org
\brief Tuner for DVB-T
Terrestric Tuner for DVB
*/

#include "CKlearAppTunerT.h"

CKlearAppTunerT::CKlearAppTunerT() : CKlearAppTuner()
{
}

CKlearAppTunerT::CKlearAppTunerT(CKlearAppConfig* KlearConfig) : CKlearAppTuner(KlearConfig)
{
  // Konfiguration is given to the super class and passed to member configuration there.
}

CKlearAppTunerT::~CKlearAppTunerT()
{
}

void CKlearAppTunerT::run()
{
  const QString __FUNC__ = "CKlearAppTunerT::run()";

  kdDebug() << "using " << this->FRONTEND_DEV << " and " << this->DEMUX_DEV.utf8() << "\n";

  try{
     this->startUp();
  }catch( CKlearAppException &e ){  // behandle alle anderen KlearExceptions als Fatal
     e.showExceptions();
     //emit this->signalCloseMain();
     std::exit(-1); // hard exit
  }catch( std::exception &x ){  // all system exceptions
     CKlearAppException e( __LOC__, x.what() );
     e.showExceptions();   // show catched expection
     //emit this->signalCloseMain();
     std::exit(-1);
  }

 return;
}

void CKlearAppTunerT::startUp()
{
 const QString __FUNC__ = "CKlearAppTunerT::startUp()";

 if(! memset(&this->frontend_param, 0, sizeof(struct dvb_frontend_parameters)) )
 {
    CKlearAppFatalException e( __LOC__, "Failed setting frontend_param to zero!" );
    throw e;
 }

 try{
    parse( this->ConfigFileAbsolute.latin1(), &frontend_param, &vpid, &apid );
 }catch ( CKlearAppException &e ){
    e.addErrorMessage( __LOC__,"Failed setting up DVB device" );
    throw;
 }

 if( (this->frontend_fd = open(FRONTEND_DEV, O_RDWR)) < 0 )
 {
    CKlearAppFatalException e( __LOC__, "Failed opening DVB device" );
    throw e;
 }

 try{
    setup_frontend( this->frontend_fd, &frontend_param );
 }catch ( CKlearAppException &e ){
    e.addErrorMessage( __LOC__, "Failed setting up DVB device" );
    throw;
 }

 if( (this->video_fd = open(DEMUX_DEV, O_RDWR)) < 0 )
 {
    CKlearAppFatalException e( __LOC__, "Failed opening DEMUX device for video" );
    throw e;
 }

 try{
   set_pesfilter( this->video_fd, this->vpid, DMX_PES_VIDEO, this->dvr );
 }catch(  CKlearAppException &e ){
    e.addErrorMessage( __LOC__, "Could not set PES filter for video" );
    throw;
 }

 if( (this->audio_fd = open(DEMUX_DEV, O_RDWR)) < 0 )
 {
    CKlearAppFatalException e( __LOC__, "Failed opening DEMUX device for audio" );
    throw e;
 }

 try{
    set_pesfilter( this->audio_fd, this->apid, DMX_PES_AUDIO, this->dvr );
 }catch( CKlearAppException &e )
 {
    e.addErrorMessage( __LOC__, "Could not set PES filter for audio" );
    throw;
 }

 // setting apid and vpid , MKr
 std::cout << "AudioPid: " << apid << std::endl;
 std::cout << "VideoPid: " << vpid << std::endl;
 this->KlearConfig->setApid( apid );
 this->KlearConfig->setVpid( vpid );

 try{
    check_frontend(frontend_fd);
 }catch( CKlearAppException &e )
 {
    e.addErrorMessage( __LOC__, "Could not check frontend device" );
    throw e;
 }

 close (this->audio_fd);
 close (this->video_fd);
 close (this->frontend_fd);
 }


int CKlearAppTunerT::parse_param(int fd, const Param * plist, int list_size, int *param)
{
 const QString __FUNC__ = "CKlearAppTunerT::parse_param(int fd, const Param * plist, int list_size, int *param)";

 char c;
 int character = 0;
 int index = 0;

 while(1) {
  if( read(fd, &c, 1) < 1 )
  {
    CKlearAppFatalException e( __LOC__, "Could not read from channellist any more without matching a channelname" );
    throw e;
  }

  if( (c == ':' || c == '\n') && plist->name[character] == '\0' )
  {
     break;
  }

  while (toupper(c) != plist->name[character])
  {
    index++;
    plist++;

    if (index >= list_size)  /*  parse error, no valid */
    {
        CKlearAppFatalException e( __LOC__, "parse error, not valid" );
        throw e;
    }
  }

  character++;
 }

 *param = plist->value;

 return 0;
}


int CKlearAppTunerT::parse_int(int fd, int *val)
{
 const QString __FUNC__ = "CKlearAppTunerT::parse_int(int fd, int *val)";

 char number[11]; /* 2^32 needs 10 digits... */
 int character = 0;

 while(1)
 {

     if( read(fd, &number[character], 1) < 1 )
     {
          CKlearAppFatalException e( __LOC__, "Could not read number from file device" );
          throw e;
     }

     if( number[character] == ':' || number[character] == '\n' )
     {
          number[character] = '\0';
          break;
     }

     if( ! isdigit( number[character] ) )
     {
          CKlearAppFatalException e( __LOC__, "Read digit is no number." );
          throw e;
     }

     character++;

     if (character > 10) /*  overflow, number too big */
     {
          CKlearAppFatalException e( __LOC__, "Read number too big" );
          throw e;
     }

 }; // end while

 *val = strtol(number, NULL, 10);

 return 0;
}


int CKlearAppTunerT::find_channel(int fd)
{
 const QString __FUNC__ = "CKlearAppTunerT::find_channel(int fd)";

 int character = 0;

 while (1)
 {
     char c;

     if( read(fd, &c, 1) < 1 )
     {
          CKlearAppErrorException e( __LOC__, "Can not read more data from channels.conf. End of file reached. Nothing found. ");
          throw e;
     }

     if ( (c == ':') && (this->ChannelName[character] == '\0') )
     {
          break;
     }

     if( toupper(c) == toupper( this->ChannelName.latin1()[character] ) )
     {
          character++;
     }
     else
     {
          character = 0;
     }
 };

 return 0;
}


int CKlearAppTunerT::try_parse_int(int fd, int *val )
{
 const QString __FUNC__ = "CKlearAppTunerT::try_parse_int(int fd, int *val )";

 try{
    parse_int(fd, val);
 }catch( CKlearAppException &e ){
    e.addErrorMessage( __LOC__, "Could not parse integers" );
    throw;
 }

 return 0;
}


int CKlearAppTunerT::try_parse_param(int fd, const Param * plist, int list_size, int *param)
{
 const QString __FUNC__ = "CKlearAppTunerT::try_parse_param(int fd, const Param * plist, int list_size, int *param)";

  try{
     parse_param(fd, plist, list_size, param);
  }catch( CKlearAppException &e ){
     e.addErrorMessage( __LOC__, "Could not parse parameters" );
     throw;
  }

  return 0;
}


int CKlearAppTunerT::parse(const char *fname, struct dvb_frontend_parameters *frontend, int *vpid, int *apid)
{
 const QString __FUNC__ = "CKlearAppTunerT::parse(const char *fname, struct dvb_frontend_parameters *frontend, int *vpid, int *apid)";

 int fd;

 if( (fd = open(fname, O_RDONLY | O_NONBLOCK)) < 0 )
 {
   CKlearAppFileException e( __LOC__, "Could not open file" );
   throw e;
 }


 try{
    find_channel( fd );
 }catch( CKlearAppErrorException &e){
    e.addErrorMessage( __LOC__, "Could not find channel in channel list" );
    throw;
 }


 try{
    try_parse_int( fd, (int *)&frontend->frequency );
 }catch( CKlearAppException &e ){
    e.addErrorMessage( __LOC__, "Could not parse_int frequency" );
    throw;
 }

 try{
    try_parse_param( fd, inversion_list, LIST_SIZE(inversion_list), (int *) &frontend->inversion );
 }catch( CKlearAppException &e ){
    e.addErrorMessage( __LOC__, "Could not parse_param inversion" );
    throw;
 }

 try{
    try_parse_param( fd, bw_list, LIST_SIZE(bw_list), (int *) &frontend->u.ofdm.bandwidth );
 }catch( CKlearAppException &e ){
    e.addErrorMessage( __LOC__, "Could not parse_param bandwidth" );
    throw;
 }

 try{
    try_parse_param( fd, fec_list, LIST_SIZE(fec_list), (int *) &frontend->u.ofdm.code_rate_HP );
 }catch( CKlearAppException &e ){
    e.addErrorMessage( __LOC__, "Could not parse_param code_rate_HP" );
    throw;
 }

 try{
    try_parse_param( fd, fec_list, LIST_SIZE(fec_list), (int *) &frontend->u.ofdm.code_rate_LP );
 }catch( CKlearAppException &e ){
    e.addErrorMessage( __LOC__, "Could not parse_param code_rate_LP" );
    throw;
 }

 try{
    try_parse_param( fd, constellation_list, LIST_SIZE(constellation_list), (int *) &frontend->u.ofdm.constellation );
 }catch( CKlearAppException &e ){
    e.addErrorMessage( __LOC__, "Could not parse_param constellation" );
    throw;
 }

 try{
    try_parse_param( fd, transmissionmode_list, LIST_SIZE(transmissionmode_list), (int *) &frontend->u.ofdm.transmission_mode );
 }catch( CKlearAppException &e ){
    e.addErrorMessage( __LOC__, "Could not parse_param transmission_mode" );
    throw;
 }

 try{
    try_parse_param( fd, guard_list, LIST_SIZE(guard_list), (int *) &frontend->u.ofdm.guard_interval );
 }catch( CKlearAppException &e ){
    e.addErrorMessage( __LOC__, "Could not parse_param guard_interval" );
    throw;
 }

 try{
    try_parse_param( fd, hierarchy_list, LIST_SIZE(hierarchy_list), (int *) &frontend->u.ofdm.hierarchy_information );
 }catch( CKlearAppException &e ){
    e.addErrorMessage( __LOC__, "Could not parse_param hierarchy_information" );
    throw;
 }

 try{
    try_parse_int( fd, vpid );
 }catch( CKlearAppException &e ){
    e.addErrorMessage( __LOC__, "Could not parse_int video pid" );
    throw;
 }

 try{
    try_parse_int( fd, apid );
 }catch( CKlearAppException &e ){
    e.addErrorMessage( __LOC__, "Could not parse_int audio pid" );
    throw;
 }

 close(fd);

 return 0;
}


int CKlearAppTunerT::set_pesfilter(int fd, int pid, dmx_pes_type_t type, int dvr)
{
   const QString __FUNC__ = "CKlearAppTunerT::set_pesfilter(int fd, int pid, dmx_pes_type_t type, int dvr)";

   struct dmx_pes_filter_params pesfilter;

   if (pid <= 0 || pid >= 0x1fff)
   {
        return 0;
   }

   pesfilter.pid = pid;
   pesfilter.input = DMX_IN_FRONTEND;
   pesfilter.output = dvr ? DMX_OUT_TS_TAP : DMX_OUT_DECODER;
   pesfilter.pes_type = type;
   pesfilter.flags = DMX_IMMEDIATE_START;

   if (ioctl(fd, DMX_SET_PES_FILTER, &pesfilter) < 0)
   {
        PERROR ("ioctl(DMX_SET_PES_FILTER) for %s PID failed", type == DMX_PES_AUDIO ? "Audio" : type == DMX_PES_VIDEO ? "Video" : "??");
        CKlearAppFatalException e( __LOC__, "ioctl(DMX_SET_PES_FILTER) failed" );
        throw e;
   }

   return 0;
}


int CKlearAppTunerT::setup_frontend (int fe_fd, struct dvb_frontend_parameters *frontend)
{
 const QString __FUNC__ = "CKlearAppTunerT::setup_frontend (int fe_fd, struct dvb_frontend_parameters *frontend)";

 struct dvb_frontend_info fe_info;

 if( ioctl(fe_fd, FE_GET_INFO, &fe_info) < 0 )
 {
  CKlearAppFatalException e( __LOC__, "ioctl FE_GET_INFO failed" );
  throw e;
 }

 if( fe_info.type != FE_OFDM )
 {
  CKlearAppFatalException e( __LOC__, "frontend device is not a OFDM (DVB-T) device" );
  throw e;
 }

 kdDebug() << "Tuning to " << frontend->frequency << " Hz\n";

 if( ioctl(fe_fd, FE_SET_FRONTEND, frontend) < 0 )
 {
  CKlearAppFatalException e( __LOC__,"ioctl FE_SET_FRONTEND failed");
  throw e;
 }

 return 0;
}


int CKlearAppTunerT::check_frontend (int fe_fd)
{
 const QString __FUNC__ = "CKlearAppTunerT::check_frontend (int fe_fd)";

 fe_status_t status;
 uint16_t snr, signal;
 uint32_t ber, uncorrected_blocks;

 this->isFinished = false;  // we are starting a new turn...

 do{
     ioctl(fe_fd, FE_READ_STATUS, &status);
     ioctl(fe_fd, FE_READ_SIGNAL_STRENGTH, &signal);
     ioctl(fe_fd, FE_READ_SNR, &snr);
     ioctl(fe_fd, FE_READ_BER, &ber);
     ioctl(fe_fd, FE_READ_UNCORRECTED_BLOCKS, &uncorrected_blocks);

     //printf ("status %02x | signal %04x | snr %04x | ber %08x | unc %08x | \n", status, signal, snr, ber, uncorrected_blocks ); // DEBUG, MKr
     /*
     if (status & FE_HAS_LOCK)
          printf("FE_HAS_LOCK");
     */

     this->isReady = true; // tuner established...
     usleep(1000000); // sleep one second

 }while( this->isFinished == false );  // run while we are not finished

 return 0;
}
