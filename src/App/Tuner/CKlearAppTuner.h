/**
\file CKlearAppTuner.h
\author Klear.org
\brief abstract tunig basefile
Tuning adapted from the t/s/c-zap files
from the dvb-utils projects and vdr
*/

#ifndef CKLEARAPPTUNER_H
#define CKLEARAPPTUNER_H

#include <linux/dvb/frontend.h>
#include <linux/dvb/dmx.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/un.h>

#include <iostream>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <time.h>
#include <stdint.h>
#include <cstdlib>

#include <linux/dvb/frontend.h>
#include <linux/dvb/dmx.h>

#include <qstring.h>
#include <qthread.h>

#ifndef TRUE
#define TRUE (1==1)
#endif
#ifndef FALSE
#define FALSE (1==0)
#endif

#include "../CKlearAppConfig.h"
#include "../Exceptions/CKlearAppException.h"
#include "../Exceptions/CKlearAppFatalException.h"
#include "../Exceptions/CKlearAppErrorException.h"
#include "../Exceptions/CKlearAppFileException.h"

typedef         __u16           uint16_t;
typedef         __u32           uint32_t;

typedef struct {
    char name[23];
    int value;
} Param;

static const Param inversion_list[] = {
    { "INVERSION_OFF", INVERSION_OFF },
    { "INVERSION_ON", INVERSION_ON },
    { "INVERSION_AUTO", INVERSION_AUTO }
};

static const Param fec_list[] = {
    { "FEC_1_2", FEC_1_2 },
    { "FEC_2_3", FEC_2_3 },
    { "FEC_3_4", FEC_3_4 },
    { "FEC_4_5", FEC_4_5 },
    { "FEC_5_6", FEC_5_6 },
    { "FEC_6_7", FEC_6_7 },
    { "FEC_7_8", FEC_7_8 },
    { "FEC_8_9", FEC_8_9 },
    { "FEC_AUTO", FEC_AUTO },
    { "FEC_NONE", FEC_NONE }
};

static const Param bw_list [] = {
 { "BANDWIDTH_6_MHZ", BANDWIDTH_6_MHZ },
 { "BANDWIDTH_7_MHZ", BANDWIDTH_7_MHZ },
 { "BANDWIDTH_8_MHZ", BANDWIDTH_8_MHZ }
};

static const Param guard_list [] = {
 {"GUARD_INTERVAL_1_16", GUARD_INTERVAL_1_16},
 {"GUARD_INTERVAL_1_32", GUARD_INTERVAL_1_32},
 {"GUARD_INTERVAL_1_4", GUARD_INTERVAL_1_4},
 {"GUARD_INTERVAL_1_8", GUARD_INTERVAL_1_8}
};

static const Param hierarchy_list [] = {
 { "HIERARCHY_1", HIERARCHY_1 },
 { "HIERARCHY_2", HIERARCHY_2 },
 { "HIERARCHY_4", HIERARCHY_4 },
 { "HIERARCHY_NONE", HIERARCHY_NONE }
};

static const Param constellation_list [] = {
 { "QPSK", QPSK },
 { "QAM_128", QAM_128 },
 { "QAM_16", QAM_16 },
 { "QAM_256", QAM_256 },
 { "QAM_32", QAM_32 },
 { "QAM_64", QAM_64 }
};

static const Param transmissionmode_list [] = {
 { "TRANSMISSION_MODE_2K", TRANSMISSION_MODE_2K },
 { "TRANSMISSION_MODE_8K", TRANSMISSION_MODE_8K },
};

static const Param modulation_list[] = {
    { "QAM_16", QAM_16 },
    { "QAM_32", QAM_32 },
    { "QAM_64", QAM_64 },
    { "QAM_128", QAM_128 },
    { "QAM_256", QAM_256 },
    { "QAM_AUTO", QAM_AUTO }
};

#define ERROR(x...)                                                     \
        do {                                                            \
                fprintf(stderr, "ERROR: ");                             \
                fprintf(stderr, x);                                     \
                fprintf (stderr, "\n");                                 \
        } while (0)

#define PERROR(x...)                                                    \
        do {                                                            \
                fprintf(stderr, "ERROR: ");                             \
                fprintf(stderr, x);                                     \
                fprintf (stderr, " (%s)\n", strerror(errno));  \
        } while (0)

#define LIST_SIZE(x) sizeof(x)/sizeof(Param)


/**
\class CKlearAppTuner
\author Klear.org
\brief Abstract tunig baseclass
Tuning adapted from the t/s/c-zap files
from the dvb-utils projects and vdr
*/
class CKlearAppTuner : public QThread
{

public:
     /**
     \fn CKlearAppTuner()
     \author Marco Kraus <marco@klear.org>
     \brief Constructor
     */
     CKlearAppTuner();

     /**
     \fn CKlearAppTuner(CKlearAppConfig* KlearConfig)
     \author Marco Kraus <marco@klear.org>
     \brief overloaded Constructor
     \param KlearConfig Configurationobject to setup tuner
     */
     CKlearAppTuner(CKlearAppConfig* KlearConfig); // overloaded constructor

     /**
     \fn ~CKlearAppTuner()
     \author Marco Kraus <marco@klear.org>
     \brief Destructor
     */
     virtual ~CKlearAppTuner();

     /**
     \fn void run() = 0
     \author Marco Kraus <marco@klear.org>
     \retval void no retval
     \brief starts the threaded main method, defined abstract
     */
     virtual void run() = 0;

     /**
     \fn void setChannel(const QString& ChannelName)
     \author Marco Kraus <marco@klear.org>
     \retval void no retval
     \param ChannelName Channelname to tell tuner which channel wants to be tuned in
     \brief Sets the local ChannelName to tell tuner which channel wants to be tuned in
     */
     virtual void setChannel(const QString& ChannelName);

     /**
     \author Marco Kraus <marco@klear.org>
     \brief isFinished flag, tells other threads that the tuner has finished. Will be set with setter/getter in future.
     */
     bool isFinished;  // flag for ending thread

     /**
     \author Marco Kraus <marco@klear.org>
     \brief isReady flag, tells other thread that tuner is set up. Will be set with setter/getter in future.
     */
     bool isReady;  // check if tuner is running and klear can start playing stream


protected:
     /**
     \author Marco Kraus <marco@klear.org>
     \brief local configuration object, for derived tuner classes
     */
     CKlearAppConfig* KlearConfig;

     /**
     \author Marco Kraus <marco@klear.org>
     \brief Absolute filename to configuration file
     */
     QString ConfigFileAbsolute;

     /**
     \author Marco Kraus <marco@klear.org>
     \brief currentlich tuning channelname
     */
     QString ChannelName;

     /**
     \author Marco Kraus <marco@klear.org>
     \brief frontend-name in dvbutil compatible format.
     */
     QString FRONTEND_DEV;

     /**
     \author Marco Kraus <marco@klear.org>
     \brief demux-name in dvbutil compatible format.
     */
     QString DEMUX_DEV;

     /**
     \author Marco Kraus <marco@klear.org>
     \brief DVB adapter number in dvbutil compatible format
     */
     int adapter;

     /**
     \author Marco Kraus <marco@klear.org>
     \brief DVB frontend number in dvbutil compatible format
     */
     int frontend;

     /**
     \author Marco Kraus <marco@klear.org>
     \brief DVB demux number in dvbutil compatible format
     */
     int demux;

     /**
     \author Marco Kraus <marco@klear.org>
     \brief DVB dvr number in dvbutil compatible format
     */
     int dvr;
};

#endif
