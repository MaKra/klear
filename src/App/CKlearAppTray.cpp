/**
\author Klear.org
\file CKlearAppTray.cpp
\brief Systemtray class to dock klear to panel
*/

#include "CKlearAppTray.h"

CKlearAppTray::CKlearAppTray( QWidget* parent, const char* name ) : KSystemTray( parent, name )
{
}

CKlearAppTray::~CKlearAppTray()
{
}

void CKlearAppTray::slotQuitTray()
{
   this->close();
}

