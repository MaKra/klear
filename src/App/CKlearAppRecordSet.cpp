/**
\author Klear.org
\file CKlearAppRecordSet.cpp
\brief Storage class for storing start -, endtime, channel and path for each recording.
*/

#include "CKlearAppRecordSet.h"

CKlearAppRecordSet::CKlearAppRecordSet()
{
}

CKlearAppRecordSet::CKlearAppRecordSet( const QString RecordFile,
                                       const QDateTime startDateTime,
                                       const QDateTime endDateTime,
                                       const QString channel)
{

    this->RecordFile = RecordFile;
    this->startDateTime = startDateTime;
    this->endDateTime = endDateTime;
    this->channel = channel;

}


CKlearAppRecordSet::~CKlearAppRecordSet()
{
}


QString CKlearAppRecordSet::getRecordFile() const
{
    return this->RecordFile;
}


QDateTime  CKlearAppRecordSet::getStartDateTime() const
{
    return this->startDateTime;
}


QDateTime CKlearAppRecordSet::getEndDateTime() const
{
    return this->endDateTime;
}


QString CKlearAppRecordSet::getChannel() const
{
    return this->channel;
}


void CKlearAppRecordSet::setRecordFile( const QString RecordFile )
{
    this->RecordFile = RecordFile;
}


void CKlearAppRecordSet::setStartDateTime( const QDateTime startDateTime )
{
    this->startDateTime = startDateTime;
}


void CKlearAppRecordSet::setEndDateTime( const QDateTime endDateTime )
{
    this->endDateTime = endDateTime;
}


void CKlearAppRecordSet::setChannel( const QString channel )
{
    this->channel = channel;
}




