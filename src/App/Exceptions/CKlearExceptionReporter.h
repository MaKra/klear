/**
\author Klear.org
\file CKlearExceptionReporter.h
\brief The exception reporting classes for klear
*/

#ifndef CKLEAREXCEPTIONREPORTING_H
#define CKLEAREXCEPTIONREPORTING_H

#include <qstring.h>
#include <qmessagebox.h>
#include <qwidget.h>

#include <klocale.h>
#include <kdebug.h>

#include <vector>
#include <iostream>

#include "CKlearAppException.h"

class ErrorData; // forward declaration

/**
\class CKlearExceptionReporter
\author Klear.org
\brief The exception reporting baseclass for Klear
*/
class CKlearExceptionReporter : public QWidget
{
  public:
     /**
     \fn CKlearExceptionReporter();
     \author Marco Kraus <marco@klear.org>
     \brief Constructor
     */
     CKlearExceptionReporter();

     /**
     \fn ~CKlearExceptionReporter();
     \author Marco Kraus <marco@klear.org>
     \brief Destructor
     */
     virtual ~CKlearExceptionReporter();

     /**
     \fn void showExceptions(std::vector<ErrorData *> ErrorStack);
     \author Marco Kraus <marco@klear.org>
     \retval void no retval
     \brief show the complete messagestack with its own GUI (independet from environment)
     */
     void showExceptions(std::vector<ErrorData *> ErrorStack);

     /**
     \fn void showExceptions(std::vector<ErrorData *> ErrorStack, QString ExceptionType);
     \author Marco Kraus <marco@klear.org>
     \retval void no retval
     \brief show the complete messagestack with its own GUI (independet from environment) with the exception-type as title
     */
     void showExceptions(std::vector<ErrorData *> ErrorStack, QString ExceptionType);

};

#endif
