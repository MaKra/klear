/**
\author Klear.org
\file CKlearAppErrorException.h
\brief  Error exception for Klear
*/

#ifndef CKLEARAPPERROREXCEPTION_H
#define CKLEARAPPERROREXCEPTION_H

#include "CKlearAppException.h"

/**
\class CKlearAppErrorException
\author Klear.org
\brief Normal error exception, derived from Exception-Baseclass
*/
class CKlearAppErrorException : public CKlearAppException
{
  public:

     /**
     \fn CKlearAppErrorException()
     \author Marco Kraus <marco@klear.org>
     \brief constructor
     */
     CKlearAppErrorException();

     /**
     \fn CKlearAppErrorException()
     \author Marco Kraus <marco@klear.org>
     \brief overloaded constructor
     */
     CKlearAppErrorException(QString loc, QString msg);

     /**
     \fn ~CKlearAppErrorException()
     \author Marco Kraus <marco@klear.org>
     \brief destructor
     */
    ~CKlearAppErrorException();

     /**
     \fn void showExceptions();
     \author Marco Kraus <marco@klear.org>
     \retval void no retval
     \brief show the complete messagestack with its own GUI (independet from environment)
     */
     void showExceptions();

};

#endif
