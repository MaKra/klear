/**
\author Klear.org
\file CKlearAppFatalException.h
\brief Fatal exception for Klear
*/

#ifndef CKLEARAPPFATALEXCEPTION_H
#define CKLEARAPPFATALEXCEPTION_H

#include "CKlearAppException.h"

/**
\class CKlearAppFatalException
\author Klear.org
\brief Fatal exception, derived from Exception-Baseclass
*/
class CKlearAppFatalException : public CKlearAppException
{

  public:

     /**
     \fn CKlearAppFatalException()
     \author Marco Kraus <marco@klear.org>
     \brief constructor
     */
     CKlearAppFatalException();

     /**
     \fn CKlearAppFatalException(QString loc, QString msg)
     \author Marco Kraus <marco@klear.org>
     \brief overloaded constructor
     */
     CKlearAppFatalException(QString loc, QString msg);

     /**
     \fn ~CKlearAppFatalException()
     \author Marco Kraus <marco@klear.org>
     \brief destructor
     */
     ~CKlearAppFatalException();

     /**
     \fn void showExceptions();
     \author Marco Kraus <marco@klear.org>
     \retval void no retval
     \brief show the complete messagestack with its own GUI (independet from environment)
     */
     void showExceptions();
};

#endif
