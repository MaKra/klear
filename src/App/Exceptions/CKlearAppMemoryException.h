/**
\author Klear.org
\file CKlearAppMemoryException.h
\brief Memory error exception for Klear
*/

#ifndef CKLEARAPPMEMORYEXCEPTION_H
#define CKLEARAPPMEMORYEXCEPTION_H

#include "CKlearAppException.h"

/**
\class CKlearAppMemoryException
\author Klear.org
\brief Memory error exception, derived from Exception-Baseclass
*/
class CKlearAppMemoryException : public CKlearAppException
{
  public:

     /**
     \fn CKlearAppMemoryException()
     \author Marco Kraus <marco@klear.org>
     \brief constructor
     */
     CKlearAppMemoryException();

     /**
     \fn CKlearAppMemoryException()
     \author Marco Kraus <marco@klear.org>
     \brief overloaded constructor
     */
     CKlearAppMemoryException(QString loc, QString msg);

     /**
     \fn ~CKlearAppMemoryException()
     \author Marco Kraus <marco@klear.org>
     \brief destructor
     */
    ~CKlearAppMemoryException();

     /**
     \fn void showExceptions();
     \author Marco Kraus <marco@klear.org>
     \retval void no retval
     \brief show the complete messagestack with its own GUI (independet from environment)
     */
     void showExceptions();

};

#endif
