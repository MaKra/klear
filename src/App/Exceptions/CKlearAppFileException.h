/**
\author Klear.org
\file CKlearAppFileException.h
\brief File error exception for Klear
*/

#ifndef CKLEARAPPFILEEXCEPTION_H
#define CKLEARAPPFILEEXCEPTION_H

#include "CKlearAppException.h"

/**
\class CKlearAppFileException
\author Klear.org
\brief File error exception, derived from Exception-Baseclass
*/
class CKlearAppFileException : public CKlearAppException
{
  public:

     /**
     \fn CKlearAppFileException()
     \author Marco Kraus <marco@klear.org>
     \brief constructor
     */
     CKlearAppFileException();

     /**
     \fn CKlearAppFileException(QString loc, QString msg)
     \author Marco Kraus <marco@klear.org>
     \brief overloaded constructor
     */
     CKlearAppFileException(QString loc, QString msg);

     /**
     \fn ~CKlearAppFileException()
     \author Marco Kraus <marco@klear.org>
     \brief destructor
     */
    ~CKlearAppFileException();

     /**
     \fn void showExceptions();
     \author Marco Kraus <marco@klear.org>
     \retval void no retval
     \brief show the complete messagestack with its own GUI (independet from environment)
     */
     void showExceptions();

};

#endif
