/**
\author Klear.org
\file CKlearAppMemoryException.cpp
\brief Memory error exception for Klear
*/
#include "CKlearAppMemoryException.h"

CKlearAppMemoryException::CKlearAppMemoryException() : CKlearAppException()
{
}

CKlearAppMemoryException::CKlearAppMemoryException(QString loc, QString msg) : CKlearAppException(loc, msg)
{
}

CKlearAppMemoryException::~CKlearAppMemoryException()
{
}

void CKlearAppMemoryException::showExceptions()
{
   CKlearExceptionReporter().showExceptions( this->getExceptionStack(), "CKlearAppMemoryException" );
}

