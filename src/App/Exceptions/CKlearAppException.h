/**
\author Klear.org
\file CKlearAppException.h
\brief The exception classes for klear
Error data segment for ErrorMessage-Stack used by the Klear exception handling
*/

#ifndef CKLEARAPPEXCEPTION_H
#define CKLEARAPPEXCEPTION_H

#include <klocale.h>
#include <kdebug.h>

#include <vector>
#include <iostream>

#include "CKlearExceptionReporter.h"

/**
\author Klear.org
\brief LOC Makro to reference to a Line, File and Function within an exception
*/
#define __LOC__ "At line "+QString::QString(QString::number(__LINE__)+" in file "+__FILE__+" and function "+__FUNC__)

/**
\class ErrorData
\author Klear.org
\brief Error data segment for ErrorMessage-Stack used by the Klear exception handling
*/
class ErrorData
{
   public:
     /**
     \fn ErrorData()
     \author Marco Kraus <marco@klear.org>
     \brief Constructor
     */
     ErrorData();

     /**
     \fn ErrorData(QString loc, QString msg)
     \author Marco Kraus <marco@klear.org>
     \brief overloaded Constructor
     \param loc Locationmessage
     \param msg Errormessage
     */
     ErrorData(QString loc, QString msg)
     {
         this->LocationMessage = loc;
         this->ErrorMessage = msg;
     }

     /**
     \fn ~ErrorData()
     \author Marco Kraus <marco@klear.org>
     \brief Destructor
     */
      ~ErrorData();

     /**
     \fn ErrorMessage
     \author Marco Kraus <marco@klear.org>
     \brief The error message in local class. Access with setter/getter in future
     */
     QString ErrorMessage;

     /**
     \fn LocationMessage
     \author Marco Kraus <marco@klear.org>
     \brief The location message in local class. Access with setter/getter in future.
     */
     QString LocationMessage;
};


/**
\class CKlearAppException
\author Klear.org
\brief The exception baseclass for Klear
*/
class CKlearAppException
{

  public:
     /**
     \fn CKlearAppException();
     \author Marco Kraus <marco@klear.org>
     \brief Constructor
     */
     CKlearAppException();

     /**
     \fn CKlearAppException(QString loc, QString msg);
     \author Marco Kraus <marco@klear.org>
     \brief overloaded Constructor
     \param loc location message
     \param msg error message
     */
     CKlearAppException(QString loc, QString msg);

     /**
     \fn ~CKlearAppException();
     \author Marco Kraus <marco@klear.org>
     \brief Destructor
     */
     virtual ~CKlearAppException();

     /**
     \fn void addErrorMessage(QString loc, QString msg);
     \author Marco Kraus <marco@klear.org>
     \retval void no retval
     \brief Adds a complete error and location message to the message stack
     */
     virtual void addErrorMessage(QString loc, QString msg);

     /**
     \fn void showExceptions();
     \author Marco Kraus <marco@klear.org>
     \retval void no retval
     \brief show the complete messagestack with its own GUI (independet from environment)
     */
     virtual void showExceptions();

     /**
     \fn void addShowException(QString loc, QString msg)
     \author Marco Kraus <marco@klear.org>
     \retval void no retval
     \brief adds a new message to the message stack and shows all
     */
     virtual void addShowException(QString loc, QString msg);

     /**
     \fn std::vector<ErrorData *> getExceptionStack();
     \author Marco Kraus <marco@klear.org>
     \retval std::vector<ErrorData *> The complete Errorstack
     \brief Returns the complete exceptionstack within the vecotr. This method is mainly for debugging purposes.
     */
     virtual std::vector<ErrorData *> getExceptionStack();

  protected:
     /**
     \fn std::vector<ErrorData *> ErrorStack
     \author Marco Kraus <marco@klear.org>
     \brief vector with ErrorData elements for exception-messagestack
     */
     std::vector<ErrorData *> ErrorStack;
};

#endif
