/**
\author Klear.org
\file CKlearAppInputException.h
\brief Input error exception for Klear
*/

#ifndef CKLEARAPPINPUTEXCEPTION_H
#define CKLEARAPPINPUTEXCEPTION_H

#include "CKlearAppException.h"

/**
\class CKlearAppInputException
\author Klear.org
\brief Normal error exception, derived from Exception-Baseclass
*/
class CKlearAppInputException : public CKlearAppException
{
  public:

     /**
     \fn CKlearAppInputException()
     \author Marco Kraus <marco@klear.org>
     \brief constructor
     */
     CKlearAppInputException();

     /**
     \fn CKlearAppInputException(QString loc, QString msg)
     \author Marco Kraus <marco@klear.org>
     \brief overloaded constructor
     */
     CKlearAppInputException(QString loc, QString msg);

     /**
     \fn ~CKlearAppInputException()
     \author Marco Kraus <marco@klear.org>
     \brief destructor
     */
    ~CKlearAppInputException();

     /**
     \fn void showExceptions();
     \author Marco Kraus <marco@klear.org>
     \retval void no retval
     \brief show the complete messagestack with its own GUI (independet from environment)
     */
     void showExceptions();

};

#endif
