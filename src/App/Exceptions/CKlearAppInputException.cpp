/**
\author Klear.org
\file CKlearAppInputException.cpp
\brief Input error exception for Klear
*/
#include "CKlearAppInputException.h"

CKlearAppInputException::CKlearAppInputException() : CKlearAppException()
{
}

CKlearAppInputException::CKlearAppInputException(QString loc, QString msg) : CKlearAppException(loc, msg)
{
}

CKlearAppInputException::~CKlearAppInputException()
{
}

void CKlearAppInputException::showExceptions()
{
   CKlearExceptionReporter().showExceptions( this->getExceptionStack(), "CKlearAppInputException" );
}

