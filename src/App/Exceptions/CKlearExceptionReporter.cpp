/**
\author Klear.org
\file CKlearExceptionReporter.cpp
\brief The klear exceptions reporting classes
*/

#include "CKlearExceptionReporter.h"

CKlearExceptionReporter::CKlearExceptionReporter()
{
}

CKlearExceptionReporter::~CKlearExceptionReporter()
{
}

void CKlearExceptionReporter::showExceptions(std::vector<ErrorData *> ErrorStack)
{
    CKlearExceptionReporter reporter;
    QString Message = i18n("<font color=black><h3><p>Error: ");
    Message += ErrorStack[0]->ErrorMessage+"</h3></font>\n";

    switch( QMessageBox::warning( this, "Klear", Message,i18n("&Close"), i18n("&Show details"), 0, 1 ) )
    {
      case 0:
        break;
      case 1:
        QString Message = i18n("<font color=black><h3><p>Error: </h3>");

        for( unsigned int i = 0; i < ErrorStack.size(); i++)
            Message += i18n("<hr><font color=black><u>At position :</u> <font color=red>")+ErrorStack[i]->LocationMessage+i18n("<br><font color=black><u>Errormessage:</u> <font color=red>")+ErrorStack[i]->ErrorMessage+"<hr>";
        Message += "</h3></font>";

        QMessageBox alert;
        alert.setCaption(i18n("Klear Error"));
        alert.setText(Message);
        alert.setIcon( QMessageBox::Critical );
        alert.setFixedWidth(600);
        alert.adjustSize();
        alert.exec();

        break;
    }

}

void CKlearExceptionReporter::showExceptions(std::vector<ErrorData *> ErrorStack, QString ExceptionType)
{
    CKlearExceptionReporter reporter;
    QString Message = i18n("<font color=black><h3><p>Error: </h3>");
    Message += ErrorStack[0]->ErrorMessage+"</h3></font>\n";

    switch( QMessageBox::warning( this, "Klear", Message,i18n("&Close"), i18n("&Show details"), 0, 1 ) )
    {
      case 0:
        break;
      case 1:
        QString Message = "<font color=black><h3><p>"+ExceptionType+"</h3>";

        for( unsigned int i = 0; i < ErrorStack.size(); i++)
            Message += i18n("<hr><font color=black><u>At position :</u> <font color=red>")+ErrorStack[i]->LocationMessage+i18n("<br><font color=black><u>Errormessage:</u> <font color=red>")+ErrorStack[i]->ErrorMessage+"<hr>";
        Message += "</h3></font>";

        QMessageBox alert;
        alert.setCaption(i18n("Klear Error"));
        alert.setText(Message);
        alert.setIcon( QMessageBox::Critical );
        alert.setFixedWidth(600);
        alert.adjustSize();
        alert.exec();

        break;
    }

}
