/**
\author Klear.org
\file CKlearAppFatalException.cpp
\brief Fatal exception for Klear
*/

#include "CKlearAppFatalException.h"

CKlearAppFatalException::CKlearAppFatalException()  : CKlearAppException()
{
}

CKlearAppFatalException::CKlearAppFatalException(QString loc, QString msg) : CKlearAppException(loc, msg)
{
}

CKlearAppFatalException::~CKlearAppFatalException()
{
}

void CKlearAppFatalException::showExceptions()
{
   std::cout << "Showing Exception..." << std::endl;
   CKlearExceptionReporter().showExceptions( this->getExceptionStack(), "CKlearAppFatalException" );
   std::cout << "Showing Exception...done" << std::endl;

}
