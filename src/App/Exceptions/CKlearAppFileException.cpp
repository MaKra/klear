/**
\author Klear.org
\file CKlearAppFileException.cpp
\brief File error exception for Klear
*/
#include "CKlearAppFileException.h"

CKlearAppFileException::CKlearAppFileException() : CKlearAppException()
{
}

CKlearAppFileException::CKlearAppFileException(QString loc, QString msg) : CKlearAppException(loc, msg)
{
}

CKlearAppFileException::~CKlearAppFileException()
{
}

void CKlearAppFileException::showExceptions()
{
   CKlearExceptionReporter().showExceptions( this->getExceptionStack(), "CKlearAppFileException" );
}

