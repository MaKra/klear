/**
\author Klear.org
\file CKlearAppErrorException.cpp
\brief Error exception for Klear
*/
#include "CKlearAppErrorException.h"

CKlearAppErrorException::CKlearAppErrorException() : CKlearAppException()
{
}

CKlearAppErrorException::CKlearAppErrorException(QString loc, QString msg) : CKlearAppException(loc, msg)
{
}

CKlearAppErrorException::~CKlearAppErrorException()
{
}

void CKlearAppErrorException::showExceptions()
{
   CKlearExceptionReporter().showExceptions( this->getExceptionStack(), "CKlearAppErrorException" );
}

