/**
\author Klear.org
\file CKlearAppException.cpp
\brief The klear exceptions classes
*/

#include "CKlearAppException.h"

CKlearAppException::CKlearAppException()
{
}

CKlearAppException::CKlearAppException(QString loc, QString msg)
{
  this->ErrorStack.push_back( new ErrorData( loc, msg ) );
}

CKlearAppException::~CKlearAppException()
{
}

void CKlearAppException::addErrorMessage(QString loc, QString msg)
{
  this->ErrorStack.push_back( new ErrorData( loc, msg ) );
}

void CKlearAppException::addShowException(QString loc, QString msg)
{
  this->ErrorStack.push_back( new ErrorData( loc, msg ) );
  this->showExceptions();
}

void CKlearAppException::showExceptions()
{
   CKlearExceptionReporter().showExceptions( this->getExceptionStack(), "CKlearAppException" );
}

std::vector<ErrorData *> CKlearAppException::getExceptionStack()
{
   return this->ErrorStack;
}
