/**
\author Klear.org
\file CKlearAppRecorder.h
\brief This File provides a Recorder-Object , which grabs the videostream directly from the device and saves it to a specified file  on HD .
*/

#ifndef CKLEARAPPRECORDER_H
#define CKLEARAPPRECORDER_H

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <string>
#include <fstream>
#include <unistd.h>
#include <fcntl.h>

#include <qstring.h>
#include <qdatetime.h>
#include <qthread.h>
#include <qfile.h>
#include <qdatastream.h>
#include <qmessagebox.h>

#include <kdebug.h>

#include "./CKlearAppConfig.h"
#include "./Exceptions/CKlearAppException.h"       // klear exception class

#include "../Utilities/ts2pes.h"

/**
   Bufferlength for recording
*/
#define BUFFER_LEN 188*8

/**
   TS size for recording
*/
#define TS_SIZE 188

/**
\class CKlearAppRecorder
\author Klear.org
\brief This File provides a Recorder-Object , which grabs the videostream directly from the device and saves it to a specified file  on HD .
*/
class CKlearAppRecorder : public QThread
{

public:
     /**
     \fn void CKlearAppRecorder();
     \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
     \retval void no return value
     \brief Constructor
     */
     CKlearAppRecorder();

     /**
     \fn void CKlearAppRecorder( const QString RecordFile, const CKlearAppConfig *const KlearConfig, const bool isTimeShifted = false );
     \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
     \param RecordFile Path and name of the record file
     \param KlearConfig Pointer to the KlearConfig object, used to extract current settings
     \param isTimeShifted Flag if timeshifting is running
     \retval void no return value
     \brief Constructor which creates a CKlearAppRecorder object using the specified parameters
     */
     CKlearAppRecorder( const QString RecordFile, const CKlearAppConfig *const KlearConfig, const bool isTimeShifted = false );

     /**
     \fn void ~CKlearAppRecorder();
     \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
     \retval void no return value
     \brief Destrucor
     */
     virtual ~CKlearAppRecorder();

     /**
     \fn void run();
     \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
     \retval void no return value
     \brief Threaded start-method, which starts recording by calling the startUp() method
     */
     virtual void run();

     /**
     \fn void StopRecording();
     \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
     \retval void no return value
     \brief method which halts the thread and thus stops any recording in progress
     */
     void StopRecording();

private:
     bool IsRunning;
     Ts2Pes *pesrecorder ;
     const CKlearAppConfig *KlearConfig;
     QString RecordFile;
     QFile *infile;
     QDataStream *instream;
     QString dvbDevice;
     char *BUFFER;
     QString recFormat;
     QString recordfifo;
     int fifowritestream;
     bool isTS;
     void startUp();
     void initRecording();
     void ts_Recording();
     void pes_Recording();

};

#endif
