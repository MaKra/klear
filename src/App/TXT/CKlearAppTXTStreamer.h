#ifndef CKLEARAPPTXTSTREAMER_H
#define CKLEARAPPTXTSTREAMER_H

#include <qthread.h>

#include "CKlearAppTXTDecoder.h"
#include "../CKlearAppConfig.h"

/**
\class CKlearAppTXTStreamer
\author Omar El-Dakhloul <omar@klear.org> with any modified code from dvbsnoop
\brief Filters data from the PAT (Program Association Table), PMT (Program Map Table) and the PES packets containing private data on demux device. It grabs data and passes the buffer to the decoder.
*/
class CKlearAppTXTStreamer : public QThread
{
  public:
    /**
    \fn CKlearAppTXTStreamer( CKlearAppConfig *config, CKlearAppTXTDecoder *decoder )
    \author Omar El-Dakhloul <omar@klear.org>
    \brief Constructor
    */
    CKlearAppTXTStreamer( CKlearAppConfig *config, CKlearAppTXTDecoder *decoder );

    /**
    \fn ~CKlearAppTXTStreamer()
    \author Omar El-Dakhloul <omar@klear.org>
    \brief Destructor
    */
    ~CKlearAppTXTStreamer();

   /**
   \fn void setRunning( bool status )
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \param status A boolean flag
    \brief Sets the flag status of true or false
   */
    void setRunning( bool status );

  private:
   /**
   \fn void setPID( int pid )
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \param pid Number of the Packet Identifier
    \brief Sets the number of the PID (Packet Identifier)
   */
    void setPID( int pid );

   /**
   \fn int getPID()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval int Packet Identifier
    \brief Returns the currently PID (Packet Identifier)
   */
    int getPID();

   /**
   \fn void run()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \brief Starts the thread of CKlearAppTXTStreamer and begin to read the content
   */
    void run();

   /**
   \fn void stopDemux()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \brief Stops the demux and close the file descriptor
   */
    void stopDemux();

   /**
   \fn void setSectionDemux()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \brief Sets the filter, init the demux and alloc the demux buffer for SECTION
   */
    void setSectionDemux();

   /**
   \fn void setPesDemux()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \brief Sets the filter, init the demux and alloc the demux buffer for PES
   */
    void setPesDemux();

   /**
   \fn void readSectionBuffer()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \brief Read Section packet for PID from buffer
   */
    void readSectionBuffer();

   /**
   \fn void readPESBuffer()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \brief Read PES packet for PID from buffer
   */
    void readPESBuffer();

   /**
   \fn void readSectorLoop()
    \author Omar El-Dakhloul <omar@klear.org> with many code from dvbsnoop
    \retval void no return value
    \brief Reads endless the content and synchronize the packets
   */
    long readSectorLoop();

    int pid;
    int fd;
    bool isRunning;
    u_char buf[ READ_BUF_SIZE ];

    CKlearAppConfig *KlearConfig;
    CKlearAppTXTDecoder *TXTDecoder;
};
#endif
