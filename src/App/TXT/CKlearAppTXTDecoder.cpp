#include "CKlearAppTXTDecoder.h"

CKlearAppTXTDecoder::CKlearAppTXTDecoder( CKlearAppConfig *config, QObject *const parent ) : QObject()
{
  const QString __FUNC__ = "CKlearAppTXTDecoder::CKlearAppTXTDecoder()";

  this->secound      = ' ';
  this->row          = 0;
  this->doubleHeight = 0;
  this->number       = "";
  this->pageNumber   = "";

  this->KlearConfig = config;

  headerMutex.lock();
    for ( int i = 0; i < 32; i++ )
      this->TXTHeaderData[ i ] = 20;
  headerMutex.unlock();

  bodyMutex.lock();
    for ( int i = 0; i < 24; i++ )
      for ( int j = 0; j < 40; j++ )
        this->TXTBodyData[ i ][ j ] = 20;
  bodyMutex.unlock();
}

CKlearAppTXTDecoder::~CKlearAppTXTDecoder()
{
  const QString __FUNC__ = "CKlearAppTXTDecoder::~CKlearAppTXTDecoder()";

  delete this->KlearConfig;
}

int CKlearAppTXTDecoder::decodeSectionPAT( u_char *buf )
{
  const QString __FUNC__ = "CKlearAppTXTDecoder::decodeSectionPAT( u_char *buf )";

  typedef struct _PAT
  {
    u_int section_length;
  } PAT;

  typedef struct _PAT_LIST
  {
    u_int program_number;
    u_int network_pmt_PID;
  } PAT_LIST;

  PAT pat;
  PAT_LIST patList;
  int patLength;

  pat.section_length = getBits( buf, 0, 12, 12 );
  patLength = pat.section_length - 9;
  buf = buf + 8;

  for( ; patLength >= 4; patLength = patLength - 4 )
  {
    patList.program_number = getBits( buf, 0, 0, 16 );
    patList.network_pmt_PID = getBits( buf, 0, 19, 13 );
    buf += 4;

    if( patList.program_number == this->KlearConfig->getServiceID() )
    {
      std::cout << "PMT: " << patList.network_pmt_PID << std::endl;
      return patList.network_pmt_PID;
    }
  }
}

int CKlearAppTXTDecoder::decodeSectionPMT( u_char *buf )
{
  const QString __FUNC__ = "CKlearAppTXTDecoder::decodeSectionPMT( u_char *buf )";

  typedef struct _PMT
  {
    int section_length;
    int program_info_length;
  } PMT;

  typedef struct _PMT_LIST
  {
    u_int stream_type;
    u_int elementary_PID;
    int ES_info_length;
  } PMT_LIST;

  PMT pmt;
  PMT_LIST pmtList;
  int sectionLength;
  int programLength;

  pmt.section_length = getBits( buf, 0, 12, 12 );
  pmt.program_info_length = getBits( buf, 0, 84, 12 );

  sectionLength = pmt.section_length - 9;
  programLength = pmt.program_info_length;
  buf += 12;

  while( sectionLength > 4 )
  {
    pmtList.stream_type = getBits( buf, 0,  0, 8 );
    pmtList.elementary_PID = getBits( buf, 0, 11, 13 );
    pmtList.ES_info_length = getBits( buf, 0, 28, 12 );

    if( pmtList.stream_type == 6 )
    {
      std::cout << "TXT: " << pmtList.elementary_PID << std::endl;
      return pmtList.elementary_PID;
    }

    buf += 5;
    sectionLength -= 5;
    programLength = pmtList.ES_info_length;

    while( programLength > 0 )
    {
      int desc;
      desc = descriptor( buf );
      programLength -= desc;
      sectionLength -= desc;
      buf += desc;
    }
  }
}

void CKlearAppTXTDecoder::decodePESTXT( u_char *buf, int len )
{
  const QString __FUNC__ = "CKlearAppTXTDecoder::decodePESTXT( u_char *buf, int len )";

  buf += 6;
  len -= 6;

  typedef struct _PES_Packet
  {
    u_int PTS_DTS_flags;
    u_int PES_header_data_length;
  } PES_Packet;

  PES_Packet p;
  u_char *buf_start = buf;

  p.PTS_DTS_flags          = getBits( buf, 0,  8, 2 );
  p.PES_header_data_length = getBits( buf, 0, 16, 8 );

  buf += 3;

  if( p.PTS_DTS_flags & 0x02 )
    buf += 5;

  {
    int headerLength = p.PES_header_data_length + 3 - ( buf - buf_start );

    if ( headerLength > 0 )
      buf += headerLength;
  }

  {
    u_int data_identifier;

    int headerLength = len - ( buf - buf_start );
    data_identifier = getBits( buf , 0,  0,  8 );

    if( data_identifier >= 0x10 && data_identifier <= 0x1F )
      decodeEBUData( buf, headerLength );
  }
}

void CKlearAppTXTDecoder::decodeEBUData( u_char *buf, int len )
{
  const QString __FUNC__ = "CKlearAppTXTDecoder::decodeEBUData( u_char *buf, int len )";

  buf++;
  len--;

  while( len > 0 )
  {
    int dataLength;
    int dataUnitID;

    dataUnitID = getBits ( buf, 0,  0,  8 );
    dataLength = getBits ( buf, 0,  8,  8 );

    buf += 2;
    len -= 2;

    if ( dataUnitID == 0x02 || dataUnitID == 0x03 || dataUnitID == 0xC0 || dataUnitID == 0xC1 )
    {
      int length;

      buf += 2;
      dataLength -= 2;

      for ( int i = 0; i < 42; i++ )
        *( buf + i ) = invtab[ *( buf + i ) ];

      length = decodeTXTPacketData( buf, 42 );

      buf += length;
      dataLength -= length;
    }

    buf += dataLength;
    len -= dataLength;
  }
}

int CKlearAppTXTDecoder::decodeTXTPacketData( u_char *buf, int len )
{
  const QString __FUNC__ = "CKlearAppTXTDecoder::decodeTXTPacketData( u_char *buf, int len )";

  int x;
  int packet_nr   = -1;
  int mag_nr      = -1;
  int page_nr     = -1;
  int sub_page_nr = -1;

  x = unhamW84( *buf, *( buf + 1 ) );
  packet_nr =  ( x >> 3 ) & 0x1F;
  mag_nr = x & 7;

  if ( ! mag_nr )
    mag_nr = 8;

  page_nr     = ( unhamB84( *( buf + 2 ) ) & 0xF ) | ( ( unhamB84( *( buf + 3 ) ) & 0xF ) << 4 );
  sub_page_nr = ( unhamW84( *( buf + 4 ), *( buf + 5 ) ) | ( unhamW84( *( buf + 6 ), *( buf + 7 ) ) << 8 ) ) & 0x3F7F;

  if( packet_nr == 0 )
  {
    if( this->pageNumber == number )
      emit changedBodyInfo();

    unParityTeletextData( buf + 10, len - 10 );
    setTXTHeaderData( buf + 10, len - 10 );
    searchPage( buf + 10, len - 10 );
    //printHexLine( buf + 10 , len - 10 );
  }

  if( packet_nr == 1 && this->pageNumber == number )
  {
    std::cout << "Seite " << this->number << " gefunden!" << std::endl;

    this->row = 0;

    bodyMutex.lock();
    for ( int i = 0; i < 24; i++ )
      for ( int j = 0; j < 40; j++ )
        this->TXTBodyData[ i ][ j ] = 20;
    bodyMutex.unlock();

    unParityTeletextData( buf + 2, len - 2 );
    setTXTBodyData( buf + 2, len - 2 );
    //printHexLine( buf + 2 , len - 2 );

    return len;
  }

  if( packet_nr >= 2 && packet_nr <= 24 && this->pageNumber == number )
  {
    unParityTeletextData( buf + 2, len - 2 );
    setTXTBodyData( buf + 2, len - 2 );
    //printHexLine( buf + 2 , len - 2 );

    return len;
  }

  if( packet_nr > 24 )
    return len;

  return len;
}

void CKlearAppTXTDecoder::searchPage( u_char *buf, int len )
{
  const QString __FUNC__ = "CKlearAppTXTDecoder::searchPage( u_char *buf, int len )";

  this->number = "";
  for ( int i = 0; i < 3; i++ )
    this->number += buf[ i ];
}

void CKlearAppTXTDecoder::setTXTHeaderData( u_char *buf, int len )
{
  const QString __FUNC__ = "CKlearAppTXTDecoder::setTXTHeaderData( u_char *buf, int len )";

  headerMutex.lock();
  for ( int column = 0; column < 32; column++ )
      this->TXTHeaderData[ column ] = ( int )buf[ column ];
  headerMutex.unlock();

  if ( this->secound != buf[ 31 ] )
  {
    this->secound = buf[ 31 ];
    emit changedHeaderInfo();
  }
}

void CKlearAppTXTDecoder::setTXTBodyData( u_char *buf, int len )
{
  const QString __FUNC__ = "CKlearAppTXTDecoder::setTXTBodyData( u_char *buf, len )";

  if ( 0 != strcmp( ( const char* )buf, "" ) )
  {
    bodyMutex.lock();
    for ( int column = 0; column < 40; column++ )
      this->TXTBodyData[ this->row ][ column ] = ( int )buf[ column ];
    this->row++;
    bodyMutex.unlock();
  }
}

void CKlearAppTXTDecoder::unParityTeletextData( u_char *buf, int len )
{
  const QString __FUNC__ = "CKlearAppTXTDecoder::unParityTeletextData( u_char *buf, int len )";

  for ( int i = 0; i < len; i++ )
    *( buf + i ) = *( buf + i ) & 0x7F;
}

void CKlearAppTXTDecoder::printHexLine( u_char *buf, int len )
{
  const QString __FUNC__ = "CKlearAppTXTDecoder::printHexLine( u_char *buf, int len )";

  for( int i = 0; i < len; i++ )
    out( "%02x ", ( ( int ) buf[ i ] ) );
  std::cout << std::endl;
}

void CKlearAppTXTDecoder::printAsciiLine( u_char *buf, int len )
{
  const QString __FUNC__ = "CKlearAppTXTDecoder::printAsciiLine( u_char *buf, int len )";

  u_char c;

  for ( int i = 0; i < len; i++ )
  {
    c = buf[ i ];
    out ( "%c", isprint( ( int ) c ) ? c : '.' );
  }
}
