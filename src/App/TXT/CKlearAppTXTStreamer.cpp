#include "CKlearAppTXTStreamer.h"

CKlearAppTXTStreamer::CKlearAppTXTStreamer( CKlearAppConfig *config, CKlearAppTXTDecoder *decoder )
{
  const QString __FUNC__ = "CKlearAppTXTStreamer::CKlearAppTXTStreamer()";

  try
  {
    this->pid = 0;
    this->fd = -1;
    this->KlearConfig = config;
    this->TXTDecoder = decoder;
    setRunning( true );
  }
  catch( CKlearAppErrorException &e )
  {
    e.addShowException( __LOC__, i18n( "Could not setup StreamReader\n" ) );
  }
  catch( std::exception &x )
  {
    CKlearAppErrorException e;
    e.addShowException( __LOC__, x.what() );
  }
}

CKlearAppTXTStreamer::~CKlearAppTXTStreamer()
{
  const QString __FUNC__ = "CKlearAppTXTStreamer::~CKlearAppTXTStreamer()";

  this->stopDemux();
  delete this->KlearConfig;
  delete this->TXTDecoder;
}

void CKlearAppTXTStreamer::setRunning( bool status )
{
  this->isRunning = status;
}

void CKlearAppTXTStreamer::setPID( int pid )
{
  const QString __FUNC__ = "CKlearAppTXTStreamer::setPID( int pid )";

  this->pid = pid;
}

int CKlearAppTXTStreamer::getPID()
{
  const QString __FUNC__ = "CKlearAppTXTStreamer::getPID()";

  return this->pid;
}

void CKlearAppTXTStreamer::run()
{
  const QString __FUNC__ = "CKlearAppTXTStreamer::run()";

  try
  {
    setSectionDemux();
    this->readSectionBuffer();
    setSectionDemux();
    this->readSectionBuffer();
    setPesDemux();
    this->readPESBuffer();
  }
  catch( CKlearAppErrorException &e )
  {
    e.addErrorMessage( __LOC__, i18n( "Could not read sector loop\n" ) );
    throw;
  }
  catch( std::exception &x )
  {
    CKlearAppErrorException e( __LOC__, x.what() );
    throw e;
  }
}

void CKlearAppTXTStreamer::stopDemux()
{
  const QString __FUNC__ = "CKlearAppTXTStreamer::stopDemux()";

  ioctl( this->fd, DMX_STOP, 0 );
  close( this->fd );
}

void CKlearAppTXTStreamer::setSectionDemux()
{
  const QString __FUNC__ = "CKlearAppTXTStreamer::setSectionDemux()";

  if( ( this->fd = open( "/dev/dvb/" + this->KlearConfig->getDVBAdapter() + "/" + this->KlearConfig->getDVBDemux(), O_RDWR ) ) < 0 )
  {
    std::cerr << "PAT/PMT: Could not open demux device for reading! Continue without TXT" << std::endl;
    CKlearAppErrorException e( __LOC__,  i18n( "Could not open demux device for reading!\nWill continue without TXT." ) );
    throw e;
  }

  if( ioctl( this->fd, DMX_SET_BUFFER_SIZE, SECT_BUF_SIZE ) < 0 )
  {
    std::cerr << "DMX_SET_BUFFER_SIZE failed" << std::endl;
    this->stopDemux();
    CKlearAppErrorException e( __LOC__,  i18n( "DMX_SET_BUFFER_SIZE for Section failed" ) );
    throw e;
  }

  struct dmx_sct_filter_params flt;
  memset( &flt, 0, sizeof( struct dmx_sct_filter_params ) );
  flt.pid = getPID();
  flt.filter.filter[ 0 ] = 0;
  flt.filter.mask[ 0 ] = 0;
  flt.timeout = 0;
  flt.flags = DMX_IMMEDIATE_START;

  if ( ioctl( this->fd, DMX_SET_FILTER, &flt ) < 0 )
  {
    std::cerr << "DMX_SET_FILTER failed" << std::endl;
    this->stopDemux();
    CKlearAppErrorException e( __LOC__,  i18n( "DMX_SET_FILTER for Section failed" ) );
    throw e;
  }
}

void CKlearAppTXTStreamer::setPesDemux()
{
  const QString __FUNC__ = "CKlearAppTXTStreamer::setPesDemux()";

  if( ( this->fd = open( "/dev/dvb/" + this->KlearConfig->getDVBAdapter() + "/" + this->KlearConfig->getDVBDemux(), O_RDWR ) ) < 0 )
  {
    std::cerr << "TXT: Could not open demux device for reading! Continue without TXT" << std::endl;
    CKlearAppErrorException e( __LOC__,  i18n( "Could not open demux device for reading!\nWill continue without TXT." ) );
    throw e;
  }

  if( ioctl( this->fd, DMX_SET_BUFFER_SIZE, PES_BUF_SIZE ) < 0 )
  {
    std::cerr << "DMX_SET_BUFFER_SIZE failed" << std::endl;
    this->stopDemux();
    CKlearAppErrorException e( __LOC__,  i18n( "DMX_SET_BUFFER_SIZE for PES failed" ) );
    throw e;
  }

  struct dmx_pes_filter_params flt;
  memset( &flt, 0, sizeof( struct dmx_pes_filter_params ) );
  flt.pid = getPID();
  flt.input  = DMX_IN_FRONTEND;
  flt.output = DMX_OUT_TAP;
  flt.pes_type = DMX_PES_OTHER;
  flt.flags = DMX_IMMEDIATE_START;

  if ( ioctl ( this->fd, DMX_SET_PES_FILTER, &flt ) < 0 )
  {
    std::cerr << "DMX_SET_FILTER failed" << std::endl;
    this->stopDemux();
    CKlearAppErrorException e( __LOC__,  i18n( "DMX_SET_FILTER for PES failed" ) );
    throw e;
  }
}

void CKlearAppTXTStreamer::readSectionBuffer()
{
  const QString __FUNC__ = "CKlearAppTXTStreamer::readSectionBuffer()";

  int readBuffer;
  int sectionLength;

  try
  {
    readBuffer = read( this->fd, this->buf, 3 );
    sectionLength = ( ( buf[1] & 0x0F ) << 8 ) + buf[ 2 ];
  }
  catch( std::exception &x )
  {
    CKlearAppErrorException e( __LOC__, x.what() );
    throw e;
  }

  if ( readBuffer <= 0 )
    std::cerr << "Error or strange!" << std::cout;

  if ( sectionLength > ( sizeof( this->buf ) - 3 ) )
    std::cerr << " Something odd!" << std::cout;

  try
  {
    readBuffer = read( this->fd, this->buf + 3, sectionLength );
  }
  catch( std::exception &x )
  {
    CKlearAppErrorException e( __LOC__, x.what() );
    throw e;
  }

  if ( readBuffer >= 0 )
    readBuffer += 3;  // we already read header bytes
  try
  {
    if ( getPID() == 0 )
      setPID( this->TXTDecoder->decodeSectionPAT( this->buf ) );
    else
      setPID( this->TXTDecoder->decodeSectionPMT( this->buf ) );
  }
  catch( CKlearAppErrorException &e )
  {
    e.addErrorMessage( __LOC__, i18n( "Could not decode buffer\n" ) );
    throw;
  }
  catch( std::exception &x )
  {
    CKlearAppErrorException e( __LOC__, x.what() );
    throw e;
  }
}

void CKlearAppTXTStreamer::readPESBuffer()
{
  const QString __FUNC__ = "CKlearAppTXTStreamer::readPESBuffer()";

  long len;

  while( isRunning )
  {
    try
    {
      len = readSectorLoop(); // modifies this->buf with demux data
    }
    catch( CKlearAppErrorException &e )
    {
      e.addErrorMessage( __LOC__, i18n( "Could not read sector loop\n" ) );
      throw;
    }
    catch( std::exception &x )
    {
      CKlearAppErrorException e( __LOC__, x.what() );
      throw e;
    }

    if ( len < 0 )
    {
      if ( errno == ETIMEDOUT )
        break;
      else
        continue;
    }

    if ( len == 0 )
      continue;  // dmxmode = no eof!

    try
    {
      this->TXTDecoder->decodePESTXT( this->buf, len );
    }
    catch( CKlearAppErrorException &e )
    {
      e.addErrorMessage( __LOC__, i18n( "Could not decode buffer\n" ) );
      throw;
    }
    catch( std::exception &x )
    {
      CKlearAppErrorException e( __LOC__, x.what() );
      throw e;
    }
  }
}

long CKlearAppTXTStreamer::readSectorLoop()
{
  const QString __FUNC__ = "CKlearAppTXTStreamer::readSectorLoop()";

  long n1;
  long n2;
  long len;
  u_long skipped_bytes = 0;
  u_long sync = 0xFFFFFFFF;

  buf[0] = 0x00;
  buf[1] = 0x00;
  buf[2] = 0x01;

  while( true )
  {
    u_char c;

    try
    {
      n1 = read( this->fd, this->buf + 3, 1 );
    }
    catch( std::exception &x )
    {
      CKlearAppErrorException e( __LOC__, x.what() );
      throw e;
    }

    if( n1 <= 0 )
      return n1;

    c = buf[ 3 ];
    sync = ( sync << 8 ) | c;

    if( ( sync & 0xFFFFFF00 ) == 0x00000100 )
    {
      if( c == 0xB9 )  // MPEG_program_end
      {
        skipped_bytes -= 3;
        return 4;
      }

      if( c == 0xBB )
        break;  // system_header_start  (same as PES packet)

      if( c >= 0xBC )
        break;  // PES packet
    }
    ( skipped_bytes )++;  // sync skip counter
  }

  skipped_bytes -= 3;

  try
  {
    n1 = read( this->fd, buf + 4, 2 );
  }
  catch( std::exception &x )
  {
    CKlearAppErrorException e( __LOC__, x.what() );
    throw e;
  }

  if ( n1 == 2 )
  {
    len = ( buf[ 4 ] << 8 ) + buf[ 5 ];  // PES packet size...
    n1 = 6;

    if ( len > 0 )
    {
      try
      {
        n2 = read( this->fd, buf + 6, ( unsigned int ) len );
      }
      catch( std::exception &x )
      {
        CKlearAppErrorException e( __LOC__, x.what() );
        throw e;
      }
      n1 = ( n2 < 0 ) ? n2 : n1 + n2;
    }
  }
  return n1;
}
