#ifndef CKLEARAPPTXTDECODER_H
#define CKLEARAPPTXTDECODER_H

#include <iostream>
#include <qobject.h>
#include <qmutex.h>

#include "../CKlearAppDecodingHelper.h"
#include "../CKlearAppConfig.h"


/**
\class CKlearAppTXTDecoder
\author Omar El-Dakhloul <omar@klear.org> with many modified code from dvbsnoop
\brief Gets the read buffer and decodes the PAT (Program Association Table), PMT (Program Map Table) and the PES packets containing private data.
*/
class CKlearAppTXTDecoder : public QObject
{
  Q_OBJECT
  public:
    /**
    \fn CKlearAppTXTDecoder( CKlearAppConfig *config, QObject *const parent = 0 )
    \author Omar El-Dakhloul <omar@klear.org>
    \brief Constructor
    */
    CKlearAppTXTDecoder( CKlearAppConfig *config, QObject *const parent = 0 );

    /**
    \fn ~CKlearAppTXTDecoder()
    \author Omar El-Dakhloul <omar@klear.org>
    \brief Destructor
    */
    ~CKlearAppTXTDecoder();

   /**
   \fn int decodeSectionPAT( u_char *buf )
    \author Omar El-Dakhloul <omar@klear.org>  with modified code from dvbsnoop
    \retval void no return value
    \param buf The buffer
    \brief Decodes the section of PAT (Program Association Table) for the teletext.
   */
    int decodeSectionPAT( u_char *buf );

   /**
   \fn int decodeSectionPMT( u_char *buf )
    \author Omar El-Dakhloul <omar@klear.org> with modified code from dvbsnoop
    \retval void no return value
    \param buf The buffer
    \brief Decodes the section of PMT (Program Map Table) for the teletext.
   */
    int decodeSectionPMT( u_char *buf );

   /**
   \fn void decodePESTXT( u_char *buf, int len )
    \author Omar El-Dakhloul <omar@klear.org>  with modified code from dvbsnoop
    \retval void no return value
    \param buf The buffer
    \param len The length of the buffer
    \brief Decodes the Packet Elementary Stream (PES) for the teletext.
   */
    void decodePESTXT( u_char *buf, int len );

    QMutex headerMutex;
    int TXTHeaderData[ 32 ];
    QMutex bodyMutex;
    int TXTBodyData[ 24 ][ 40 ];
    int doubleHeight;
    char secound;
    QString pageNumber;
    QString number;

  signals:
   /**
   \fn void changedHeaderInfo()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \brief Send a signal if the header information of the teletext are changed.
   */
    void changedHeaderInfo();

   /**
   \fn void changedBodyInfo()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \brief Send a signal if the body information of the teletext are changed.
   */
    void changedBodyInfo();

  private:

   /**
   \fn void decodeEBUData( u_char *buf, int len )
    \author Omar El-Dakhloul <omar@klear.org> with modified code from dvbsnoop
    \retval void no return value
    \param buf The buffer
    \param len The length of the buffer
    \brief Decodes the Packet Elementary Stream (PES) of the EBU-Data (European Broadcast Unit) for the teletext.
   */
    void decodeEBUData( u_char *buf, int len );

   /**
   \fn void decodeTXTPacketData( u_char *buf, int len )
    \author Omar El-Dakhloul <omar@klear.org> with modified code from dvbsnoop
    \retval void no return value
    \param buf The buffer
    \param len The length of the buffer
    \brief Decodes the Packet Elementary Stream (PES) of the Packet-Data for the teletext.
   */
    int decodeTXTPacketData( u_char *buf, int len );

   /**
   \fn void searchPage( u_char *buf, int len )
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \param buf The buffer
    \param len The length of the buffer
    \brief Search the enter page of the teletext.
   */
    void searchPage( u_char *buf, int len );

   /**
   \fn void setTXTHeaderData( u_char *buf, int len )
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \param buf The buffer
    \param len The length of the buffer
    \brief Sets the header data of the Teletext in an array.
   */
    void setTXTHeaderData( u_char *buf, int len );

   /**
   \fn void setTXTBodyData( u_char *buf, int len )
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \param buf The buffer
    \param len The length of the buffer
    \brief Sets the body data of the Teletext in an array.
   */
    void setTXTBodyData( u_char *buf, int len );

   /**
   \fn void unParityTeletextData( u_char *buf, int len )
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \param buf The buffer
    \param len The length of the buffer
    \brief Controls the unparity data of the teletext.
   */
    void unParityTeletextData( u_char *buf, int len );

   /**
   \fn void printHexLine( u_char *buf, int len )
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \param buf The buffer
    \param len The length of the buffer
    \brief Prints the data of the buffer in hex form
   */
    void printHexLine( u_char *buf, int len );

   /**
   \fn void printAsciiLine( u_char *buf, int len )
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \param buf The buffer
    \param len The length of the buffer
    \brief Prints the data of the buffer in ascii form
   */
    void printAsciiLine( u_char *buf, int len );

    int row;

    CKlearAppConfig *KlearConfig;
};
#endif
