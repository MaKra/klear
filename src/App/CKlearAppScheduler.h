/**
\author Klear.org
\file CKlearAppScheduler.h
\brief Class which handles and organizes recording.

*/
#ifndef CKLEARAPPSCHEDULER_H
#define CKLEARAPPSCHEDULER_H

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <string>

#include <algorithm>
#include <vector>
#include <fstream>

#include <qdatetime.h>
#include <qthread.h>
#include <qstring.h>
#include <qfileinfo.h>
#include <qdir.h>

#include "CKlearAppRecorder.h"
#include "CKlearAppRecordSet.h"
#include "CKlearAppConfig.h"

#include "Tuner/CKlearAppTuner.h"

#include "Exceptions/CKlearAppException.h"       // klear exception class
#include "Exceptions/CKlearAppErrorException.h"       // klear exception class
#include "Exceptions/CKlearAppFatalException.h"       // klear exception class
#include "Exceptions/CKlearAppInputException.h"       // klear exception class
#include "Exceptions/CKlearAppFileException.h"       // klear exception class

#include <qapplication.h>
#include <qmessagebox.h>
#include <qwidget.h>

#include <kdebug.h>

/**
\class CKlearAppScheduler
\author Klear.org
\brief Class which handles and organizes recording.
*/
class CKlearAppScheduler : public QObject, public QThread
{

Q_OBJECT

public:

   /**
   \fn void CKlearAppScheduler(CKlearAppConfig *const KlearConfig, CKlearAppTuner *const KlearTuner, QObject *const parent );
   \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
   \param   KlearConfig pointer to CKlearAppConfig Object.
   \param   KlearTuner pointer to CKlearAppTuner Object for inner tuning by scheduler.
   \param   parent   pointer to MainDialog used only for interprocess communication via slots (to be able to combine emit with postevent to enable qt to communicate with an other threat in version 3.x since otherwise qt has problems in doing so ).
   \retval void no return value
   \brief  Constructor for CKlearAppScheduler Object
   */
   CKlearAppScheduler(  CKlearAppConfig  *const KlearConfig,  CKlearAppTuner  *const KlearTuner,  QObject  *const parent );


   /**
   \fn void ~CKlearAppScheduler();
   \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
   \retval void no return value
   \brief  Destructor for CKlearAppScheduler Object
   */
   virtual ~CKlearAppScheduler();


   /**
   \fn void run();
   \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
   \retval void no return value
   \brief  method called when threat is started evokes mainloop to manage recording
   */
   virtual void run();

   /**
   \fn void stop();
   \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
   \retval void no return value
   \brief  method called to stop the thread, and thus any scheduling functions
   */
   void stop();

   /**
   \fn void startQuickRecording();
   \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
   \retval void no return value
   \brief  creates an recordset with currentTime as StartTime, i.e. starts instand recording
    */
    void startQuickRecording();


   /**
   \fn void addRecordSet( const QString RecordFile, const QDateTime startDateTime, const QDateTime endDateTime, const QString channel );
   \author  Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
   \param   RecordFile    the Filename and Path of the recent RecordSet as QString.
   \param   startDateTime the sec. exact starttime and date for the new RecordObject represented as QDateTime.
   \param   endDateTime   the sec. exact endtime and date for the new RecordObject represented as QDateTime.
   \param   channel       the channel the recorder shall record from when currenttime equals starttime.
   \retval  void          no return value
   \throws  CKlearAppInputException Is thrown if specified start/end.times are not valid or if the channel is is not in channels.conf
   \brief   creates a new Recordset and inserts it to the RecordStorage vector from where it will be checked wheater to start or end recording every second if recordset is the "current - one".
   */
   void addRecordSet( const QString RecordFile, const QDateTime startDateTime, const QDateTime endDateTime, const QString channel);


   /**
   \fn void abortRecording();
   \author  Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
   \retval  void no return value
   \brief   stops the corresponding recorder, waits for it's thread to end and therafter deletes the recorder object by calling removeRecordset() and removes RecordSet from RecordStorage vector.
   */
   void abortRecording();


   /**
   \fn void removeRecordSet( const int i );
   \author  Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
   \param   i is handeled as the i'th element from index of the Recordstorage as int.
   \retval  void no return value
   \brief   Method call deletes the i 'th element of the RecordStorage vector and the corresponding RecordSetObject.
   */
   void removeRecordSet( const int i);


   /**
   \fn QString getRecordFile();
   \author  Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
   \retval  QString path and name of the recordfile
   \brief   Returns the Filename and Path of either the currently active or next scheduled RecordSet as QString.
   */
   QString getRecordFile();


  /**
  \fn void WriteScheduledRecords();
  \author  Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
  \retval  void no return value
  \brief   Saves all scheduled RecordSets into the file KlearScheduledDatafile.dat which will have to be proccessed in future. Will be loaded at the next klear program launch.
  */
  void WriteScheduledRecords();


  /**
  \fn bool isRecordInProgress()
  \author  Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
  \retval  bool true when Scheduler is in its recording-phase.
  \brief   returns true when Scheduler is at the moment handling a record, false when no record is in progress.
  */
  bool isRecordInProgress();


  /**
  \fn int getRecordStorageSize();
  \author Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
  \retval int size of vector RecordStorage.
  \brief  returns the size of the vector RecordStorage i.e. the number of existing RecordSets as int.
  */
  int getRecordStorageSize();


  /**
  \fn CKlearAppRecordSet* getRecordSet( const int i );
  \author  Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
  \param   i specifies the index of the desired element in the recordstorage
  \retval  CKlearAppRecordSet* pointer to the CKlearAppRecordSetobject specified by parameter i
  \brief   returns pointer to the i'ths CKlearAppRecordSetobject from the RecordStorage vector.
  */
  CKlearAppRecordSet* getRecordSet(const int i);

    /**
  \fn void isValidRecordSet( const CKlearAppRecordSet *const rs ) const;
  \author  Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
  \param   rs the recordset to be validated
  \retval  void no return value
  \throws  CKlearAppInputException Is thrown if the recordset's specified start/end.times are not valid or if the channel is is not in channels.conf
  \brief   checks the recordset for validity of its attributes i.e. valid date etc.
  */
  void isValidRecordSet( const CKlearAppRecordSet *const rs ) const;

    /**
     \fn bool isTimeShifted()
     \author Patric Sherif <patric@klear.org> and Manuel Habermann <manuel@klear.org>
     \retval bool returns true when Timeshifting is activated
     \brief returns timeshifting status
     */
  bool isTimeShifted();

    /**
     \fn void setTimeShifted( bool timeShifting )
     \author Patric Sherif <patric@klear.org> and Manuel Habermann <manuel@klear.org>
     \retval void no return value
     \param  timeShifting timeshift status
     \brief sets timeshifting status
     */
  void setTimeShifted( bool timeShifting );

signals:

   /**
   \fn void signalStoppingScheduled();
   \author  Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
   \retval  void no return value
   \brief   emits an signal to maindialog that Recording is over, thus maindialog can for example switch button state or react on the event in general.
   */
   void signalStoppingScheduled();

  /**
  \fn void signalStartingScheduled();
  \author  Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
  \retval  void no return value
  \brief   emits signal to maindialog that Recording will now start and Maindialog can swith button state of record button ect.
  */
  void signalStartingScheduled();

  /**
  \fn void signalPushQueue();
  \author  Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
  \retval  void no return value
  \brief   emits signal to maindialog to switch playback between live and recorded file or vice versa.
  */
  void signalPushQueue();

  /**
  \fn void signalCloseMain();
  \author  Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
  \retval  void no return value
  \brief   emits signal to maindialog to shutdown the system.
  */
  void signalCloseMain();


private:
  CKlearAppRecordSet *current;
  bool RecordInProgress;
  bool IsRunning;
  bool isQuick;
  bool timeShifting;
  std::vector<CKlearAppRecordSet *> RecordStorage;

  CKlearAppRecorder *Recorder;
  CKlearAppConfig  *KlearConfig;
  QObject  *parent;
  CKlearAppTuner  *KlearTuner;

  void initRecordSet();
  void removeRecordSet();
  void sortRecordSets();
  void ParseScheduledRecords( const QString recordDataLine );
  void startUp();
  void startRecording();
  void stopRecording();

};

#endif
