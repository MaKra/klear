/**
\author Klear.org
\file config.h
\brief Configuration class for compilation
*/

// EIT settings
#define EIT_PID 0x012
#define SECT_BUF_SIZE (256*1024)    /* default DMX buffer size */
#define READ_BUF_SIZE (32*1024)     /* section work buffer (section is @5K) */
#define PES_BUF_SIZE (256 * 1024)      /* default DMX buffer size */

// define current klear version
#define __KLEARVERSION__ "0.6.1"

// the developers change this value if config-file format changed with a new release
#define __CONFIGVERSION__ "19"

// compile klear with Xine-Engine-Support, comment out when not wanted
#define COMPILE_XINE_ENGINE

// compile klear with MPlayer-Engine-Support, comment out when not wanted
//#define COMPILE_MPLAYER_ENGINE
