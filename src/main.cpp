/**
\file main.cpp
\author Klear.org
\brief Main file
Main file for Klear DVB application
*/

#include <kapplication.h>
#include <kaboutdata.h>
#include <kcmdlineargs.h>
#include <klocale.h>

#include <kpopupmenu.h>
#include <ksystemtray.h>

#include <qpopupmenu.h>
#include <qiconset.h>
#include <qpixmap.h>
#include <qdir.h>
#include <qobject.h>
#include <qsplashscreen.h>

#include <qtranslator.h>
#include <qtextcodec.h>
#include <qapplication.h>
#include <qstatusbar.h>

#include "config.h"

#include "App/CKlearAppTray.h"
#include "GUI/Controller/CKlearControllerMain.h"

static const char description[] = I18N_NOOP("Klear - DVB TV application and harddiskrecorder for Linux");
static const char version[] = __KLEARVERSION__;
static KCmdLineOptions options[] = { KCmdLineLastOption };

int main(int argc, char **argv)
{
    KAboutData about("klear", "Klear", version, description,  KAboutData::License_Custom, "(C) %{YEAR} Klear.org", 0, 0, "developers@klear.org");
    about.addAuthor( "Klear.org", 0, "developers@klear.org" );
    KCmdLineArgs::init(argc, argv, &about);
    KCmdLineArgs::addCmdLineOptions( options );
    KApplication app;

    // translation file for Qt
    //QTranslator translator( 0 );
    //translator.load( "klear."+QString( QTextCodec::locale() ), QDir::homeDirPath()+"/.klear/translations" );
    //app.installTranslator( &translator );

    CKlearControllerMain *mainWin = 0;

    // start up with splash screen
    QSplashScreen *splash = new QSplashScreen( QPixmap::QPixmap(KlearSplashScreen) );
    splash->show();

    std::cout << "Starting up Klear version " << version << std::endl;
    KCmdLineArgs *args = KCmdLineArgs::parsedArgs();

    QPixmap aboutPX( aboutPixmap );
    QPixmap configurePX( configurePixmap );
    QPixmap tmp( hi16LogoPixmap );

    mainWin = new CKlearControllerMain();
    mainWin->setIcon(tmp);
    mainWin->statusBar()->hide();

    CKlearAppTray *tray = new CKlearAppTray( mainWin, "Klear-Systemtray" );
    tray->setPixmap( tmp );

    QObject::connect( mainWin, SIGNAL ( signalCloseTray() ), tray, SLOT ( slotQuitTray() ) );

    // create the menu for system tray
    KPopupMenu *menutray;
    menutray = tray->contextMenu();

    QIconSet *aboutIcon = new QIconSet( aboutPX );
    QIconSet *configureIcon = new QIconSet( configurePX );

    menutray->removeItemAt(0);
    menutray->insertTitle( tmp, "Klear", -1,0 );
    menutray->insertItem( *aboutIcon, "&About Klear", mainWin, SLOT( slotAboutKlear() ), Qt::Key_A, -1, 1 );
    menutray->insertItem( *configureIcon, "&Configure", mainWin, SLOT( slotConfigDialog() ), Qt::Key_C, -1, 2 );
    menutray->insertSeparator( 3 );

    tray->show();
    app.setMainWidget( mainWin );

    mainWin->show();

    // finish and delete spalsh screen
    splash->finish( mainWin );
    delete splash;

    args->clear();

    // mainWin has WDestructiveClose flag by default, so it will delete itself.
    return app.exec();
}
