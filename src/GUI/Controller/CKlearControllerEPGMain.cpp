#include <iostream>
#include <qlistview.h>
#include "CKlearControllerEPGMain.h"

CKlearControllerEPGMain::CKlearControllerEPGMain(std::vector<CKlearAppEITData *> *storage, QString chan, CKlearControllerSchedulerOverview *SOD, CKlearAppConfig *conf, QWidget* parent, const char* name, WFlags fl) : CKlearUIEPGMain(parent,name,fl)
{
  this->EITDataVector = storage; // map storage vector to local member (static is still thread local, so no direct access possible)
  this->SchedulerOverviewDialog = SOD; // map SchedulerOverviewDialog to local var.
  this->KlearConfig = conf;

  this->slotRefreshView(); // show all already collected data on GUI startup
}


CKlearControllerEPGMain::~CKlearControllerEPGMain()
{
}


void CKlearControllerEPGMain::slotRebuildDatabase()
{
  std::cout << "Cleaning up data storage vector" << std::endl;

  CKlearAppDataLayer *DataLayer = new CKlearAppDataLayer( this->EITDataVector );
  DataLayer->cleanUpDatabase();
  delete DataLayer;

  std::cout << "data storage empty" << std::endl;

  EPGDateBox->clear(); // clear up date box

   // set today to date selector list
  QListViewItem *i = new QListViewItem( EPGProgramDataBox, QDateTime::currentDateTime().date().toString(Qt::ISODate) );
  EPGDateBox->insertItem( i );
  EPGDateBox->setSelected( i, true );

  this->slotRefreshView(); // clean up GUI and refresh view
}


void CKlearControllerEPGMain::slotRefreshView()
{
  std::cout << "Refreshing GUI with collected vector data" << std::endl;

  // set channelname to EPG label header
  EPG_Label->setText( i18n("<h1><font color=red><b>E</b><font color=black>lectronic <font color=blue><b>P</b><font color=black>rogram <font color=green><b>G</b><font color=black>uide</font></h1>") );
  EPG_Label->setText(QString(EPG_Label->text()+i18n("<h2>for ")+this->KlearConfig->getCurrentChannel()+"</h2>"));

  // cleaning up fields
  EPGProgramDataBox->clear(); // clean up programm item window
  EPGLongDescBox->clear(); // clean up description box

  std::cout << "Vectorsize: " << this->EITDataVector->size() << std::endl;

  std::cout << "Timeshift calculated: " << QDateTime::currentDateTime( Qt::LocalTime ).toTime_t() - QDateTime::currentDateTime( Qt::UTC ).toTime_t()  << std::endl;
  int TIMESHIFT = QDateTime::currentDateTime( Qt::LocalTime ).toTime_t() - QDateTime::currentDateTime( Qt::UTC ).toTime_t();

   QListViewItem *markitem = NULL;

   for( std::vector<CKlearAppEITData *>::iterator it = this->EITDataVector->begin(); it != this->EITDataVector->end(); ++it )
   {
         this->ST.setTime_t( (*it)->StartTime+TIMESHIFT );
         if( ! isInDateList() ) // check currently set ST
         {
               EPGDateBox->insertItem( new QListViewItem( EPGDateBox, this->ST.date().toString(Qt::ISODate) ) ); // add to date selector list
         }

         if( EPGDateBox->currentItem()->key(0, true) ==  this->ST.date().toString(Qt::ISODate) ) // if data are from selected date
         {
            // check if item is current running
             if( ( (*it)->StartTime < QDateTime::currentDateTime(Qt::UTC).toTime_t() )  &&
                  ( (*it)->StartTime+(*it)->Duration > QDateTime::currentDateTime(Qt::UTC).toTime_t() ) )
            {
                  markitem = new QListViewItem( EPGProgramDataBox, ST.toString("hh:mm"), (*it)->ShortDesc  );
            }else{
                  // add item to item box
                  EPGProgramDataBox->insertItem( new QListViewItem( EPGProgramDataBox, ST.toString("hh:mm"), (*it)->ShortDesc  ) );
            }
         }
    }

     if( markitem != NULL ){  // if current running item was found within current window
            EPGProgramDataBox->setSelected(markitem, true);   // mark it selected
            EPGProgramDataBox->ensureItemVisible ( markitem ); // auto scroll to item
    }
}


bool CKlearControllerEPGMain::isInDateList()
{
   QListViewItemIterator it( this->EPGDateBox );
   while ( it.current() )
   {
        if( this->ST.date().toString(Qt::ISODate) == it.current()->key(0,true) )
                   return true;
        ++it;
   }

   return false;
}


void CKlearControllerEPGMain::slotShowItemDetails()
{
   QDateTime TempDateStart;  // temp. Date and Time object to identify selected object
   QDateTime TempDateEnd;  // temp. Date and Time object to claculate end time

   int TIMESHIFT = QDateTime::currentDateTime ( Qt::LocalTime ).toTime_t() -   QDateTime::currentDateTime ( Qt::UTC ).toTime_t();

   for( std::vector<CKlearAppEITData *>::iterator it = this->EITDataVector->begin(); it != this->EITDataVector->end(); ++it )
   {
       TempDateStart.setTime_t( (*it)->StartTime+TIMESHIFT );

       if(  ( TempDateStart.toString("hh:mm") == EPGProgramDataBox->currentItem()->text(0) )  &&
            ( TempDateStart.date().toString(Qt::ISODate) ==  EPGDateBox->currentItem()->key(0,true) ) )
      {

            // calculate end-time
            TempDateEnd.setTime_t( (*it)->StartTime+TIMESHIFT+(*it)->Duration );

            // object found, now add text
            QString LongDescription("");

            LongDescription = LongDescription + "<h3>" + (*it)->ShortDesc + "</h3>"; // program name

            //LongDescription = LongDescription + this->currentChannel + "<br>"; // channel name

            LongDescription = LongDescription + "<h4>" + TempDateStart.time().toString("hh:mm") + " - " + TempDateEnd.time().toString("hh:mm") + i18n(" o'clock "); // time
            LongDescription = LongDescription + "( " + QString().setNum( (*it)->Duration/60 )  + i18n(" min. )</h4>"); // duration

/*
            LongDescription = LongDescription + "<i>Genre:</i> " + findTableID(ContentTable, (*it)->Genre) + "<br>"; // Genre
            LongDescription = LongDescription + "<i>Age:</i> " + findTableID(ParentalRatingTable, (*it)->Rating) + "<br>"; // Age
            LongDescription = LongDescription + "<i>Conditional access:</i> " + QString().setNum((*it)->CA) + "<br><p>"; // CA
*/

            LongDescription = LongDescription+ (*it)->LongDesc;  // desc.

            EPGLongDescBox->setText( LongDescription );
       }
   }
}


void CKlearControllerEPGMain::slotActivateScheduledRecording()
{
   std::cout << "Starting scheduled recording for item" << std::endl;

   QDateTime TempDateStart;  // temp. Date and Time object to identify selected object
   QDateTime TempDateEnd;  // temp. Date and Time object to claculate end time

   int TIMESHIFT = QDateTime::currentDateTime ( Qt::LocalTime ).toTime_t() -   QDateTime::currentDateTime ( Qt::UTC ).toTime_t();

   for( std::vector<CKlearAppEITData *>::iterator it = this->EITDataVector->begin(); it != this->EITDataVector->end(); ++it )
   {
       TempDateStart.setTime_t( (*it)->StartTime+TIMESHIFT);

       if(  ( TempDateStart.toString("hh:mm") == EPGProgramDataBox->currentItem()->text(0) )  &&
            ( TempDateStart.date().toString(Qt::ISODate) ==  EPGDateBox->currentItem()->key(0,true) ) )
      {   // if start time was found in vector...
            // .. now add margins
            TempDateStart.setTime_t( (*it)->StartTime+TIMESHIFT-(this->KlearConfig->getPreEPGmargin()*60) );
            // ....calculate end-time with duration and margin
            TempDateEnd.setTime_t( (*it)->StartTime+TIMESHIFT+(*it)->Duration+(this->KlearConfig->getPostEPGmargin()*60) );
            SchedulerOverviewDialog->slotAddRecordSet( this->KlearConfig->getCurrentChannel(), TempDateStart, TempDateEnd, this->KlearConfig->getRecordingDir()+"/"+(*it)->ShortDesc);
            break;
       }
   }

}


void CKlearControllerEPGMain::slotCloseEPGwindow()
{
   this->close();
}
