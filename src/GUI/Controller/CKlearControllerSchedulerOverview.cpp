/**
\author Klear.org
\file CKlearControllerSchedulerOverview.cpp
\brief The scheduler overview dialog implementation for Klear.
*/


#include "./CKlearControllerSchedulerOverview.h"

CKlearControllerSchedulerOverview::CKlearControllerSchedulerOverview( CKlearAppScheduler *KlearScheduler, QWidget* parent, const char* name, WFlags fl )
    : CKlearUISchedulerOverview( parent, name, fl )
{
    this->KlearScheduler = KlearScheduler;
    SchedulerDialog = NULL;
    KlearRecordSet = NULL;
    item = NULL;
}

CKlearControllerSchedulerOverview::~CKlearControllerSchedulerOverview()
{
}


void CKlearControllerSchedulerOverview::loadListView()
{
     kdDebug() << "Loading scheduled data file to GUI\n";
     listView->clear();
     listView->setSortColumn( -1 );

     for( int i = 0; i < KlearScheduler->getRecordStorageSize(); i++ )
     {
        item = new QListViewItem( listView, listView->lastItem() );
        KlearRecordSet = KlearScheduler->getRecordSet(i);

        item->setText( 0, KlearRecordSet->getChannel() );
        item->setText( 1, KlearRecordSet->getStartDateTime().toString() );
        item->setText( 2, KlearRecordSet->getEndDateTime().toString() );
        item->setText( 3, KlearRecordSet->getRecordFile() );
     }

     if( KlearScheduler->getRecordStorageSize() == 0 )
     {
          editButton->setEnabled( FALSE );
          removeButton->setEnabled( FALSE );
     }else{
          editButton->setEnabled( TRUE );
          removeButton->setEnabled( TRUE );
     }
}

void CKlearControllerSchedulerOverview::slotSaveRecordSet()
{
    const QString __FUNC__ = "CKlearControllerSchedulerOverview::slotSaveRecordSet()";

    try{
        KlearScheduler->WriteScheduledRecords();
    }catch ( CKlearAppException &e ){   // Wir fangen eine Referenz
        e.addShowException( __LOC__, i18n("Could not save recordsets to disk \n") );
    }
    this->close();
}

void CKlearControllerSchedulerOverview::slotCancelRecordSet()
{
    this->close();
}

void CKlearControllerSchedulerOverview::slotAddRecordSet()
{
    const QString __FUNC__ = "CKlearControllerSchedulerOverview::slotAddRecordSet()";

   try{
       SchedulerDialog = new CKlearControllerScheduler( KlearScheduler );
   }catch( CKlearAppException &e ){
          e.addShowException( __LOC__, i18n("Could not create ControllerScheduler dialog." ));
          exit(-1);
   }

    SchedulerDialog->exec();

    this->setActiveWindow();
    this->raise();
    this->loadListView();
}

void CKlearControllerSchedulerOverview::slotAddRecordSet(QString chan, QDateTime start, QDateTime end, QString recname)
{
    const QString __FUNC__ = "CKlearControllerSchedulerOverview::slotAddRecordSet()";

   try{
       SchedulerDialog = new CKlearControllerScheduler( KlearScheduler, chan, start, end, recname );
   }catch( CKlearAppException &e ){
          e.addShowException( __LOC__, i18n("Could not create ControllerScheduler dialog.") );
          exit(-1);
   }

    SchedulerDialog->exec();

    this->setActiveWindow();
    this->raise();
    this->loadListView();
}

void CKlearControllerSchedulerOverview::slotEditRecordSet()
{

    for ( int i = 0; i < KlearScheduler->getRecordStorageSize(); i++ )
    {
        KlearRecordSet = KlearScheduler->getRecordSet( i );


        if ( listView->currentItem()->text(1) == KlearRecordSet->getStartDateTime().toString() )
        {
             CKlearAppRecordSet tempRec = *KlearRecordSet;
             KlearScheduler->removeRecordSet( i );
             SchedulerDialog = new CKlearControllerScheduler( KlearScheduler, tempRec );
        }
    }
    SchedulerDialog->exec();
    this->setActiveWindow();
    this->raise();
    loadListView();
    //KlearScheduler->WriteScheduledRecords();
}

void CKlearControllerSchedulerOverview::slotRemoveRecordSet()
{
    const QString __FUNC__ = "CKlearControllerSchedulerOverview::slotRemoveRecordSet()";
    for ( int i = 0; i <= KlearScheduler->getRecordStorageSize(); i++ )
    {
        KlearRecordSet = KlearScheduler->getRecordSet( i );

        if ( listView->currentItem()->text( 1 ) == KlearRecordSet->getStartDateTime().toString() )
        {
           KlearScheduler->removeRecordSet( i );
        }
    }
    this->setActiveWindow();
    this->raise();
    this->show();
    loadListView();

    try{
        KlearScheduler->WriteScheduledRecords();
    }catch ( CKlearAppException &e ){   // Wir fangen eine Referenz
       e.addShowException( __LOC__, i18n("Could not save recordsets to disk \n") );
    }
}
