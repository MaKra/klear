/**
\author Klear.org
\file <filname>.cpp
\brief <Short Description Here>
*/
#include "CKlearControllerScanView.h"

CKlearControllerScanView::CKlearControllerScanView( QWidget* parent, const char* name, WFlags fl ) : CKlearUIScanView( parent, name, fl ){


}

void CKlearControllerScanView::slotScan()
{
    this->scan();
}

void CKlearControllerScanView::scan()
{
    const QString __FUNC__ = "CKlearControllerScanView::scan()";


    this->pushButton_Start->setEnabled(false);
    this->pushButton_Stop->setEnabled(true);
    this->start();

    CKlearAppErrorException e(__LOC__, i18n("Channel scannng is not yet implemented. Sorry for that. Have a look at www.Klear.org for more information!"));
    e.showExceptions();
}

void CKlearControllerScanView::slotOk()
{
    kdDebug() << "Ending scanning session...\n";
    this->wait();
    this->hide();
}

void CKlearControllerScanView::slotAbort()
{
     kdDebug() << "abord scanning...\n";
     this->IsRunning = false;
     this->pushButton_Start->setEnabled(true);
     this->pushButton_Stop->setEnabled(false);
     this->wait();

}

void CKlearControllerScanView::run(){

this->IsRunning = true;
this->progressBar->setEnabled(true);

int i = 0;
 while( this->IsRunning == true )
 {
        i++;
        usleep(200000);
        this->progressBar->setProgress(i*2 );
        this->textLabel_tv->setText( "" + QString::number(i) );
        this->update();

        this->listBox->insertItem(  i18n("New Channel ") + QString::number(i));
        if(i==50) this->IsRunning = false;
  }

  this->pushButton_Start->setEnabled(true);
  this->pushButton_Stop->setEnabled(false);
  //this->terminate ();
}



void CKlearControllerScanView::slotQuit()
{
    this->slotAbort();
    this->hide();
}



