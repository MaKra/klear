/**
\author Klear.org
\file CKlearControllerSchedulerOverview.h
\brief The scheduler overview dialog implementation for Klear.
*/

#ifndef CKLEARCONTROLLERSCHEDULEROVERVIEW_H
#define CKLEARCONTROLLERSCHEDULEROVERVIEW_H

#include <qlistview.h>
#include <qcombobox.h>
#include <qdatetime.h>
#include <qstring.h>
#include <qwidget.h>
#include <qapplication.h>
#include <qpushbutton.h>

#include "../../App/CKlearAppScheduler.h"    // the scheduler setting algorithms
#include "../../App/CKlearAppRecordSet.h"    // the record setting algorithms

#include "./CKlearControllerScheduler.h"    // the scheduler interface

#include "../View/CKlearUISchedulerOverview.h"

/**
\class CKlearControllerSchedulerOverview
\author Omar El-Dakhloul <omar@klear.org>
\brief The GUI-controller to add, edit and remove a scheduled data to backend scheduler. The dialog writes to the klearrecordset class.
*/
class CKlearControllerSchedulerOverview : public CKlearUISchedulerOverview
{
  Q_OBJECT

public:
    /**
    \fn CKlearControllerSchedulerOverview(CKlearAppScheduler *KlearScheduler, QWidget* parent = 0, const char* name = 0, WFlags fl = 0 )
    \author Omar El-Dakhloul <omar@klear.org>
    \brief Constructor
    */
    CKlearControllerSchedulerOverview(CKlearAppScheduler *KlearScheduler, QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );

    /**
    \fn ~CKlearControllerSchedulerOverview()
    \author Omar El-Dakhloul <omar@klear.org>
    \brief Destructor
    */
    ~CKlearControllerSchedulerOverview();

    /**
    \fn void loadListView();
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \brief Loads the listview with record items.
    */
    void loadListView();

public slots:
    /**
    \fn void slotSaveRecordSet();
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \brief Saves all changes at record timescheduler.
    */
    void slotSaveRecordSet();

    /**
    \fn void slotCancelRecordSet();
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \brief Quits the scheduler overview dialog.
    */
    void slotCancelRecordSet();

    /**
    \fn void slotAddRecordSet()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \brief Added a new record timescheduler.
    */
    void slotAddRecordSet();

    /**
    \fn void slotAddRecordSet(QString chan, QDateTime start, QDateTime end, QString recname)
    \author Marco Kraus <marco@klear.org>
    \retval void no return value
    \param chan channel name
    \param start start time
    \param end end time
    \param recname recording name
    \brief Added a new record timescheduler with values as parameters
    */
    void slotAddRecordSet(QString chan, QDateTime start, QDateTime end, QString recname);

    /**
    \fn void slotRemoveRecordSet();
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \brief Removes a record timescheduler.
    */
    void slotRemoveRecordSet();

    /**
    \fn void slotEditRecordSet();
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \brief Edits a record timescheduler
    */
    void slotEditRecordSet();

private:
    CKlearControllerScheduler* SchedulerDialog;    //  scheduler dialog
    CKlearAppScheduler* KlearScheduler;    //  scheduler
    CKlearAppRecordSet* KlearRecordSet;    //  record set
    QListViewItem* item;    // record item
};

#endif
