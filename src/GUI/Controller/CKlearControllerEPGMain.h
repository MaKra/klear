/**
\author Marco Kraus <marco@klear.org>
\file CKlearControllerEPGMain.h
\brief The EPG window controller
*/

#ifndef CKLEARCONTROLLEREPGMAIN_H
#define CKLEARCONTROLLEREPGMAIN_H

#include <vector>

#include <qlistbox.h>
#include <qtextedit.h>
#include <qlabel.h>

#include "../../config.h"

#include "../View/CKlearUIEPGMain.h"

#include "CKlearControllerScheduler.h"   // Scheduler GUI
#include "CKlearControllerSchedulerOverview.h"   // Scheduler Overview GUI

#include "../../App/EPG/CKlearAppEITData.h"
#include "../../App/EPG/CKlearAppDataLayer.h"
#include "../../App/CKlearAppDecodingHelper.h"
#include "../../App/EPG/CKlearAppDecodingTables.h"
#include "../../App/CKlearAppConfig.h"


/**
\class CKlearControllerEPGMain
\author Marco Kraus <marco@klear.org>
\brief The GUI-controller for the EPG. Controlls the user interaction with the GUI.
*/
class CKlearControllerEPGMain : public CKlearUIEPGMain
{
 Q_OBJECT

 public:
   CKlearControllerEPGMain( std::vector<CKlearAppEITData *> *storage, QString chan, CKlearControllerSchedulerOverview *SOD, CKlearAppConfig *conf, QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
   ~CKlearControllerEPGMain();

 public slots:
   /**
   \fn void slotRebuildDatabase()
    \author Marco Kraus <marco@klear.org>
    \retval void no return value
    \brief Deletes data vector completely and reads in new content
   */
   void slotRebuildDatabase();

   /**
   \fn void slotRefreshView()
    \author Marco Kraus <marco@klear.org>
    \retval void no return value
    \brief refreshes the GUI, but doesn't refresh the vector data
   */
   void slotRefreshView();

   /**
   \fn void slotCloseEPGwindow()
    \author Marco Kraus <marco@klear.org>
    \retval void no retval
    \brief closes the EPG window
   */
   void slotCloseEPGwindow();

   /**
   \fn void slotShowItemDetails()
    \author Marco Kraus <marco@klear.org>
    \retval void no retval
    \brief called when item is seleced, and show event details in detail-window
   */
  void slotShowItemDetails();

   /**
   \fn void slotActivateScheduledRecording()
    \author Marco Kraus <marco@klear.org>
    \retval void no retval
    \brief is called with double click on item and calls scheduled recording with item data
   */
  void slotActivateScheduledRecording();

private:
   bool isInDateList();

   QString currentChannel;
   std::vector<CKlearAppEITData *> *EITDataVector;   //  local map of storage vector
   CKlearControllerSchedulerOverview *SchedulerOverviewDialog; // scheduler dialog
   CKlearAppConfig *KlearConfig;
   QDateTime ST; // start time object used for adding data to vector
};

#endif

