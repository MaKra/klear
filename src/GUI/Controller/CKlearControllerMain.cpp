/**
\file CKlearControllerMain.cpp
\author Klear.org
\brief The main dialog implementation for Klear.
*/

#include "./CKlearControllerMain.h"

CKlearControllerMain::CKlearControllerMain(QWidget* parent, const char* name, WFlags fl) : CKlearUIMain(parent,name, fl)
{
    const QString __FUNC__ = "CKlearControllerMain::CKlearControllerMain";

    kdDebug() << "Starting up Klear\n";
    this->isExiting = false;

    // starting up with exception protection
    try{

         // init all given object pointers
         KlearConfig = NULL;
         acceleratorKeys = NULL;
         AboutKlear = NULL;
         KlearScheduler = NULL;
         SchedulerOverviewDialog = NULL;
         PlaybackWindow = NULL;
         EITDataVector = NULL;

         // create AboutDialog
         AboutKlear = new CKlearUIAbout();

         // load current Klearconfig
         KlearConfig = new CKlearAppConfig();
         std::cout << "Klear config loaded" << std::endl;

         // setting upn EPG database
         EITDataVector = new std::vector<CKlearAppEITData *>();
         std::cout << "EIT vector set up" << std::endl;

         // add Playerwindow to Mainpanel with new PlayerEngine
         EngineSocket = new QWidgetStack( centralWidget(), "EngineSocket" );
         EngineSocket->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, EngineSocket->sizePolicy().hasHeightForWidth() ) );

        // QWidget *EngineSocketPage = new QWidget( EngineSocket, "EngineSocketPage" );
         //EngineSocketPage->setPaletteBackgroundColor(QColor(255,255,255)); //black

         // add engine (selected in configuration) to QT window
         if( this->KlearConfig->getPlaybackEngine() == "KlearMPlayer" ){
               PlaybackWindow = new CKlearEngineMPlayer( EngineSocket, "PlaybackWindow" );     // MPLAYER
         }else if( this->KlearConfig->getPlaybackEngine() == "KlearXine" ){
               PlaybackWindow = new CKlearEngineXine( EngineSocket, "PlaybackWindow", this->EITDataVector );        // Klear XINE
         }else{
               PlaybackWindow = new CKlearEngineXineTNG( EngineSocket, "PlaybackWindow", this->EITDataVector );        // Klear XINE
         }
         PlaybackWindow->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, PlaybackWindow->sizePolicy().hasHeightForWidth() ) );

         //QGridLayout *EngineSocketPageLayout = new QGridLayout(EngineSocketPage);
         //EngineSocketPageLayout->setAutoAdd(true);

         //EngineSocketPageLayout->addWidget( PlaybackWindow, 0, 0 );

         EngineSocket->addWidget( PlaybackWindow, 0 );
         CKlearUIMainLayout->addWidget( EngineSocket );

         std::cout << "Playback Engine added to GUI" << std::endl;

         if ( this->KlearConfig->getMenuAlignment() == 0)  // RIGHT
         {
            CKlearUIMainLayout->remove(MenuGroupBox);
            CKlearUIMainLayout->addWidget( MenuGroupBox );
         }  else {  // LEFT

              CKlearUIMainLayout->remove( EngineSocket );
              CKlearUIMainLayout->remove(MenuGroupBox);

              CKlearUIMainLayout->addWidget( MenuGroupBox );
              CKlearUIMainLayout->addWidget( EngineSocket );
         }

        std::cout << "Starting exception protected startup" << std::endl;
        this->startUp();

    }catch ( CKlearAppErrorException &e ){  // Spezialisierung
       kdDebug() << "StartUp() KlearErrorException catched\n";
       e.showExceptions();   // show catched expection
       exit(-1);  // exit hard
    }catch ( CKlearAppFatalException &e ){  // Spezialisierung
       std::cout << "StartUp() KlearFatalException catched" << std::endl;
       e.showExceptions();   // show catched expection
       this->slotCloseWindow();  // clean exit
       exit(-1);  // exit hard
    }catch ( CKlearAppException &e ){  // alle anderen Klear-Exceptions
       kdDebug() << "StartUp() KlearException catched\n";
       e.showExceptions();   // show catched expection
       this->slotCloseWindow();  // clean exit
       exit(-1);  // exit hard
    }catch( std::exception &x ){  // all system exceptions
       kdDebug() << "StartUp() system exception catched\n";
       CKlearAppException e( __LOC__, x.what() );
       e.showExceptions();   // show catched expection
       this->slotCloseWindow();  // clean exit
       exit(-1);  // exit hard
    }

}


CKlearControllerMain::~CKlearControllerMain()
{
  // all QObjects are deleted automatically by QT
}


void CKlearControllerMain::startUp()
{
  const QString __FUNC__ = "CKlearControllerMain::startUp()";

    this->resize( KlearConfig->getWindowSize() );

   std::cout << "Window resized to last value" << std::endl;

    // load Channel-List
    try{
       this->loadChannelList();
    }catch ( CKlearAppFatalException &e ){
       std::cout << "Error ControllerMain: Could not load channel list" << std::endl;
       e.addErrorMessage( __LOC__, "Could not load channel list" );
       throw;
    }

   std::cout << "Channellist loaded" << std::endl;

    // load tunerclass
    if( KlearConfig->getDVBMode() == 1 ){   //   TERRESTRIC MODE
          kdDebug() << "Tuning for DVB-T\n";

         try{
              KlearTuner = new CKlearAppTunerT(KlearConfig);  // create object
          }catch( std::exception &x ){  // all system exceptions
               CKlearAppFatalException e( __LOC__, x.what() );
               throw e;
          }

          if( KlearConfig->getCurrentChannel() != "none" ){   // tune in channel
               this->ChannelBox->setCurrentItem( this->ChannelBox->findItem( KlearConfig->getCurrentChannel() ) );
               TuneChannel( KlearConfig->getCurrentChannel() );
          }else{
               this->ChannelBox->setCurrentItem(0); // set to first element
               TuneChannel( this->ChannelBox->currentText() );
          }
    }else if( KlearConfig->getDVBMode() == 2 ){   //   SATELLITE MODE
          kdDebug() << "Tuning for DVB-S\n";

          try{
             KlearTuner = new CKlearAppTunerS( KlearConfig );  // create object
          }catch( std::exception &x ){  // all system exceptions
               CKlearAppFatalException e( __LOC__, x.what() );
               throw e;
          }

          if( KlearConfig->getCurrentChannel() != "none" ){   // tune in channel
               this->ChannelBox->setCurrentItem( this->ChannelBox->findItem(KlearConfig->getCurrentChannel() ) );
               TuneChannel( KlearConfig->getCurrentChannel() );
          }else{
               this->ChannelBox->setCurrentItem(0); // set to first element
               TuneChannel( this->ChannelBox->currentText() );
          }
    }else if( KlearConfig->getDVBMode() == 3 ){    //   CABLE MODE
          kdDebug() << "Tuning for DVB-C\n";

          try{
             KlearTuner = new CKlearAppTunerC( KlearConfig );  // create object
          }catch( std::exception &x ){  // all system exceptions
               CKlearAppFatalException e( __LOC__, x.what() );
               throw e;
          }

          if( KlearConfig->getCurrentChannel() != "none" ){   // tune in channel
               this->ChannelBox->setCurrentItem( this->ChannelBox->findItem(KlearConfig->getCurrentChannel() ) );
               TuneChannel( KlearConfig->getCurrentChannel() );
          }else{
               this->ChannelBox->setCurrentItem(0); // set to first element
               TuneChannel( this->ChannelBox->currentText() );
          }
    }else{
          kdFatal() << i18n("No Tuner used. This should not happen with sane config-files.\n");
          CKlearAppFatalException e( __LOC__, "Could not find suitable tuner. This should not happen with sane config files. Try to delete klear.conf and restart." );
          throw e;
    }


    // create Scheduler object
    try{
        KlearScheduler = new CKlearAppScheduler(KlearConfig, KlearTuner, this);
    }catch( std::exception &x ){  // all system exceptions
        CKlearAppFatalException e( __LOC__, x.what() );
        throw e;
    }

    //define connections for scheduled recording
    QObject::connect( KlearScheduler, SIGNAL ( signalStartingScheduled() ), this, SLOT( slotStartingScheduled() ) );
    QObject::connect( KlearScheduler, SIGNAL ( signalStoppingScheduled() ), this, SLOT( slotStoppingScheduled() ) );
    QObject::connect( KlearScheduler, SIGNAL ( signalPushQueue() ), this, SLOT( slotPushQueue() ) );
    QObject::connect( KlearScheduler, SIGNAL ( signalCloseMain() ), this, SLOT( slotCloseWindow() ) );
    QObject::connect( PlaybackWindow, SIGNAL ( signalStreamEstablished() ), this, SLOT( slotSetDeinterlaceFromConfig() ) );

    // testing for KWin (post icccm compliant window manager). Sadly Gnomes WMs seems not to be icccm compliant ;-/
    isKWin = KApplication::dcopClient()->isApplicationRegistered("kwin");
    if (isKWin)
        kdDebug() << "WM: KWin found" << endl;
    else
        kdDebug() << "WM: KWin not used" << endl;

    // get a keycode for a faked key event to hold down screensaver
    kdDebug() << "Setting up screensaver fake events" << endl;
    int a,b,c,d;
    m_haveXTest = XTestQueryExtension(x11Display(), &a, &b, &c, &d);
    if (m_haveXTest)
        m_xTestKeycode = XKeysymToKeycode(x11Display(), XK_Shift_L);
    connect(&screensaverTimer, SIGNAL(timeout()), this, SLOT(slotFakeKeyEvent()));

    // start screensaver-timer the first time
    slotSetScreensaverTimeout();

    try{
       KlearScheduler->start();  // start scheduler thread
    }catch( std::exception &x ){  // all system exceptions
       CKlearAppFatalException e( __LOC__, x.what() );
       throw e;
    }

    setAcceleratorKeys();

    // pre-set status flags...changes will be set in local slots directly
    isMinimized  = FALSE;
    isFullscreen = FALSE;

    // set the last used volume
    kdDebug() << "Setting up last volume...\n";
    PlaybackWindow->setVolume( KlearConfig->getCurrentVolume() );
    volumeSlider->setValue( KlearConfig->getCurrentVolume() );

    // start mouse hider......mouse pointer disappears after some time
    kdDebug() << "Hiding Mousepointer...\n";
    PlaybackWindow->hideMouse();

   // start EPG backend
   kdDebug() << "Starting up EIT EPG System\n";

   // start reading content
   try{
           StreamReader = new CKlearAppStreamReader( EITDataVector, this->KlearConfig);
           StreamReader->start(); // start thread
   }catch( std::exception &x ){  // all system exceptions
           CKlearAppFatalException e( __LOC__, x.what() );
           throw e;
   }

  std::cout << "StreamReader thread started" << std::endl;

    // start stream playback
    this->slotPushQueue();

   // load current recording scheduler GUI
   try{
          SchedulerOverviewDialog = new CKlearControllerSchedulerOverview(KlearScheduler);
   }catch( CKlearAppException &e ){
          e.addErrorMessage( __LOC__, i18n("Could not create SchedulerOverview dialog.") );
          throw;
   }catch( std::exception &x ){  // all system exceptions
          CKlearAppException e( __LOC__, x.what() );
          throw e;
   }

   // if minimized is enabled in configfile, enable it here on startup
   if( KlearConfig->getIsMinimized() == true )
         this->slotMinimizeWindow();
}


void CKlearControllerMain::slotSetDeinterlaceFromConfig()   // if deinterlacing was enabled in config, we need to active this when stream is established
{
   // if deinterlacing is enabled in configfile, enable it here on startup
   if( KlearConfig->getIsDeinterlaced() == true )
   {
       std::cout << "setting deinterlacing as wished via configfile" << std::endl;
       this->slotToggleDeinterlaceButton();
       this->slotToggleDeinterlace();
   }
   else
      std::cout << "setting deinterlacing NOT wished via configfile" << std::endl;

}

void CKlearControllerMain::closeEvent( QCloseEvent* ce )   // overwrite close event to finish our local threads first
{
    const QString __FUNC__ = "CKlearControllerMain::closeEvent()";

    std::cout << "Close event called" << std::endl;

    this->slotCloseWindow();  // call our local closing function
}


void CKlearControllerMain::resizeEvent ( QResizeEvent * re)
{
    if( KlearConfig != NULL )
    {
        if( isFullscreen == FALSE )
        {
            kdDebug() << "resized MainWin...\n";
            KlearConfig->setWindowSize( this->size() );
            kdDebug() << "Writing Config\n";
            KlearConfig->WriteConfig();
        } else  kdDebug() << "in resizeEvent Was fullscreen - config not saved \n";
    }
}


void CKlearControllerMain::hideEvent ( QHideEvent * ce )
{
    if ( this->KlearConfig->getMuteSystray() == true )
    {
        PlaybackWindow->EngineStopStream();
    }
}

void CKlearControllerMain::showEvent ( QShowEvent * ce )
{
    if ( this->KlearConfig->getMuteSystray() == true )
    {
        this->slotPushQueue();
    }
}


void CKlearControllerMain::slotMinimizeWindow()
{
    if (isMinimized == FALSE)
    {
        if (isFullscreen == FALSE)
        {
            MenuGroupBox->hide();
            this->resize( KlearConfig->getWindowSize().width() - ( MenuGroupBox->width() ), KlearConfig->getWindowSize().height() );
            isMinimized = TRUE;
        }
        else
        {
            MenuGroupBox->hide();
            isMinimized = TRUE;
        }

        // set minimized menu and save to configuration
        KlearConfig->setIsMinimized( true );
    }
    else
    {
        if (isFullscreen == FALSE)
        {
            MenuGroupBox->show();
            this->resize( ( KlearConfig->getWindowSize().width() + MenuGroupBox->width() ), KlearConfig->getWindowSize().height());
            isMinimized = FALSE;
        }
        else
        {
            MenuGroupBox->show();
            isMinimized = FALSE;
        }

        // set minimized menu and save to configuration
        KlearConfig->setIsMinimized( false );
    }

   KlearConfig->WriteConfig(); // write config to store the last settings
}

void CKlearControllerMain::slotFullscreenWindowButton()
{
    this->fullscreenButton->toggle();
    this->slotFullscreenWindow();
}

void CKlearControllerMain::slotFullscreenWindow()
{
    const QString __FUNC__ = "CKlearControllerMain::slotFullscreenWindow()";

        if( isFullscreen == FALSE )
        {
            groupboxWasVisible = isMinimized;

            if(this->isKWin)
            {
                  std::cout << "Loading fullscreen for KWin" << std::endl;
                  KWin::activateWindow(winId());
                  KWin::setState(winId(), NET::FullScreen);
            }
            else  // not KWin but icccm compliant envorinment; now works for gnome  too ;-)
            {
                  std::cout << "Loading fullscreen for non KWin" << std::endl;
                  this->showFullScreen();
            }

            this->isFullscreen = TRUE;
            MenuGroupBox->hide();
            isMinimized = TRUE;
        }
        else
        {
            this->showNormal();
            if ( !groupboxWasVisible == TRUE )
            {
                 MenuGroupBox->show();
                 isMinimized = FALSE;
            }

            if ( !groupboxWasVisible == isMinimized )
            {
                this->resize( ( KlearConfig->getWindowSize().width() + MenuGroupBox->width() ), KlearConfig->getWindowSize().height());
            }
            this->isFullscreen = FALSE;
        }
}


void CKlearControllerMain::slotCloseWindow()
{
    const QString __FUNC__ = "CKlearControllerMain::slotCloseWindow()";

    std::cerr  << "CloseWindow method called" << std::endl;

 if( (this->isExiting != true) && (KlearScheduler != NULL) && (KlearConfig != NULL) && (PlaybackWindow != NULL) && (KlearTuner != NULL) && (AboutKlear != NULL) ) // if not all object exists; happens when startup fails
 { // if all objects are set up and this method was never called before....

    this->isExiting = true; //to prevent this stuff is called twice.

    std::cerr  << "Deleting created objects and freeing memory." << std::endl;

    if( KlearScheduler && KlearScheduler->isRecordInProgress() == true )
    {
      CKlearAppInputException e( __LOC__, i18n("You must not quit Klear while recording or timeshifting is in progress.\n"));
      e.showExceptions();
      return;
    }

     if( PlaybackWindow->isMute() )
     {
          PlaybackWindow->toggleMute();
     }
     emit signalCloseTray();

    if( isMinimized == TRUE )
    {
        if( isFullscreen == FALSE )
        {   // correct windowsize and save it using windowsize + menuesize
             this->resize( QSize( KlearConfig->getWindowSize().width() + MenuGroupBox->width() , KlearConfig->getWindowSize().height() ) );
        }
    }

     if( KlearScheduler->isRecordInProgress() == true )
     {
          kdDebug() << "Closing Recorder\n";
          KlearScheduler->abortRecording();
          while( KlearScheduler->isRecordInProgress() )
          {
               usleep(1000); // sleep 1000 �sec and wait for recorder-exit
          }
     }

     std::cerr << "Stopping playback" <<std::endl;
     PlaybackWindow->EngineStopStream();

     kdDebug() << "Closing Scheduler\n";

     // save ScheduledRecords to file
     try{
           KlearScheduler->WriteScheduledRecords();  // beendet er den Thread danach dann automnatisch ? MKr
     }catch ( CKlearAppException &e ){
           e.addShowException(__LOC__, i18n("Scheduler operation failed \n"));   // show catched expection
     }

     // stop scheduler
     KlearScheduler->stop();
     while( KlearScheduler->running() )
          KlearScheduler->wait();
     std::cerr << "Scheduler stopped" <<std::endl;

     // stop EPG
     this->StreamReader->setRunning(false);
     while( StreamReader->running() )
          StreamReader->wait();
     std::cerr << "EPG stopped" << std::endl;

     // stop tuner
     KlearTuner->isFinished = true; // finish thread regularly
     while( KlearTuner->running() )
          KlearTuner->wait(); //  wait for thread to return
     std::cerr << "Tuner stopped" <<std::endl;

     // delete allocated objects
     //delete PlaybackWindow;

     std::cerr << "Delete object KlearConfig" << std::endl;
     delete KlearConfig;
     //delete KlearScheduler;
     std::cerr << "Delete object EITDataVector" << std::endl;
     delete EITDataVector;
     std::cerr  << "Delete object AboutKlear" << std::endl;
     delete AboutKlear;
     std::cerr  << "Delete object KlearTuner" << std::endl;
     delete KlearTuner;

     std::cout << "Bye..." << std::endl;

     // close mainwindow
     this->close();
     exit(0);
  }  // end objects exist check

}


void CKlearControllerMain::slotTakeScreenshot()
{
  const QString __FUNC__ = "CKlearControllerMain::slotTakeScreenshot()";

  try{
      PlaybackWindow->getStreamSnapshot().save( KlearConfig->getScreenshotDir()+"/"+KlearConfig->getScreenshotName()+"_"+KlearConfig->getCurrentChannel()+"_"+QDateTime::currentDateTime().toString("dd.MM.yy-hh.mm.ss")+"."+KlearConfig->getScreenshotFormat(),KlearConfig->getScreenshotFormat().upper(), 90 );
      PlaybackWindow->showOSDMessage( KlearConfig->getScreenshotName()+"_"+KlearConfig->getCurrentChannel()+"_"+QDateTime::currentDateTime().toString("dd.MM.yy-hh.mm.ss")+"."+KlearConfig->getScreenshotFormat()+" "+i18n("saved"), 2000 );
  }catch ( CKlearAppException &e ){
      e.addShowException(__LOC__, i18n("Engine error \n"));
  }
}


void CKlearControllerMain::slotXineFatal(const QString& ferror)
{
  const QString __FUNC__ = "CKlearControllerMain::slotXineFatal()";

  kdFatal() << i18n("Klear: Fatal xine error: ")+ferror+"\n";
  CKlearAppFatalException e( __LOC__, i18n("Fatal xine error catched from xine-engine!" ));
  throw e;
}


void CKlearControllerMain::slotAboutKlear()
{
   AboutKlear->show();   // modal about dialog
}


void CKlearControllerMain::slotConfigDialog()
{
  const QString __FUNC__ = "CKlearControllerMain::slotConfigDialog()";

   CKlearControllerConfig::CKlearControllerConfig(KlearConfig).exec();  // modal dialog

   kdDebug() << "Processing post config dialog stuff...\n";

   this->setAcceleratorKeys();  // set new accel keys
   this->slotSetScreensaverTimeout();   // set new screensaver timeout

   try{
       this->ChannelBox->clear();
       loadChannelList();
       this->ChannelBox->setSelected( ChannelBox->findItem( KlearConfig->getCurrentChannel(),0 ), true);
   }catch ( CKlearAppFatalException &e ){
       e.addErrorMessage( __LOC__, i18n("Could not load channel list") );
       throw e;
   }

   if ( this->KlearConfig->getMenuAlignment() == 0)   // RIGHT
   {
       CKlearUIMainLayout->remove(MenuGroupBox);
       CKlearUIMainLayout->addWidget( MenuGroupBox );
   }  else {   // LEFT
      CKlearUIMainLayout->remove( EngineSocket );
      CKlearUIMainLayout->remove(MenuGroupBox);
      CKlearUIMainLayout->addWidget( MenuGroupBox );
      CKlearUIMainLayout->addWidget( EngineSocket );
   }

   kdDebug() << "Ending config session...\n";
}

void CKlearControllerMain::slotShowInfoOSD()
{
   if( PlaybackWindow->EngineIsPlaying() )
     PlaybackWindow->showInfoOSD( KlearConfig->getCurrentChannel() );
}

void CKlearControllerMain::slotToggleDeinterlaceButton()
{
     this->isDeinterlaceButton->toggle();
     this->slotToggleDeinterlace();
}

void CKlearControllerMain::slotToggleDeinterlace()
{
      kdDebug() << "Toggling deinterlace-filter\n";

      if( KlearConfig->getIsDeinterlaced() == true )
      {
         PlaybackWindow->showOSDMessage(i18n("Switch off deinterlacefilter"),1000);
         KlearConfig->setIsDeinterlaced( false );
      }
      else
      {
         PlaybackWindow->showOSDMessage(i18n("Switch on deinterlacefilter"),1000);
         KlearConfig->setIsDeinterlaced( true );
      }

      PlaybackWindow->toggleDeinterlacing();

      KlearConfig->WriteConfig(); // write config to store the last settings
}

void CKlearControllerMain::slotToggleMuteButton()
{
    this->isMuteButton->toggle();
    this->slotToggleMute();
}


void CKlearControllerMain::slotToggleMute()
{
         kdDebug() << "Toggling Mute \n";

     PlaybackWindow->toggleMute();
     if ( PlaybackWindow->isMute() ) this->isMuteButton->setPixmap( QPixmap::QPixmap( QImage::QImage( audio_mute ) ) );
        else this->isMuteButton->setPixmap( QPixmap::QPixmap( QImage::QImage( audio ) ) );
}

void CKlearControllerMain::slotSetVolume()  // gets data when slider moved
{
     PlaybackWindow->setVolume( volumeSlider->value() );  // sets the volume in playback

     KlearConfig->setCurrentVolume( volumeSlider->value() ); // save last set volume for configuration
     KlearConfig->WriteConfig(); // write config to store the last settings

     char vol[3]; // only ansi compliant way of converting int to char*, MKr, 19.11.04
     sprintf(vol, "%d", volumeSlider->value());
     PlaybackWindow->showOSDMessage(i18n("Volume: ")+vol+"%",1000);
}


void CKlearControllerMain::slotShowXineStatus(const QString& status)
{
     PlaybackWindow->showOSDMessage(status,1000);
}

void CKlearControllerMain::slotChangeChannel()
{
  const QString __FUNC__ = "CKlearControllerMain::slotChangeChannel()";
  kdDebug() << "Trying to change channel\n";

  if( KlearScheduler && KlearScheduler->isRecordInProgress() == true )
  {
    CKlearAppInputException e( __LOC__, i18n("Switching not allowed while recording is running.\n"));
    e.showExceptions();
    return;
  }

  PlaybackWindow->EngineStopStream();

  TuneChannel(ChannelBox->currentText()); // set receiver to marked channel

  // rebuild EPG database
  CKlearAppDataLayer *DatabaseLayer = new CKlearAppDataLayer( this->EITDataVector );
  DatabaseLayer->cleanUpDatabase();
  delete DatabaseLayer;

  this->slotPushQueue();  // start playback
//   Please test only one pushQueue() !! MKr, 29.08
//  usleep(1000000);
//  this->slotPushQueue();
}


void CKlearControllerMain::TuneChannel(const QString& ChannelName)
{

  kdDebug() << "Tuning in: " << ChannelName << "\n";

  if( KlearTuner->running() ){
     KlearTuner->isFinished = true; // finish thread regularly
     KlearTuner->wait(); //  wait for thread to return
     kdDebug() << "Tuner stopped....\n";
  }

  KlearTuner->setChannel(ChannelName);

  KlearTuner->start(); // start threaded Tuner
  while( KlearTuner->isReady == false) // wait here while Tuner isn't established correctly
  {
     usleep(10000);  // wait another 1000s for the tuner
  }

  kdDebug() << "Tuner started....\n";

  KlearConfig->setCurrentChannel( ChannelName ); // set current channel in config-class
  KlearConfig->WriteConfig();

  KlearConfig->StoreServiceID( ChannelName ); // set current channel in config-class

}


void CKlearControllerMain::loadChannelList()
{
  const QString __FUNC__ = "CKlearControllerMain::loadChannelList()";

  std::cout << "Loading channels list" << std::endl;
  QFile ChannelsConf( KlearConfig->getKlearConfigPath()+"/"+KlearConfig->getKlearChannelsConf() );

  std::cout << "Channelsconf is:" << ChannelsConf.name() << std::endl;
  if ( ChannelsConf.exists() && ChannelsConf.open( IO_ReadOnly ) )
  {
        std::cout << "ControllerMain: Channels list found. Reading in...." << std::endl;
        QTextStream stream( &ChannelsConf );
        QString line;
        for( int i = 1; !stream.atEnd(); i++ )
        {
            line = stream.readLine().latin1();
            ChannelBox->insertItem( line.section( ':', 0, 0 ) );
        }
        ChannelsConf.close();
  }else{
     CKlearAppFatalException e( __LOC__, i18n("No channels.conf found. You need a t-,s-,c-Zap compatible channels.conf in your .klear directory. Visit www.Klear.org for more information!"));
     throw e;
  }
}


void CKlearControllerMain::slotPushQueue()
{
  const QString __FUNC__ = "CKlearControllerMain::slotPushQueue";
  QString PlayStream;
  kdDebug() << "slotPushQueue\n";

  if( KlearScheduler->isRecordInProgress() == false )
  {
    // no recording, so play live from DVB device
    kdDebug() << "Playing live from DVB device...\n";
    PlayStream = "fifo:/dev/dvb/"+KlearConfig->getDVBAdapter()+"/"+KlearConfig->getDVBDvr();
  }
  else
  {
    // we are currently recording a stream, so we want to see the recorded file
    kdDebug() << "Playing from current recording \n";
    PlayStream = "fifo:/tmp/Klear_temp.fifo";
    //PlayStream = KlearScheduler->getRecordFile();
  }

  try{
       PlaybackWindow->EnginePlayStream( PlayStream );
  }catch ( CKlearAppErrorException &e ){
       e.addShowException(__LOC__, " Engine error \n");
  }catch ( CKlearAppException &e ){
       e.addShowException(__LOC__, " Engine error \n");
       this->slotCloseWindow();  // clean exit
       exit(-1);  // exit hard
  }

    // test if xine stream is established !! // MKr, 06.10.2005
    PlaybackWindow->showOSDMessage(ChannelBox->currentText(),5000);

    if ( this->KlearConfig->getAutoShowOsd() )
    {
       usleep(3000000);
       this->slotShowInfoOSD();
    }

}


void CKlearControllerMain::slotPushRecording()
{
  const QString __FUNC__ = "CKlearControllerMain::slotPushRecording()";

  if( KlearScheduler->isRecordInProgress() == true )
  {
    kdDebug() << "Stopping recording\n";

    recordButton->setPixmap( QPixmap::QPixmap( recordPixmap ) );

    PlaybackWindow->EngineStopStream();
    KlearScheduler->abortRecording();

    while( KlearScheduler->isRecordInProgress() )
    {
      usleep(1000); // sleep 1000 sec
    }

    TimeshiftStartButton->setEnabled(true);
    this->slotPushQueue();
    PlaybackWindow->showOSDMessage(i18n("Switched back to live stream..."),1000);
  }
  else
  {
    kdDebug() << "main Starting recording\n";

    recordButton->setPixmap( QPixmap::QPixmap( stopPixmap ) );

    PlaybackWindow->EngineStopStream(); // stop playback stream, free dvr device
    /*while( PlaybackWindow->EngineIsPlaying() == true )
    {
        usleep(50000);  // sleep another 50 000 �s and wait for xine stopping with its playback
    }*/

    TimeshiftStartButton->setEnabled(false);
    KlearScheduler->startQuickRecording();
  }
}


void CKlearControllerMain::slotPushTimeshifting()
{
    const QString __FUNC__ = "CKlearControllerMain::slotPushTimeshifting()";
    if ( !KlearScheduler->isTimeShifted() )
    {
        try{
            ((CKlearEngineXine*)PlaybackWindow)->resumeTime=0;
            TimeshiftResumeButton->setEnabled(true);
            TimeshiftStartButton->setPixmap( QPixmap::QPixmap( ts_stopp ) );
            KlearScheduler->setTimeShifted(true);
            recordButton->setEnabled(false);
            PlaybackWindow->EngineStopStream();
            KlearScheduler->startQuickRecording();
            kdDebug() << "main: startingtimeshift " << "\n";
            PlaybackWindow->showOSDMessage(i18n("starting timeshifting..."),3000);
        }catch ( CKlearAppException &e ){
            e.addShowException(__LOC__, i18n("Error while starting Timeshifting.\n"));
        }
    }
    else
    {
        try{
            PlaybackWindow->EngineStopStream();
            ((CKlearEngineXine*)PlaybackWindow)->resumeTime=1;
            TimeshiftResumeButton->setEnabled( false );
            TimeshiftStartButton->setPixmap( QPixmap::QPixmap( ts_start ) );
            TimeshiftResumeButton->setPixmap( QPixmap::QPixmap( playPixmap ) );
            KlearScheduler->setTimeShifted(false);
            recordButton->setEnabled(true);

            KlearScheduler->abortRecording();

            while( KlearScheduler->isRecordInProgress() )
            {
               usleep(1000); // sleep 1000 �sec
            }
            this->slotPushQueue();

        }catch ( CKlearAppException &e ){
            e.addShowException(__LOC__, i18n("Error while stopping Timeshifting.\n"));
        }
    }
}

void CKlearControllerMain::slotResumeTimeshifting()
{
    const QString __FUNC__ = "CKlearControllerMain::slotResumeTimeshifting()";

    if ( KlearScheduler->isTimeShifted() )
    {
        try{
            ((CKlearEngineXine*)PlaybackWindow)->timeShift( "fifo:"+KlearConfig->getRecordingDir() +"/"+ KlearConfig->getTimeShiftName() +".mpeg" );
        }catch ( CKlearAppErrorException &e ){
            e.addShowException(__LOC__, i18n("Error while stopping Timeshifting.\n"));
        }

        if( PlaybackWindow->EngineIsPlaying() == true )
        {
            TimeshiftResumeButton->setPixmap( QPixmap::QPixmap( pausePixmap ) );
        } else { TimeshiftResumeButton->setPixmap( QPixmap::QPixmap( playPixmap ) ); }

    }
}

void CKlearControllerMain::slotLaunchEPG()
{
    const QString __FUNC__ = "CKlearControllerMain::slotLaunchEPG()";

    CKlearControllerEPGMain *mainEPG = new CKlearControllerEPGMain( this->EITDataVector, this->KlearConfig->getCurrentChannel(), this->SchedulerOverviewDialog, this->KlearConfig );
    mainEPG->show();
}

void CKlearControllerMain::slotLaunchTXT()
{
    const QString __FUNC__ = "CKlearControllerMain::slotLaunchTXT()";

    CKlearControllerTXTMain *mainTXT = new CKlearControllerTXTMain( this->KlearConfig );
    mainTXT->show();
}

void CKlearControllerMain::slotStartingScheduled()
{
    const QString __FUNC__ = "CKlearControllerMain::slotStartingScheduled()";
    TimeshiftStartButton->setEnabled(false);
    recordButton->toggle();
    recordButton->setPixmap( QPixmap::QPixmap( QImage::QImage( stopPixmap ) ) );
    this->ChannelBox->setCurrentItem( this->ChannelBox->findItem( KlearConfig->getCurrentChannel() ) );
    kdDebug() << " slotstartingscheduled " << "\n";
    PlaybackWindow->EngineStopStream(); // stop playback stream, free dvr device
}

void CKlearControllerMain::slotStoppingScheduled()
{
    const QString __FUNC__ = "CKlearControllerMain::slotStoppingScheduled()";

    PlaybackWindow->EngineStopStream(); // stop playback stream, free dvr device
    recordButton->toggle();
    TimeshiftStartButton->setEnabled(true);
    recordButton->setPixmap( QPixmap::QPixmap( recordPixmap ) );
    kdDebug() << " slotstoppingscheduled\n";
    this->slotPushQueue();
    PlaybackWindow->showOSDMessage(i18n("Switched back to live stream..."), 1000);
}

void CKlearControllerMain::slotSchedulerOverviewDialog()
{
  const QString __FUNC__ = "CKlearControllerMain::startUp()";

    // load Channel-List
      SchedulerOverviewDialog->show();
      SchedulerOverviewDialog->raise();
      SchedulerOverviewDialog->setActiveWindow();
      SchedulerOverviewDialog->loadListView();
}

void CKlearControllerMain::slotIncChannel()
{
  const QString __FUNC__ = "CKlearControllerMain::slotIncChannel()";

   if( KlearScheduler && KlearScheduler->isRecordInProgress() == true )
   {
     CKlearAppInputException e( __LOC__, i18n("Switching not allowed while recording or timeshifting is running.\n"));
     e.showExceptions();
     return;
   }

   if( (unsigned)this->ChannelBox->currentItem() < this->ChannelBox->count()-1 )  // only decrement when not first element in list
   {
      this->TuneChannel( this->ChannelBox->text( this->ChannelBox->currentItem() + 1 ) );  // tune in channel above
      this->ChannelBox->setCurrentItem( this->ChannelBox->currentItem() + 1 );  // mark channel above in channellist

      // rebuild EPG database
      CKlearAppDataLayer *DatabaseLayer = new CKlearAppDataLayer( this->EITDataVector );
      DatabaseLayer->cleanUpDatabase();
      delete DatabaseLayer;

      this->slotPushQueue();  // start playback

      if ( this->KlearConfig->getAutoShowOsd() )
      {
         this->slotShowInfoOSD();
      }

  }
}

void CKlearControllerMain::slotDecChannel()
{
  const QString __FUNC__ = "CKlearControllerMain::slotDecChannel()";

   if( KlearScheduler && KlearScheduler->isRecordInProgress() == true )
   {
     CKlearAppInputException e( __LOC__, i18n("Switching not allowed while recording or timeshifting is running.\n"));
     e.showExceptions();
     return;
   }

   if( this->ChannelBox->currentItem() > 0 )  // only decrement when not first element in list
   {
      this->TuneChannel( this->ChannelBox->text( this->ChannelBox->currentItem() - 1 ) );  // tune in channel above
      this->ChannelBox->setCurrentItem( this->ChannelBox->currentItem() - 1 );  // mark channel above in channellist

      // rebuild EPG database
      CKlearAppDataLayer *DatabaseLayer = new CKlearAppDataLayer( this->EITDataVector );
      DatabaseLayer->cleanUpDatabase();
      delete DatabaseLayer;

      this->slotPushQueue();  // start playback

      if ( this->KlearConfig->getAutoShowOsd() )
      {
         this->slotShowInfoOSD();
      }
  }
}

void CKlearControllerMain::setAcceleratorKeys()
{
    kdDebug() << "Connecting Hotkeys...\n";

    // define accelerator keys for shotscuts
    acceleratorKeys = new QAccel(this);

    acceleratorKeys->connectItem( acceleratorKeys->insertItem( QKeySequence( KlearConfig->getAccDecChannel())) , this, SLOT( slotDecChannel() ) );
    acceleratorKeys->connectItem( acceleratorKeys->insertItem( QKeySequence( KlearConfig->getAccIncChannel())), this, SLOT( slotIncChannel() ) );
    acceleratorKeys->connectItem( acceleratorKeys->insertItem( QKeySequence( KlearConfig->getAccAbout()) ), this, SLOT( slotAboutKlear() ) );
    acceleratorKeys->connectItem( acceleratorKeys->insertItem( QKeySequence( KlearConfig->getAccConfig())), this, SLOT( slotConfigDialog() ) );
    acceleratorKeys->connectItem( acceleratorKeys->insertItem( QKeySequence( KlearConfig->getAccDeinterlace()) ), this, SLOT( slotToggleDeinterlaceButton() ) );
    acceleratorKeys->connectItem( acceleratorKeys->insertItem( QKeySequence( KlearConfig->getAccEpg()) ), this, SLOT( slotLaunchEPG() ) );
    acceleratorKeys->connectItem( acceleratorKeys->insertItem( QKeySequence( KlearConfig->getAccTxt()) ), this, SLOT( slotLaunchTXT() ) );
    acceleratorKeys->connectItem( acceleratorKeys->insertItem( QKeySequence( KlearConfig->getAccFullscreen() ) ), this, SLOT( slotFullscreenWindowButton() ) );
    acceleratorKeys->connectItem( acceleratorKeys->insertItem( QKeySequence( KlearConfig->getAccOsd())), this, SLOT( slotShowInfoOSD() ) );
    acceleratorKeys->connectItem( acceleratorKeys->insertItem( QKeySequence( KlearConfig->getAccChannellist()) ), this, SLOT( slotMinimizeWindow() ) );
    acceleratorKeys->connectItem( acceleratorKeys->insertItem( QKeySequence( KlearConfig->getAccQuit())), this, SLOT( slotCloseWindow() ) );
    acceleratorKeys->connectItem( acceleratorKeys->insertItem( QKeySequence( KlearConfig->getAccRecording())), this, SLOT( slotPushRecording() ) );
    acceleratorKeys->connectItem( acceleratorKeys->insertItem( QKeySequence( KlearConfig->getAccShoot())), this, SLOT( slotTakeScreenshot() ) );
    acceleratorKeys->connectItem( acceleratorKeys->insertItem( QKeySequence( KlearConfig->getAccTimeshift())), this, SLOT( slotPushTimeshifting() ) );
    acceleratorKeys->connectItem( acceleratorKeys->insertItem( QKeySequence( KlearConfig->getAccMute())), this, SLOT( slotToggleMuteButton() ) );
    //TODO: fix me im stuck
    acceleratorKeys->connectItem( acceleratorKeys->insertItem( Key_Space ), this, SLOT( slotResumeTimeshifting()  ));

    updateToolTips();
}


void CKlearControllerMain::updateToolTips()
{
    kdDebug() << "Updating Tooltips for new defined keys\n";

    //TODO: i18n for tooltips

    QToolTip::remove( isMuteButton );
    QToolTip::add( isMuteButton, i18n("Mute audio. Hotkey [ ") +  KlearConfig->getAccMute()  + " ]" );

    QToolTip::remove( recordButton );
    QToolTip::add( recordButton, i18n("Record current videostream. Hotkey [ ") +  KlearConfig->getAccRecording()  + " ]" );

    QToolTip::remove( TimeshiftStartButton );
    QToolTip::add( TimeshiftStartButton, i18n("Start / Stop timeshifting.  Hotkey [ ") +  KlearConfig->getAccTimeshift()  + " ]" );

    QToolTip::remove( EPGButton );
    QToolTip::add( EPGButton, i18n("Show electronic program guide. Hotkey [ ") +  KlearConfig->getAccEpg()  + " ]" );

    QToolTip::remove( TXTButton );
    QToolTip::add( TXTButton, i18n("Teletext Hotkey [ ") +  KlearConfig->getAccTxt() + " ]" );

    QToolTip::remove( isDeinterlaceButton );
    QToolTip::add( isDeinterlaceButton, i18n("Toggle video deinterlacing. Hotkey [ ") +  KlearConfig->getAccDeinterlace()  + " ]" );

    QToolTip::remove( screenshotButton );
    QToolTip::add( screenshotButton, i18n("Take a screenshot of the current videostream. Screenshots options can be found in the configuration dialog. Hotkey [ ") +  KlearConfig->getAccShoot()  + " ]" );

    QToolTip::remove( minimizeButton );
    QToolTip::add( minimizeButton, i18n("Toggle video deinterlacing. Hotkey [ ") +  KlearConfig->getAccMute()  + " ]" );

    QToolTip::remove( minimizeButton );
    QToolTip::add( minimizeButton, i18n("Switch to minimalmode. Hotkey [ ") +  KlearConfig->getAccChannellist()  + " ]" );

    QToolTip::remove( fullscreenButton );
    QToolTip::add( fullscreenButton, i18n("Switch to fullscreenmode. Hotkey [ ") +  KlearConfig->getAccFullscreen() + " ]" );

    QToolTip::remove( settingsButton );
    QToolTip::add( settingsButton, i18n("Configure Klear. Hotkey [ ") +  KlearConfig->getAccConfig()  + " ]" );

    QToolTip::remove( OSDbutton );
    QToolTip::add( OSDbutton, i18n("Show channelinformation via OSD. Hotkey [ ") +  KlearConfig->getAccOsd() + " ]" );

    QToolTip::remove( aboutButton );
    QToolTip::add( aboutButton, i18n("About the Klear project. Hotkey [ ") +  KlearConfig->getAccAbout() + " ]" );

    QToolTip::remove( TimeshiftResumeButton );
    QToolTip::add( TimeshiftResumeButton, i18n("Resume / Pause timeshifting Playback. Hotkey [ Space ]") );

}


void CKlearControllerMain::slotFakeKeyEvent()
{
    kdDebug() << "Fake keypress event" << endl;
    if (m_haveXTest)
    {
        XTestFakeKeyEvent(x11Display(), m_xTestKeycode, true, CurrentTime);
        XTestFakeKeyEvent(x11Display(), m_xTestKeycode, false, CurrentTime);
        XSync(x11Display(), false);
    }
}

void CKlearControllerMain::slotSetScreensaverTimeout()
{
      kdDebug() << "Set screensaver timeout to: " << this->KlearConfig->getDisableScreensaver() << " seconds" << endl;
      if (this->KlearConfig->getDisableScreensaver() > 0)
         screensaverTimer.start( this->KlearConfig->getDisableScreensaver() * 1000);
      else
         screensaverTimer.stop();
}

void CKlearControllerMain::wheelEvent (  QWheelEvent * e )
{
    kdDebug() << "Wheelevent...\n";

        e->accept();
        int value = volumeSlider->value() - e->delta()/20;

       if ( value >100 ) value=100;
       if ( value<0 ) value=0;

        volumeSlider->setValue ( value );
        slotSetVolume();
}

void CKlearControllerMain::keyPressEvent(QKeyEvent* e)
{
    if(e->text()=="Esc")
    {
        kdDebug() << "Esc Pressed \n";
    }
}

