#include "CKlearControllerTXTMain.h"

CKlearControllerTXTMain::CKlearControllerTXTMain( CKlearAppConfig *config, QWidget* parent, const char* name, WFlags fl ) : CKlearUITXTMain( parent, name, fl )
{
  const QString __FUNC__ = "CKlearControllerTXTMain::CKlearControllerTXTMain()";

  this->counter    = 0;
  this->digit      = 0;
  this->row        = 0;
  this->position   = 0;
  this->alpha      = "";
  this->character  = "";
  this->pageNumber = "100";

  this->KlearConfig = config;
  this->TXTDecoder  = new CKlearAppTXTDecoder( this->KlearConfig );
  this->TXTStreamer = new CKlearAppTXTStreamer( this->KlearConfig, this->TXTDecoder );

  setDefaultSettings();
  createWindow();

  this->TXTStreamer->start();
  this->TXTDecoder->pageNumber = this->pageNumber;

  connect( this->TXTDecoder, SIGNAL( changedHeaderInfo() ), this, SLOT( slotPaintHeader() ) );
  connect( this->TXTDecoder, SIGNAL( changedBodyInfo() ), this, SLOT( slotPaintBody() ) );
}

CKlearControllerTXTMain::~CKlearControllerTXTMain()
{
  const QString __FUNC__ = "CKlearControllerTXTMain::~CKlearControllerTXTMain()";

  delete this->TXTDecoder;
  delete this->TXTStreamer;
  delete this->KlearConfig;
}

void CKlearControllerTXTMain::createWindow()
{
  const QString __FUNC__ = "CKlearControllerTXTMain::createWindow()";

  int width  = 680;
  int height = 525;

  this->setCaption( "Klear - Teletext: " + this->KlearConfig->getCurrentChannel() );
  this->setFixedSize( width, height );

  setRectWidth( width );
  setRectHeight( height );
  setMosaicsWidth( getRectWidth() );
  setMosaicsHeight( getRectHeight() );

  setTXTFont();
}

void CKlearControllerTXTMain::slotPaintHeader()
{
  const QString __FUNC__ = "CKlearControllerTXTMain::slotPaintHeader()";

  this->TXTDecoder->bodyMutex.lock();
  QPainter *p = new QPainter( this );
  setDefaultSettings();

  for( int column = counter; column < 32; column++ )
  {
    this->TXTDecoder->headerMutex.lock();
    setBackgroundColor( this->TXTDecoder->TXTHeaderData[ column ] );
    setAlphaColor( this->TXTDecoder->TXTHeaderData[ column ] );
    setMosaicsColor( this->TXTDecoder->TXTHeaderData[ column ] );
    setLatinOption( this->TXTDecoder->TXTHeaderData[ column ] );
    this->TXTDecoder->headerMutex.unlock();

    p->fillRect( getRectWidth() * ( column + 8 ), 0, getRectWidth(), getRectHeight(),  getBackgroundColor() );
    p->setPen( getAlphaColor() );
    p->setFont( getTXTFont() );

    this->alphaMutex.lock();
    p->drawText( getRectWidth() * ( column + 8 ), 0, getRectWidth(), getRectHeight(), Qt::AlignCenter, this->alpha );
    this->alphaMutex.unlock();
  }

  delete p;
  this->TXTDecoder->bodyMutex.unlock();
}

void CKlearControllerTXTMain::slotPaintBody()
{
  const QString __FUNC__ = "CKlearControllerTXTMain::slotPaintBody()";

  this->TXTDecoder->bodyMutex.lock();
  QPainter *p = new QPainter( this );
  setDefaultSettings();
  this->counter = 3;

  for( int column = 0; column < 3; column++ )
  {
    this->alphaMutex.lock();
    this->alpha = this->pageNumber.at( column );
    this->alphaMutex.unlock();

    p->fillRect( getRectWidth() * ( column + 8 ), 0, getRectWidth(), getRectHeight(),  getBackgroundColor() );
    p->setPen( getAlphaColor() );
    p->setFont( getTXTFont() );
    this->alphaMutex.lock();
    p->drawText( getRectWidth() * ( column + 8 ), 0, getRectWidth(), getRectHeight(), Qt::AlignCenter, this->alpha );
    this->alphaMutex.unlock();
  }

  this->position = 1;
  this->row = 0;

  while( this->position != 25 )
  {
    setDefaultSettings();
    setDoubleHeight( this->row );

    for ( int column = 0; column < 40; column++ )
    {
      if ( this->TXTDecoder->TXTBodyData[ this->row ][ column ] <= 31 )
      {
        setBackgroundColor( this->TXTDecoder->TXTBodyData[ this->row ][ column ] );
        setAlphaColor( this->TXTDecoder->TXTBodyData[ this->row ][ column ] );
        setMosaicsColor( this->TXTDecoder->TXTBodyData[ this->row ][ column ] );
        setHoldMosaics( this->row, column );
      }

      p->fillRect( getRectWidth() * column, getRectHeight() * this->position, getRectWidth(), getRectHeight() * this->doubleHeight, getBackgroundColor() );

      if( this->character == "alpha" )
      {
        setLatinOption( this->TXTDecoder->TXTBodyData[ this->row ][ column ] );
        p->setPen( getAlphaColor() );
        p->setFont( getTXTFont() );
        p->drawText( getRectWidth() * column, getRectHeight() * this->position, getRectWidth(), getRectHeight() * this->doubleHeight, Qt::AlignCenter, this->alpha );
      }

      if( this->character == "mosaics" )
      {
        if ( this->TXTDecoder->TXTBodyData[ this->row ][ column ] <= 31 )
          for ( int i = 0; i < 7; i++ )
            this->binary[ i ] = 0;

        if ( this->TXTDecoder->TXTBodyData[ this->row ][ column ] >= 32 )
          setBinary( this->TXTDecoder->TXTBodyData[ this->row ][ column ] );

        if ( this->isHoldMosaics )
          setBinary( this->holdMosaics );

        // rechts unten
        if( this->binary[ 0 ] == 1 )
          p->fillRect( getRectWidth() * column + getMosaicsWidth(), getRectHeight() * this->position + ( 2 * getMosaicsHeight() ), getMosaicsWidth(), getMosaicsHeight(), getMosaicsColor() );
        else
          p->fillRect( getRectWidth() * column + getMosaicsWidth(), getRectHeight() * this->position + ( 2 * getMosaicsHeight() ), getMosaicsWidth(), getMosaicsHeight(), getBackgroundColor() );

        // links unten
        if( this->binary[ 2 ] == 1 )
          p->fillRect( getRectWidth() * column, getRectHeight() * this->position  + ( 2 * getMosaicsHeight() ), getMosaicsWidth(), getMosaicsHeight(), getMosaicsColor() );
        else
          p->fillRect( getRectWidth() * column, getRectHeight() * this->position  + ( 2 * getMosaicsHeight() ), getMosaicsWidth(), getMosaicsHeight(), getBackgroundColor() );

        // rechts mitte
        if( this->binary[ 3 ] == 1 )
          p->fillRect( getRectWidth() * column + getMosaicsWidth(), getRectHeight() * this->position + getMosaicsHeight(), getMosaicsWidth(), getMosaicsHeight(), getMosaicsColor() );
        else
          p->fillRect( getRectWidth() * column + getMosaicsWidth(), getRectHeight() * this->position + getMosaicsHeight(), getMosaicsWidth(), getMosaicsHeight(), getBackgroundColor() );

        // links mitte
        if( this->binary[ 4 ] == 1 )
          p->fillRect( getRectWidth() * column, getRectHeight() * this->position + getMosaicsHeight(), getMosaicsWidth(), getMosaicsHeight(), getMosaicsColor() );
        else
          p->fillRect( getRectWidth() * column, getRectHeight() * this->position + getMosaicsHeight(), getMosaicsWidth(), getMosaicsHeight(), getBackgroundColor() );

        // rechts oben
        if( this->binary[ 5 ] == 1 )
          p->fillRect( getRectWidth() * column + getMosaicsWidth(), getRectHeight() * this->position, getMosaicsWidth(), getMosaicsHeight(), getMosaicsColor() );
        else
          p->fillRect( getRectWidth() * column + getMosaicsWidth(), getRectHeight() * this->position, getMosaicsWidth(), getMosaicsHeight(), getBackgroundColor() );

       // links oben
        if( this->binary[ 6 ] == 1 )
          p->fillRect( getRectWidth() * column, getRectHeight() * this->position, getMosaicsWidth(), getMosaicsHeight(), getMosaicsColor() );
        else
          p->fillRect( getRectWidth() * column, getRectHeight() * this->position, getMosaicsWidth(), getMosaicsHeight(), getBackgroundColor() );

        if ( this->TXTDecoder->TXTBodyData[ this->row ][ column ] >= 16 && this->TXTDecoder->TXTBodyData[ this->row ][ column ] <= 23 && this->isHoldMosaics )
        {
          this->isHoldMosaics = false;
          setMosaicsColor( this->TXTDecoder->TXTBodyData[ this->row ][ column ] );
        }
      }
    }

    this->position++;
    this->row++;

    if ( this->doubleHeight == 2 )
    {
      this->doubleHeight = 1;
      this->position++;
    }
  }
  delete p;
  this->TXTDecoder->bodyMutex.unlock();
}

void CKlearControllerTXTMain::setRectHeight( int height )
{
  const QString __FUNC__ = "CKlearControllerTXTMain::setRectHeight( int height )";

  this->rectHeight = height / 25;
}

void CKlearControllerTXTMain::setRectWidth( int width )
{
  const QString __FUNC__ = "CKlearControllerTXTMain::setRectWidth( int width )";

  this->rectWidth = width / 40;
}

void CKlearControllerTXTMain::setMosaicsHeight( int rectHeight )
{
  const QString __FUNC__ = "CKlearControllerTXTMain::setMosaicsHeight( int rectHeight )";

  this->mosaicsHeight = rectHeight / 3;

  if ( mosaicsHeight % 3 == 0 )
    mosaicsHeight += 1;
  else
    mosaicsHeight;
}

void CKlearControllerTXTMain::setMosaicsWidth( int rectWidth )
{
  const QString __FUNC__ = "CKlearControllerTXTMain::setMosaicsWidth( int rectWidth )";

  this->mosaicsWidth = rectWidth / 2;

  if ( mosaicsWidth % 2 == 0 )
    mosaicsWidth += 1;
  else
    mosaicsWidth;
}

void CKlearControllerTXTMain::setTXTFont()
{
  const QString __FUNC__ = "CKlearControllerTXTMain::setTXTFont()";

  font.setFamily( "Helvetica" );
  font.setBold( true );
  font.setPixelSize( getRectHeight() - 1 );
}

void CKlearControllerTXTMain::setPageNumber( QString num )
{
  const QString __FUNC__ = "CKlearControllerTXTMain::setPageNumber( QString num )";

  this->TXTDecoder->bodyMutex.lock();

  this->pageNumber = "";

  if( this->digit == 0 )
  {
    this->number[ 0 ] = num;
    this->number[ 1 ] = "-";
    this->number[ 2 ] = "-";
  }

  if ( this->digit == 1 )
  {
    this->number[ 1 ] = num;
    this->number[ 2 ] = "-";
  }

  if ( this->digit == 2 )
    this->number[ 2 ] = num;

  for ( int i = 0; i < 3; i++ )
    this->pageNumber += this->number[ i ];

  this->TXTDecoder->bodyMutex.unlock();

  paintPageNumber();

  this->digit++;

  if( this->digit == 3 )
  {
    this->TXTDecoder->pageNumber = this->pageNumber;
    this->digit = 0;
    this->counter = 0;
  }
}

void CKlearControllerTXTMain::setDefaultSettings()
{
  const QString __FUNC__ = "CKlearControllerTXTMain::setDefaultSettings()";

  this->setPaletteBackgroundColor( Qt::black );
  this->backgroundColor = Qt::black;
  this->alphaColor = Qt::white;
  this->doubleHeight = 1;
  this->character = "alpha";
}

void CKlearControllerTXTMain::setBackgroundColor( int data )
{
  const QString __FUNC__ = "CKlearControllerTXTMain::setBackgroundColor( int data )";

  if( data == 29 )
    this->backgroundColor = this->alphaColor;
}

void CKlearControllerTXTMain::setAlphaColor( int data )
{
  const QString __FUNC__ = "CKlearControllerTXTMain::setAlphaColor( int data )";

  if( data >= 0 && data <= 7 || data == 28 )
  {
    switch( data )
    {
      case 0: this->alphaColor = Qt::black; break;
      case 1: this->alphaColor = Qt::red; break;
      case 2: this->alphaColor = Qt::green; break;
      case 3: this->alphaColor = Qt::yellow; break;
      case 4: this->alphaColor = Qt::blue; break;
      case 5: this->alphaColor = Qt::magenta; break;
      case 6: this->alphaColor = Qt::cyan; break;
      case 7: this->alphaColor = Qt::white; break;
    }
    this->character = "alpha";
  }
}

void CKlearControllerTXTMain::setMosaicsColor( int data )
{
  const QString __FUNC__ = "CKlearControllerTXTMain::setMosaicsColor( int data )";

  if( data >= 16 && data <= 23 )
  {
    if ( !this->isHoldMosaics )
      switch( data )
      {
        case 16: this->mosaicsColor = Qt::black; break;
        case 17: this->mosaicsColor = Qt::red; break;
        case 18: this->mosaicsColor = Qt::green; break;
        case 19: this->mosaicsColor = Qt::yellow; break;
        case 20: this->mosaicsColor = Qt::blue; break;
        case 21: this->mosaicsColor = Qt::magenta; break;
        case 22: this->mosaicsColor = Qt::cyan; break;
        case 23: this->mosaicsColor = Qt::white; break;
      }
      this->character = "mosaics";
  }
}

void CKlearControllerTXTMain::setLatinOption( int data )
{
  const QString __FUNC__ = "CKlearControllerTXTMain::setLatinOption( int data )";

  alphaMutex.lock();

  switch( data )
  {
    case 35: this->alpha = "#"; break;
    case 36: this->alpha = "$"; break;
    case 64: this->alpha = "�"; break;
    case 91: this->alpha = "�"; break;
    case 92: this->alpha = "�"; break;
    case 93: this->alpha = "�"; break;
    case 94: this->alpha = "^"; break;
    case 95: this->alpha = "_"; break;
    case 96: this->alpha = "�"; break;
    case 123: this->alpha = "�"; break;
    case 124: this->alpha = "�"; break;
    case 125: this->alpha = "�"; break;
    case 126: this->alpha = "�"; break;
    default: this->alpha = data;
  }
  alphaMutex.unlock();
}

void CKlearControllerTXTMain::setDoubleHeight( int row )
{
  const QString __FUNC__ = "CKlearControllerTXTMain::setDoubleHeight( int row )";

  for ( int i = 0; i < 40; i++ )
    if ( this->TXTDecoder->TXTBodyData[ row ][ i ] == 13 )
      this->doubleHeight = 2;
}

void CKlearControllerTXTMain::setHoldMosaics( int row, int column )
{
  const QString __FUNC__ = "CKlearControllerTXTMain::setHoldMosaics( int row, int column )";

  if ( this->TXTDecoder->TXTBodyData[ row ][ column ] == 30 )
  {
    this->isHoldMosaics = true;
    this->holdMosaics = this->TXTDecoder->TXTBodyData[ row ][ column - 1 ];
  }

  if ( this->TXTDecoder->TXTBodyData[ row ][ column ] == 31 )
    this->isHoldMosaics = false;
}

void CKlearControllerTXTMain::setBinary( int decimal )
{
  const QString __FUNC__ = "CKlearControllerTXTMain::setBinary( int decimal )";

  for ( int i = 6; i >= 0; i-- )
  {
    this->binary[ i ] = decimal % 2;
    decimal = decimal / 2;
  }
}

void CKlearControllerTXTMain::paintPageNumber()
{
  const QString __FUNC__ = "CKlearControllerTXTMain::paintPageNumber()";

  this->TXTDecoder->bodyMutex.lock();
  QPainter *p = new QPainter( this );

  for ( int i = 0; i < 2; i++ )
    p->fillRect( getRectWidth() * i , 0, getRectWidth(), getRectHeight(), Qt::black );

  for ( int i = 0; i < 3; i++ )
  {
    p->fillRect( getRectWidth() * ( i + 2 ), 0, getRectWidth(), getRectHeight(), Qt::black );
    p->setPen( Qt::white );
    p->setFont( getTXTFont() );
    p->drawText( getRectWidth() * ( i + 2 ), 0, getRectWidth(), getRectHeight(), Qt::AlignLeft, this->number[ i ] );
  }

  for ( int i = 0; i < 2; i++ )
    p->fillRect( getRectWidth() * ( i + 5 ), 0, getRectWidth(), getRectHeight(), Qt::black );

  delete p;
  this->TXTDecoder->bodyMutex.unlock();
}

void CKlearControllerTXTMain::paintEvent( QPaintEvent *pe )
{
    const QString __FUNC__ = "CKlearControllerTXTMain::paintEvent( QPaintEvent *pe )";

    slotPaintHeader();
    slotPaintBody();
    paintPageNumber();
}

void CKlearControllerTXTMain::keyPressEvent( QKeyEvent *ke )
{
  const QString __FUNC__ = "CKlearControllerTXTMain::keyPressEvent( QKeyEvent *ke )";

  QString num;

  for ( int i = 0; i <= 9; i++ )
  {
    num = num.setNum( i );
    if( ke->text() == num )
      setPageNumber( num );
  }
}

void CKlearControllerTXTMain::closeEvent( QCloseEvent *ce )
{
  const QString __FUNC__ = "CKlearControllerTXTMain::closeEvent( QCloseEvent *ce )";

  this->TXTStreamer->setRunning( false );

  while( this->TXTStreamer->running() )
    this->TXTStreamer->wait();

  std::cout << "TXT stopped" << std::endl;

  this->hide();
  this->close();
}

int CKlearControllerTXTMain::getRectHeight()
{
  const QString __FUNC__ = "CKlearControllerTXTMain::getRectHeight()";

  return this->rectHeight;
}

int CKlearControllerTXTMain::getRectWidth()
{
  const QString __FUNC__ = "CKlearControllerTXTMain::getRectWidth()";

  return this->rectWidth;
}

int CKlearControllerTXTMain::getMosaicsHeight()
{
  const QString __FUNC__ = "CKlearControllerTXTMain::getMosaicsHeight()";

  return mosaicsHeight;
}

int CKlearControllerTXTMain::getMosaicsWidth()
{
  const QString __FUNC__ = "CKlearControllerTXTMain::getMosaicsWidth()";

  return mosaicsWidth;
}

int CKlearControllerTXTMain::getHoldMosaics()
{
  const QString __FUNC__ = "CKlearControllerTXTMain::getHoldMosaics()";

  return this->holdMosaics;
}

QString CKlearControllerTXTMain::getPageNumber()
{
  const QString __FUNC__ = "CKlearControllerTXTMain::getPageNumber()";

  return this->pageNumber;
}

QColor CKlearControllerTXTMain::getBackgroundColor()
{
  const QString __FUNC__ = "CKlearControllerTXTMain::getBackgroundColor()";

  return this->backgroundColor;
}

QColor CKlearControllerTXTMain::getAlphaColor()
{
  const QString __FUNC__ = "CKlearControllerTXTMain::getAlphaColor()";

  return this->alphaColor;
}

QColor CKlearControllerTXTMain::getMosaicsColor()
{
  const QString __FUNC__ = "CKlearControllerTXTMain::getMosaicsColor()";

  return this->mosaicsColor;
}

QFont CKlearControllerTXTMain::getTXTFont()
{
  const QString __FUNC__ = "CKlearControllerTXTMain::getTXTFont()";

  return font;
}
