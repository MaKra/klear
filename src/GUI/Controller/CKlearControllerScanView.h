/**
\author Klear.org
\file CKlearControllerScanView.h
\brief Controllerfile of scan window
*/

#ifndef CKLEARCONTROLLERSCANVIEW_H
#define CKLEARCONTROLLERSCANVIEW_H


#include "../View/CKlearUIScanView.h"

#include <qpixmap.h>
#include <qthread.h>
#include <qlistbox.h>
#include "kdebug.h"
#include <qlabel.h>
#include <qpushbutton.h>
#include <qprogressbar.h>

//#include "../../Utilities/icons.cpp"            // the icons

#include "../../App/Exceptions/CKlearAppException.h"       // klear exception class
#include "../../App/Exceptions/CKlearAppErrorException.h"       // klear exception class
#include "../../App/Exceptions/CKlearAppFatalException.h"       // klear exception class
#include <exception>

class CKlearControllerScanView : public CKlearUIScanView, public QThread
{
  Q_OBJECT


public:

    CKlearControllerScanView( QWidget* parent, const char* name, WFlags fl );

    void scan();
 virtual void run();



public slots:
    virtual void slotScan();
    virtual void slotOk();
    virtual void slotAbort();
    virtual void slotQuit();

private:
 bool IsRunning;

};



#endif
