/**
 \author Klear.org
 \file CKlearControllerConfig.cpp
 \brief Controller class for graphical configuration class
*/

#include "CKlearControllerConfig.h"

CKlearControllerConfig::CKlearControllerConfig(CKlearAppConfig *KlearConfig, QWidget* parent, const char* name, bool modal, WFlags fl) : CKlearUIConfig(parent,name, modal,fl)
{
  this->KlearConfig = KlearConfig; // set external config via reference to local member

  // now fill the GUI lines with the current setting values
  this->ConfigAdapterLine->setText( this->KlearConfig->getDVBAdapter() );
  this->ConfigFrontendLine->setText( this->KlearConfig->getDVBFrontend() );
  this->ConfigDemuxLine->setText( this->KlearConfig->getDVBDemux() );
  this->ConfigDVRLine->setText( this->KlearConfig->getDVBDvr() );

  this->muteSystrayCheckbox->setChecked( this->KlearConfig->getMuteSystray() );
  this->checkboxIsOsdAutoShown->setChecked( this->KlearConfig->getAutoShowOsd() );
  this->ScreenSaverTimeoutBox->setValue( this->KlearConfig->getDisableScreensaver() );

  // this way is independent from the combobox text. This makes i18n possible, MKr, 21.11.04
  if(this->KlearConfig->getDVBMode() == 1)  // terrestric
     this->ConfigModeBox->setCurrentItem(0);
  else if(this->KlearConfig->getDVBMode() == 2)  // satellite
     this->ConfigModeBox->setCurrentItem(1);
  else if(this->KlearConfig->getDVBMode() == 3)  // cable
    this->ConfigModeBox->setCurrentItem(2);

  if(this->KlearConfig->getPlaybackEngine() == "KlearXineTNG")
     this->ConfigEngineBox->setCurrentItem(0);
  else if(this->KlearConfig->getPlaybackEngine() == "KlearXine")
    this->ConfigEngineBox->setCurrentItem(1);
  else if(this->KlearConfig->getPlaybackEngine() == "KlearMPlayer")
    this->ConfigEngineBox->setCurrentItem(2);

  if(this->KlearConfig->getMenuAlignment() == 0)  // current value is RIGHT...
       this->MenuAlignBox->setCurrentItem(0);
  else  // ....or LEFT
       this->MenuAlignBox->setCurrentItem(1);

  this->ConfigScreenshotDirLine->setText( this->KlearConfig->getScreenshotDir() );
  this->ConfigScreenshotNameLine->setText( this->KlearConfig->getScreenshotName() );
  this->ConfigScreenshotFormatBox->setCurrentText( this->KlearConfig->getScreenshotFormat() );

  this->ConfigRecordingDirLine->setText( this->KlearConfig->getRecordingDir() );
  this->ConfigRecordingNameLine->setText( this->KlearConfig->getRecordingName() );
  this->ConfigRecordingFormatBox->setCurrentText( this->KlearConfig->getRecordingFormat() );
  this->loadChannelList();

  this->EPGpreMarginBox->setValue( this->KlearConfig->getPreEPGmargin() );
  this->EPGpostMarginBox->setValue( this->KlearConfig->getPostEPGmargin() );

  presetAcceleratorKeysettings();
  //QObject::connect( this->ChannelListBox, SIGNAL ( doubleClicked ( QListBoxItem * item ) ), this, SLOT( slotRenameItem() ) );
}

CKlearControllerConfig::~CKlearControllerConfig()
{
}

void CKlearControllerConfig::slotCancelSettings()
{
   this->close();  // do nothing. Action aborted.
}

void CKlearControllerConfig::slotSaveSettings()
{
   const QString __FUNC__ = "CKlearControllerConfig::slotSaveSettings()";

   kdDebug() << "Saving new settings\n";
   this->KlearConfig->setDVBAdapter( this->ConfigAdapterLine->text() );
   this->KlearConfig->setDVBDvr( this->ConfigDVRLine->text() );
   this->KlearConfig->setDVBDemux( this->ConfigDemuxLine->text() );
   this->KlearConfig->setDVBFrontend( this->ConfigFrontendLine->text() );

   this->KlearConfig->setMuteSystray( this->muteSystrayCheckbox->isChecked() );
   this->KlearConfig->setAutoShowOsd( this->checkboxIsOsdAutoShown->isChecked() );
   this->KlearConfig->setDisableScreensaver( this->ScreenSaverTimeoutBox->value() );

   // this way is independent from the combobox text. This makes i18n possible, MKr, 21.11.04
   if( this->ConfigModeBox->currentItem() == 0 )
      this->KlearConfig->setDVBMode( 1 );   // terrestric
   else if( this->ConfigModeBox->currentItem() == 1 )
      this->KlearConfig->setDVBMode( 2 );  // satellite
   else if( this->ConfigModeBox->currentItem() == 2 )
      this->KlearConfig->setDVBMode( 3 );  // cable
   else // should never happen....to prevent errors
      this->KlearConfig->setDVBMode( 1 );   // terrestric default

   // this way is independent from the combobox text. This makes i18n possible, MKr, 21.11.04
   if( this->MenuAlignBox->currentItem() == 0 )  // set to RIGHT
      this->KlearConfig->setMenuAlignment( 0 );
   else if( this->MenuAlignBox->currentItem() == 1 ) // set to LEFT
      this->KlearConfig->setMenuAlignment( 1 );
   else                                                     // SHOULD NEVER HAPPEN
      this->KlearConfig->setMenuAlignment( 0 );

   if( this->ConfigEngineBox->currentItem() == 0 )
      this->KlearConfig->setPlaybackEngine( "KlearXineTNG" );
   else if( this->ConfigEngineBox->currentItem() == 1 )
      this->KlearConfig->setPlaybackEngine( "KlearXine" );
   else if( this->ConfigEngineBox->currentItem() == 2 )
      this->KlearConfig->setPlaybackEngine( "KlearMPlayer" );

   this->KlearConfig->setScreenshotDir( this->ConfigScreenshotDirLine->text() );
   this->KlearConfig->setScreenshotName( this->ConfigScreenshotNameLine->text() );
   this->KlearConfig->setScreenshotFormat( this->ConfigScreenshotFormatBox->currentText() );

   this->KlearConfig->setRecordingDir( this->ConfigRecordingDirLine->text() );
   this->KlearConfig->setRecordingName( this->ConfigRecordingNameLine->text() );
   this->KlearConfig->setRecordingFormat( this->ConfigRecordingFormatBox->currentText() );

   // set EPG recording margins
   this->KlearConfig->setPreEPGmargin( this->EPGpreMarginBox->value() );
   this->KlearConfig->setPostEPGmargin( this->EPGpostMarginBox->value() );

  // store channel list
   try{
      this->saveChannelList();
   }catch ( CKlearAppException &e ){
           e.addShowException(__LOC__, i18n("Channellist could not be saved.. \n"));
   }

   // save acc keys
   this->storeAcceleratorKeysettings();

   // write settings and close dialog
   this->KlearConfig->WriteConfig(); // and save the changes data

   this->close();  // and finally also close this dialog
}

void CKlearControllerConfig::slotDeleteListEntry()
{
    kdDebug() << "slotDeleteListEntry " << ChannelListBox->currentItem()  << "\n";

    if( ChannelListBox->currentItem() < 0 )
    return;

    int index = ChannelListBox->currentItem();
    this->ChannelListBox->removeItem( index );
    ChannelListBox->setSelected( ChannelListBox->currentItem () ,true  );

    kdDebug() << "delete "<< index << "'th  of " << ChannelStorage.size()<< "elements\n";

    // move all remaing elements one step forward
/*
     for( unsigned int i = 0 + index; i < ChannelStorage.size()-1; i++ )
     {   kdDebug() << "copy  element "<< ChannelStorage[i+1] << "to " << ChannelStorage[i] << "\n";
                  ChannelStorage[i] = ChannelStorage[i+1];
     }
    ChannelStorage[ChannelStorage.size()-1] = ChannelStorage.back();
     // delete the last one

  //  ChannelStorage.back() = "";

  ChannelStorage.erase( ChannelStorage.begin() + ChannelStorage.size() );*/


ChannelStorage.erase( ChannelStorage.begin() + index );

    kdDebug() << "deleted   last new vector size "<< ChannelStorage.size()<< "\n";

     for( unsigned int i = 0; i < ChannelStorage.size(); i++ )
     {  kdDebug() << "item "<< ChannelStorage[i]<< "\n";

     }

}


void CKlearControllerConfig::slotPushToTop()
{
    kdDebug() << "slotPushToTop\n";
    if( ChannelListBox->currentItem() < 0 )
    return;


    if( (ChannelListBox->currentItem() != 0 ) && ( ChannelListBox->currentItem() != -1 ))
    {
        int index = ChannelListBox->currentItem();
        swapChannelinListBox( index, 0 );
        swapChannelinStorage( index, 0 );
    }
}


void CKlearControllerConfig::slotPushOneUp()
{
    kdDebug() << "slotPushOneUp\n";
    if( ChannelListBox->currentItem() < 0 )
    return;


    if( ( ChannelListBox->currentItem() != 0 ))
    {
        int index = ChannelListBox->currentItem();
        swapChannelinListBox( index,  index - 1 );
        swapChannelinStorage( index, index -1);;
    }

}


void CKlearControllerConfig::slotPushToBottom()
{
    kdDebug() << "slotPushToBottom\n";
    if( ChannelListBox->currentItem() < 0 )
    return;

    if( ( ChannelListBox->currentItem() < ChannelListBox->count() -1 ) )
    {
        int index = ChannelListBox->currentItem();
        swapChannelinListBox( index,  ChannelListBox->count() -1);
        swapChannelinStorage( index,  ChannelListBox->count() -1);
    }
}


void CKlearControllerConfig::slotPushOneDown()
{
    kdDebug() << "slotPushOneDown\n";
    if( ChannelListBox->currentItem() < 0 )
    return;

    if( ( ChannelListBox->currentItem() < ChannelListBox->count() -1 ) )
    {
        int index = ChannelListBox->currentItem();
        swapChannelinListBox( index,  index + 1 );
        swapChannelinStorage( index,  index + 1 );
    }
}


void CKlearControllerConfig::loadChannelList()
{
  const QString __FUNC__ = "CKlearControllerConfig::loadChannelList()";

  QFile ChannelsConf( KlearConfig->getKlearConfigPath()+"/"+KlearConfig->getKlearChannelsConf() );
   std::cout << "Channelfile: " << ChannelsConf.name() << std::endl;
  if ( ChannelsConf.exists() && ChannelsConf.open( IO_ReadOnly ) )
  {
        kdDebug() << "ControllerConfig: Channels.conf found. Reading in...\n";
        QTextStream stream( &ChannelsConf );
        QString line;
        for (int i=1; !stream.atEnd(); i++ )
        {
            line = stream.readLine().latin1();
            ChannelStorage.push_back( line );
            QString channel = line.section( ':', 0, 0 );
            this->ChannelListBox->insertItem ( channel, i );
        }
        ChannelsConf.close();
        ChannelListBox->setSelected( 0, true);
   }else{
        kdDebug() << "No channels.conf found\n";
    // there is no need to break up klear here. Checking for a channels.conf is also done in tuner.
    // Config should be available also without a channels.conf file // MKr // 25.01.2005
    //CKlearAppException e( __LOC__, i18n("<h3>No channels.conf found. You need a t/c/s-zap compatible channels.conf in your .klear directory. You can find a lot of channels.conf on our website www.klear.org or you can create your own with the scan utility from the dvb-utils packages</h3>"));
    //throw e;
  }
}


void CKlearControllerConfig::saveChannelList()
{
  const QString __FUNC__ = "CKlearControllerConfig::saveChannelList()";

  kdDebug() << "Saving ChannelList\n";

  QFile ChannelsConf( KlearConfig->getKlearConfigPath()+"/"+KlearConfig->getKlearChannelsConf()/* +"2"*/);

  kdDebug() << KlearConfig->getKlearConfigPath()+"/"+KlearConfig->getKlearChannelsConf() <<"\n";

  // only save channels.conf when already existing; prevents creating zero-byte file / invalid channels.conf
  if( ChannelsConf.exists() )
  {
         if ( ChannelsConf.open( IO_ReadWrite | IO_Truncate) )
         {
               QTextStream stream( &ChannelsConf );

               for (unsigned int i=0; i < (ChannelStorage.size() ) ; i++ )
               {
                     stream << ChannelStorage[i] << "\n";
               }
               ChannelsConf.close();

            }else{
            kdDebug() << "Error in writting channels.conf \n";

            CKlearAppException e( __LOC__, i18n("<h3>Error in writting channels.conf</h3>"));
            throw e;
         }
   }
}


void CKlearControllerConfig::swapChannelinStorage( int indexOld, int indexNew )
{
     QString Channel = ChannelStorage[indexOld];
     ChannelStorage.erase( ChannelStorage.begin() + indexOld );
     ChannelStorage.insert( ChannelStorage.begin() + indexNew, Channel);
     kdDebug() << "inserted " << ChannelStorage[indexNew] << "\n";
}

void CKlearControllerConfig::swapChannelinListBox( int indexOld, int indexNew  )
{
    QString text = ChannelListBox->currentText();
    ChannelListBox->removeItem( indexOld );
    ChannelListBox->insertItem( text, indexNew );
    ChannelListBox->setSelected( indexNew, true  );
    ChannelListBox->ensureCurrentVisible();
}

void CKlearControllerConfig::slotRenameItem()
{
    if( ChannelListBox->currentItem() < 0 )
    return;

    kdDebug() << "renamed " << ChannelListBox->currentText() << "\n";

    bool ok;
    QString text = QInputDialog::getText(
            i18n("Enter new name for selected channel"), i18n("Enter new name for channel: ") + ChannelListBox->currentText(), QLineEdit::Normal,
            ChannelListBox->currentText(), &ok, this );
    if ( ok && !text.isEmpty() ) {

        // user entered something and pressed OK
        ChannelListBox->changeItem( text, ChannelListBox->currentItem() );
        QString tmp = (this->ChannelStorage[ ChannelListBox->currentItem() ]).section( ':', 1, 12 );
        tmp = ChannelListBox->currentText() + ":" + tmp;
        this->ChannelStorage[ ChannelListBox->currentItem() ] = tmp;

    } else {
        // user entered nothing or pressed Cancel
          kdDebug() << "cancelled\n";
    }
}


void CKlearControllerConfig::slotScan()
{
    const QString __FUNC__ = "CKlearControllerConfig::slotScan()";

   CKlearControllerScanView::CKlearControllerScanView(this, 0, 0).exec();
}


void CKlearControllerConfig::slotClear()
{
     kdDebug() << "slotClear\n";
     ChannelListBox->clear();
     ChannelStorage.clear();
}


void CKlearControllerConfig::keyPressEvent( QKeyEvent *e )
{
    // only one key is mapped conjunction with e->state() seems not to work...

   // only proccess any further if right tab "keys" is the curret one
   if (  this->tabWidget->currentPageIndex() == 5  )
    {

        if ( this->lineEdit_DecChannel->hasFocus() && ( this->lineEdit_DecChannel->text() != (QString)QKeySequence( e->key() ) ) )
        {
            this->lineEdit_DecChannel->setText( isKeyInUse( QKeySequence( e->key() ) ) );
            kdDebug() <<  (QString)QKeySequence( e->key())<< "\n";
        }

        if ( this->lineEdit_IncChannel->hasFocus() && ( this->lineEdit_IncChannel->text() != (QString)QKeySequence( e->key() ) ) )
        {
            this->lineEdit_IncChannel->setText( isKeyInUse( (QString)QKeySequence( e->key() ) ) );
            kdDebug() <<  (QString)QKeySequence( e->key())<< "\n";
        }

        if ( this->lineEdit_Deinterlace->hasFocus() && ( this->lineEdit_Deinterlace->text() != (QString)QKeySequence( e->key() ) )  )
        {
            this->lineEdit_Deinterlace->setText( isKeyInUse( (QString)QKeySequence( e->key() ) ) );
            kdDebug() <<  (QString)QKeySequence( e->key())<< "\n";
        }

        if ( this->lineEdit_Fullscreen->hasFocus() && ( this->lineEdit_Fullscreen->text() != (QString)QKeySequence( e->key() ) )  )
        {
            this->lineEdit_Fullscreen->setText( isKeyInUse( (QString)QKeySequence( e->key() ) ) );
            kdDebug() <<  (QString)QKeySequence( e->key())<< "\n";
        }

        if ( this->lineEdit_Channellist->hasFocus()  && ( this->lineEdit_Channellist->text() != (QString)QKeySequence( e->key() ) ) )
        {
            this->lineEdit_Channellist->setText( isKeyInUse( (QString)QKeySequence( e->key() ) ) );
            kdDebug() <<  (QString)QKeySequence( e->key())<< "\n";
        }

        if ( this->lineEdit_Osd->hasFocus()  && ( this->lineEdit_Osd->text() != (QString)QKeySequence( e->key() ) ) )
        {
            this->lineEdit_Osd->setText( isKeyInUse( (QString)QKeySequence( e->key() ))  );
            kdDebug() <<  (QString)QKeySequence( e->key())<< "\n";
        }

        if ( this->lineEdit_Epg->hasFocus() && ( this->lineEdit_Epg->text() != (QString)QKeySequence( e->key() ) ))
        {
            this->lineEdit_Epg->setText( isKeyInUse( (QString)QKeySequence( e->key() ) ) );
            kdDebug() <<  (QString)QKeySequence( e->key())<< "\n";
        }

        if ( this->lineEdit_Quit->hasFocus() && ( this->lineEdit_Quit->text() != (QString)QKeySequence( e->key() ) ) )
        {
            this->lineEdit_Quit->setText( isKeyInUse( (QString)QKeySequence( e->key() ) ) );
            kdDebug() <<  (QString)QKeySequence( e->key())<< "\n";
        }

        if ( this->lineEdit_Config->hasFocus() && ( this->lineEdit_Config->text() != (QString)QKeySequence( e->key() ) ) )
        {
            this->lineEdit_Config->setText( isKeyInUse( (QString)QKeySequence( e->key() )) );
            kdDebug() <<  (QString)QKeySequence( e->key())<< "\n";
        }

        if ( this->lineEdit_About->hasFocus() && ( this->lineEdit_About->text() != (QString)QKeySequence( e->key() ) ) )
        {
            this->lineEdit_About->setText( isKeyInUse( (QString)QKeySequence( e->key() ))  );
            kdDebug() <<  (QString)QKeySequence( e->key())<< "\n";
        }

        if ( this->lineEdit_Mute->hasFocus() && ( this->lineEdit_Mute->text() != (QString)QKeySequence( e->key() ) ) )
        {
            this->lineEdit_Mute->setText( isKeyInUse( (QString)QKeySequence( e->key() ) ) );
            kdDebug() <<  (QString)QKeySequence( e->key())<< "\n";
        }

        if ( this->lineEdit_Recording->hasFocus() && ( this->lineEdit_Recording->text() != (QString)QKeySequence( e->key() ) ) )
        {
            this->lineEdit_Recording->setText( isKeyInUse( (QString)QKeySequence( e->key() ) ) );
            kdDebug() <<  (QString)QKeySequence( e->key())<< "\n";
        }

        if ( this->lineEdit_shoot->hasFocus() && ( this->lineEdit_shoot->text() != (QString)QKeySequence( e->key() ) ) )
        {
            this->lineEdit_shoot->setText( isKeyInUse( (QString)QKeySequence( e->key() ) ) );
            kdDebug() <<  (QString)QKeySequence( e->key())<< "\n";
        }

        if ( this->lineEdit_Timrshift->hasFocus() && ( this->lineEdit_Timrshift->text() != (QString)QKeySequence( e->key() ) ) )
        {
            this->lineEdit_Timrshift->setText( isKeyInUse( (QString)QKeySequence( e->key() ) ) );
            kdDebug() <<  (QString)QKeySequence( e->key())<< "\n";
        }
    }
}

void CKlearControllerConfig::presetAcceleratorKeysettings()
{
      this->lineEdit_DecChannel->setText(  KlearConfig->getAccDecChannel() );
      this->lineEdit_IncChannel->setText(  KlearConfig->getAccIncChannel() );
      this->lineEdit_Deinterlace->setText( KlearConfig->getAccDeinterlace());
      this->lineEdit_Fullscreen->setText( KlearConfig->getAccFullscreen());
      this->lineEdit_Channellist->setText( KlearConfig->getAccChannellist());
      this->lineEdit_Osd->setText( KlearConfig->getAccOsd());
      this->lineEdit_Epg->setText( KlearConfig->getAccEpg());
      this->lineEdit_Txt->setText( KlearConfig->getAccTxt());
      this->lineEdit_Quit->setText( KlearConfig->getAccQuit());
      this->lineEdit_Config->setText( KlearConfig->getAccConfig());
      this->lineEdit_About->setText( KlearConfig->getAccAbout());
      this->lineEdit_Mute->setText( KlearConfig->getAccMute());
      this->lineEdit_Recording->setText( KlearConfig->getAccRecording());
      this->lineEdit_shoot->setText( KlearConfig->getAccShoot());
      this->lineEdit_Timrshift->setText( KlearConfig->getAccTimeshift());
}


void CKlearControllerConfig::storeAcceleratorKeysettings()
{
      KlearConfig->setAccDecChannel( this->lineEdit_DecChannel->text() );
      KlearConfig->setAccIncChannel( this->lineEdit_IncChannel->text()  );
      KlearConfig->setAccChannellist( this->lineEdit_Channellist->text() );
      KlearConfig->setAccFullscreen( this->lineEdit_Fullscreen->text() );
      KlearConfig->setAccDeinterlace( this->lineEdit_Deinterlace->text()  );
      KlearConfig->setAccFullscreen( this->lineEdit_Fullscreen->text()  );
      KlearConfig->setAccChannellist(  this->lineEdit_Channellist->text()  );
      KlearConfig->setAccOsd( this->lineEdit_Osd->text() );
      KlearConfig->setAccEpg( this->lineEdit_Epg->text() );
      KlearConfig->setAccTxt( this->lineEdit_Txt->text() );
      KlearConfig->setAccQuit( this->lineEdit_Quit->text());
      KlearConfig->setAccConfig( this->lineEdit_Config->text() );
      KlearConfig->setAccAbout( this->lineEdit_About->text());
      KlearConfig->setAccMute( this->lineEdit_Mute->text() );
      KlearConfig->setAccRecording( this->lineEdit_Recording->text() );
      KlearConfig->setAccShoot( this->lineEdit_shoot->text() );
      KlearConfig->setAccTimeshift( this->lineEdit_Timrshift->text() );
}

QString CKlearControllerConfig::isKeyInUse( QString key )
{

   if ( ( key == ( this->lineEdit_DecChannel->text() ) )
            | ( key == ( this->lineEdit_IncChannel->text() ) )
            | ( key == ( this->lineEdit_Channellist->text() ) )
            | ( key == ( this->lineEdit_Fullscreen->text() ) )
            | ( key == ( this->lineEdit_Deinterlace->text() ) )
            | ( key == ( this->lineEdit_Fullscreen->text() ) )
            | ( key == ( this->lineEdit_Channellist->text() ) )
            | ( key == ( this->lineEdit_Osd->text() ) )
            | ( key == ( this->lineEdit_Epg->text() ) )
            | ( key == ( this->lineEdit_Txt->text() ) )
            | ( key == ( this->lineEdit_Quit->text() ) )
            | ( key == ( this->lineEdit_Config->text() ) )
            | ( key == ( this->lineEdit_About->text() ) )
            | ( key == ( this->lineEdit_Mute->text() ) )
            | ( key == ( this->lineEdit_shoot->text() ) )
            | ( key == ( this->lineEdit_Timrshift->text() ) )
            | ( key == ( this->lineEdit_Recording->text() ) )
       )
       {
            return i18n("Key in use !");
       }

    return key;
}
