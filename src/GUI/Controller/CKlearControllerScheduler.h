/**
\author Klear.org
\file CKlearControllerScheduler.h
\brief The scheduler dialog implementation for Klear.
*/

#ifndef CKLEARCONTROLLERSCHEDULER_H
#define CKLEARCONTROLLERSCHEDULER_H

#include <qlineedit.h>
#include <qfiledialog.h>
#include <qstring.h>
#include <qmessagebox.h>
#include <qcombobox.h>
#include <qdatetimeedit.h>
#include <qdatetime.h>

#include "../../App/CKlearAppConfig.h"    // the configure and setting algorithms
#include "../../App/CKlearAppScheduler.h"    // the scheduler setting algorithms
#include "../View/CKlearUIScheduler.h"
#include "../../App/CKlearAppRecordSet.h"

#include "../../App/Exceptions/CKlearAppException.h"

/**
\class CKlearControllerScheduler
\author Klear.org
\brief The GUI-controller to choose a new scheduled data to backend scheduler. The dialog writes to the klearscheduler
*/
class CKlearControllerScheduler : public CKlearUIScheduler
{
  Q_OBJECT

public:
    /**
    \fn CKlearControllerScheduler(CKlearAppScheduler *KlearScheduler, QWidget* parent = 0, const char* name = 0, WFlags fl = 0)
    \author Omar El-Dakhloul <omar@klear.org>
    \brief Constructor
    */
    CKlearControllerScheduler(CKlearAppScheduler *KlearScheduler, QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );

    /**
    \fn CKlearControllerScheduler(CKlearAppScheduler *KlearScheduler, QString chan, QDateTime start, QDateTime end, QString recname, QWidget* parent = 0, const char* name = 0, WFlags fl = 0)
    \author Marco Kraus <marco@klear.org>
    \brief Constructor overloaded for adding new item directly with paramters
    */
   CKlearControllerScheduler( CKlearAppScheduler *KlearScheduler, QString chan, QDateTime start, QDateTime end, QString recname, QWidget* parent = 0, const char* name = 0, WFlags fl = 0);

    /**
    \fn CKlearControllerScheduler(CKlearAppScheduler *KlearScheduler, CKlearAppRecordSet KlearRecordSet, QWidget* parent = 0, const char* name = 0, WFlags fl = 0 )
    \author Omar El-Dakhloul <omar@klear.org>
    \brief Constructor for edit window
    */
    CKlearControllerScheduler( CKlearAppScheduler *KlearScheduler, CKlearAppRecordSet KlearRecordSet, QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );

    /**
    \fn ~CKlearControllerScheduler()
    \author Omar El-Dakhloul <omar@klear.org>
    \brief Destructor
    */
    ~CKlearControllerScheduler();

public slots:

    /**
    \fn void slotGetChannel();
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \brief Get the active Channel from the combobox.
    */
    void slotGetChannel();

    /**
    \fn void slotBrowse()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \brief browse to  choose a directory (path).
    */
    void slotBrowse();

    /**
    \fn void slotAddRecordSet()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \brief Submit all scheduler information. Sends this information to CKlearScheduler::addRecordSet(...)
    */
    void slotAddRecordSet();

    /**
    \fn void slotCancelScheduler();
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \brief Quits KlearSchedulerDialog.
    */
    void slotCancelScheduler();

private:
    int channelIndex;
    CKlearAppConfig* KlearConfig;   // configuration class for klear-config
    CKlearAppScheduler* KlearScheduler;
    CKlearAppRecordSet  KlearRecordSetCopy;
    CKlearAppRecordSet *KlearRecordSet;

    void SchedulerInit();

    void loadChannelList();
    void loadDateTime();
    void loadPathLine();
};

#endif
