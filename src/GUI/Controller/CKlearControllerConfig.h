/**
\author Klear.org
\file CKlearControllerConfig.h
\brief The GUI controller to configure Klear.
*/

#ifndef CKLEARCONTROLLERCONFIG_H
#define CKLEARCONTROLLERCONFIG_H

#include <qtabwidget.h>
#include <qkeysequence.h>
#include <qlineedit.h>
#include <qcombobox.h>
#include <qlistbox.h>
#include <qcheckbox.h>
#include <qspinbox.h>
#include <qstring.h>
#include <qinputdialog.h>
#include <qevent.h>
#include <vector>
#include <iostream>

#include "../../App/Exceptions/CKlearAppException.h"
#include "../../App/CKlearAppConfig.h"      // the configure and setting algorithms
#include "../View/CKlearUIConfig.h"
#include "../View/CKlearUIScanView.h"
#include "./CKlearControllerScanView.h"

/**
\class CKlearControllerConfig
\author Klear.org
\brief The GUI to configure Klear. The dialog writes to the klearconfig class
*/
class CKlearControllerConfig : public CKlearUIConfig
{
  Q_OBJECT

public:
     /**
     \fn CKlearControllerConfig( CKlearAppConfig *KlearConfig, QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 )
     \author Marco Kraus <marco@klear.org>
     \brief Constructor
     */
     CKlearControllerConfig( CKlearAppConfig *KlearConfig, QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );

     /**
     \fn void ~CKlearControllerConfig()
     \author Marco Kraus <marco@klear.org>
     \brief Destructor
     */
     ~CKlearControllerConfig();

public slots:
     /**
     \fn void slotCancelSettings()
     \author Marco Kraus <marco@klear.org>
     \retval void no returnvalue
     \brief User wants to abort, close window without saving changes
     */
     void slotCancelSettings();

     /**
     \fn void slotSaveSettings()
     \author Marco Kraus <marco@klear.org>
     \retval void no returnvalue
     \brief User wants to save file, so save GUI data to Configobject and say object to save to disk
     */
     void slotSaveSettings();

       /**
     \fn void slotPushToTop( )
     \author Manuel Habermann <manuel@klear.org>
     \retval void no return value
     \brief user wants to move channel to top position
     */
     void slotPushToTop();

       /**
     \fn void slotPushOneUp( )
     \author Manuel Habermann <manuel@klear.org>
     \retval void no return value
     \brief user wants to move channel on position up
     */
     void slotPushOneUp();

       /**
     \fn void slotPushToBottom( )
     \author Manuel Habermann <manuel@klear.org>
     \retval void no return value
     \brief user wants to move channel to last position
     */
     void slotPushToBottom();

       /**
     \fn void slotPushOneDown( )
     \author Manuel Habermann <manuel@klear.org>
     \retval void no return value
     \brief user wants to move channel on position down
     */
     void slotPushOneDown();

      /**
     \fn void slotDeleteListEntry( )
     \author Manuel Habermann <manuel@klear.org>
     \retval void no return value
     \brief user wants to delete current channel
     */
     void slotDeleteListEntry();

      /**
     \fn void slotScan( )
     \author Manuel Habermann <manuel@klear.org>
     \retval void no return value
     \brief user wants to scan for new channels
     */
     void slotScan();


        /**
     \fn void slotClear( )
     \author Manuel Habermann <manuel@klear.org>
     \retval void no return value
     \brief user wants to remove all existing channels
     */
     void slotClear();


      /**
     \fn void slotRenameItem( )
     \author Manuel Habermann <manuel@klear.org>
     \retval void no return value
     \brief user wants to rename current channel
     */
     void slotRenameItem( );

     /**
     \fn void keyPressEvent( QKeyEvent *e );
     \author Manuel Habermann <manuel@klear.org>
     \param e the pressed keys
     \retval void no return value
     \brief keyevents used to define key shortcuts
     */
     void keyPressEvent( QKeyEvent *e );

signals:

    void doubleClicked ( QListBoxItem * item );


private:

    CKlearAppConfig *KlearConfig;  // to get configuration data
    std::vector<QString> ChannelStorage;

    void loadChannelList();
    void saveChannelList();
    void swapChannelinStorage( int indexOld, int indexNew  );
    void swapChannelinListBox( int indexOld, int indexNew  );
    void presetAcceleratorKeysettings();
    void storeAcceleratorKeysettings();
    QString isKeyInUse( QString key );

};

#endif

