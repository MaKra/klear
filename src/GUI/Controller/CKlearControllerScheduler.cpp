/**
\author Klear.org
\file CKlearControllerScheduler.cpp
\brief The scheduler dialog implementation for Klear.
*/

#include "CKlearControllerScheduler.h"

CKlearControllerScheduler::CKlearControllerScheduler( CKlearAppScheduler *KlearScheduler, QWidget* parent, const char* name, WFlags fl ) : CKlearUIScheduler( parent, name, fl )
{
    const QString __FUNC__ = "CKlearControllerScheduler::CKlearControllerScheduler()";

    this->KlearRecordSet = NULL;
    this->KlearScheduler = KlearScheduler;
    this->SchedulerInit();
}

CKlearControllerScheduler::CKlearControllerScheduler( CKlearAppScheduler *KlearScheduler, QString chan, QDateTime start, QDateTime end, QString recname, QWidget* parent, const char* name, WFlags fl ) : CKlearUIScheduler( parent, name, fl )
{
    const QString __FUNC__ = "CKlearControllerScheduler::CKlearControllerScheduler()";

    this->KlearRecordSet = NULL;
    this->KlearScheduler = KlearScheduler;

    this->SchedulerInit();

    this->channelComboBox->setCurrentText( chan );
    this->startDateTimeEdit->setDateTime( start );
    this->endDateTimeEdit->setDateTime( end );
    this->pathLineEdit->setText( recname );

    this->setCaption( i18n("New Klear record from EPG") );
}

CKlearControllerScheduler::CKlearControllerScheduler( CKlearAppScheduler *KlearScheduler, CKlearAppRecordSet KlearRecordSet, QWidget* parent, const char* name, WFlags fl )  : CKlearUIScheduler( parent, name, fl )
{
    const QString __FUNC__ = "CKlearControllerScheduler::CKlearControllerScheduler()";

    this->KlearRecordSetCopy = KlearRecordSet;
    this->KlearRecordSet = &KlearRecordSetCopy;
    this->KlearScheduler = KlearScheduler;

    this->SchedulerInit();

    this->channelComboBox->setCurrentText( this->KlearRecordSet->getChannel() );
    this->startDateTimeEdit->setDateTime( this->KlearRecordSet->getStartDateTime() );
    this->endDateTimeEdit->setDateTime( this->KlearRecordSet->getEndDateTime() );
    this->pathLineEdit->setText( this->KlearRecordSet->getRecordFile() );
    this->setCaption( i18n("Edit Klear record") );
}



void CKlearControllerScheduler::SchedulerInit()
{
    const QString __FUNC__ = "CKlearControllerScheduler::SchedulerInit()";

    KlearConfig = NULL;
    KlearConfig = new CKlearAppConfig();

    try{
         this->loadChannelList();
         this->loadDateTime();
         this->loadPathLine();
    }catch( CKlearAppException &e ){
          e.addShowException( __LOC__, i18n("Could not load data file") );
    }

    channelIndex = 0;
}


CKlearControllerScheduler::~CKlearControllerScheduler()
{
}

void CKlearControllerScheduler::loadChannelList()
{
  const QString __FUNC__ = "CKlearControllerScheduler::loadChannelList()";

  QFile ChannelsConf( KlearConfig->getKlearConfigPath()+"/"+KlearConfig->getKlearChannelsConf() );

  if ( ChannelsConf.open( IO_ReadOnly ) )
  {
        QTextStream stream( &ChannelsConf );
        QString line;
        for (int i=1; !stream.atEnd(); i++ )
        {
            line = stream.readLine().latin1();
            QString channel = line.section( ':', 0, 0 );
            if ( channel == KlearConfig->getCurrentChannel() )
            {
                channelIndex = i - 1 ;
            }
            channelComboBox->insertItem( channel );
        }
        ChannelsConf.close();
        channelComboBox->setCurrentItem( channelIndex );
  }else{
    kdDebug() << "No channels.conf found\n";

    CKlearAppException e( __LOC__, i18n("<h3>No channels.conf found. You need a t/c/s-zap compatible channels.conf in your .klear directory. You can find a lot of channels.conf on our website www.klear.org or you can create your own with the scan utility from the dvb-utils packages</h3>"));
    throw e;
  }
}

void CKlearControllerScheduler::loadDateTime()
{
    startDateTimeEdit->setDateTime( QDateTime().currentDateTime() );
    endDateTimeEdit->setDateTime( QDateTime().currentDateTime() );
}

void CKlearControllerScheduler::loadPathLine()
{
    pathLineEdit->setText( KlearConfig->getRecordingDir()+"/"+KlearConfig->getRecordingName());
}

void CKlearControllerScheduler::slotGetChannel()
{
    channelComboBox->currentText();
}

void CKlearControllerScheduler::slotBrowse()
{
    QString s = QFileDialog::getSaveFileName( pathLineEdit->text(), "MPEG video (*.mpeg *.mpg)", this, i18n("save recordingfile dialog"),i18n( "Choose a directory/filename for recording") );

    if( s.length() < 1 )  // if no name was selected
    {
        s = pathLineEdit->text();
    }

    if( ( s.contains( ".mpeg", false ) == 0 ) && ( s.contains(".mpg",false) == 0 ) )  // if no mpeg extension was found
    {
         s.append(".mpeg");
    }

    pathLineEdit->setText(s);
}

void CKlearControllerScheduler::slotAddRecordSet()
{
   const QString __FUNC__ = "CKlearControllerScheduler::slotAddRecordSet()";

   try{
      KlearScheduler->addRecordSet(pathLineEdit->text(), startDateTimeEdit->dateTime(), endDateTimeEdit->dateTime(), channelComboBox->currentText());
      KlearScheduler->WriteScheduledRecords();
      this->close( TRUE );
   }catch( CKlearAppException &e)
   {
       e.addShowException( __LOC__, i18n("Failed adding RecordSet" ));
   }
}

void CKlearControllerScheduler::slotCancelScheduler()
{
   const QString __FUNC__ = "CKlearControllerScheduler::slotCancelScheduler()";

    if( KlearRecordSet != NULL )
    {
         try {
            KlearScheduler->addRecordSet( KlearRecordSet->getRecordFile(), KlearRecordSet->getStartDateTime(), KlearRecordSet->getEndDateTime(), KlearRecordSet->getChannel() );
         }catch( CKlearAppException &e)
         {
            // ausgabe unterdrückt; // MHa, 06.06.05
         }
    }
    this->close( TRUE );
}
