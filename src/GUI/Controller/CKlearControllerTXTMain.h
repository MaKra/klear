/**
\author Omar El-Dakhloul <omar@klear.org>
\file CKlearControllerTXTMain.h
\brief The Teletext window controller
*/

#ifndef CKLEARCONTROLLERTXTMAIN_H
#define CKLEARCONTROLLERTXTMAIN_H

#include <qpainter.h>

#include "../../App/TXT/CKlearAppTXTStreamer.h"
#include "../../App/TXT/CKlearAppTXTDecoder.h"
#include "../../App/CKlearAppConfig.h"
#include "../View/CKlearUITXTMain.h"


/**
\class CKlearControllerTXTMain
\author Omar El-Dakhloul <omar@klear.org>
\brief The GUI-controller for the Teletext. Controlls the user interaction with the GUI.
*/
class CKlearControllerTXTMain : public CKlearUITXTMain
{
  Q_OBJECT

  public:
    /**
    \fn CKlearControllerTXTMain( CKlearAppConfig *config, QWidget* parent = 0, const char* name = 0, WFlags fl = 0 )
    \author Omar El-Dakhloul <omar@klear.org>
    \brief Constructor
    */
    CKlearControllerTXTMain( CKlearAppConfig *config, QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );

    /**
    \fn ~CKlearControllerTXTMain()
    \author Omar El-Dakhloul <omar@klear.org>
    \brief Destructor
    */
    ~CKlearControllerTXTMain();

  public slots:
   /**
   \fn void slotPaintHeader()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \brief Is called when receive a signal from changedHeaderInfo() and paint the header of the teletext page
   */
    void slotPaintHeader();

   /**
   \fn void slotPaintBody()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \brief Is called when receive a signal from changedBodyInfo() and paint the body of the teletext page
   */
    void slotPaintBody();

  signals:
   /**
   \fn void changedHeaderInfo()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \brief Send a signal to slotPaintHeader() if the header information are changed.
   */
    void changedHeaderInfo();

   /**
   \fn void changedBodyInfo()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \brief Send a signal to slotPaintBody() if the body information are changed.
   */
    void changedBodyInfo();

  private:
   /**
   \fn void createWindow()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \brief Sets the default settings (width, height, caption, font, ...) of the teletext page
   */
    void createWindow();

   /**
   \fn void setRectWidth( int width )
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \param width the width of a rectangle
    \brief Sets the width of a rectangle
   */
    void setRectWidth( int width );

   /**
   \fn void setRectHeight( int height )
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \param height the height of a rectangle
    \brief Sets the height of a rectangle
   */
    void setRectHeight( int height );

   /**
   \fn void setMosaicsWidth( int rectWidth )
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \param rectWidth the width of a mosaic
    \brief Sets the width of a mosaic
   */
    void setMosaicsWidth( int rectWidth );

   /**
   \fn void setMosaicsHeight( int rectHeight )
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \param rectWidth the height of a mosaic
    \brief Sets the height of a mosaic
   */
    void setMosaicsHeight( int rectHeight );

   /**
   \fn void setTXTFont()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \brief Sets the default font of the teletext
   */
    void setTXTFont();

   /**
   \fn void setPageNumber( QString num )
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \param num the input of the teletext page number
    \brief Sets the teletext page number
   */
    void setPageNumber( QString num );

   /**
   \fn void setDefaultSettings()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \brief Sets the default settings (color, double height charakter, ...) of the teletext page
   */
    void setDefaultSettings();

   /**
   \fn void setBackgroundColor( int data )
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \param data information about the background color
    \brief Sets the background color of a teletext charakter
   */
    void setBackgroundColor( int data );

   /**
   \fn void setAlphaColor( int data )
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \param data information about the alpha color
    \brief Sets the foreground alphabetic color
   */
    void setAlphaColor( int data );

   /**
   \fn void setMosaicsColor( int data )
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \param data information about the mosaic color
    \brief Sets the foreground mosaic color
   */
    void setMosaicsColor( int data );

   /**
   \fn void setLatinOption( int data )
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \param data information about the national latin option
    \brief Sets the latin nation option
   */
    void setLatinOption( int data );

   /**
   \fn void setDoubleHeight( int row )
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \param row current line number
    \brief Sets the double height of a teletext row
   */
    void setDoubleHeight( int row );

   /**
   \fn void setHoldMosaics( int row, int column )
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \param row current row number
    \param column current column number
    \brief Sets the hold mosaic of the teletext
   */
    void setHoldMosaics( int row, int column );

   /**
   \fn void setBinary( int decimal )
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \param decimal decimal notation of the information
    \brief Sets the binary coded decimal representation
   */
    void setBinary( int decimal );

   /**
   \fn void paintPageNumber()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \brief Paints the current input page number
   */
    void paintPageNumber();

   /**
   \fn void paintEvent( QPaintEvent *pe )
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \param pe contains event parameters for paint events
    \brief Sends to widgets that need to update themselves
   */
    void paintEvent( QPaintEvent *pe );

   /**
   \fn void keyPressEvent( QKeyEvent *ke )
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \param ke contains describes a key event
    \brief Occur when a key is pressed or released and filter only the numerical information
   */
    void keyPressEvent( QKeyEvent *ke );

   /**
   \fn void closeEvent( QCloseEvent *ce )
    \author Omar El-Dakhloul <omar@klear.org>
    \retval void no return value
    \param ce contains parameters that describe a close event
    \brief Closes the Teletext window
   */
    void closeEvent( QCloseEvent *ce );

   /**
   \fn int getRectHeight()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval int the height of the rectangle
    \brief Returns the currently set height of the rectangle
   */
    int getRectHeight();

   /**
   \fn int getRectWidth()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval int the width of the rectangle
    \brief Returns the currently set width of the rectangle
   */
    int getRectWidth();

   /**
   \fn int getMosaicsHeight()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval int the height of the mosaic
    \brief Returns the currently set height of the mosaic
   */
    int getMosaicsHeight();

   /**
   \fn int getMosaicsWidth()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval int the width of the mosaic
    \brief Returns the currently set width of the mosaic
   */
    int getMosaicsWidth();

   /**
   \fn int getHoldMosaics()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval int the marking of the hold mosaic
    \brief Returns the currently set hold mosaic
   */
    int getHoldMosaics();

   /**
   \fn QFont getTXTFont()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval QFont the typeface of the teletext
    \brief Returns the currently set font of the teletext
   */
    QFont getTXTFont();

   /**
   \fn QString getPageNumber()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval QString the page number of the teletext
    \brief Returns the currently set page number of the teletext
   */
    QString getPageNumber();

   /**
   \fn QColor getBackgroundColor()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval QColor the color of the background
    \brief Returns the currently set background color
   */
    QColor getBackgroundColor();

   /**
   \fn QColor getAlphaColor()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval QColor the color of the alphabet
    \brief Returns the currently set alphabet color
   */
    QColor getAlphaColor();

   /**
   \fn QColor getMosaicsColor()
    \author Omar El-Dakhloul <omar@klear.org>
    \retval QColor the color of the mosaic
    \brief Returns the currently set mosiac color
   */
    QColor getMosaicsColor();

    int position;
    int row;
    int digit;
    int counter;
    int doubleHeight;
    int binary[ 7 ];
    int rectHeight;
    int rectWidth;
    int mosaicsHeight;
    int mosaicsWidth;
    int holdMosaics;
    bool isHoldMosaics;

    QString pageNumber;
    QString number[ 3 ];
    QString character;
    QMutex alphaMutex;
    QString alpha;
    QColor backgroundColor;
    QColor alphaColor;
    QColor mosaicsColor;
    QFont font;

    CKlearAppConfig *KlearConfig;
    CKlearAppTXTStreamer *TXTStreamer;
    CKlearAppTXTDecoder *TXTDecoder;
};
#endif
