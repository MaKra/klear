/**
\author Klear.org
\file CKlearControllerMain.h
\brief The main dialog implementation for Klear.
*/

#ifndef KLEARCONTROLLERMAIN_H
#define KLEARCONTROLLERMAIN_H

#include <iostream>
#include <cstdlib>
#include <exception>                                // standard exceptions

#include <kapplication.h>
#include <kmainwindow.h>
#include <dcopclient.h>
#include <dcopobject.h>
#include <kwin.h>

#include <qevent.h>
#include <qlistbox.h>
#include <qwidget.h>
#include <qdialog.h>
#include <qlayout.h>
#include <qframe.h>
#include <qwidgetstack.h>
#include <qsizepolicy.h>
#include <qkeysequence.h>
#include <qtooltip.h>
#include <qdockarea.h>
#include <qslider.h>
#include <qvariant.h>
#include <qdir.h>

#include <X11/keysym.h>
#include <X11/extensions/XTest.h>

#include "../../config.h"

#include "../View/CKlearUIAbout.h"           // the about dialog
#include "../View/CKlearUIMain.h"            // the main user interface

#include "../../Utilities/icons.cpp"            // the icons

#include "../../App/CKlearAppConfig.h"           // the configure and setting algorithms

#include "../../App/Tuner/CKlearAppTunerT.h"           // terrestrical tuner class. derived class from kleartuner.h
#include "../../App/Tuner/CKlearAppTunerS.h"           // satellite tuner class. derived class from kleartuner.h
#include "../../App/Tuner/CKlearAppTunerC.h"           // cable tuner class. derived class from kleartuner.h

#include "../../App/CKlearAppScheduler.h"        // the scheduler setting algorithms

#include "../../App/EPG/CKlearAppEITData.h"               // EPG
#include "../../App/EPG/CKlearAppStreamReader.h"    // EPG Reader
#include "../../App/EPG/CKlearAppDataLayer.h"            // database layer

#include "../../App/Exceptions/CKlearAppException.h"       // klear exception class
#include "../../App/Exceptions/CKlearAppErrorException.h"       // klear exception class
#include "../../App/Exceptions/CKlearAppFatalException.h"       // klear exception class

#include "../../App/Engines/CKlearEngineAdapter.h"
#include "../../App/Engines/KlearXine/CKlearEngineXine.h"
#include "../../App/Engines/KlearXineTNG/CKlearEngineXineTNG.h"
#include "../../App/Engines/KlearMPlayer/CKlearEngineMPlayer.h"

#include "./CKlearControllerConfig.h"     // the configure interface
#include "./CKlearControllerScheduler.h"   // Scheduler GUI
#include "./CKlearControllerSchedulerOverview.h"   // Scheduler Overview GUI
#include "./CKlearControllerEPGMain.h"  // EPG GUI
#include "./CKlearControllerTXTMain.h"  // TXT GUI


/**
\class CKlearControllerMain
\author Klear.org
\brief The main dialog class for Klear
*/
class CKlearControllerMain : public CKlearUIMain
{
  Q_OBJECT

public:

     /**
     \fn CKlearControllerMain(QWidget* parent = 0, const char* name = 0,  WFlags fl = 0 )
     \author Marco Kraus <marco@klear.org>
     \brief Constructor
     */
     CKlearControllerMain(QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );

     /**
     \fn ~CKlearControllerMain()
     \author Marco Kraus <marco@klear.org>
     \brief Destructor
     */
     ~CKlearControllerMain();

 signals:

   /**
   \fn void signalCloseTray();
   \author  Manuel Habermann <manuel@klear.org>, Patric Sherif <patric@klear.org>
   \retval  void no return value
   \brief   emits a closing signal to CKlearAppTray
   */
   void signalCloseTray();

public slots:

     /**
     \fn void slotMinimizeWindow();
     \author Omar El-Dakhloul <omar@klear.org>
     \retval void no return value
     \brief Only show playback window. Hide all menus.
     */
     void slotMinimizeWindow();

     /**
     \fn void slotFullscreenWindow();
     \author Omar El-Dakhloul <omar@klear.org>
     \retval void no return value
     \brief Show klear minimal and fullscreen.
     */
     void slotFullscreenWindow();


     /**
     \fn void slotCloseWindow();
     \author Omar El-Dakhloul <omar@klear.org>
     \retval void no return value
     \brief Quits Klear.
     */
     void slotCloseWindow();

     /**
     \fn void slotTakeScreenshot();
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \brief Takes a screenshot from the current playback window.
     */
     void slotTakeScreenshot();

     /**
     \fn void slotXineFatal(const QString& ferror);
     \author Marco Kraus <marco@klear.org>
     \param ferror The errormessage to be shown via OSD
     \retval void no return value
     \exception e CKlearException:Fatal xine error catched
     \brief Slot to catch fatal error emits from playback window
     */
     void slotXineFatal(const QString& ferror);

     /**
     \fn void slotShowXineStatus(const QString& status);
     \author Marco Kraus <marco@klear.org>
     \param status The status string to be shown via OSD
     \retval void no return value
     \brief Slot to catch the xine status emits from playback window
     */
     void slotShowXineStatus(const QString& status);

     /**
     \fn void closeEvent( QCloseEvent* ce )
     \author Marco Kraus <marco@klear.org>
     \param ce A closing event signal, we just reach throught the super class
     \retval void no return value
     \brief Overwrite closing method to clean up all threads befor destroying window
     */
     void closeEvent( QCloseEvent* ce );

     /**
     \fn void slotAboutKlear( )
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \brief Gets signal from About-Button, and shows the Klear-About-Dialog
     */
     void slotAboutKlear();

     /**
     \fn void slotConfigDialog( )
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \brief  Calls the graphical configuration dialog with the Klear-config-backend
     */
     void slotConfigDialog();

     /**
     \fn void slotShowInfoOSD( )
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \brief shows the information OSD for the current channel
     */
     void slotShowInfoOSD();

     /**
     \fn void slotSetVolume( )
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \brief Sets the volume to the current slider position (called everytime a users moves the slider)
     */
     void slotSetVolume();

     /**
     \fn void slotLaunchEPG()
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \brief Starts the EPG dialog
     */
     void slotLaunchEPG();

     /**
     \fn void slotLaunchTXT()
     \author Omar El-Dakhloul <omar@klear.org>
     \retval void no return value
     \brief Starts the Teletext dialog
     */
     void slotLaunchTXT();

     /**
     \fn void slotChangeChannel( )
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \brief  Just tunes the currently marked channel-entry (used for hotkey channelswitching)
     */
     void slotChangeChannel();

     /**
     \fn void slotPushRecording( )
     \author Patric Sherif <patric@klear.org> and Manuel Habermann <manuel@klear.org>
     \retval void no return value
     \brief start recording
     */
     void slotPushRecording();

     /**
     \fn void slotPushTimeshifting( )
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \brief start timeshifting
     */
     void slotPushTimeshifting();

     /**
     \fn void slotResumeTimeshifting()
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \brief resume a already started timeshifting process
     */
     void slotResumeTimeshifting();

     /**
     \fn void slotStartingScheduled( )
     \author Patric Sherif <patric@klear.org> and Manuel Habermann <manuel@klear.org>
     \retval void no return value
     \brief start scheduled recording
     */
     void slotStartingScheduled();

     /**
     \fn void slotStoppingScheduled( )
     \author Patric Sherif <patric@klear.org> and Manuel Habermann <manuel@klear.org>
     \retval void no return value
     \brief stop recording
     */
     void slotStoppingScheduled();

     /**
     \fn void slotPushQueue( )
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \brief Start playing datastream, depending on the current playback-mode (recording or live)
     */
     void slotPushQueue();

     /**
     \fn void slotSchedulerOverviewDialog( )
     \author Patric Sherif <patric@klear.org> and Manuel Habermann <manuel@klear.org>
     \retval void no return value
     \brief show scheduler overview dialog
     */
     void slotSchedulerOverviewDialog();

     /**
     \fn void slotToggleDeinterlaceButton()
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \brief switch to easy deinterlace mode (tell xine to double lines) called by emitted signal
     */
     void slotToggleDeinterlaceButton();

     /**
     \fn void slotToggleDeinterlace()
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \brief switch to easy deinterlace mode (tell xine to double lines)
     */
     void slotToggleDeinterlace();

     /**
     \fn void slotToggleMute()
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \brief switch on/off audio (seignal to xine to set softwaremixer to zero)
     */
     void slotToggleMute();

     /**
     \fn void slotIncChannel()
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \brief Switch to the next channel below (see channelliste)
     */
     void slotIncChannel();

     /**
     \fn void slotDecChannel()
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \brief Switch to the next channel above (see channelliste)
     */
     void slotDecChannel();

     /**
     \fn void hideEvent( QHideEvent * ce  )
     \author Patric Sherif <patric@klear.org> and Manuel Habermann <manuel@klear.org>
     \retval void no return value
     \param ce An event
     \brief drag Klear to systemtray
     */
     void hideEvent ( QHideEvent * ce );


     /**
     \fn void resizeEvent ( QResizeEvent * re )
     \author Manuel Habermann <manuel@klear.org>
     \retval void no return value
     \param re An resize event
     \brief sets klearconfig windowsize values
     */
     void resizeEvent ( QResizeEvent * re);

     /**
     \fn void showEvent( QShowEvent * ce  )
     \author Patric Sherif <patric@klear.org> and Manuel Habermann <manuel@klear.org>
     \retval void no return value
     \param ce An event
     \brief show up Klear
     */
     void showEvent( QShowEvent * ce );


      /**
     \fn void slotToggleMuteButton ()
     \author Manuel Habermann <manuel@klear.org>
     \retval void no return value
     \brief toggles button and calls slottogglemute
     */
     void slotToggleMuteButton();

      /**
     \fn void slotFullscreenWindowButton ()
     \author Manuel Habermann <manuel@klear.org>
     \retval void no return value
     \brief toggles button and calls slotFullscreen
     */
     void slotFullscreenWindowButton();

      /**
     \fn void wheelEvent (  QWheelEvent * e )
     \author Manuel Habermann <manuel@klear.org>
     \retval void no return value
     \param e An mousewheelevent
     \brief mousewheelevent value is used to control volume level
     */
     void wheelEvent (  QWheelEvent * e );

      /**
     \fn void slotFakeKeyEvent()
     \author J�rgen Kofler <kaffeine@gmx.net>
     \retval void no return value
     \brief sends fake key event to prevent screensaver from startup
     */
      void slotFakeKeyEvent();

      /**
     \fn void slotSetScreensaverTimeout()
     \author J�rgen Kofler <kaffeine@gmx.net>
     \retval void no return value
     \brief sets timeout for screensaver
     */
      void slotSetScreensaverTimeout();

      /**
     \fn void slotSetDeinterlaceFromConfig();
     \author Marco Kraus <marco@klear.org>
     \retval void no return value
     \brief activate deinterlacing via signal, when wished via configfile
     */
     void slotSetDeinterlaceFromConfig();

private:

     CKlearEngineAdapter *PlaybackWindow; // Playback engine Widget

     std::vector<CKlearAppEITData *> *EITDataVector;  // EPG Data storage
     CKlearAppStreamReader *StreamReader;

     QAccel *acceleratorKeys;     // accelerator keys
     QTimer *klearTunerStreamTimer;

     CKlearUIAbout *AboutKlear;       // about dialog

     CKlearAppTuner *KlearTuner;         // tuner
     CKlearAppScheduler *KlearScheduler; // scheduler backend
     CKlearAppConfig *KlearConfig;   // configuration class for klear-config

     CKlearControllerSchedulerOverview *SchedulerOverviewDialog; // scheduler dialog
     QWidgetStack *EngineSocket;

     void loadChannelList();
     void TuneChannel(const QString&);
     void startUp(); // starting all other devices at one point for exception handling

     void setAcceleratorKeys();
     void updateToolTips();

     // fake events to prevent screensaver startup
     QTimer screensaverTimer;
     int m_haveXTest;
     int m_xTestKeycode;

     // status flags
     bool groupboxWasVisible;
     bool isMinimized;
     bool isFullscreen;
     bool isKWin;
     bool isExiting;

     void keyPressEvent(QKeyEvent* e);
};

#endif
