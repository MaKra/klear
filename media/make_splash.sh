#/bin/bash  

  if [ "$#" == "0" ]; then
    echo "I need text."
    exit 1
  fi

  string=""
  while (($#)); do
    string="$string $1"
    shift
  done

  convert \
  -size 200x50 xc:none \
  -gravity center \
  -font Helvetica-Bold \
  -pointsize 16 \
  -stroke darkblue -strokewidth 1 -annotate 0 "$string" \
  -channel RGBA -blur 0x2 \
  -stroke none -fill lightgrey     -annotate 0 "$string" \
  +size template_splashscreen.png +swap \
  -gravity south -geometry +0+10  -composite splashscreen.png

  xv splashscreen.png

