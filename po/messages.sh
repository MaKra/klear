#!/bin/sh

# Inspired by Makefile.common from coolo
# this script is used to update the .po files

# To update the translations, you will need a specific gettext 
# patched for kde and a lot of patience, tenacity, luck, time ..


# I guess one should only update the .po files when all .cpp files 
# are generated (after a make or scons)

# If you have a better way to do this, do not keep that info 
# for yourself and help me to improve this script, thanks
# (tnagyemail-mail tat yahoo d0tt fr)
#
# oringinal by t. nagy for kdissert
# hacked for klear by m.kraus

SRCDIR=../src # srcdir is the directory containing the source code

KDEDIR=`kde-config --prefix`
EXTRACTRC=extractrc
KDEPOT=`kde-config --prefix`/include/kde/kde.pot
XGETTEXT="xgettext -C -ki18n -ktr2i18n -kI18N_NOOP -ktranslate -kaliasLocale -x $KDEPOT "

## check that kde.pot is available
if ! test -e $KDEPOT; then
	echo "$KDEPOT does not exist, there is something wrong with your installation!"
	XGETTEXT="xgettext -C -ki18n -ktr2i18n -kI18N_NOOP -ktranslate -kaliasLocale "
	exit
fi

> rc.cpp

## extract the strings
echo "Extracting strings..."

# process the .ui and .rc files
$EXTRACTRC `find $SRCDIR -iname *.rc` >> rc.cpp
echo "RC files extracted..."
$EXTRACTRC `find $SRCDIR -iname *.ui` >> rc.cpp
echo "UI files extracted..."

echo -e 'i18n("_: NAME OF TRANSLATORS\\n"\n"Your names")\ni18n("_: EMAIL OF TRANSLATORS\\n"\n"Your emails")' > $SRCDIR/_translatorinfo.cpp
echo "Headers added..."

$XGETTEXT `find $SRCDIR -name "*.cpp" -o -name "*.h"` rc.cpp -o LANG.pot
echo "Source files extracted..."

# remove the intermediate files
rm -f rc.cpp
rm -f $SRCDIR/_translatorinfo.cpp
echo "Temp files removed..."

## now merge the .po files ..
echo "merging to existing po files..."

for i in `ls *.po`; do
    msgmerge $i LANG.pot -o $i || exit 1
done

## finished
echo "Done !"

