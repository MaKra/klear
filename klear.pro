# Diese Datei wurde mit dem qmake-Manager von KDevelop erstellt. 
# ------------------------------------------- 
# Unterordner relativ zum Projektordner: .
# Das Target ist Projekt im Unterordner 

LIBS += -lxine \
        -lcppunit \
        -lkdeui 
INCLUDEPATH += ../klear \
               ../klear/src 
QMAKE_LIBDIR = /usr/lib/ \
               /opt/kde3/lib 
CONFIG += release \
          warn_on \
          qt \
          thread \
          x11 \
          exceptions \
          stl \
          rtti 
TEMPLATE = subdirs 
SUBDIRS += distfiles \
           src \
           templates \
           media \
           po 
