Name:		klear
Summary:	KDE Desktop application for watching DVB-C/T/S
Version:	0.4.1
Release:	1
License:	GPL
URL:		http://www.klear.org/
Source:		http://tr0ll.net/kraus.tk/projects/klear/sources/%{name}-%{version}.tar.bz2
Group:		Video
BuildRoot:	%{_tmppath}/%{name}-buildroot
BuildRequires:	kdelibs-devel libxine-devel libdvbpsi-devel
Requires:	libxine1 >= 1.1

%description
Klear is a KDE application for recording video streams from your DVB
device and watching TV with all advantages of digital television (like
EPG, OSD, and so on). It runs under Linux with the usage of the KDE/QT
GUI-Framework from Trolltech. It uses a flexible video engine for playback.
An engine with libXine is included. It supports almost all DVB devices
supported by Linux, including all budget devices.

%prep
%setup -q

%build
%configure

%make

%install
rm -rf $RPM_BUILD_ROOT

install -d $RPM_BUILD_ROOT%{_bindir}
install -m 755 bin/klear $RPM_BUILD_ROOT%{_bindir}

#menu
mkdir -p $RPM_BUILD_ROOT%{_menudir}
cat << EOF > $RPM_BUILD_ROOT%{_menudir}/%{name}
?package(%{name}): command="%{name}" icon="%{name}.png" needs="x11" title="Klear" longtitle="KDE DVB Application" section="Multimedia/Video"
EOF

#icons
mkdir -p $RPM_BUILD_ROOT/%_iconsdir
cat media/hi32-app-klear.png > $RPM_BUILD_ROOT/%_iconsdir/%name.png
mkdir -p $RPM_BUILD_ROOT/%_miconsdir
cat media/hi16-app-klear.png > $RPM_BUILD_ROOT/%_miconsdir/%name.png
mkdir -p $RPM_BUILD_ROOT/%_liconsdir
cat media/klear-icon-48x48.png > $RPM_BUILD_ROOT/%_liconsdir/%name.png

#%find_lang %{name}

%clean
rm -rf $RPM_BUILD_ROOT

%post
%{update_menus}

%postun
%{clean_menus}

%files
#%files -f %{name}.lang
%defattr(-,root,root)
%doc distfiles/README distfiles/changelog distfiles/COPYING distfiles/AUTHORS distfiles/BUGS
%{_bindir}/*
%{_menudir}/%name
%{_liconsdir}/%name.png
%{_iconsdir}/%name.png
%{_miconsdir}/%name.png
    
%changelog
* Sun Sep 10 2005 AndyRTR <rpm@mandrivauser.de> 0.4.1mud
- changed buildrequires
- updated build steps
- use all icons sizes
- prepared for localization
- changed file list

* Sun Jul 03 2005 AndyRTR <andreas.radke@freenet.de> 0.2-1rtr
- generic spec for initial build
