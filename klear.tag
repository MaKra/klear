<?xml version='1.0' encoding='ISO-8859-1' standalone='yes' ?>
<tagfile>
  <compound kind="file">
    <name>CKlearAppConfig.h</name>
    <path>/home/marco/svn/klear/src/App/</path>
    <filename>CKlearAppConfig_8h</filename>
    <includes id="CKlearAppException_8h" name="CKlearAppException.h" local="yes" imported="no">./Exceptions/CKlearAppException.h</includes>
    <includes id="CKlearAppFatalException_8h" name="CKlearAppFatalException.h" local="yes" imported="no">./Exceptions/CKlearAppFatalException.h</includes>
    <includes id="CKlearAppErrorException_8h" name="CKlearAppErrorException.h" local="yes" imported="no">./Exceptions/CKlearAppErrorException.h</includes>
    <includes id="CKlearAppFileException_8h" name="CKlearAppFileException.h" local="yes" imported="no">./Exceptions/CKlearAppFileException.h</includes>
    <includes id="config_8h" name="config.h" local="yes" imported="no">../config.h</includes>
    <class kind="class">CKlearAppConfig</class>
  </compound>
  <compound kind="file">
    <name>CKlearAppRecorder.h</name>
    <path>/home/marco/svn/klear/src/App/</path>
    <filename>CKlearAppRecorder_8h</filename>
    <includes id="CKlearAppConfig_8h" name="CKlearAppConfig.h" local="yes" imported="no">./CKlearAppConfig.h</includes>
    <includes id="CKlearAppException_8h" name="CKlearAppException.h" local="yes" imported="no">./Exceptions/CKlearAppException.h</includes>
    <class kind="class">CKlearAppRecorder</class>
    <member kind="define">
      <type>#define</type>
      <name>BUFFER_LEN</name>
      <anchorfile>CKlearAppRecorder_8h.html</anchorfile>
      <anchor>46130dc86f2322714bba26960b64e7bb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TS_SIZE</name>
      <anchorfile>CKlearAppRecorder_8h.html</anchorfile>
      <anchor>711d7329c4243dfb232fe5b5d53e1c75</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CKlearAppRecordSet.h</name>
    <path>/home/marco/svn/klear/src/App/</path>
    <filename>CKlearAppRecordSet_8h</filename>
    <includes id="CKlearAppException_8h" name="CKlearAppException.h" local="yes" imported="no">./Exceptions/CKlearAppException.h</includes>
    <class kind="class">CKlearAppRecordSet</class>
  </compound>
  <compound kind="file">
    <name>CKlearAppScheduler.h</name>
    <path>/home/marco/svn/klear/src/App/</path>
    <filename>CKlearAppScheduler_8h</filename>
    <includes id="CKlearAppRecorder_8h" name="CKlearAppRecorder.h" local="yes" imported="no">CKlearAppRecorder.h</includes>
    <includes id="CKlearAppRecordSet_8h" name="CKlearAppRecordSet.h" local="yes" imported="no">CKlearAppRecordSet.h</includes>
    <includes id="CKlearAppConfig_8h" name="CKlearAppConfig.h" local="yes" imported="no">CKlearAppConfig.h</includes>
    <includes id="CKlearAppTuner_8h" name="CKlearAppTuner.h" local="yes" imported="no">Tuner/CKlearAppTuner.h</includes>
    <includes id="CKlearAppException_8h" name="CKlearAppException.h" local="yes" imported="no">Exceptions/CKlearAppException.h</includes>
    <includes id="CKlearAppErrorException_8h" name="CKlearAppErrorException.h" local="yes" imported="no">Exceptions/CKlearAppErrorException.h</includes>
    <includes id="CKlearAppFatalException_8h" name="CKlearAppFatalException.h" local="yes" imported="no">Exceptions/CKlearAppFatalException.h</includes>
    <includes id="CKlearAppInputException_8h" name="CKlearAppInputException.h" local="yes" imported="no">Exceptions/CKlearAppInputException.h</includes>
    <includes id="CKlearAppFileException_8h" name="CKlearAppFileException.h" local="yes" imported="no">Exceptions/CKlearAppFileException.h</includes>
    <class kind="class">CKlearAppScheduler</class>
  </compound>
  <compound kind="file">
    <name>CKlearAppTray.h</name>
    <path>/home/marco/svn/klear/src/App/</path>
    <filename>CKlearAppTray_8h</filename>
    <class kind="class">CKlearAppTray</class>
  </compound>
  <compound kind="file">
    <name>CKlearEngineAdapter.h</name>
    <path>/home/marco/svn/klear/src/App/Engines/</path>
    <filename>CKlearEngineAdapter_8h</filename>
    <includes id="CKlearAppConfig_8h" name="CKlearAppConfig.h" local="yes" imported="no">../CKlearAppConfig.h</includes>
    <includes id="config_8h" name="config.h" local="yes" imported="no">../../config.h</includes>
    <class kind="class">CKlearEngineAdapter</class>
  </compound>
  <compound kind="file">
    <name>CKlearEngineMPlayer.h</name>
    <path>/home/marco/svn/klear/src/App/Engines/KlearMPlayer/</path>
    <filename>CKlearEngineMPlayer_8h</filename>
    <includes id="CKlearEngineAdapter_8h" name="CKlearEngineAdapter.h" local="yes" imported="no">../CKlearEngineAdapter.h</includes>
  </compound>
  <compound kind="file">
    <name>CKlearEngineXine.h</name>
    <path>/home/marco/svn/klear/src/App/Engines/KlearXine/</path>
    <filename>CKlearEngineXine_8h</filename>
    <includes id="CKlearAppConfig_8h" name="CKlearAppConfig.h" local="yes" imported="no">../../CKlearAppConfig.h</includes>
    <includes id="CKlearEngineAdapter_8h" name="CKlearEngineAdapter.h" local="yes" imported="no">../CKlearEngineAdapter.h</includes>
    <includes id="CKlearAppException_8h" name="CKlearAppException.h" local="yes" imported="no">../../Exceptions/CKlearAppException.h</includes>
    <includes id="CKlearAppFatalException_8h" name="CKlearAppFatalException.h" local="yes" imported="no">../../Exceptions/CKlearAppFatalException.h</includes>
    <includes id="CKlearAppFileException_8h" name="CKlearAppFileException.h" local="yes" imported="no">../../Exceptions/CKlearAppFileException.h</includes>
    <includes id="CKlearAppErrorException_8h" name="CKlearAppErrorException.h" local="yes" imported="no">../../Exceptions/CKlearAppErrorException.h</includes>
    <class kind="class">CKlearEngineXine</class>
  </compound>
  <compound kind="file">
    <name>CKlearAppErrorException.h</name>
    <path>/home/marco/svn/klear/src/App/Exceptions/</path>
    <filename>CKlearAppErrorException_8h</filename>
    <includes id="CKlearAppException_8h" name="CKlearAppException.h" local="yes" imported="no">CKlearAppException.h</includes>
    <class kind="class">CKlearAppErrorException</class>
  </compound>
  <compound kind="file">
    <name>CKlearAppException.h</name>
    <path>/home/marco/svn/klear/src/App/Exceptions/</path>
    <filename>CKlearAppException_8h</filename>
    <includes id="CKlearExceptionReporter_8h" name="CKlearExceptionReporter.h" local="yes" imported="no">CKlearExceptionReporter.h</includes>
    <class kind="class">ErrorData</class>
    <class kind="class">CKlearAppException</class>
    <member kind="define">
      <type>#define</type>
      <name>__LOC__</name>
      <anchorfile>CKlearAppException_8h.html</anchorfile>
      <anchor>842fc8b6466bbad328d0939d3c094216</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CKlearAppFatalException.h</name>
    <path>/home/marco/svn/klear/src/App/Exceptions/</path>
    <filename>CKlearAppFatalException_8h</filename>
    <includes id="CKlearAppException_8h" name="CKlearAppException.h" local="yes" imported="no">CKlearAppException.h</includes>
    <class kind="class">CKlearAppFatalException</class>
  </compound>
  <compound kind="file">
    <name>CKlearAppFileException.h</name>
    <path>/home/marco/svn/klear/src/App/Exceptions/</path>
    <filename>CKlearAppFileException_8h</filename>
    <includes id="CKlearAppException_8h" name="CKlearAppException.h" local="yes" imported="no">CKlearAppException.h</includes>
    <class kind="class">CKlearAppFileException</class>
  </compound>
  <compound kind="file">
    <name>CKlearAppInputException.h</name>
    <path>/home/marco/svn/klear/src/App/Exceptions/</path>
    <filename>CKlearAppInputException_8h</filename>
    <includes id="CKlearAppException_8h" name="CKlearAppException.h" local="yes" imported="no">CKlearAppException.h</includes>
    <class kind="class">CKlearAppInputException</class>
  </compound>
  <compound kind="file">
    <name>CKlearAppMemoryException.h</name>
    <path>/home/marco/svn/klear/src/App/Exceptions/</path>
    <filename>CKlearAppMemoryException_8h</filename>
    <includes id="CKlearAppException_8h" name="CKlearAppException.h" local="yes" imported="no">CKlearAppException.h</includes>
    <class kind="class">CKlearAppMemoryException</class>
  </compound>
  <compound kind="file">
    <name>CKlearExceptionReporter.h</name>
    <path>/home/marco/svn/klear/src/App/Exceptions/</path>
    <filename>CKlearExceptionReporter_8h</filename>
    <includes id="CKlearAppException_8h" name="CKlearAppException.h" local="yes" imported="no">CKlearAppException.h</includes>
    <class kind="class">CKlearExceptionReporter</class>
  </compound>
  <compound kind="file">
    <name>CKlearAppTuner.h</name>
    <path>/home/marco/svn/klear/src/App/Tuner/</path>
    <filename>CKlearAppTuner_8h</filename>
    <includes id="CKlearAppConfig_8h" name="CKlearAppConfig.h" local="yes" imported="no">../CKlearAppConfig.h</includes>
    <includes id="CKlearAppException_8h" name="CKlearAppException.h" local="yes" imported="no">../Exceptions/CKlearAppException.h</includes>
    <includes id="CKlearAppFatalException_8h" name="CKlearAppFatalException.h" local="yes" imported="no">../Exceptions/CKlearAppFatalException.h</includes>
    <includes id="CKlearAppErrorException_8h" name="CKlearAppErrorException.h" local="yes" imported="no">../Exceptions/CKlearAppErrorException.h</includes>
    <includes id="CKlearAppFileException_8h" name="CKlearAppFileException.h" local="yes" imported="no">../Exceptions/CKlearAppFileException.h</includes>
    <class kind="class">CKlearAppTuner</class>
  </compound>
  <compound kind="file">
    <name>CKlearAppTunerC.h</name>
    <path>/home/marco/svn/klear/src/App/Tuner/</path>
    <filename>CKlearAppTunerC_8h</filename>
    <includes id="CKlearAppTuner_8h" name="CKlearAppTuner.h" local="yes" imported="no">CKlearAppTuner.h</includes>
    <class kind="class">CKlearAppTunerC</class>
  </compound>
  <compound kind="file">
    <name>CKlearAppTunerS.h</name>
    <path>/home/marco/svn/klear/src/App/Tuner/</path>
    <filename>CKlearAppTunerS_8h</filename>
    <includes id="CKlearAppTuner_8h" name="CKlearAppTuner.h" local="yes" imported="no">CKlearAppTuner.h</includes>
    <class kind="class">CKlearAppTunerS</class>
  </compound>
  <compound kind="file">
    <name>CKlearAppTunerT.h</name>
    <path>/home/marco/svn/klear/src/App/Tuner/</path>
    <filename>CKlearAppTunerT_8h</filename>
    <includes id="CKlearAppTuner_8h" name="CKlearAppTuner.h" local="yes" imported="no">CKlearAppTuner.h</includes>
    <class kind="class">CKlearAppTunerT</class>
  </compound>
  <compound kind="file">
    <name>config.h</name>
    <path>/home/marco/svn/klear/src/</path>
    <filename>config_8h</filename>
  </compound>
  <compound kind="file">
    <name>CKlearControllerConfig.h</name>
    <path>/home/marco/svn/klear/src/GUI/Controller/</path>
    <filename>CKlearControllerConfig_8h</filename>
    <includes id="CKlearAppException_8h" name="CKlearAppException.h" local="yes" imported="no">../../App/Exceptions/CKlearAppException.h</includes>
    <includes id="CKlearAppConfig_8h" name="CKlearAppConfig.h" local="yes" imported="no">../../App/CKlearAppConfig.h</includes>
    <includes id="CKlearControllerScanView_8h" name="CKlearControllerScanView.h" local="yes" imported="no">./CKlearControllerScanView.h</includes>
    <class kind="class">CKlearControllerConfig</class>
  </compound>
  <compound kind="file">
    <name>CKlearControllerEPGMain.h</name>
    <path>/home/marco/svn/klear/src/GUI/Controller/</path>
    <filename>CKlearControllerEPGMain_8h</filename>
    <includes id="config_8h" name="config.h" local="yes" imported="no">../../config.h</includes>
    <includes id="CKlearControllerScheduler_8h" name="CKlearControllerScheduler.h" local="yes" imported="no">CKlearControllerScheduler.h</includes>
    <includes id="CKlearControllerSchedulerOverview_8h" name="CKlearControllerSchedulerOverview.h" local="yes" imported="no">CKlearControllerSchedulerOverview.h</includes>
    <includes id="CKlearAppConfig_8h" name="CKlearAppConfig.h" local="yes" imported="no">../../App/CKlearAppConfig.h</includes>
    <class kind="class">CKlearControllerEPGMain</class>
  </compound>
  <compound kind="file">
    <name>CKlearControllerMain.h</name>
    <path>/home/marco/svn/klear/src/GUI/Controller/</path>
    <filename>CKlearControllerMain_8h</filename>
    <includes id="config_8h" name="config.h" local="yes" imported="no">../../config.h</includes>
    <includes id="CKlearAppConfig_8h" name="CKlearAppConfig.h" local="yes" imported="no">../../App/CKlearAppConfig.h</includes>
    <includes id="CKlearAppTunerT_8h" name="CKlearAppTunerT.h" local="yes" imported="no">../../App/Tuner/CKlearAppTunerT.h</includes>
    <includes id="CKlearAppTunerS_8h" name="CKlearAppTunerS.h" local="yes" imported="no">../../App/Tuner/CKlearAppTunerS.h</includes>
    <includes id="CKlearAppTunerC_8h" name="CKlearAppTunerC.h" local="yes" imported="no">../../App/Tuner/CKlearAppTunerC.h</includes>
    <includes id="CKlearAppScheduler_8h" name="CKlearAppScheduler.h" local="yes" imported="no">../../App/CKlearAppScheduler.h</includes>
    <includes id="CKlearAppException_8h" name="CKlearAppException.h" local="yes" imported="no">../../App/Exceptions/CKlearAppException.h</includes>
    <includes id="CKlearAppErrorException_8h" name="CKlearAppErrorException.h" local="yes" imported="no">../../App/Exceptions/CKlearAppErrorException.h</includes>
    <includes id="CKlearAppFatalException_8h" name="CKlearAppFatalException.h" local="yes" imported="no">../../App/Exceptions/CKlearAppFatalException.h</includes>
    <includes id="CKlearEngineAdapter_8h" name="CKlearEngineAdapter.h" local="yes" imported="no">../../App/Engines/CKlearEngineAdapter.h</includes>
    <includes id="CKlearEngineXine_8h" name="CKlearEngineXine.h" local="yes" imported="no">../../App/Engines/KlearXine/CKlearEngineXine.h</includes>
    <includes id="CKlearEngineMPlayer_8h" name="CKlearEngineMPlayer.h" local="yes" imported="no">../../App/Engines/KlearMPlayer/CKlearEngineMPlayer.h</includes>
    <includes id="CKlearControllerConfig_8h" name="CKlearControllerConfig.h" local="yes" imported="no">./CKlearControllerConfig.h</includes>
    <includes id="CKlearControllerScheduler_8h" name="CKlearControllerScheduler.h" local="yes" imported="no">./CKlearControllerScheduler.h</includes>
    <includes id="CKlearControllerSchedulerOverview_8h" name="CKlearControllerSchedulerOverview.h" local="yes" imported="no">./CKlearControllerSchedulerOverview.h</includes>
    <includes id="CKlearControllerEPGMain_8h" name="CKlearControllerEPGMain.h" local="yes" imported="no">./CKlearControllerEPGMain.h</includes>
    <includes id="CKlearControllerTXTMain_8h" name="CKlearControllerTXTMain.h" local="yes" imported="no">./CKlearControllerTXTMain.h</includes>
    <class kind="class">CKlearControllerMain</class>
  </compound>
  <compound kind="file">
    <name>CKlearControllerScanView.h</name>
    <path>/home/marco/svn/klear/src/GUI/Controller/</path>
    <filename>CKlearControllerScanView_8h</filename>
    <includes id="CKlearAppException_8h" name="CKlearAppException.h" local="yes" imported="no">../../App/Exceptions/CKlearAppException.h</includes>
    <includes id="CKlearAppErrorException_8h" name="CKlearAppErrorException.h" local="yes" imported="no">../../App/Exceptions/CKlearAppErrorException.h</includes>
    <includes id="CKlearAppFatalException_8h" name="CKlearAppFatalException.h" local="yes" imported="no">../../App/Exceptions/CKlearAppFatalException.h</includes>
  </compound>
  <compound kind="file">
    <name>CKlearControllerScheduler.h</name>
    <path>/home/marco/svn/klear/src/GUI/Controller/</path>
    <filename>CKlearControllerScheduler_8h</filename>
    <includes id="CKlearAppConfig_8h" name="CKlearAppConfig.h" local="yes" imported="no">../../App/CKlearAppConfig.h</includes>
    <includes id="CKlearAppScheduler_8h" name="CKlearAppScheduler.h" local="yes" imported="no">../../App/CKlearAppScheduler.h</includes>
    <includes id="CKlearAppRecordSet_8h" name="CKlearAppRecordSet.h" local="yes" imported="no">../../App/CKlearAppRecordSet.h</includes>
    <includes id="CKlearAppException_8h" name="CKlearAppException.h" local="yes" imported="no">../../App/Exceptions/CKlearAppException.h</includes>
    <class kind="class">CKlearControllerScheduler</class>
  </compound>
  <compound kind="file">
    <name>CKlearControllerSchedulerOverview.h</name>
    <path>/home/marco/svn/klear/src/GUI/Controller/</path>
    <filename>CKlearControllerSchedulerOverview_8h</filename>
    <includes id="CKlearAppScheduler_8h" name="CKlearAppScheduler.h" local="yes" imported="no">../../App/CKlearAppScheduler.h</includes>
    <includes id="CKlearAppRecordSet_8h" name="CKlearAppRecordSet.h" local="yes" imported="no">../../App/CKlearAppRecordSet.h</includes>
    <includes id="CKlearControllerScheduler_8h" name="CKlearControllerScheduler.h" local="yes" imported="no">./CKlearControllerScheduler.h</includes>
    <class kind="class">CKlearControllerSchedulerOverview</class>
  </compound>
  <compound kind="file">
    <name>CKlearControllerTXTMain.h</name>
    <path>/home/marco/svn/klear/src/GUI/Controller/</path>
    <filename>CKlearControllerTXTMain_8h</filename>
    <includes id="CKlearAppConfig_8h" name="CKlearAppConfig.h" local="yes" imported="no">../../App/CKlearAppConfig.h</includes>
    <class kind="class">CKlearControllerTXTMain</class>
  </compound>
  <compound kind="class">
    <name>CKlearAppConfig</name>
    <filename>classCKlearAppConfig.html</filename>
    <member kind="function">
      <type></type>
      <name>CKlearAppConfig</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>ae8204d1db0096ce46154ebc2a19933f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CKlearAppConfig</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>b6c2cd8374431a913d5d8316cf84c0a4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>ReadConfig</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>5dc027c6086a37ca780a0e740805263a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>WriteConfig</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>f1a859b477f9685e59e41bed66adf546</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>StoreServiceID</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>5c683a803a733dea2d659b5125f76f44</anchor>
      <arglist>(QString ChannelName)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>PrintSettings</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>ba6580df5668c33d3e882326f50ea1a5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>CheckConfigfileRevision</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>83c432a809a5e49ad2f2ec73ddec7399</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>CreateNewConfigfile</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>60a4f469e2fd059a399a86fc851991a2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>QString</type>
      <name>getKlearConfigPath</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>0c41dfed81cf6bdb4008c8c9a7eb700b</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>QString</type>
      <name>getKlearSchedulerDatafile</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>37e4825bd4041bbdb6cefcd74e370109</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>QString</type>
      <name>getKlearChannelsConf</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>8d59d538a88af072eb78ab0c1bffc97a</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>QString</type>
      <name>getDVBAdapter</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>f45136bc398177befebda12ebd060917</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>QString</type>
      <name>getDVBDvr</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>347b9b818c81e16cc032e8a3215eef43</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>QString</type>
      <name>getDVBDemux</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>ded7244c8afc7cc79544fe94dd2fcc9c</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>QString</type>
      <name>getDVBFrontend</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>cabe248aae2caabb40188509af3c6010</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getDVBMode</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>5b03c3c0199bb9dd8f1a93c3c3f0afb1</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>QString</type>
      <name>getPlaybackEngine</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>4e6ea001f2835f685dad892201441a1e</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setDVBAdapter</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>23e965b0a520e11d64a853230d8ed561</anchor>
      <arglist>(QString setDVBAdapter)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setDVBDvr</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>5c545900ceb56dc3975e91c17c9cc1b3</anchor>
      <arglist>(QString setDVBDvr)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setDVBDemux</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>c55ca241eb67ad2d3db7c3f3180d3c4d</anchor>
      <arglist>(QString setDVBDemux)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setDVBFrontend</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>4525a90cbfbf38f797fe35c4f8f4157c</anchor>
      <arglist>(QString setDVBFrontend)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPlaybackEngine</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>6c18862d5ad552d21865ecda63c59792</anchor>
      <arglist>(QString setPlaybackEngine)</arglist>
    </member>
    <member kind="function">
      <type>QString</type>
      <name>getScreenshotDir</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>c1192dba0155325172fa2341069c1e24</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>QString</type>
      <name>getScreenshotName</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>673fa67a39f1ae6a664c8cfeeed54ba4</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>QString</type>
      <name>getScreenshotFormat</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>1df097cff04ad2b7071d5ea9812cabaf</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setScreenshotDir</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>9dae590d2a285f658d9fe3e3f5184aae</anchor>
      <arglist>(QString setScreenshotDir)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setScreenshotName</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>b5cf68fa724502098d636821ce3971c1</anchor>
      <arglist>(QString setScreenshotName)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setScreenshotFormat</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>3f307054186250cbf49ac05c20cef9ad</anchor>
      <arglist>(QString setScreenshotFormat)</arglist>
    </member>
    <member kind="function">
      <type>QString</type>
      <name>getRecordingDir</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>14948960b4a1b34c3a7be077da5c867f</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>QString</type>
      <name>getRecordingName</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>5f4a26883fd52bb705047bec6b960421</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>QString</type>
      <name>getRecordingFormat</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>0dae28b710e4c15b9601c418e44e92ed</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRecordingDir</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>3e6110156df9f956ac45ff307984934b</anchor>
      <arglist>(QString setRecordingDir)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRecordingName</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>10ac339de102b8484f4e12df453470ce</anchor>
      <arglist>(QString setRecordingName)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRecordingFormat</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>d84ea326bd959d0c9bd742787a6e9527</anchor>
      <arglist>(QString setRecordingFormat)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getApid</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>86abf89f1b12090b783d34141bd3f6df</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getVpid</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>6cf95c10ea28765dcb16fd798cdb7769</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setApid</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>8721b87687068f9201ce2747d1bec409</anchor>
      <arglist>(int setApid)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setVpid</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>8bafeff2b51d3a468a817f48696419e7</anchor>
      <arglist>(int setVpid)</arglist>
    </member>
    <member kind="function">
      <type>QString</type>
      <name>getCurrentChannel</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>f0ab8a82fe881a103e778ccd74ee1d69</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setCurrentChannel</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>ef64a3739f4dd894b52df4dfa128824e</anchor>
      <arglist>(QString setCurrentChannel)</arglist>
    </member>
    <member kind="function">
      <type>u_int</type>
      <name>getServiceID</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>430621318382865831ce0e2da4be2c45</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getCurrentVolume</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>6552efb61fc77c2fb3bd66caa47b9c36</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setCurrentVolume</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>b4005a0ea18e8893a3e074d1f21f0fbf</anchor>
      <arglist>(int setCurrentVolume)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getMuteSystray</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>2eae887472bf734b5b462c33b99297b7</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setMuteSystray</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>06cad4e8bdbd25e9b30b7099934a7a97</anchor>
      <arglist>(bool isMuteSystray)</arglist>
    </member>
    <member kind="function">
      <type>QString</type>
      <name>getTimeShiftName</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>4440dc7af8d96021a6114421037e4a5b</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setTimeShiftName</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>62dc60ee0e9ba37de4c01bc67802f702</anchor>
      <arglist>(QString TimeShiftName)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setWindowSize</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>832f41851c27ec22c1ea9f3a101eecd1</anchor>
      <arglist>(QSize size)</arglist>
    </member>
    <member kind="function">
      <type>QSize</type>
      <name>getWindowSize</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>fb619443b492756200fe8ed12838687e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getMenuAlignment</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>840bdbbe397602433331eb231c92dc44</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getAutoShowOsd</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>0c2126114e8131d64495dd0f85a48d00</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setAutoShowOsd</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>d116379cc06348132b226c70b6354c77</anchor>
      <arglist>(bool isOsdAutoShown)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getIsDeinterlaced</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>9fa7390b8c580308dc616625d051ad6b</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setIsDeinterlaced</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>e6baaa7a1ebf436b218c1ad64c30aeed</anchor>
      <arglist>(bool isDeint)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getIsMinimized</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>c20dbdea1f9934960e726a47ead1045d</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setIsMinimized</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>31f91f2ebf0a4faafecfd3b90e4708c5</anchor>
      <arglist>(bool isMini)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getDisableScreensaver</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>b846765037cf0e3f2c439851382020ac</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPreEPGmargin</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>9c4d465fe16cc091ece1a0344b77eb6f</anchor>
      <arglist>(int pre)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getPostEPGmargin</name>
      <anchorfile>classCKlearAppConfig.html</anchorfile>
      <anchor>35ffe80e5aba4658321c38f9fe5724fc</anchor>
      <arglist>() const</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CKlearAppDataDecoder</name>
    <filename>classCKlearAppDataDecoder.html</filename>
  </compound>
  <compound kind="class">
    <name>CKlearAppDataLayer</name>
    <filename>classCKlearAppDataLayer.html</filename>
    <member kind="function">
      <type>void</type>
      <name>readInContent</name>
      <anchorfile>classCKlearAppDataLayer.html</anchorfile>
      <anchor>37b553380d341696ab067eee05786d15</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>cleanUpDatabase</name>
      <anchorfile>classCKlearAppDataLayer.html</anchorfile>
      <anchor>dcb7f32d18d102a4fa0d4c7a79ec5390</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>CKlearAppEITData *</type>
      <name>getCurrentRunning</name>
      <anchorfile>classCKlearAppDataLayer.html</anchorfile>
      <anchor>f06c46691eee2674dd62f9d63f0ba7f5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>CKlearAppEITData *</type>
      <name>getNextRunning</name>
      <anchorfile>classCKlearAppDataLayer.html</anchorfile>
      <anchor>9221b353722a144b12a43d398fbb1c4e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addItem</name>
      <anchorfile>classCKlearAppDataLayer.html</anchorfile>
      <anchor>073535f44c7e036630cbca7b3caaf0d7</anchor>
      <arglist>(CKlearAppEITData *item)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>searchItem</name>
      <anchorfile>classCKlearAppDataLayer.html</anchorfile>
      <anchor>a8b8b6b43588847728a67edb4eaed705</anchor>
      <arglist>(long EID)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CKlearAppDescriptorData</name>
    <filename>classCKlearAppDescriptorData.html</filename>
  </compound>
  <compound kind="class">
    <name>CKlearAppDescriptorParser</name>
    <filename>classCKlearAppDescriptorParser.html</filename>
  </compound>
  <compound kind="class">
    <name>CKlearAppEITData</name>
    <filename>classCKlearAppEITData.html</filename>
  </compound>
  <compound kind="class">
    <name>CKlearAppErrorException</name>
    <filename>classCKlearAppErrorException.html</filename>
    <base>CKlearAppException</base>
    <member kind="function">
      <type></type>
      <name>CKlearAppErrorException</name>
      <anchorfile>classCKlearAppErrorException.html</anchorfile>
      <anchor>ffee493298fc05a2349f78014fdb59fd</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CKlearAppErrorException</name>
      <anchorfile>classCKlearAppErrorException.html</anchorfile>
      <anchor>89fea995e726ff470f45595d04707af2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>showExceptions</name>
      <anchorfile>classCKlearAppErrorException.html</anchorfile>
      <anchor>0c3b21b5d54b07090f0a71b6bfc4587c</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CKlearAppException</name>
    <filename>classCKlearAppException.html</filename>
    <member kind="function">
      <type></type>
      <name>CKlearAppException</name>
      <anchorfile>classCKlearAppException.html</anchorfile>
      <anchor>be069725104fcb1eb586b880578908f8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CKlearAppException</name>
      <anchorfile>classCKlearAppException.html</anchorfile>
      <anchor>531edcf54289fdba20c193f7e1afa55d</anchor>
      <arglist>(QString loc, QString msg)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CKlearAppException</name>
      <anchorfile>classCKlearAppException.html</anchorfile>
      <anchor>7fdd1115e5948b63945f946379fcd5a1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addErrorMessage</name>
      <anchorfile>classCKlearAppException.html</anchorfile>
      <anchor>919e1907e359155daa9c7d75d07456e6</anchor>
      <arglist>(QString loc, QString msg)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>showExceptions</name>
      <anchorfile>classCKlearAppException.html</anchorfile>
      <anchor>872c4cd06755b53c724fedfe9e3738b6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>addShowException</name>
      <anchorfile>classCKlearAppException.html</anchorfile>
      <anchor>a838df588a162e9943c9a5f59cc67740</anchor>
      <arglist>(QString loc, QString msg)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::vector&lt; ErrorData * &gt;</type>
      <name>getExceptionStack</name>
      <anchorfile>classCKlearAppException.html</anchorfile>
      <anchor>1aa4119e363190731f164e7b4007b6f1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::vector&lt; ErrorData * &gt;</type>
      <name>ErrorStack</name>
      <anchorfile>classCKlearAppException.html</anchorfile>
      <anchor>95c275ed395c5d3fbe3ef5edc68596da</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CKlearAppFatalException</name>
    <filename>classCKlearAppFatalException.html</filename>
    <base>CKlearAppException</base>
    <member kind="function">
      <type></type>
      <name>CKlearAppFatalException</name>
      <anchorfile>classCKlearAppFatalException.html</anchorfile>
      <anchor>33f8a528b5161290fb99d13b6e3a761a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CKlearAppFatalException</name>
      <anchorfile>classCKlearAppFatalException.html</anchorfile>
      <anchor>2e3abdfedacf3e3910e8b0a53aea221f</anchor>
      <arglist>(QString loc, QString msg)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CKlearAppFatalException</name>
      <anchorfile>classCKlearAppFatalException.html</anchorfile>
      <anchor>679b76384a68f2a5ffe234845a9cc1cb</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>showExceptions</name>
      <anchorfile>classCKlearAppFatalException.html</anchorfile>
      <anchor>b09b79ef7b83f5b30029cfb9f75869c2</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CKlearAppFileException</name>
    <filename>classCKlearAppFileException.html</filename>
    <base>CKlearAppException</base>
    <member kind="function">
      <type></type>
      <name>CKlearAppFileException</name>
      <anchorfile>classCKlearAppFileException.html</anchorfile>
      <anchor>6127a972abc0ef9f82146231f9e9c4f2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CKlearAppFileException</name>
      <anchorfile>classCKlearAppFileException.html</anchorfile>
      <anchor>a0dc4c37d9bdac48655111285d8c829d</anchor>
      <arglist>(QString loc, QString msg)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CKlearAppFileException</name>
      <anchorfile>classCKlearAppFileException.html</anchorfile>
      <anchor>fd8aa397c36bf631fef0fb6d897e0eeb</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>showExceptions</name>
      <anchorfile>classCKlearAppFileException.html</anchorfile>
      <anchor>45a2c3fc1e79c27b7bbc68dab7992ede</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CKlearAppInputException</name>
    <filename>classCKlearAppInputException.html</filename>
    <base>CKlearAppException</base>
    <member kind="function">
      <type></type>
      <name>CKlearAppInputException</name>
      <anchorfile>classCKlearAppInputException.html</anchorfile>
      <anchor>55bc6c0714dd6f6466b0a7d832950003</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CKlearAppInputException</name>
      <anchorfile>classCKlearAppInputException.html</anchorfile>
      <anchor>de43c360661474e0185e1674119f89b0</anchor>
      <arglist>(QString loc, QString msg)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CKlearAppInputException</name>
      <anchorfile>classCKlearAppInputException.html</anchorfile>
      <anchor>36ed114e35b4e9031cfa8d50f00b5d8e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>showExceptions</name>
      <anchorfile>classCKlearAppInputException.html</anchorfile>
      <anchor>adef488714da18178e37ea892f3473ab</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CKlearAppMemoryException</name>
    <filename>classCKlearAppMemoryException.html</filename>
    <base>CKlearAppException</base>
    <member kind="function">
      <type></type>
      <name>CKlearAppMemoryException</name>
      <anchorfile>classCKlearAppMemoryException.html</anchorfile>
      <anchor>4b4fd85729de866cf9fa40274a0423a9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CKlearAppMemoryException</name>
      <anchorfile>classCKlearAppMemoryException.html</anchorfile>
      <anchor>355adf8d3cec47af5123e78aa5d419a3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>showExceptions</name>
      <anchorfile>classCKlearAppMemoryException.html</anchorfile>
      <anchor>6de0c7dd19cf97fc670a96cf9a8c9020</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CKlearAppRecorder</name>
    <filename>classCKlearAppRecorder.html</filename>
    <member kind="function">
      <type></type>
      <name>CKlearAppRecorder</name>
      <anchorfile>classCKlearAppRecorder.html</anchorfile>
      <anchor>4c1fad64323bc169dd6204c0ce0dce20</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CKlearAppRecorder</name>
      <anchorfile>classCKlearAppRecorder.html</anchorfile>
      <anchor>6861fe1d660bc91727b0ed037ed6b76d</anchor>
      <arglist>(const QString RecordFile, const CKlearAppConfig *const KlearConfig, const bool isTimeShifted=false)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CKlearAppRecorder</name>
      <anchorfile>classCKlearAppRecorder.html</anchorfile>
      <anchor>0e0f330862128f4057df41f94bfe8327</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>run</name>
      <anchorfile>classCKlearAppRecorder.html</anchorfile>
      <anchor>a5e48b33409048c3a59e5049d74f9f58</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>StopRecording</name>
      <anchorfile>classCKlearAppRecorder.html</anchorfile>
      <anchor>995597e20483e768b2015765475741ad</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CKlearAppRecordSet</name>
    <filename>classCKlearAppRecordSet.html</filename>
    <member kind="function">
      <type></type>
      <name>CKlearAppRecordSet</name>
      <anchorfile>classCKlearAppRecordSet.html</anchorfile>
      <anchor>fc280d985e0b104e9573e84f9a0b69f9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CKlearAppRecordSet</name>
      <anchorfile>classCKlearAppRecordSet.html</anchorfile>
      <anchor>9424edce90df85bde96ee959be7237a2</anchor>
      <arglist>(const QString RecordFile, const QDateTime startDateTime, const QDateTime endDateTime, const QString channel)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CKlearAppRecordSet</name>
      <anchorfile>classCKlearAppRecordSet.html</anchorfile>
      <anchor>61c9fbb01759b73890675d1f4dcbb7a3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>QString</type>
      <name>getRecordFile</name>
      <anchorfile>classCKlearAppRecordSet.html</anchorfile>
      <anchor>b0ae158c8099cb9311127ec5b6b22902</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>QDateTime</type>
      <name>getStartDateTime</name>
      <anchorfile>classCKlearAppRecordSet.html</anchorfile>
      <anchor>2dc1830a45cb6c71d633bf0c90d5fd8e</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>QDateTime</type>
      <name>getEndDateTime</name>
      <anchorfile>classCKlearAppRecordSet.html</anchorfile>
      <anchor>067840f9ebe06b99b75f1f1e1aafb376</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>QString</type>
      <name>getChannel</name>
      <anchorfile>classCKlearAppRecordSet.html</anchorfile>
      <anchor>a1467584d975c91ae96a8cc31666f35f</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRecordFile</name>
      <anchorfile>classCKlearAppRecordSet.html</anchorfile>
      <anchor>0df4a980e133f3d3e145feefc441eee4</anchor>
      <arglist>(const QString RecordFile)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setStartDateTime</name>
      <anchorfile>classCKlearAppRecordSet.html</anchorfile>
      <anchor>1889bf32ef0f6fe00ce3ddd958b9b014</anchor>
      <arglist>(const QDateTime startDateTime)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setEndDateTime</name>
      <anchorfile>classCKlearAppRecordSet.html</anchorfile>
      <anchor>4d184037f79ee7b67762e6299ddd54fe</anchor>
      <arglist>(const QDateTime endDateTime)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setChannel</name>
      <anchorfile>classCKlearAppRecordSet.html</anchorfile>
      <anchor>bf5d41db454c4c6c648b8b429a383deb</anchor>
      <arglist>(const QString channel)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CKlearAppScheduler</name>
    <filename>classCKlearAppScheduler.html</filename>
    <member kind="signal">
      <type>void</type>
      <name>signalStoppingScheduled</name>
      <anchorfile>classCKlearAppScheduler.html</anchorfile>
      <anchor>fc51a2a0641f7dac03025b22094701fb</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="signal">
      <type>void</type>
      <name>signalStartingScheduled</name>
      <anchorfile>classCKlearAppScheduler.html</anchorfile>
      <anchor>f9067802bf8db8f0e5620cf3290939f8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="signal">
      <type>void</type>
      <name>signalPushQueue</name>
      <anchorfile>classCKlearAppScheduler.html</anchorfile>
      <anchor>1b92236256f85ab6d0796cc7c1bcf4a1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="signal">
      <type>void</type>
      <name>signalCloseMain</name>
      <anchorfile>classCKlearAppScheduler.html</anchorfile>
      <anchor>5c4d5db2ad943dbccffd9e1331cb1fc2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CKlearAppScheduler</name>
      <anchorfile>classCKlearAppScheduler.html</anchorfile>
      <anchor>d71cb0ffd7fb9592b82db4a0a719df04</anchor>
      <arglist>(CKlearAppConfig *const KlearConfig, CKlearAppTuner *const KlearTuner, QObject *const parent)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CKlearAppScheduler</name>
      <anchorfile>classCKlearAppScheduler.html</anchorfile>
      <anchor>ee7cb80b57c6972f5d7648dedeeb62e1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>run</name>
      <anchorfile>classCKlearAppScheduler.html</anchorfile>
      <anchor>a545f1ae7fb607a6587e845c52dee305</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>stop</name>
      <anchorfile>classCKlearAppScheduler.html</anchorfile>
      <anchor>9767f842789519c20a9f7de76d1ca2fd</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>startQuickRecording</name>
      <anchorfile>classCKlearAppScheduler.html</anchorfile>
      <anchor>57066b238069ec0b68200aa0f94311e2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addRecordSet</name>
      <anchorfile>classCKlearAppScheduler.html</anchorfile>
      <anchor>469a043f79db9aadc95f9d0b961e0e02</anchor>
      <arglist>(const QString RecordFile, const QDateTime startDateTime, const QDateTime endDateTime, const QString channel)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>abortRecording</name>
      <anchorfile>classCKlearAppScheduler.html</anchorfile>
      <anchor>7a0dc1db2370f1d90bf91e932a69d611</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>removeRecordSet</name>
      <anchorfile>classCKlearAppScheduler.html</anchorfile>
      <anchor>c3ca4e63404e474649fc4d8381d9e782</anchor>
      <arglist>(const int i)</arglist>
    </member>
    <member kind="function">
      <type>QString</type>
      <name>getRecordFile</name>
      <anchorfile>classCKlearAppScheduler.html</anchorfile>
      <anchor>61e2e5df4a1f122606bda3ca71a79fea</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>WriteScheduledRecords</name>
      <anchorfile>classCKlearAppScheduler.html</anchorfile>
      <anchor>9e2c1e3bde1e2b399b5bd7419e3567d7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isRecordInProgress</name>
      <anchorfile>classCKlearAppScheduler.html</anchorfile>
      <anchor>6ebf0a71fafab10b8be2740336bb4104</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getRecordStorageSize</name>
      <anchorfile>classCKlearAppScheduler.html</anchorfile>
      <anchor>ba75f3d2a5ef5f242c0321ae76a25590</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>CKlearAppRecordSet *</type>
      <name>getRecordSet</name>
      <anchorfile>classCKlearAppScheduler.html</anchorfile>
      <anchor>6e638d11f3e8bbd9c46ee42069c0cd01</anchor>
      <arglist>(const int i)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>isValidRecordSet</name>
      <anchorfile>classCKlearAppScheduler.html</anchorfile>
      <anchor>172e68c97474a2bcdc62702aa6cbd256</anchor>
      <arglist>(const CKlearAppRecordSet *const rs) const</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isTimeShifted</name>
      <anchorfile>classCKlearAppScheduler.html</anchorfile>
      <anchor>fbd7b4db1e7f9607383b38bf4f7ace16</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setTimeShifted</name>
      <anchorfile>classCKlearAppScheduler.html</anchorfile>
      <anchor>f397eb3ca63bfa8366e242bb754330b0</anchor>
      <arglist>(bool timeShifting)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CKlearAppSectionData</name>
    <filename>classCKlearAppSectionData.html</filename>
  </compound>
  <compound kind="class">
    <name>CKlearAppStreamReader</name>
    <filename>classCKlearAppStreamReader.html</filename>
  </compound>
  <compound kind="class">
    <name>CKlearAppTray</name>
    <filename>classCKlearAppTray.html</filename>
    <member kind="slot">
      <type>void</type>
      <name>slotQuitTray</name>
      <anchorfile>classCKlearAppTray.html</anchorfile>
      <anchor>3e34b5f10beede14ecaa3a9f477ae1b1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CKlearAppTray</name>
      <anchorfile>classCKlearAppTray.html</anchorfile>
      <anchor>5a5c99c296e1b1f310d71b57e0004c36</anchor>
      <arglist>(QWidget *parent=0, const char *name=0)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CKlearAppTray</name>
      <anchorfile>classCKlearAppTray.html</anchorfile>
      <anchor>c3e93686692f1f2bda0c01c32392f72e</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CKlearAppTuner</name>
    <filename>classCKlearAppTuner.html</filename>
    <member kind="function">
      <type></type>
      <name>CKlearAppTuner</name>
      <anchorfile>classCKlearAppTuner.html</anchorfile>
      <anchor>53ee592fe1b8044600773d3421dbfe33</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CKlearAppTuner</name>
      <anchorfile>classCKlearAppTuner.html</anchorfile>
      <anchor>1cfd97fe71991bd3e4a5f04f9dc09094</anchor>
      <arglist>(CKlearAppConfig *KlearConfig)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CKlearAppTuner</name>
      <anchorfile>classCKlearAppTuner.html</anchorfile>
      <anchor>6f771fd6b9022c0ee779aa39bffcdad7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>run</name>
      <anchorfile>classCKlearAppTuner.html</anchorfile>
      <anchor>8943e2d3d053b509e2c63bf7c243252c</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setChannel</name>
      <anchorfile>classCKlearAppTuner.html</anchorfile>
      <anchor>2f6a1e9e201b159c5542ef6394641b6e</anchor>
      <arglist>(const QString &amp;ChannelName)</arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>isFinished</name>
      <anchorfile>classCKlearAppTuner.html</anchorfile>
      <anchor>8a4ab9f58b789caff0d95ee3fa354f52</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>isReady</name>
      <anchorfile>classCKlearAppTuner.html</anchorfile>
      <anchor>d3550e0ef85ece338a83372a63ba6d76</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CKlearAppConfig *</type>
      <name>KlearConfig</name>
      <anchorfile>classCKlearAppTuner.html</anchorfile>
      <anchor>0effb7226c7b7c9d8035c6a3ae0143ae</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>QString</type>
      <name>ConfigFileAbsolute</name>
      <anchorfile>classCKlearAppTuner.html</anchorfile>
      <anchor>d566bc4d115dc42baaefe9dcd6ed5a3d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>QString</type>
      <name>ChannelName</name>
      <anchorfile>classCKlearAppTuner.html</anchorfile>
      <anchor>6d82102e5cc5717d14c3bfa511960b2d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>QString</type>
      <name>FRONTEND_DEV</name>
      <anchorfile>classCKlearAppTuner.html</anchorfile>
      <anchor>be1153fe9ecd7c620c5c9f311d3d9536</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>QString</type>
      <name>DEMUX_DEV</name>
      <anchorfile>classCKlearAppTuner.html</anchorfile>
      <anchor>79128d67d6832b4453f519dc604599cd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>adapter</name>
      <anchorfile>classCKlearAppTuner.html</anchorfile>
      <anchor>a9b28908bf6fe6a9060188e2807868a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>frontend</name>
      <anchorfile>classCKlearAppTuner.html</anchorfile>
      <anchor>01850a8e53f6b0343bfc7eef0dc281de</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>demux</name>
      <anchorfile>classCKlearAppTuner.html</anchorfile>
      <anchor>705d9a198ac61b76e3f5d088bd3ba0d1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>dvr</name>
      <anchorfile>classCKlearAppTuner.html</anchorfile>
      <anchor>c3cf3e49417ec2e475733aa6f984eb80</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CKlearAppTunerC</name>
    <filename>classCKlearAppTunerC.html</filename>
    <base>CKlearAppTuner</base>
    <member kind="function">
      <type></type>
      <name>CKlearAppTunerC</name>
      <anchorfile>classCKlearAppTunerC.html</anchorfile>
      <anchor>5deede256d7f88a1e8eb9ea74e5c75b8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CKlearAppTunerC</name>
      <anchorfile>classCKlearAppTunerC.html</anchorfile>
      <anchor>c4f82452f35e07df06cd27847f078d06</anchor>
      <arglist>(CKlearAppConfig *KlearConfig)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CKlearAppTunerC</name>
      <anchorfile>classCKlearAppTunerC.html</anchorfile>
      <anchor>7afffd97f3ea372ce72030676495e102</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>run</name>
      <anchorfile>classCKlearAppTunerC.html</anchorfile>
      <anchor>77f47d0a056905fa8356b5c1dffc2541</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CKlearAppTunerS</name>
    <filename>classCKlearAppTunerS.html</filename>
    <base>CKlearAppTuner</base>
    <member kind="function">
      <type></type>
      <name>CKlearAppTunerS</name>
      <anchorfile>classCKlearAppTunerS.html</anchorfile>
      <anchor>40d35acfeb864387cb7e95ddc69589f8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CKlearAppTunerS</name>
      <anchorfile>classCKlearAppTunerS.html</anchorfile>
      <anchor>63802081942f5f792f0fd98ee27eccdf</anchor>
      <arglist>(CKlearAppConfig *KlearConfig)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CKlearAppTunerS</name>
      <anchorfile>classCKlearAppTunerS.html</anchorfile>
      <anchor>d52378debdc5b3b195e8afe46566c07f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>classCKlearAppTunerS.html</anchorfile>
      <anchor>aa88db23a6da3b2c4885ef97b805048f</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CKlearAppTunerT</name>
    <filename>classCKlearAppTunerT.html</filename>
    <base>CKlearAppTuner</base>
    <member kind="function">
      <type></type>
      <name>CKlearAppTunerT</name>
      <anchorfile>classCKlearAppTunerT.html</anchorfile>
      <anchor>0a79a0b6a09294efe0e9a403a535828e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CKlearAppTunerT</name>
      <anchorfile>classCKlearAppTunerT.html</anchorfile>
      <anchor>3262faad20134b000a824839ed19a77f</anchor>
      <arglist>(CKlearAppConfig *KlearConfig)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CKlearAppTunerT</name>
      <anchorfile>classCKlearAppTunerT.html</anchorfile>
      <anchor>dd6f63d944ea70cca2a064595db26245</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>run</name>
      <anchorfile>classCKlearAppTunerT.html</anchorfile>
      <anchor>495495febf4d7317aad38933b8ac77c2</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CKlearAppTXTDecoder</name>
    <filename>classCKlearAppTXTDecoder.html</filename>
    <member kind="signal">
      <type>void</type>
      <name>changedHeaderInfo</name>
      <anchorfile>classCKlearAppTXTDecoder.html</anchorfile>
      <anchor>e7f3e6859330c9ba9a255d22eaa5a945</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="signal">
      <type>void</type>
      <name>changedBodyInfo</name>
      <anchorfile>classCKlearAppTXTDecoder.html</anchorfile>
      <anchor>4ccedcd041e7162544a66dbe89e6913d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CKlearAppTXTDecoder</name>
      <anchorfile>classCKlearAppTXTDecoder.html</anchorfile>
      <anchor>0767d090cbac9239364edf0810153c74</anchor>
      <arglist>(CKlearAppConfig *config, QObject *const parent=0)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CKlearAppTXTDecoder</name>
      <anchorfile>classCKlearAppTXTDecoder.html</anchorfile>
      <anchor>2e8d348dcae11984777989445d214eb4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>decodeSectionPAT</name>
      <anchorfile>classCKlearAppTXTDecoder.html</anchorfile>
      <anchor>646c94f0be94a472630df0668afd25eb</anchor>
      <arglist>(u_char *buf)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>decodeSectionPMT</name>
      <anchorfile>classCKlearAppTXTDecoder.html</anchorfile>
      <anchor>fd36afa930b19fc52ad21ac5ec018f0d</anchor>
      <arglist>(u_char *buf)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>decodePESTXT</name>
      <anchorfile>classCKlearAppTXTDecoder.html</anchorfile>
      <anchor>7050e6e845a10c7440fa2cada08589db</anchor>
      <arglist>(u_char *buf, int len)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CKlearAppTXTStreamer</name>
    <filename>classCKlearAppTXTStreamer.html</filename>
    <member kind="function">
      <type></type>
      <name>CKlearAppTXTStreamer</name>
      <anchorfile>classCKlearAppTXTStreamer.html</anchorfile>
      <anchor>bff4f5b2063ff118aee462399febc5a5</anchor>
      <arglist>(CKlearAppConfig *config, CKlearAppTXTDecoder *decoder)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CKlearAppTXTStreamer</name>
      <anchorfile>classCKlearAppTXTStreamer.html</anchorfile>
      <anchor>aadd8afa73055917e808022a54b67a26</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRunning</name>
      <anchorfile>classCKlearAppTXTStreamer.html</anchorfile>
      <anchor>8df2598db96df4497d5fb6bf8c04029a</anchor>
      <arglist>(bool status)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CKlearControllerConfig</name>
    <filename>classCKlearControllerConfig.html</filename>
    <member kind="slot">
      <type>void</type>
      <name>slotCancelSettings</name>
      <anchorfile>classCKlearControllerConfig.html</anchorfile>
      <anchor>ef5062fcdeafc20b1f47a6db49967fcf</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotSaveSettings</name>
      <anchorfile>classCKlearControllerConfig.html</anchorfile>
      <anchor>a110f973880273954281ecf479478c03</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotPushToTop</name>
      <anchorfile>classCKlearControllerConfig.html</anchorfile>
      <anchor>d40db73192fabdaed47343db1880dd09</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotPushOneUp</name>
      <anchorfile>classCKlearControllerConfig.html</anchorfile>
      <anchor>ae2eead201d9a18f1d7f38435f157bc1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotPushToBottom</name>
      <anchorfile>classCKlearControllerConfig.html</anchorfile>
      <anchor>46175b749a13a0c0bc466bccf3460265</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotPushOneDown</name>
      <anchorfile>classCKlearControllerConfig.html</anchorfile>
      <anchor>1eb55618d3e419e530555d41ec60a200</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotDeleteListEntry</name>
      <anchorfile>classCKlearControllerConfig.html</anchorfile>
      <anchor>c2aecf8f157a7b757e84d0cbb0c3cb2d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotScan</name>
      <anchorfile>classCKlearControllerConfig.html</anchorfile>
      <anchor>580fd1f0e639505540f1ccf33caeb767</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotClear</name>
      <anchorfile>classCKlearControllerConfig.html</anchorfile>
      <anchor>af2b452b3c5d9eaa6863319815b36892</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotRenameItem</name>
      <anchorfile>classCKlearControllerConfig.html</anchorfile>
      <anchor>23e3720781c9bdea36e71542357a2717</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>keyPressEvent</name>
      <anchorfile>classCKlearControllerConfig.html</anchorfile>
      <anchor>0374949b6ecfe6cec6c303afc7ebc013</anchor>
      <arglist>(QKeyEvent *e)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CKlearControllerConfig</name>
      <anchorfile>classCKlearControllerConfig.html</anchorfile>
      <anchor>5b72245007eafbc37ef2b273b7ade372</anchor>
      <arglist>(CKlearAppConfig *KlearConfig, QWidget *parent=0, const char *name=0, bool modal=FALSE, WFlags fl=0)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CKlearControllerConfig</name>
      <anchorfile>classCKlearControllerConfig.html</anchorfile>
      <anchor>9f7e7f2acbaaa7eb8a2b35f8c87f8a86</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CKlearControllerEPGMain</name>
    <filename>classCKlearControllerEPGMain.html</filename>
    <member kind="slot">
      <type>void</type>
      <name>slotRebuildDatabase</name>
      <anchorfile>classCKlearControllerEPGMain.html</anchorfile>
      <anchor>0ddcdb392f2eba552ad0ee65f4f39713</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotRefreshView</name>
      <anchorfile>classCKlearControllerEPGMain.html</anchorfile>
      <anchor>253ae8e1bc92f0fb98b3b5543af8d8d6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotCloseEPGwindow</name>
      <anchorfile>classCKlearControllerEPGMain.html</anchorfile>
      <anchor>6b693bfe0fbfacf18b7d7fb8fa55557e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotShowItemDetails</name>
      <anchorfile>classCKlearControllerEPGMain.html</anchorfile>
      <anchor>c64f5ef734c7b0fe24995787367a7c4e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotActivateScheduledRecording</name>
      <anchorfile>classCKlearControllerEPGMain.html</anchorfile>
      <anchor>3e61f3f83fde160d7058530d5fc2be97</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CKlearControllerMain</name>
    <filename>classCKlearControllerMain.html</filename>
    <member kind="slot">
      <type>void</type>
      <name>slotMinimizeWindow</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>cb34b930e1f8b3d4dc8903765222eb4d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotFullscreenWindow</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>082f9503d1a16233620c6ec2fc782347</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotCloseWindow</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>d6426de09f56f4f09821edd4e797c520</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotTakeScreenshot</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>4c71bcee41d62bd10a0cace993fbb25c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotXineFatal</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>b25905eefbf16c82e83a561ffa58abd6</anchor>
      <arglist>(const QString &amp;ferror)</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotShowXineStatus</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>5666e1aaf223198f83338c546cdcad94</anchor>
      <arglist>(const QString &amp;status)</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>closeEvent</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>5e1f1a9b1e74073f03af42790e6d9783</anchor>
      <arglist>(QCloseEvent *ce)</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotAboutKlear</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>f990e516501e1aac8c3696aa31982159</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotConfigDialog</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>26a40ebb6215103683ff48ac0d357789</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotShowInfoOSD</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>9e176e17d230786ad0429bd8e9c819d4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotSetVolume</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>77af41914d11c0699a3003fe568c0e5a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotLaunchEPG</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>b97ab3d9786ad92caab88a71f5a9bd1a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotLaunchTXT</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>1887e5663533d551904b83e3a455caba</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotChangeChannel</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>48488eef7cd136ca8f335e9baf51a41e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotPushRecording</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>ae96c864b5535fe3fdf11a89eacc9298</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotPushTimeshifting</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>801fd22759173a3067abce6467872e1c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotResumeTimeshifting</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>7d8175995b71ee4a97165af394b39ce8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotStartingScheduled</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>6cdd9e995c4ad762fe245e828c2586e8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotStoppingScheduled</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>cf8e41c34c2a4a158ab806ddd2292c16</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotPushQueue</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>29386040bab47e515813cdbd0c7c8cef</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotSchedulerOverviewDialog</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>8266769777314451d005ec42f488710a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotToggleDeinterlaceButton</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>a8a43ab6f7446fadb8b2ed2d886dfab1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotToggleDeinterlace</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>77032e4f3f871917ae49c66d22489d2b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotToggleMute</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>e0744dc4e36daf0a3bdeb82a8d07d8da</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotIncChannel</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>446a19f2d3f89b96c290c8498a88865b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotDecChannel</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>3436e334e8608ba3b68ce7fb8df170b1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>hideEvent</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>2e43fdeabd1413d7581659664b791860</anchor>
      <arglist>(QHideEvent *ce)</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>resizeEvent</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>97e1da950c9b7f578d5c34939a6e902a</anchor>
      <arglist>(QResizeEvent *re)</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>showEvent</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>6c4aec819ac7d78181f4e64716d0790d</anchor>
      <arglist>(QShowEvent *ce)</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotToggleMuteButton</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>5dcd53e3b754e8f29eb5babd48bc5b94</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotFullscreenWindowButton</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>d0f3f3490fa13df36ed281cbe19f4ab7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>wheelEvent</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>693b4456e40aea559254f75237cbf3e2</anchor>
      <arglist>(QWheelEvent *e)</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotFakeKeyEvent</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>244eda64bf3e1653e6a588ddd4e2d783</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotSetScreensaverTimeout</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>4c0d8cf447ad2c5a35a811386d68e456</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotSetDeinterlaceFromConfig</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>238aa26448294a1cd4fb686c6256cdae</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="signal">
      <type>void</type>
      <name>signalCloseTray</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>46e2897a5d71ff1c02722365bf0f9ac7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CKlearControllerMain</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>ed53d7e23e22db13803e1ac9814dd1b1</anchor>
      <arglist>(QWidget *parent=0, const char *name=0, bool modal=FALSE, WFlags fl=0)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CKlearControllerMain</name>
      <anchorfile>classCKlearControllerMain.html</anchorfile>
      <anchor>c5a12af53de25f21d5b5ee4b08fa3ddf</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CKlearControllerScheduler</name>
    <filename>classCKlearControllerScheduler.html</filename>
    <member kind="slot">
      <type>void</type>
      <name>slotGetChannel</name>
      <anchorfile>classCKlearControllerScheduler.html</anchorfile>
      <anchor>5f2d2b2606ea1c6e416f9938e80baefe</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotBrowse</name>
      <anchorfile>classCKlearControllerScheduler.html</anchorfile>
      <anchor>7de2c694ce113094392e09994266977f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotAddRecordSet</name>
      <anchorfile>classCKlearControllerScheduler.html</anchorfile>
      <anchor>276f887b986db6e67d291be4d176e04b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotCancelScheduler</name>
      <anchorfile>classCKlearControllerScheduler.html</anchorfile>
      <anchor>26ad1a2cf029bbf758fe33c3db8d9b95</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CKlearControllerScheduler</name>
      <anchorfile>classCKlearControllerScheduler.html</anchorfile>
      <anchor>0752fe1fa62671c17b29c2b2cad07a04</anchor>
      <arglist>(CKlearAppScheduler *KlearScheduler, QWidget *parent=0, const char *name=0, WFlags fl=0)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CKlearControllerScheduler</name>
      <anchorfile>classCKlearControllerScheduler.html</anchorfile>
      <anchor>974c16ae8286c778aa92ee8777e04fd6</anchor>
      <arglist>(CKlearAppScheduler *KlearScheduler, QString chan, QDateTime start, QDateTime end, QString recname, QWidget *parent=0, const char *name=0, WFlags fl=0)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CKlearControllerScheduler</name>
      <anchorfile>classCKlearControllerScheduler.html</anchorfile>
      <anchor>44c4f6c2eade5608dde99061d6a2e0bc</anchor>
      <arglist>(CKlearAppScheduler *KlearScheduler, CKlearAppRecordSet KlearRecordSet, QWidget *parent=0, const char *name=0, WFlags fl=0)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CKlearControllerScheduler</name>
      <anchorfile>classCKlearControllerScheduler.html</anchorfile>
      <anchor>c29d19752d76df1cba0a40f53b3ab74e</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CKlearControllerSchedulerOverview</name>
    <filename>classCKlearControllerSchedulerOverview.html</filename>
    <member kind="slot">
      <type>void</type>
      <name>slotSaveRecordSet</name>
      <anchorfile>classCKlearControllerSchedulerOverview.html</anchorfile>
      <anchor>0294bced7b5ecff348a99bcf160cdc6d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotCancelRecordSet</name>
      <anchorfile>classCKlearControllerSchedulerOverview.html</anchorfile>
      <anchor>ea99d6f767a90aeab0590082a363ee9d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotAddRecordSet</name>
      <anchorfile>classCKlearControllerSchedulerOverview.html</anchorfile>
      <anchor>6efacece2ec2225b7d6c3b506a0163c0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotAddRecordSet</name>
      <anchorfile>classCKlearControllerSchedulerOverview.html</anchorfile>
      <anchor>cebfb4360928a34e5ba1757b93717681</anchor>
      <arglist>(QString chan, QDateTime start, QDateTime end, QString recname)</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotRemoveRecordSet</name>
      <anchorfile>classCKlearControllerSchedulerOverview.html</anchorfile>
      <anchor>40e57db6557fc7c041851fefa96673d1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotEditRecordSet</name>
      <anchorfile>classCKlearControllerSchedulerOverview.html</anchorfile>
      <anchor>3480c44472d051134ebf975e320589fc</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CKlearControllerSchedulerOverview</name>
      <anchorfile>classCKlearControllerSchedulerOverview.html</anchorfile>
      <anchor>eeb0c85dec128bc4c5d0b6075702941d</anchor>
      <arglist>(CKlearAppScheduler *KlearScheduler, QWidget *parent=0, const char *name=0, WFlags fl=0)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CKlearControllerSchedulerOverview</name>
      <anchorfile>classCKlearControllerSchedulerOverview.html</anchorfile>
      <anchor>53877f7f857ac47d523aec2ceccdda9c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>loadListView</name>
      <anchorfile>classCKlearControllerSchedulerOverview.html</anchorfile>
      <anchor>10a219271ef39c334b40aa5ce3dd168e</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CKlearControllerTXTMain</name>
    <filename>classCKlearControllerTXTMain.html</filename>
    <member kind="slot">
      <type>void</type>
      <name>slotPaintHeader</name>
      <anchorfile>classCKlearControllerTXTMain.html</anchorfile>
      <anchor>a2dee62300516ec00a339e9d258ac5f2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot">
      <type>void</type>
      <name>slotPaintBody</name>
      <anchorfile>classCKlearControllerTXTMain.html</anchorfile>
      <anchor>e9028c9416f5856fc99b2341c7515a97</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="signal">
      <type>void</type>
      <name>changedHeaderInfo</name>
      <anchorfile>classCKlearControllerTXTMain.html</anchorfile>
      <anchor>201f71f8adb0b23cdc8914a50186353d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="signal">
      <type>void</type>
      <name>changedBodyInfo</name>
      <anchorfile>classCKlearControllerTXTMain.html</anchorfile>
      <anchor>334ecd987be6e6502ba4e88008e93ead</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CKlearControllerTXTMain</name>
      <anchorfile>classCKlearControllerTXTMain.html</anchorfile>
      <anchor>9824ce03331a7910f163eb8bae4a1b6b</anchor>
      <arglist>(CKlearAppConfig *config, QWidget *parent=0, const char *name=0, WFlags fl=0)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CKlearControllerTXTMain</name>
      <anchorfile>classCKlearControllerTXTMain.html</anchorfile>
      <anchor>ed642348f8e64cd46c8d6bf5999932f4</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CKlearEngineAdapter</name>
    <filename>classCKlearEngineAdapter.html</filename>
    <member kind="function">
      <type></type>
      <name>CKlearEngineAdapter</name>
      <anchorfile>classCKlearEngineAdapter.html</anchorfile>
      <anchor>3fe2c51e8f8d00fcf186eac8fda8488e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CKlearEngineAdapter</name>
      <anchorfile>classCKlearEngineAdapter.html</anchorfile>
      <anchor>52fe4bc2af7264b01bc6cfc153c002d9</anchor>
      <arglist>(QWidget *parent, const char *name)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CKlearEngineAdapter</name>
      <anchorfile>classCKlearEngineAdapter.html</anchorfile>
      <anchor>f91690c3423841f4cb6bc6f450da72f5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>EnginePlayStream</name>
      <anchorfile>classCKlearEngineAdapter.html</anchorfile>
      <anchor>7a86d538d664c1c524ab269d37057517</anchor>
      <arglist>(const QString &amp;PlayFile)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>EngineStopStream</name>
      <anchorfile>classCKlearEngineAdapter.html</anchorfile>
      <anchor>a12449040d3dbd7fc5797322d463a984</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual bool</type>
      <name>EngineIsPlaying</name>
      <anchorfile>classCKlearEngineAdapter.html</anchorfile>
      <anchor>c5f964f8b39c7b25a2ff45601434fd56</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>hideMouse</name>
      <anchorfile>classCKlearEngineAdapter.html</anchorfile>
      <anchor>17b07f5e07e3cb4ee801d29af0357073</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual QImage</type>
      <name>getStreamSnapshot</name>
      <anchorfile>classCKlearEngineAdapter.html</anchorfile>
      <anchor>73a1934c634ee5e96f606542da51ee4c</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>showOSDMessage</name>
      <anchorfile>classCKlearEngineAdapter.html</anchorfile>
      <anchor>c364557f9fa99dd70324885801347af1</anchor>
      <arglist>(const QString &amp;message, uint duration)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>showInfoOSD</name>
      <anchorfile>classCKlearEngineAdapter.html</anchorfile>
      <anchor>0bd1ce8136ea101c7776cd12d1b6eceb</anchor>
      <arglist>(const QString CurrChannel)=0</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setVolume</name>
      <anchorfile>classCKlearEngineAdapter.html</anchorfile>
      <anchor>2812eb0f743e88276a1ff953fbac3498</anchor>
      <arglist>(int vol)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getVolume</name>
      <anchorfile>classCKlearEngineAdapter.html</anchorfile>
      <anchor>13d84dd062d0009d45f5881d1485c17e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>toggleMute</name>
      <anchorfile>classCKlearEngineAdapter.html</anchorfile>
      <anchor>e1a8fa8743ed74c62fa625465b092298</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>toggleDeinterlacing</name>
      <anchorfile>classCKlearEngineAdapter.html</anchorfile>
      <anchor>f332ff7b49a9224b48aaee8b3a4ee46c</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual bool</type>
      <name>isMute</name>
      <anchorfile>classCKlearEngineAdapter.html</anchorfile>
      <anchor>8e94e1390c9603546732d54d5db0957a</anchor>
      <arglist>()=0</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CKlearEngineXine</name>
    <filename>classCKlearEngineXine.html</filename>
    <base>CKlearEngineAdapter</base>
    <member kind="function">
      <type></type>
      <name>CKlearEngineXine</name>
      <anchorfile>classCKlearEngineXine.html</anchorfile>
      <anchor>05ac5c499d51217ae0f771662697b4c3</anchor>
      <arglist>(QWidget *parent=0, const char *name=0, std::vector&lt; CKlearAppEITData * &gt; *storage=NULL, const QString &amp;audioDriver=QString::null, const QString &amp;videoDriver=QString::null, bool verbose=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~CKlearEngineXine</name>
      <anchorfile>classCKlearEngineXine.html</anchorfile>
      <anchor>4f50d7a55f45bb5f1bc2b50ebbc92786</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>EnginePlayStream</name>
      <anchorfile>classCKlearEngineXine.html</anchorfile>
      <anchor>a784993c2fb7fdcae8d593978e4ec445</anchor>
      <arglist>(const QString &amp;PlayFile)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>EngineStopStream</name>
      <anchorfile>classCKlearEngineXine.html</anchorfile>
      <anchor>2d6df38452cbaa8da84bf7e4fde0ea4a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>EngineIsPlaying</name>
      <anchorfile>classCKlearEngineXine.html</anchorfile>
      <anchor>7d66962d2e019415eac39a63242b4780</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>uchar *</type>
      <name>yv12ToRgb</name>
      <anchorfile>classCKlearEngineXine.html</anchorfile>
      <anchor>69ba12d9c404e0013dc83de051b05e8f</anchor>
      <arglist>(uint8_t *src_y, uint8_t *src_u, uint8_t *src_v, int width, int height)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>yuy2Toyv12</name>
      <anchorfile>classCKlearEngineXine.html</anchorfile>
      <anchor>05741abc6b6a65543f211a2c2ad7c1ad</anchor>
      <arglist>(uint8_t *y, uint8_t *u, uint8_t *v, uint8_t *input, int width, int height)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>hideMouse</name>
      <anchorfile>classCKlearEngineXine.html</anchorfile>
      <anchor>4a202f6419e6ee3d38a363fb169aff68</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setVolume</name>
      <anchorfile>classCKlearEngineXine.html</anchorfile>
      <anchor>f5c19dcfdc9ddf948410322ab2ca7181</anchor>
      <arglist>(const int vol)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getVolume</name>
      <anchorfile>classCKlearEngineXine.html</anchorfile>
      <anchor>1511f421ca309939e785cc29aca2f642</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>toggleMute</name>
      <anchorfile>classCKlearEngineXine.html</anchorfile>
      <anchor>1a9d6eb75f5b5d9df5be8636526907e9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>QImage</type>
      <name>getStreamSnapshot</name>
      <anchorfile>classCKlearEngineXine.html</anchorfile>
      <anchor>d363a7cdee1270545669ccecdc29534f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>showOSDMessage</name>
      <anchorfile>classCKlearEngineXine.html</anchorfile>
      <anchor>47228d647b3e6bc2995bc8ba32191cac</anchor>
      <arglist>(const QString &amp;message, const uint duration)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>showInfoOSD</name>
      <anchorfile>classCKlearEngineXine.html</anchorfile>
      <anchor>be1374e539e2b0f4580e56ca29b0c75b</anchor>
      <arglist>(const QString CurrChannel)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>toggleDeinterlacing</name>
      <anchorfile>classCKlearEngineXine.html</anchorfile>
      <anchor>b5b5dbdb9f3538f240361321eb5a7802</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>play</name>
      <anchorfile>classCKlearEngineXine.html</anchorfile>
      <anchor>44db49346489b3b6bc0e3df513a49799</anchor>
      <arglist>(const char *const iMrl)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>stop</name>
      <anchorfile>classCKlearEngineXine.html</anchorfile>
      <anchor>664d81fab2db5bcd0fcb16ce17b4b4f3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>init</name>
      <anchorfile>classCKlearEngineXine.html</anchorfile>
      <anchor>1c760b3ae1215b276cd013898aea3fb5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>toggleMuting</name>
      <anchorfile>classCKlearEngineXine.html</anchorfile>
      <anchor>bfb391ff6b43563f48f155f05810e15f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isMute</name>
      <anchorfile>classCKlearEngineXine.html</anchorfile>
      <anchor>96fabc924999755657d8e4d7b770dd39</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CKlearEngineXineTNG</name>
    <filename>classCKlearEngineXineTNG.html</filename>
    <base>CKlearEngineAdapter</base>
    <member kind="function">
      <type>void</type>
      <name>EnginePlayStream</name>
      <anchorfile>classCKlearEngineXineTNG.html</anchorfile>
      <anchor>f876b6fc2470fb0883eb0dae29a27c34</anchor>
      <arglist>(const QString &amp;)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>EngineStopStream</name>
      <anchorfile>classCKlearEngineXineTNG.html</anchorfile>
      <anchor>9498b48448032d769be988e88749bfd0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>EngineIsPlaying</name>
      <anchorfile>classCKlearEngineXineTNG.html</anchorfile>
      <anchor>f8e3d453e53d4879dfdd9df3db6108d3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>hideMouse</name>
      <anchorfile>classCKlearEngineXineTNG.html</anchorfile>
      <anchor>eab16ef55eda7ae1b913ecaa6d040b82</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>QImage</type>
      <name>getStreamSnapshot</name>
      <anchorfile>classCKlearEngineXineTNG.html</anchorfile>
      <anchor>d2dcf7228cdd5aa9b3ccac73528f45f6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>showOSDMessage</name>
      <anchorfile>classCKlearEngineXineTNG.html</anchorfile>
      <anchor>c5dc4e3474acbb69f17b9ec709eccb8a</anchor>
      <arglist>(const QString &amp;, uint)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>showInfoOSD</name>
      <anchorfile>classCKlearEngineXineTNG.html</anchorfile>
      <anchor>5fb56fd50195c1688ab29e8b7e27bdba</anchor>
      <arglist>(QString)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>toggleDeinterlacing</name>
      <anchorfile>classCKlearEngineXineTNG.html</anchorfile>
      <anchor>2989620eb9eadf6b32140c159d7d58f7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isMute</name>
      <anchorfile>classCKlearEngineXineTNG.html</anchorfile>
      <anchor>f0d9cc46f8734a49005668979b8a27e3</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CKlearExceptionReporter</name>
    <filename>classCKlearExceptionReporter.html</filename>
    <member kind="function">
      <type></type>
      <name>CKlearExceptionReporter</name>
      <anchorfile>classCKlearExceptionReporter.html</anchorfile>
      <anchor>6d01dab07e067cf5b791b053b0a64b91</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CKlearExceptionReporter</name>
      <anchorfile>classCKlearExceptionReporter.html</anchorfile>
      <anchor>fddb7fa255167bf98590de105965e9ba</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>showExceptions</name>
      <anchorfile>classCKlearExceptionReporter.html</anchorfile>
      <anchor>58bc8f6560d07e50fee859ccc5aae4a3</anchor>
      <arglist>(std::vector&lt; ErrorData * &gt; ErrorStack)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>showExceptions</name>
      <anchorfile>classCKlearExceptionReporter.html</anchorfile>
      <anchor>6144880292ebd607d48c8e3190aca7c7</anchor>
      <arglist>(std::vector&lt; ErrorData * &gt; ErrorStack, QString ExceptionType)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>ErrorData</name>
    <filename>classErrorData.html</filename>
    <member kind="function">
      <type></type>
      <name>ErrorData</name>
      <anchorfile>classErrorData.html</anchorfile>
      <anchor>15f6367ab7b3eebcf13faefd89580c43</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ErrorData</name>
      <anchorfile>classErrorData.html</anchorfile>
      <anchor>97e489c26508c034dc35240087da8c2e</anchor>
      <arglist>(QString loc, QString msg)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~ErrorData</name>
      <anchorfile>classErrorData.html</anchorfile>
      <anchor>5ae3f3cb0ed3bd29dc865770afde2b8c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable">
      <type>QString</type>
      <name>ErrorMessage</name>
      <anchorfile>classErrorData.html</anchorfile>
      <anchor>898d64fb9d18dd1b945e2bd9d6c69127</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>QString</type>
      <name>LocationMessage</name>
      <anchorfile>classErrorData.html</anchorfile>
      <anchor>8949931177e8066941d7e0f87e635a83</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="dir">
    <name>svn/klear/src/App/</name>
    <path>/home/marco/svn/klear/src/App/</path>
    <filename>dir_dd51f9e0fa64d0f5b5e44361a04bbc9b.html</filename>
    <dir>svn/klear/src/App/Engines/</dir>
    <dir>svn/klear/src/App/EPG/</dir>
    <dir>svn/klear/src/App/Exceptions/</dir>
    <dir>svn/klear/src/App/Tuner/</dir>
    <dir>svn/klear/src/App/TXT/</dir>
    <file>CKlearAppConfig.h</file>
    <file>CKlearAppDecodingHelper.h</file>
    <file>CKlearAppRecorder.h</file>
    <file>CKlearAppRecordSet.h</file>
    <file>CKlearAppScheduler.h</file>
    <file>CKlearAppTray.h</file>
  </compound>
  <compound kind="dir">
    <name>svn/klear/src/GUI/Controller/</name>
    <path>/home/marco/svn/klear/src/GUI/Controller/</path>
    <filename>dir_92a6db1b6dbd70b1018950f9acd981c6.html</filename>
    <file>CKlearControllerConfig.h</file>
    <file>CKlearControllerEPGMain.h</file>
    <file>CKlearControllerMain.h</file>
    <file>CKlearControllerScanView.h</file>
    <file>CKlearControllerScheduler.h</file>
    <file>CKlearControllerSchedulerOverview.h</file>
    <file>CKlearControllerTXTMain.h</file>
  </compound>
  <compound kind="dir">
    <name>svn/klear/src/App/Engines/</name>
    <path>/home/marco/svn/klear/src/App/Engines/</path>
    <filename>dir_cf0cee9e8655998cda68b83dfcb6b2a4.html</filename>
    <dir>svn/klear/src/App/Engines/KlearMPlayer/</dir>
    <dir>svn/klear/src/App/Engines/KlearXine/</dir>
    <dir>svn/klear/src/App/Engines/KlearXineTNG/</dir>
    <file>CKlearEngineAdapter.h</file>
  </compound>
  <compound kind="dir">
    <name>svn/klear/src/App/EPG/</name>
    <path>/home/marco/svn/klear/src/App/EPG/</path>
    <filename>dir_a63242d6d71b8420ad35333c4c7634c6.html</filename>
    <file>CKlearAppDataDecoder.h</file>
    <file>CKlearAppDataLayer.h</file>
    <file>CKlearAppDecodingTables.h</file>
    <file>CKlearAppDescriptorData.h</file>
    <file>CKlearAppDescriptorParser.h</file>
    <file>CKlearAppEITData.h</file>
    <file>CKlearAppSectionData.h</file>
    <file>CKlearAppStreamReader.h</file>
  </compound>
  <compound kind="dir">
    <name>svn/klear/src/App/Exceptions/</name>
    <path>/home/marco/svn/klear/src/App/Exceptions/</path>
    <filename>dir_466b390b76c7763c286db4499254593e.html</filename>
    <file>CKlearAppErrorException.h</file>
    <file>CKlearAppException.h</file>
    <file>CKlearAppFatalException.h</file>
    <file>CKlearAppFileException.h</file>
    <file>CKlearAppInputException.h</file>
    <file>CKlearAppMemoryException.h</file>
    <file>CKlearExceptionReporter.h</file>
  </compound>
  <compound kind="dir">
    <name>svn/klear/src/GUI/</name>
    <path>/home/marco/svn/klear/src/GUI/</path>
    <filename>dir_f780de135d00617820c28976d31ea26a.html</filename>
    <dir>svn/klear/src/GUI/Controller/</dir>
    <dir>svn/klear/src/GUI/View/</dir>
  </compound>
  <compound kind="dir">
    <name>svn/klear/</name>
    <path>/home/marco/svn/klear/</path>
    <filename>dir_23ee9238f92eca7b2f731740696fde89.html</filename>
    <dir>svn/klear/src/</dir>
  </compound>
  <compound kind="dir">
    <name>svn/klear/src/App/Engines/KlearMPlayer/</name>
    <path>/home/marco/svn/klear/src/App/Engines/KlearMPlayer/</path>
    <filename>dir_92eb2abf74eb97a64f29dbfcb7dde315.html</filename>
    <file>CKlearEngineMPlayer.h</file>
  </compound>
  <compound kind="dir">
    <name>svn/klear/src/App/Engines/KlearXine/</name>
    <path>/home/marco/svn/klear/src/App/Engines/KlearXine/</path>
    <filename>dir_516ba9f9a2d54f4d71162d0dc895ba83.html</filename>
    <file>CKlearEngineXine.h</file>
  </compound>
  <compound kind="dir">
    <name>svn/klear/src/App/Engines/KlearXineTNG/</name>
    <path>/home/marco/svn/klear/src/App/Engines/KlearXineTNG/</path>
    <filename>dir_13fa9331d0e89becabe84226ba5dad5b.html</filename>
    <file>CKlearEngineXineTNG.h</file>
  </compound>
  <compound kind="dir">
    <name>svn/klear/src/</name>
    <path>/home/marco/svn/klear/src/</path>
    <filename>dir_9be1a85da473cd298cd89465568db424.html</filename>
    <dir>svn/klear/src/App/</dir>
    <dir>svn/klear/src/GUI/</dir>
    <dir>svn/klear/src/Utilities/</dir>
    <file>config.h</file>
  </compound>
  <compound kind="dir">
    <name>svn/</name>
    <path>/home/marco/svn/</path>
    <filename>dir_5b21f21554dafc5285719301124cf49b.html</filename>
    <dir>svn/klear/</dir>
  </compound>
  <compound kind="dir">
    <name>svn/klear/src/App/Tuner/</name>
    <path>/home/marco/svn/klear/src/App/Tuner/</path>
    <filename>dir_24777310d07fbed3d72cf9d2a85ccad7.html</filename>
    <file>CKlearAppTuner.h</file>
    <file>CKlearAppTunerC.h</file>
    <file>CKlearAppTunerS.h</file>
    <file>CKlearAppTunerT.h</file>
  </compound>
  <compound kind="dir">
    <name>svn/klear/src/App/TXT/</name>
    <path>/home/marco/svn/klear/src/App/TXT/</path>
    <filename>dir_ed70217de3fbe29154de81b82e652a33.html</filename>
    <file>CKlearAppTXTDecoder.h</file>
    <file>CKlearAppTXTStreamer.h</file>
  </compound>
  <compound kind="dir">
    <name>svn/klear/src/Utilities/</name>
    <path>/home/marco/svn/klear/src/Utilities/</path>
    <filename>dir_f5d6d6422a8cd90de021df40df19fdd7.html</filename>
    <file>ts2pes.h</file>
  </compound>
  <compound kind="dir">
    <name>svn/klear/src/GUI/View/</name>
    <path>/home/marco/svn/klear/src/GUI/View/</path>
    <filename>dir_0d6008f9560e7c54cf9cae57b04f4880.html</filename>
    <file>CKlearUIAbout.h</file>
    <file>CKlearUIConfig.h</file>
    <file>CKlearUIEPGMain.h</file>
    <file>CKlearUIMain.h</file>
    <file>CKlearUIScanView.h</file>
    <file>CKlearUIScheduler.h</file>
    <file>CKlearUISchedulerOverview.h</file>
    <file>CKlearUITXTMain.h</file>
  </compound>
</tagfile>
